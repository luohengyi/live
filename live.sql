/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50641
 Source Host           : localhost:3306
 Source Schema         : live

 Target Server Type    : MySQL
 Target Server Version : 50641
 File Encoding         : 65001

 Date: 20/03/2019 16:59:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员编号',
  `name` varchar(255) DEFAULT NULL COMMENT '管理员账号',
  `password` varchar(255) DEFAULT NULL COMMENT '管理员登录密码',
  `role_id` int(11) DEFAULT NULL COMMENT '权限ID，与权限表关联',
  `time` datetime DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员';

-- ----------------------------
-- Records of admin
-- ----------------------------
BEGIN;
INSERT INTO `admin` VALUES (1, 'root', 'root', NULL, NULL);
INSERT INTO `admin` VALUES (2, 'admin', 'admin', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for anchor
-- ----------------------------
DROP TABLE IF EXISTS `anchor`;
CREATE TABLE `anchor` (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '主播主键',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户登录账号',
  `idcard` varchar(20) NOT NULL COMMENT '主播身份证号码',
  `kind_id` int(20) NOT NULL COMMENT '主播分类',
  `lasttime` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `cardinformation1` varchar(255) NOT NULL COMMENT '身份证件信息1',
  `cardinformation2` varchar(255) NOT NULL COMMENT '身份证件信息2',
  `flag` int(255) NOT NULL COMMENT '主播状态 1表示可以直播0表示不能直播',
  `headline` varchar(255) DEFAULT NULL COMMENT '直播标题',
  `img` varchar(100) DEFAULT NULL COMMENT '封面',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='主播信息';

-- ----------------------------
-- Records of anchor
-- ----------------------------
BEGIN;
INSERT INTO `anchor` VALUES (1, 123, '1', 1, '2019-03-13 00:00:01', '1', '1', 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for beanchor
-- ----------------------------
DROP TABLE IF EXISTS `beanchor`;
CREATE TABLE `beanchor` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主播申请表主键',
  `user_id` int(20) NOT NULL COMMENT '用户登录账号',
  `idcard` varchar(18) NOT NULL COMMENT '主播身份证号码',
  `kind_id` int(20) NOT NULL COMMENT '主播分类',
  `cardinformation1` varchar(255) NOT NULL COMMENT '身份证信息1',
  `cardinformation2` varchar(255) NOT NULL COMMENT '身份证信息2',
  `time` datetime DEFAULT NULL COMMENT '申请时间',
  `admin_id` varchar(255) DEFAULT NULL COMMENT '审核人',
  `status` int(1) DEFAULT '0' COMMENT '2未审核 1 同意。0拒绝',
  `lasttime` datetime DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='主播申请表';

-- ----------------------------
-- Table structure for live_kind
-- ----------------------------
DROP TABLE IF EXISTS `live_kind`;
CREATE TABLE `live_kind` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '直播类别编号',
  `name` varchar(255) DEFAULT NULL COMMENT '直播类别名称',
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='直播类别';

-- ----------------------------
-- Table structure for navigation
-- ----------------------------
DROP TABLE IF EXISTS `navigation`;
CREATE TABLE `navigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '导航栏编号',
  `name` varchar(255) DEFAULT NULL COMMENT '导航栏名称',
  `url` varchar(255) DEFAULT NULL COMMENT '跳转地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='导航栏';

-- ----------------------------
-- Records of navigation
-- ----------------------------
BEGIN;
INSERT INTO `navigation` VALUES (1, '首页', '123123');
INSERT INTO `navigation` VALUES (4, '123,123', NULL);
INSERT INTO `navigation` VALUES (5, '13,2', NULL);
INSERT INTO `navigation` VALUES (6, 'ddsa,asd', NULL);
INSERT INTO `navigation` VALUES (9, '1231', 'wwwq/');
COMMIT;

-- ----------------------------
-- Table structure for rimitv_user
-- ----------------------------
DROP TABLE IF EXISTS `rimitv_user`;
CREATE TABLE `rimitv_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT 'username   用户昵称',
  `password` varchar(255) DEFAULT NULL COMMENT 'password  用户密码；',
  `account` varchar(255) DEFAULT NULL COMMENT 'account  用户登录账号',
  `is_role` int(11) DEFAULT '0' COMMENT 'role  是否为主播，1为主播，0为普通用户；',
  `attention` int(11) DEFAULT '0' COMMENT 'attention  用户关注；',
  `time` datetime DEFAULT NULL COMMENT '最后一次登陆时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户列表';

-- ----------------------------
-- Records of rimitv_user
-- ----------------------------
BEGIN;
INSERT INTO `rimitv_user` VALUES (1, '张二', '123', '123', 123, 0, '0000-00-00 00:00:00');
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `ruleconten` varchar(255) NOT NULL DEFAULT '[]' COMMENT '功能',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色列表';

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (3, '1231', '');
INSERT INTO `role` VALUES (4, '12311', '');
INSERT INTO `role` VALUES (5, '123111', '');
INSERT INTO `role` VALUES (6, '阿萨德', '');
INSERT INTO `role` VALUES (7, '123阿萨', '');
INSERT INTO `role` VALUES (8, '123阿萨 啊', '');
INSERT INTO `role` VALUES (9, '123阿萨 啊a', '');
INSERT INTO `role` VALUES (10, 'c', '');
INSERT INTO `role` VALUES (11, 'ca', '');
INSERT INTO `role` VALUES (12, '嗷嗷', '');
INSERT INTO `role` VALUES (13, '123ss', '');
INSERT INTO `role` VALUES (14, '12311111', '');
INSERT INTO `role` VALUES (15, '13', '');
INSERT INTO `role` VALUES (16, '123123', '');
INSERT INTO `role` VALUES (17, 'm', '');
INSERT INTO `role` VALUES (18, '123,123', '');
COMMIT;

-- ----------------------------
-- Table structure for rule
-- ----------------------------
DROP TABLE IF EXISTS `rule`;
CREATE TABLE `rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级菜单',
  `rule` varchar(255) DEFAULT NULL COMMENT '访问地址',
  `show` int(1) NOT NULL DEFAULT '1' COMMENT '是否菜单',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='接口列表';

-- ----------------------------
-- Records of rule
-- ----------------------------
BEGIN;
INSERT INTO `rule` VALUES (1, '系统管理', 0, NULL, 1);
INSERT INTO `rule` VALUES (2, '系统用首页', 1, '/admin/index', 1);
INSERT INTO `rule` VALUES (3, '系统用户添加页面', 1, '/admin/add', 0);
INSERT INTO `rule` VALUES (4, '系统用户修改', 1, '/admin/update', 0);
COMMIT;

-- ----------------------------
-- Table structure for slideshow
-- ----------------------------
DROP TABLE IF EXISTS `slideshow`;
CREATE TABLE `slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '轮播图编号',
  `url` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `skip_url` varchar(255) DEFAULT NULL COMMENT '跳转地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='轮播图';

-- ----------------------------
-- Records of slideshow
-- ----------------------------
BEGIN;
INSERT INTO `slideshow` VALUES (2, 'qweqeqe11111', '/upload/b31bea85-1c17-4198-b8b0-2a7a97fd3051.png');
INSERT INTO `slideshow` VALUES (3, '123', '/upload/d3e1d5a2-1c20-46d2-8878-275dbdad9c3c.png');
INSERT INTO `slideshow` VALUES (4, '21312', '/upload/cbec0d19-87c8-4d8f-8659-4d286b662907.png');
INSERT INTO `slideshow` VALUES (5, '123', '/upload/6c089367-8231-4f5a-9224-e31687063ff5.png');
INSERT INTO `slideshow` VALUES (6, '123123', '/upload/be612554-eca1-47ee-a88c-d70085df10ab.png');
INSERT INTO `slideshow` VALUES (7, '12313', '/upload/304264b7-e5d1-4998-bd31-3b6799ca4259.png');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
