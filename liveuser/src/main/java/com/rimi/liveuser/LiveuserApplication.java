package com.rimi.liveuser;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication()
@MapperScan("com.rimi.liveuser.mapper")
@ComponentScan("com.rimi.liveuser.*")
public class LiveuserApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiveuserApplication.class, args);
    }

}

