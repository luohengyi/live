package com.rimi.liveuser.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rimi.liveuser.bean.Date;
import com.rimi.liveuser.bean.Response;
import com.rimi.liveuser.bean.RimitvUser;
import com.rimi.liveuser.service.RimitvUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.List;

/**
 * @author luohengyi
 */
@Controller
@RequestMapping("/login")
@SessionAttributes({"user", "a"})
public class LoginController {

    @Autowired
    private RimitvUserService rimitvUserService;

    @RequestMapping("/login")
    @ResponseBody
    public String login(RimitvUser rimitvUser, ModelMap modelMap) {

        rimitvUser = rimitvUserService.login(rimitvUser);
        if (null == rimitvUser) {
            return Response.creatError("账号或密码错误！");
        }
        modelMap.addAttribute("user", rimitvUser);
        return Response.creatSuccestUrlReload("登陆成功！");
    }

    @RequestMapping("/register")
    @ResponseBody
    //   mobile: 15228917841
    //nickname: 123123q
    //password: asd213123
    public String register(ModelMap modelMap,String mobile, String nickname,String password) {
        RimitvUser rimitvUser = new RimitvUser();
        rimitvUser.setUsername(nickname);
        rimitvUser.setAccount(mobile);
        rimitvUser.setPassword(password);
        rimitvUserService.register(rimitvUser);

        Date date =null;
        if (!rimitvUserService.getMsg().equals("注册成功")){
             date = new Date("0", rimitvUserService.getMsg());
        }else {
             modelMap.addAttribute("user",rimitvUser);
             date = new Date("1", "ok");
        }
        return JSONObject.toJSONString(date);
    }

    @RequestMapping("/nickname")
    @ResponseBody
    public String nickname(String nickname) {

        List<RimitvUser> rimitvUserByUsername = rimitvUserService.getRimitvUserByUsername(nickname);

        if (rimitvUserByUsername.size()>0){
            return "{\"code\": 0, \"message\": 昵称已被使用 }";
        }
        return "{\"code\": 0, \"message\": 昵称已被使用 }";
    }



}
