package com.rimi.liveuser.controller;

import com.rimi.liveuser.bean.Anchor;
import com.rimi.liveuser.bean.LiveKind;
import com.rimi.liveuser.mapper.LiveKindMapper;
import com.rimi.liveuser.service.AnchorService;
import com.rimi.liveuser.service.LiveKindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author luohengyi
 * 直播间
 */
@Controller
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private AnchorService anchorService;
    @Autowired
    private LiveKindService liveKindService;

    @RequestMapping("/index")
    public String index(ModelMap modelMap, Integer id, HttpSession session) {

        Anchor anchor = anchorService.getAnchorById(id);
        modelMap.addAttribute("anchor",anchor);
        Object user = session.getAttribute("user");
        System.out.println(user);

        return "room/index";
    }

    @RequestMapping("/group")
    public String groupindex(ModelMap modelMap, Integer id) {
        modelMap.addAttribute("liveKind", liveKindService.getAllKind());

        return "room/group";
    }

    @RequestMapping("/games")
    public String gamesindex(ModelMap modelMap, Integer id) {
        List<Anchor> anchorByKindId = anchorService.getAnchorByKindId(id);
        modelMap.addAttribute("anchor",anchorByKindId);

        return "room/games";
    }


}
