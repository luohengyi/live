package com.rimi.liveuser.controller;

import com.rimi.liveuser.bean.Beanchor;
import com.rimi.liveuser.bean.LiveKind;
import com.rimi.liveuser.bean.RimitvUser;
import com.rimi.liveuser.service.LiveKindService;
import com.rimi.liveuser.service.RimitvUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用户控制器
 *
 * @author rimi
 * @date 2019-03-05
 */
@Controller
@SessionAttributes("rimitvusert")
public class RimitvuserController {

    @Autowired
    RimitvUserService rimitvUserService;

    @Resource
    LiveKindService liveKindService;

    @RequestMapping("/register")
    @ResponseBody
    public String register(@Validated(value={RimitvUser.Insert.class}) RimitvUser rimitvUser) {
        rimitvUserService.register(rimitvUser);
        return rimitvUserService.getResponseSave();
    }

   /* @RequestMapping("/login")
    @ResponseBody
    public String login(RimitvUser rimitvUser, ModelMap modelMap, HttpServletRequest request) {
        final List <RimitvUser> rimitvUsers=rimitvUserService.selectRimitvUser(rimitvUser);
        final HttpSession session=request.getSession();
        modelMap.addAttribute("");
        if (!rimitvUsers.isEmpty()) {
            for (RimitvUser user : rimitvUsers) {
                session.setAttribute("rimitvusert", user);
                modelMap.addAttribute("rimitvusert",user);
            }
        }
        return rimitvUserService.getResponseSelect();*//*
    }*/
    @RequestMapping("/apply")
    public String apply(HttpServletRequest request , Model model){

        HttpSession session = request.getSession();
        //
        RimitvUser user = (RimitvUser) session.getAttribute("rimitvusert");

        RimitvUser test = new RimitvUser();
        test.setUsername("哈");
        test.setAccount("542");
        List<LiveKind> allKind = liveKindService.getAllKind();
        model.addAttribute("livekind",allKind);
        model.addAttribute("rimitvusert",test);

        return "index/apply";
    }

    @RequestMapping("/toapply")
    @ResponseBody
    public  void toapply(@RequestParam("file1") MultipartFile file1 , @RequestParam("file2") MultipartFile file2, Beanchor beanchor) throws ParseException {
        System.out.println("直接空指针");
        System.out.println(beanchor);
        System.out.println("直接空指针");
        System.out.println(file1.getName());
        System.out.println(file2.getName());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String format = df.format(new Date());
        Date data = df.parse(format);
        beanchor.setTime(data);

        rimitvUserService.toapply(file1,file2,beanchor);


    }

}



