package com.rimi.liveuser.controller;

import com.rimi.liveuser.bean.Beanchor;
import com.rimi.liveuser.bean.LiveKind;
import com.rimi.liveuser.bean.Response;
import com.rimi.liveuser.bean.RimitvUser;
import com.rimi.liveuser.service.LiveKindService;
import com.rimi.liveuser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author rimi
 * @date 2019-03-12
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService ;
    @Autowired
    LiveKindService liveKindService;

    @RequestMapping("/info")
    public String getInfo(){

        return "user/info";
    }

    @RequestMapping("/liveSetting")
    public String setting(ModelMap modelMap , Integer id){
        return  "user/liveSetting" ;
    }


    @RequestMapping("/beanchor")
    public String beAnchor(){
        return "user/beanchor";
    }

    @RequestMapping("/apply")
    public String apply(@RequestParam(value="kindId") Integer id, @RequestParam(value="user_id") Integer user_id, @RequestParam(value="fileimg1")MultipartFile file1 , @RequestParam(value="fileimg2") MultipartFile file2 , Beanchor beanchor, Model model){

        final boolean b=userService.insertBeAnchor( id,user_id, file1, file2, beanchor);
        if (!b){
            return Response.creatError("申请失败");
        }
        return Response.creatSuccestUrlReload("申请成功，请等待审批结果");

    }

}
