package com.rimi.liveuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.rimi.liveuser.bean.*;
import com.rimi.liveuser.service.AnchorService;
import com.rimi.liveuser.service.LiveKindService;
import com.rimi.liveuser.service.SlideshowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author luohengyi
 */
@Controller
@SessionAttributes("a")
public class IndexController {
    @RequestMapping("/cs")
    @ResponseBody
    public String cs(){
        Date date = new Date("1", "抱歉，短信验证失败，请重新发送验证码！");
        System.out.println(JSONObject.toJSONString(date));
        return "";
    }

    @Autowired
    private LiveKindService liveKindService;

    @Autowired
    private AnchorService anchorService;
    @Autowired
    private SlideshowService slideshowService;

    @RequestMapping("/")
    public String index(ModelMap modelMap, Integer id, String name, HttpSession session){
        //分类的处理
        List<LiveKind> allKind = liveKindService.getAllKind();
//        modelMap.addAttribute("liveKind",allKind);

//        //首页显示的主播,如果ID为空那么将分类的第一个id传入否则传入页面的ID
//        List<Anchor> anchorByLiveKind = anchorService.getAnchorByLiveKind(null==id?allKind.get(0).getId():id,name);
//        modelMap.addAttribute("anchors", anchorByLiveKind);

        List<LiveKind> kindAnchor = liveKindService.getKindAnchor();
        modelMap.addAttribute("kindAnchor",kindAnchor);
        List<Slideshow> slideshowAll = slideshowService.getSlideshowAll();
        modelMap.addAttribute("slideshow",slideshowAll);
        System.out.println(slideshowAll);
        Anchor anchorById = anchorService.getAnchorById(5);

        modelMap.addAttribute("anchors",anchorById);
        System.out.println("明明什么也没做就7点了 ");

        return "index/index";
    }


}
