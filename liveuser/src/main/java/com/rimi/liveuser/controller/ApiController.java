package com.rimi.liveuser.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author luohengyi
 */
@Controller
public class ApiController {
    @RequestMapping("/api/user/user.info")
    @ResponseBody
    public String cs(ModelMap modelMap, Integer id){

        return "{\"code\":100,\"message\":\"您还未登录\",\"data\":[]}";
    }

    @RequestMapping("/api/touch/search/v2.1/hotwords.json?_v=25872870")
    @ResponseBody
    public String cs1(ModelMap modelMap, Integer id){

        return "{\"code\":0,\"message\":\"SUCCESS\",\"data\":[{\"hotWord\":\"OWL\\u5b88\\u671b\\u5148\\u950b\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"\\u6587\\u68ee\\u7279\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"\\u7eaf\\u9ed1\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"\\u4f20\\u5947\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"MYleave\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"Alex\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"90God\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"\\u6218\\u65d7\\u9ed1\\u9a6c\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"\\u9a91\\u58eb\\u718a\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"},{\"hotWord\":\"\\u6210\\u6bc5\",\"pcAdWord\":\"\",\"pcAdUrl\":\"\",\"iosAdWord\":\"\",\"iosAdUrl\":\"\",\"androidAdWord\":\"\",\"androidAdUrl\":\"\"}]}";
}

}
