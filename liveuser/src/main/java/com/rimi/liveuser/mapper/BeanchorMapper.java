package com.rimi.liveuser.mapper;

import com.rimi.liveuser.bean.Beanchor;
import com.rimi.liveuser.bean.BeanchorExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BeanchorMapper {
    long countByExample(BeanchorExample example);

    int deleteByExample(BeanchorExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Beanchor record);

    int insertSelective(Beanchor record);

    List<Beanchor> selectByExample(BeanchorExample example);

    Beanchor selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Beanchor record, @Param("example") BeanchorExample example);

    int updateByExample(@Param("record") Beanchor record, @Param("example") BeanchorExample example);

    int updateByPrimaryKeySelective(Beanchor record);

    int updateByPrimaryKey(Beanchor record);
}