package com.rimi.liveuser.mapper;


import com.rimi.liveuser.bean.Anchor;
import com.rimi.liveuser.bean.LiveKind;
import com.rimi.liveuser.bean.LiveKindExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LiveKindMapper {
    long countByExample(LiveKindExample example);

    int deleteByExample(LiveKindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(LiveKind record);

    int insertSelective(LiveKind record);

    List<LiveKind> selectByKindName(String kindName);

    List<LiveKind> selectByExample(LiveKindExample example);

    LiveKind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") LiveKind record, @Param("example") LiveKindExample example);

    int updateByExample(@Param("record") LiveKind record, @Param("example") LiveKindExample example);

    int updateByPrimaryKeySelective(LiveKind record);

    int updateByPrimaryKey(LiveKind record);

    @Select("select * from live_kind order by id")
    List<LiveKind> getAll();

    List<Anchor> getLiveKindAndAnchor();
    List<LiveKind> getAnchorByKindId();
}