package com.rimi.liveuser.mapper;

import com.rimi.liveuser.bean.AnchorPlus;

import java.util.List;

public interface AnchorPulsMapper {
   void   updataAnchor(AnchorPlus anchorPlus);

   AnchorPlus FindAnchorPulsByID(AnchorPlus anchorPlus);
   List<AnchorPlus> getAllAnchor();
}
