package com.rimi.liveuser.mapper;


import com.rimi.liveuser.bean.Anchor;
import com.rimi.liveuser.bean.AnchorExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnchorMapper {
    long countByExample(AnchorExample example);

    int deleteByExample(AnchorExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Anchor record);

    int insertSelective(Anchor record);

    List<Anchor> selectByExample(AnchorExample example);

    Anchor selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Anchor record, @Param("example") AnchorExample example);

    int updateByExample(@Param("record") Anchor record, @Param("example") AnchorExample example);

    int updateByPrimaryKeySelective(Anchor record);

    int updateByPrimaryKey(Anchor record);
}