package com.rimi.liveuser.service;

import com.rimi.liveuser.bean.Navigation;
import com.rimi.liveuser.mapper.NavigationMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author luohengyi
 */
@Service
public class NavigationService {

    @Resource
    NavigationMapper navigationMapper;

   public List<Navigation> getAllNavigation(){
        return navigationMapper.selectByExample(null);
    }
}
