package com.rimi.liveuser.service;

import com.rimi.liveuser.bean.Anchor;
import com.rimi.liveuser.bean.AnchorExample;
import com.rimi.liveuser.bean.AnchorPlus;
import com.rimi.liveuser.mapper.AnchorMapper;
import com.rimi.liveuser.mapper.AnchorPulsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author luohengyi
 */
@Service
public class AnchorService {

    @Resource
    private AnchorMapper anchorMapper;
    @Resource
    private AnchorPulsMapper anchorPulsMapper;
    public List<Anchor> getAnchorByLiveKind(Integer id,String name) {
        if (null == id) {
            return new ArrayList<Anchor>();
        }
        AnchorExample anchorExample = new AnchorExample();
        AnchorExample.Criteria criteria = anchorExample.createCriteria().andKindIdEqualTo(id);
        if (null!=name && !name.isEmpty()){
            criteria.andHeadlineLike("%"+name+"%");
        }
        return anchorMapper.selectByExample(anchorExample);
    }

    public Anchor getAnchorById(Integer id){
        return anchorMapper.selectByPrimaryKey(id);
    }

    public List<Anchor> getAnchorByKindId(Integer id){
        AnchorExample anchorExample = new AnchorExample();
        anchorExample.createCriteria().andKindIdEqualTo(id);
        return anchorMapper.selectByExample(anchorExample);
    }

    public  void  updataAnchor(AnchorPlus anchorPlus){

        Anchor anchor = new Anchor();
        BeanUtils.copyProperties(anchorPlus,anchor);
        int i = anchorMapper.updateByPrimaryKeySelective(anchor);
        if(anchorPlus.getImg()!=""&&anchorPlus.getImg()!=null){
            anchorPulsMapper.updataAnchor(anchorPlus);


        }


    }
    public AnchorPlus findAnchorById(Integer id, AnchorPlus anchorPlus){
        anchorPlus.setId(id);
        AnchorPlus anchorPlus1 = anchorPulsMapper.FindAnchorPulsByID(anchorPlus);
        return anchorPlus1;
    }

    public  List<AnchorPlus> getAllAnchorPuls(){
        List<AnchorPlus> allAnchor = anchorPulsMapper.getAllAnchor();
        for (AnchorPlus anchorPlus : allAnchor) {
            System.out.println(anchorPlus);
        }
            return allAnchor;
    }

}
