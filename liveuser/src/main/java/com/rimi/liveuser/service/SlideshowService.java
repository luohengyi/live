package com.rimi.liveuser.service;

import com.rimi.liveuser.bean.Slideshow;
import com.rimi.liveuser.mapper.SlideshowMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author luohengyi
 */
@Service
public class SlideshowService {

    @Resource
    private SlideshowMapper slideshowMapper;

    public List<Slideshow> getSlideshowAll(){
        return slideshowMapper.selectByExample(null);
    }

}
