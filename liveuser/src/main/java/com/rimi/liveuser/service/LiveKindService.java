package com.rimi.liveuser.service;

import com.rimi.liveuser.bean.LiveKind;
import com.rimi.liveuser.bean.LiveKindExample;
import com.rimi.liveuser.mapper.LiveKindMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author luohengyi
 */
@Service
public class LiveKindService {

    @Resource
    private LiveKindMapper liveKindMapper;

    public List<LiveKind> getAllKind() {
        return liveKindMapper.getAll();
    }

    public LiveKind getAllKindOrderByIdFirst() {
        return getAllKind().get(0);
    }

    public List<LiveKind> getKindAnchor(){
        List<LiveKind> liveKinds = liveKindMapper.selectByExample(null);
        return liveKinds;
    }

    public List<LiveKind> getLiveKindId (LiveKindExample liveKindExample){
        final List <LiveKind> liveKinds=liveKindMapper.selectByExample(liveKindExample);

       return liveKinds;
    }

}
