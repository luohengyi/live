package com.rimi.liveuser.service;

import com.rimi.liveuser.bean.Beanchor;
import com.rimi.liveuser.bean.RimitvUser;
import com.rimi.liveuser.bean.RimitvUserExample;
import com.rimi.liveuser.mapper.BeanchorMapper;
import com.rimi.liveuser.mapper.RimitvUserMapper;
import com.rimi.liveuser.util.UploadUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户接口
 *
 * @author rimi
 * @date 2019-03-05
 */
@Service
public class RimitvUserService extends CommonServer {

    @Resource
    RimitvUserMapper rimitvUserMapper;
    @Resource
    BeanchorMapper beanchorMapper;

    {
        listUrl = "/";
    }


    public RimitvUser  login(RimitvUser rimitvUser) {
        RimitvUserExample rimitvUserExample = new RimitvUserExample();
        rimitvUserExample.createCriteria().andAccountEqualTo(rimitvUser.getAccount()).andPasswordEqualTo(rimitvUser.getPassword());
        List<RimitvUser> rimitvUsers = rimitvUserMapper.selectByExample(rimitvUserExample);
        if (rimitvUsers.isEmpty()) {

            return null;
        }

        return rimitvUsers.get(0);
    }

    /**
     * 注册
     *
     * @param rimitvUser
     */
    public void register(RimitvUser rimitvUser) {
        RimitvUserExample rimitvUserExample = new RimitvUserExample();
        rimitvUserExample.createCriteria().andAccountEqualTo(rimitvUser.getAccount());
        List<RimitvUser> rimitvUsers = rimitvUserMapper.selectByExample(rimitvUserExample);
        if (!rimitvUsers.isEmpty()) {
            setErrorMsg("用户名已存在");
            return;
        }
        int back = rimitvUserMapper.insert(rimitvUser);
        if (back > 0) {
            setSuccessMsg("注册成功");
        } else {
            setErrorMsg("注册失败");
        }
    }

    /**
     * @param rimitvUser
     * @return 查询时
     */
    public List<RimitvUser> selectRimitvUser(RimitvUser rimitvUser) {
        RimitvUserExample rimitvUserExample = new RimitvUserExample();
        rimitvUserExample.createCriteria().andAccountEqualTo(rimitvUser.getAccount()).andPasswordEqualTo(rimitvUser.getPassword());
        final List<RimitvUser> rimitvUsers = rimitvUserMapper.selectByExample(rimitvUserExample);
        return rimitvUsers;
    }

    public List<RimitvUser> getRimitvUserByUsername(String username) {
        RimitvUserExample rimitvUserExample = new RimitvUserExample();
        rimitvUserExample.createCriteria().andUsernameEqualTo(username);
        List<RimitvUser> rimitvUsers = rimitvUserMapper.selectByExample(rimitvUserExample);
        return rimitvUsers;
    }


    public void toapply(MultipartFile file1, MultipartFile file2, Beanchor beanchor) {
        try {
            String zhen = UploadUtil.uploading(file1);
            System.out.println("正面");
            String fan = UploadUtil.uploading(file2);
            System.out.println("反面");
            beanchor.setCardinformation1(zhen);
            beanchor.setCardinformation2(fan);


            System.out.println(beanchor);
            int insert = beanchorMapper.insert(beanchor);
            System.out.println("修改i");
            if (insert != 1) {
                setSuccessMsg("修改失败");
            } else {
                setErrorMsg("修改成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            setErrorMsg("修改失败");
        }


    }
}
