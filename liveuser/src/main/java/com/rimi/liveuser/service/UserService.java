package com.rimi.liveuser.service;

import com.rimi.liveuser.bean.*;
import com.rimi.liveuser.mapper.BeanchorMapper;
import com.rimi.liveuser.mapper.RimitvUserMapper;
import com.rimi.liveuser.util.UploadUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author rimi
 * @date 2019-03-12
 */
@Service
public class UserService extends CommonServer {

    @Resource
    RimitvUserMapper rimitvUserMapper;

    @Resource
    BeanchorMapper beanchorMapper;

    {
        listUrl="/user/info";
    }


    public List<RimitvUser> getInfo(RimitvUser rimitvUser){
        RimitvUserExample rimitvUserExample = new RimitvUserExample();
        rimitvUserExample.createCriteria().andAccountEqualTo(rimitvUser.getAccount()).andPasswordEqualTo(rimitvUser.getPassword());
        return   rimitvUserMapper.selectByExample(rimitvUserExample);
    }


    public boolean insertBeAnchor( Integer id,Integer user_id, MultipartFile file1 ,  MultipartFile file2 , Beanchor beanchor){
       String fileimg1 =UploadUtil.uploading(file1);
       String fileimg2 =UploadUtil.uploading(file2);
       beanchor.setCardinformation1(fileimg1);
       beanchor.setCardinformation2(fileimg2);
       beanchor.setKindId(id);
       beanchor.setUserId(user_id);
        final int insert=beanchorMapper.insert(beanchor);
        if (insert == 1){
            return true ;
        }else {
            return false;
        }
    }
}
