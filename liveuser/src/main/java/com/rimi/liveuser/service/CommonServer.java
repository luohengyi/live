package com.rimi.liveuser.service;


import com.rimi.liveuser.bean.Response;

/**
 * server层公共响应方法的封装
 *
 * @author luohengyi
 */
public class CommonServer {

    protected boolean type = true;
    protected String msg;
    protected String listUrl;

    public String getMsg() {
        return msg;
    }

    public boolean isType() {
        return type;
    }

    public void setSuccessMsg(String msg) {
        this.msg = msg;
    }

    public void setErrorMsg(String msg) {
        type=false;
        this.msg = msg;
    }

    /**
     * 添加，修改
     * @return Response
     */
    public String getResponseSave() {
        if (type) {
            return Response.creatSuccess(msg, listUrl);
        }
        return creatErrorResponse();
    }

    /**
     * @return
     * 查询方法的响应
     */
    public String getResponseSelect() {
        if (type) {
            return  Response.creatSuccestUrlReload(msg);
        }
        return creatErrorResponse();
    }

    /**
     * 删除方法的响应
     *
     * @return Response
     */
    public String getResponseDelete() {
        if (type) {
            return Response.creatSuccestUrlReload(msg);
        }
        return creatErrorResponse();
    }

    /**
     * 根据信息构建一个警告信息
     *
     * @return Response
     */
    private String creatErrorResponse() {
        return Response.creatError(msg);
    }


}
