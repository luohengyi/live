package com.rimi.liveuser.core;





import com.rimi.liveuser.bean.LiveKind;
import com.rimi.liveuser.bean.Navigation;
import com.rimi.liveuser.service.LiveKindService;
import com.rimi.liveuser.service.NavigationService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.util.List;

/**
 * @author luohengyi
 * 用户处理前端多个页面需要主播分类的需求
 */
@Component
@Aspect
public class ClassifyAop {

    @Autowired
    private LiveKindService liveKindService;
    @Autowired
    private NavigationService navigationService;

    /**
     * 切入点
     * 匹配com.example.controller1包及其子包下的所有类的所有方法
     */
    @Pointcut("execution(* com.rimi.liveuser.controller.*.*index(..))")
    public void executePackage() {
    }

    /**
     *
     * @param joinPoint
     */
    @After("executePackage()")
    public void beforeAdvice(JoinPoint joinPoint) {
        //获取该方法的参数列表
        Object[] obj = joinPoint.getArgs();
        //第一个参数必须是modelMap类用户传输数据
        ModelMap modelMap = (ModelMap) obj[0];

        List<Navigation> allNavigation = navigationService.getAllNavigation();
        modelMap.addAttribute("allNavigation",allNavigation);
    }


}
