package com.rimi.liveuser.socket;


import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @param {roomID} 房间号
 * @param {userId} 用户ID
 * @author luohengyi
 */
@ServerEndpoint("/live/{roomID}/{userId}")
@Component
public class MyWebSocket {

    /**
     * 静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
     */
    private static int onlineCount = 0;


    /**
     * 将用户链接的websocket分组存放，然后发送消息的时候分组发送
     */
    private static Map<String, CopyOnWriteArraySet<MyWebSocket>> webSocketLive = new Hashtable<>();

    /**
     * 与某个客户端的连接会话，需要通过它来给客户端发送数据
     */
    private Session session;

    /**
     *当前用户的房间号
     */
    private String room;

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(@PathParam(value = "roomID") String roomID, @PathParam(value = "userId") String userId, Session session) {
        System.out.println(roomID);
        this.room=roomID;
        //总人数的处理
        this.session = session;
        //在线数加1
        addOnlineCount();
        //房间的处理
        CopyOnWriteArraySet room = null;
        if (webSocketLive.containsKey(roomID)) {
            room = webSocketLive.get(roomID);
        } else {
            room = new CopyOnWriteArraySet();
            webSocketLive.put(roomID, room);
        }
        room.add(this);

    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        System.out.println(room);
        //删除房间号的链接
        webSocketLive.get(room).remove(this);
        //在线数减1
        subOnlineCount();
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        //群发消息
        System.out.println(message);
        for (MyWebSocket item : webSocketLive.get(room)) {
            try {
                item.sendMessage(item,message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 发生错误时调用
     *
     * @OnError
     **/
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");
        error.printStackTrace();
    }


    public void sendMessage(MyWebSocket socket,String message) throws IOException {
        socket.getSession().getBasicRemote().sendText(message);
    }


    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public Session getSession() {
        return session;
    }

    public static synchronized void addOnlineCount() {
        MyWebSocket.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        MyWebSocket.onlineCount--;
    }
}
