package com.rimi.liveuser.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

/**
 * @author luohengyi
 */
public class Role {
    public interface Update{};
    public interface Insert{};

    @Min(value = 1,message = "非法数据",groups = Update.class)
    private Integer id;

    @NotEmpty(message = "角色名称不能为空",groups = Insert.class)
    private String name;

    private String ruleconten;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRuleconten() {
        return ruleconten;
    }

    public void setRuleconten(String ruleconten) {
        this.ruleconten = ruleconten == null ? null : ruleconten.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", ruleconten=").append(ruleconten);
        sb.append("]");
        return sb.toString();
    }
}