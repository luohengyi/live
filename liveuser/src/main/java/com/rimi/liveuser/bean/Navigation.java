package com.rimi.liveuser.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class Navigation extends GroupValidate{

    @Min(value = 1,message = "非法数据",groups = Role.Update.class)
    private Integer id;

    @NotEmpty(message = "菜单名称不能为空！",groups = Navigation.Insert.class)
    private String name;

    @NotEmpty(message = "菜单url不能为空",groups = Navigation.Insert.class)
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", url=").append(url);
        sb.append("]");
        return sb.toString();
    }
}