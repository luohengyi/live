package com.rimi.liveuser.bean;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class LiveKind {
    public interface Update{};
    public interface Insert{};

    @Min(value = 1,message = "非法数据",groups = Role.Update.class)
    private Integer id;

    @NotEmpty(message = "直播类别不能为空",groups = Role.Insert.class)
    private String name;
    private String img;

    private List<Anchor> anchorList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append("]");
        return sb.toString();
    }
}