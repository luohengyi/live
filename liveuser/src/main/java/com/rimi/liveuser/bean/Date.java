package com.rimi.liveuser.bean;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class Date {
    String code;
    String message;

    public Date(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
