package com.rimi.liveuser.bean;

import javax.validation.constraints.Min;

public class Slideshow extends GroupValidate{
    @Min(value = 1,message = "非法数据",groups = Slideshow.Update.class)
    private Integer id;

    private String url;

    private String skipUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getSkipUrl() {
        return skipUrl;
    }

    public void setSkipUrl(String skipUrl) {
        this.skipUrl = skipUrl == null ? null : skipUrl.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", url=").append(url);
        sb.append(", skipUrl=").append(skipUrl);
        sb.append("]");
        return sb.toString();
    }
}