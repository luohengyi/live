package com.rimi.liveuser.bean;

import lombok.Data;

import java.util.Date;
@Data
public class Anchor {
    private Integer id;
    private String img;
    private String userId;

    private String idcard;

    private Integer kindId;

    private Date lasttime;

    private String cardinformation1;

    private String cardinformation2;

    private Integer flag;

    private String headline;

    private RimitvUser rimitvUser;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId=userId;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard == null ? null : idcard.trim();
    }

    public Integer getKindId() {
        return kindId;
    }

    public void setKindId(Integer kindId) {
        this.kindId = kindId;
    }

    public Date getLasttime() {
        return lasttime;
    }

    public void setLasttime(Date lasttime) {
        this.lasttime = lasttime;
    }

    public String getCardinformation1() {
        return cardinformation1;
    }

    public void setCardinformation1(String cardinformation1) {
        this.cardinformation1 = cardinformation1 == null ? null : cardinformation1.trim();
    }

    public String getCardinformation2() {
        return cardinformation2;
    }

    public void setCardinformation2(String cardinformation2) {
        this.cardinformation2 = cardinformation2 == null ? null : cardinformation2.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline == null ? null : headline.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", idcard=").append(idcard);
        sb.append(", kindId=").append(kindId);
        sb.append(", lasttime=").append(lasttime);
        sb.append(", cardinformation1=").append(cardinformation1);
        sb.append(", cardinformation2=").append(cardinformation2);
        sb.append(", flag=").append(flag);
        sb.append(", headline=").append(headline);
        sb.append("]");
        return sb.toString();
    }
}