!function(t, i) {
    !function(t) {
        "use strict";
        function i(t) {
            if (Array.isArray(t)) {
                for (var i = 0, e = Array(t.length); i < t.length; i++)
                    e[i] = t[i];
                return e
            }
            return Array.from(t)
        }
        function e(t) {
            return "string" == typeof t
        }
        function a(t) {
            return "number" == typeof t && !j(t)
        }
        function n(t) {
            return void 0 === t
        }
        function o(t, i) {
            for (var e = arguments.length, a = Array(e > 2 ? e - 2 : 0), n = 2; n < e; n++)
                a[n - 2] = arguments[n];
            return function() {
                for (var e = arguments.length, n = Array(e), o = 0; o < e; o++)
                    n[o] = arguments[o];
                return t.apply(i, a.concat(n))
            }
        }
        function r(t) {
            var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1e11;
            return N.test(t) ? Math.round(t * i) / i : t
        }
        function s(t) {
            var i = t.match(z);
            return i && (i[1] !== E.protocol || i[2] !== E.hostname || i[3] !== E.port)
        }
        function h(t) {
            var i = "timestamp=" + (new Date).getTime();
            return t + (-1 === t.indexOf("?") ? "?" : "&") + i
        }
        function c(t) {
            var i = t.rotate
                , e = t.scaleX
                , n = t.scaleY
                , o = t.translateX
                , r = t.translateY
                , s = [];
            return a(o) && 0 !== o && s.push("translateX(" + o + "px)"),
            a(r) && 0 !== r && s.push("translateY(" + r + "px)"),
            a(i) && 0 !== i && s.push("rotate(" + i + "deg)"),
            a(e) && 1 !== e && s.push("scaleX(" + e + ")"),
            a(n) && 1 !== n && s.push("scaleY(" + n + ")"),
                s.length ? s.join(" ") : "none"
        }
        function d(t, i) {
            if (t.naturalWidth && !I)
                return void i(t.naturalWidth, t.naturalHeight);
            var e = document.createElement("img");
            e.onload = function() {
                i(e.width, e.height)
            }
                ,
                e.src = t.src
        }
        function l(i) {
            var e = t.extend({}, i)
                , a = [];
            return t.each(i, function(i, n) {
                delete e[i],
                    t.each(e, function(t, i) {
                        var e = Math.abs(n.startX - i.startX)
                            , o = Math.abs(n.startY - i.startY)
                            , r = Math.abs(n.endX - i.endX)
                            , s = Math.abs(n.endY - i.endY)
                            , h = Math.sqrt(e * e + o * o)
                            , c = Math.sqrt(r * r + s * s)
                            , d = (c - h) / h;
                        a.push(d)
                    })
            }),
                a.sort(function(t, i) {
                    return Math.abs(t) < Math.abs(i)
                }),
                a[0]
        }
        function p(i, e) {
            var a = i.pageX
                , n = i.pageY
                , o = {
                endX: a,
                endY: n
            };
            return e ? o : t.extend({
                startX: a,
                startY: n
            }, o)
        }
        function m(i) {
            var e = 0
                , a = 0
                , n = 0;
            return t.each(i, function(t, i) {
                var o = i.startX
                    , r = i.startY;
                e += o,
                    a += r,
                    n += 1
            }),
                e /= n,
                a /= n,
                {
                    pageX: e,
                    pageY: a
                }
        }
        function f(t) {
            var i = t.aspectRatio
                , e = t.height
                , a = t.width
                , n = function(t) {
                return U(t) && t > 0
            };
            return n(a) && n(e) ? e * i > a ? e = a / i : a = e * i : n(a) ? e = a / i : n(e) && (a = e * i),
                {
                    width: a,
                    height: e
                }
        }
        function g(t) {
            var i = t.width
                , e = t.height
                , a = t.degree;
            if ((a = Math.abs(a)) % 180 == 90)
                return {
                    width: e,
                    height: i
                };
            var n = a % 90 * Math.PI / 180
                , o = Math.sin(n)
                , r = Math.cos(n);
            return {
                width: i * r + e * o,
                height: i * o + e * r
            }
        }
        function u(e, a, n, o) {
            var s = a.naturalWidth
                , h = a.naturalHeight
                , c = a.rotate
                , d = void 0 === c ? 0 : c
                , l = a.scaleX
                , p = void 0 === l ? 1 : l
                , m = a.scaleY
                , g = void 0 === m ? 1 : m
                , u = n.aspectRatio
                , v = n.naturalWidth
                , w = n.naturalHeight
                , x = o.fillColor
                , b = void 0 === x ? "transparent" : x
                , y = o.imageSmoothingEnabled
                , C = void 0 === y || y
                , $ = o.imageSmoothingQuality
                , M = void 0 === $ ? "low" : $
                , B = o.maxWidth
                , k = void 0 === B ? 1 / 0 : B
                , D = o.maxHeight
                , W = void 0 === D ? 1 / 0 : D
                , P = o.minWidth
                , T = void 0 === P ? 0 : P
                , Y = o.minHeight
                , H = void 0 === Y ? 0 : Y
                , X = f({
                aspectRatio: u,
                width: k,
                height: W
            })
                , j = f({
                aspectRatio: u,
                width: T,
                height: H
            })
                , R = Math.min(X.width, Math.max(j.width, v))
                , N = Math.min(X.height, Math.max(j.height, w))
                , E = document.createElement("canvas")
                , z = E.getContext("2d")
                , O = [-s / 2, -h / 2, s, h];
            return E.width = r(R),
                E.height = r(N),
                z.fillStyle = b,
                z.fillRect(0, 0, R, N),
                z.save(),
                z.translate(R / 2, N / 2),
                z.rotate(d * Math.PI / 180),
                z.scale(p, g),
                z.imageSmoothingEnabled = !!C,
                z.imageSmoothingQuality = M,
                z.drawImage.apply(z, [e].concat(i(t.map(O, function(t) {
                    return Math.floor(r(t))
                })))),
                z.restore(),
                E
        }
        function v(t, i, e) {
            var a = ""
                , n = void 0;
            for (e += i,
                     n = i; n < e; n += 1)
                a += L(t.getUint8(n));
            return a
        }
        function w(i) {
            var e = i.replace(A, "")
                , a = atob(e)
                , n = new ArrayBuffer(a.length)
                , o = new Uint8Array(n);
            return t.each(o, function(t) {
                o[t] = a.charCodeAt(t)
            }),
                n
        }
        function x(i, e) {
            var a = new Uint8Array(i)
                , n = "";
            return t.each(a, function(t, i) {
                n += L(i)
            }),
            "data:" + e + ";base64," + btoa(n)
        }
        function b(t) {
            var i = new DataView(t)
                , e = void 0
                , a = void 0
                , n = void 0
                , o = void 0;
            if (255 === i.getUint8(0) && 216 === i.getUint8(1))
                for (var r = i.byteLength, s = 2; s < r; ) {
                    if (255 === i.getUint8(s) && 225 === i.getUint8(s + 1)) {
                        n = s;
                        break
                    }
                    s += 1
                }
            if (n) {
                var h = n + 4
                    , c = n + 10;
                if ("Exif" === v(i, h, 4)) {
                    var d = i.getUint16(c);
                    if (((a = 18761 === d) || 19789 === d) && 42 === i.getUint16(c + 2, a)) {
                        var l = i.getUint32(c + 4, a);
                        l >= 8 && (o = c + l)
                    }
                }
            }
            if (o) {
                var p = i.getUint16(o, a)
                    , m = void 0
                    , f = void 0;
                for (f = 0; f < p; f += 1)
                    if (m = o + 12 * f + 2,
                    274 === i.getUint16(m, a)) {
                        m += 8,
                            e = i.getUint16(m, a),
                            i.setUint16(m, 1, a);
                        break
                    }
            }
            return e
        }
        function y(t) {
            var i = 0
                , e = 1
                , a = 1;
            switch (t) {
                case 2:
                    e = -1;
                    break;
                case 3:
                    i = -180;
                    break;
                case 4:
                    a = -1;
                    break;
                case 5:
                    i = 90,
                        a = -1;
                    break;
                case 6:
                    i = 90;
                    break;
                case 7:
                    i = 90,
                        e = -1;
                    break;
                case 8:
                    i = -90
            }
            return {
                rotate: i,
                scaleX: e,
                scaleY: a
            }
        }
        function C(t) {
            if (Array.isArray(t)) {
                for (var i = 0, e = Array(t.length); i < t.length; i++)
                    e[i] = t[i];
                return e
            }
            return Array.from(t)
        }
        function $(t, i) {
            if (!(t instanceof i))
                throw new TypeError("Cannot call a class as a function")
        }
        t = t && t.hasOwnProperty("default") ? t.default : t;
        var M = "undefined" != typeof window ? window : {}
            , B = "cropper-hidden"
            , k = M.PointerEvent ? "pointerdown" : "touchstart mousedown"
            , D = M.PointerEvent ? "pointermove" : "touchmove mousemove"
            , W = M.PointerEvent ? " pointerup pointercancel" : "touchend touchcancel mouseup"
            , P = /^(e|w|s|n|se|sw|ne|nw|all|crop|move|zoom)$/
            , T = /^data:/
            , Y = /^data:image\/jpeg;base64,/
            , H = /^(img|canvas)$/i
            , X = {
            viewMode: 0,
            dragMode: "crop",
            aspectRatio: NaN,
            data: null,
            preview: "",
            responsive: !0,
            restore: !0,
            checkCrossOrigin: !0,
            checkOrientation: !0,
            modal: !0,
            guides: !0,
            center: !0,
            highlight: !0,
            background: !0,
            autoCrop: !0,
            autoCropArea: .8,
            movable: !0,
            rotatable: !0,
            scalable: !0,
            zoomable: !0,
            zoomOnTouch: !0,
            zoomOnWheel: !0,
            wheelZoomRatio: .1,
            cropBoxMovable: !0,
            cropBoxResizable: !0,
            toggleDragModeOnDblclick: !0,
            minCanvasWidth: 0,
            minCanvasHeight: 0,
            minCropBoxWidth: 0,
            minCropBoxHeight: 0,
            minContainerWidth: 200,
            minContainerHeight: 100,
            ready: null,
            cropstart: null,
            cropmove: null,
            cropend: null,
            crop: null,
            zoom: null
        }
            , j = Number.isNaN || M.isNaN
            , R = Object.keys || function(i) {
            var e = [];
            return t.each(i, function(t) {
                e.push(t)
            }),
                e
        }
            , N = /\.\d*(?:0|9){12}\d*$/i
            , E = M.location
            , z = /^(https?:)\/\/([^:\/?#]+):?(\d*)/i
            , O = M.navigator
            , I = O && /(Macintosh|iPhone|iPod|iPad).*AppleWebKit/i.test(O.userAgent)
            , U = Number.isFinite || M.isFinite
            , L = String.fromCharCode
            , A = /^data:.*,/
            , F = {
            render: function() {
                this.initContainer(),
                    this.initCanvas(),
                    this.initCropBox(),
                    this.renderCanvas(),
                this.cropped && this.renderCropBox()
            },
            initContainer: function() {
                var t = this.$element
                    , i = this.options
                    , e = this.$container
                    , a = this.$cropper;
                a.addClass(B),
                    t.removeClass(B),
                    a.css(this.container = {
                        width: Math.max(e.width(), Number(i.minContainerWidth) || 200),
                        height: Math.max(e.height(), Number(i.minContainerHeight) || 100)
                    }),
                    t.addClass(B),
                    a.removeClass(B)
            },
            initCanvas: function() {
                var i = this.container
                    , e = this.image
                    , a = this.options.viewMode
                    , n = Math.abs(e.rotate) % 180 == 90
                    , o = n ? e.naturalHeight : e.naturalWidth
                    , r = n ? e.naturalWidth : e.naturalHeight
                    , s = o / r
                    , h = i.width
                    , c = i.height;
                i.height * s > i.width ? 3 === a ? h = i.height * s : c = i.width / s : 3 === a ? c = i.width / s : h = i.height * s;
                var d = {
                    aspectRatio: s,
                    naturalWidth: o,
                    naturalHeight: r,
                    width: h,
                    height: c
                };
                d.left = (i.width - h) / 2,
                    d.top = (i.height - c) / 2,
                    d.oldLeft = d.left,
                    d.oldTop = d.top,
                    this.canvas = d,
                    this.limited = 1 === a || 2 === a,
                    this.limitCanvas(!0, !0),
                    this.initialImage = t.extend({}, e),
                    this.initialCanvas = t.extend({}, d)
            },
            limitCanvas: function(t, i) {
                var e = this.options
                    , a = this.container
                    , n = this.canvas
                    , o = this.cropBox
                    , r = e.viewMode
                    , s = n.aspectRatio
                    , h = this.cropped && o;
                if (t) {
                    var c = Number(e.minCanvasWidth) || 0
                        , d = Number(e.minCanvasHeight) || 0;
                    r > 0 && (r > 1 ? (c = Math.max(c, a.width),
                        d = Math.max(d, a.height),
                    3 === r && (d * s > c ? c = d * s : d = c / s)) : c ? c = Math.max(c, h ? o.width : 0) : d ? d = Math.max(d, h ? o.height : 0) : h && (c = o.width,
                        d = o.height,
                        d * s > c ? c = d * s : d = c / s));
                    var l = f({
                        aspectRatio: s,
                        width: c,
                        height: d
                    });
                    c = l.width,
                        d = l.height,
                        n.minWidth = c,
                        n.minHeight = d,
                        n.maxWidth = 1 / 0,
                        n.maxHeight = 1 / 0
                }
                if (i)
                    if (r > 0) {
                        var p = a.width - n.width
                            , m = a.height - n.height;
                        n.minLeft = Math.min(0, p),
                            n.minTop = Math.min(0, m),
                            n.maxLeft = Math.max(0, p),
                            n.maxTop = Math.max(0, m),
                        h && this.limited && (n.minLeft = Math.min(o.left, o.left + o.width - n.width),
                            n.minTop = Math.min(o.top, o.top + o.height - n.height),
                            n.maxLeft = o.left,
                            n.maxTop = o.top,
                        2 === r && (n.width >= a.width && (n.minLeft = Math.min(0, p),
                            n.maxLeft = Math.max(0, p)),
                        n.height >= a.height && (n.minTop = Math.min(0, m),
                            n.maxTop = Math.max(0, m))))
                    } else
                        n.minLeft = -n.width,
                            n.minTop = -n.height,
                            n.maxLeft = a.width,
                            n.maxTop = a.height
            },
            renderCanvas: function(t, i) {
                var e = this.canvas
                    , a = this.image;
                if (i) {
                    var n = g({
                        width: a.naturalWidth * Math.abs(a.scaleX || 1),
                        height: a.naturalHeight * Math.abs(a.scaleY || 1),
                        degree: a.rotate || 0
                    })
                        , o = n.width
                        , r = n.height
                        , s = e.width * (o / e.naturalWidth)
                        , h = e.height * (r / e.naturalHeight);
                    e.left -= (s - e.width) / 2,
                        e.top -= (h - e.height) / 2,
                        e.width = s,
                        e.height = h,
                        e.aspectRatio = o / r,
                        e.naturalWidth = o,
                        e.naturalHeight = r,
                        this.limitCanvas(!0, !1)
                }
                (e.width > e.maxWidth || e.width < e.minWidth) && (e.left = e.oldLeft),
                (e.height > e.maxHeight || e.height < e.minHeight) && (e.top = e.oldTop),
                    e.width = Math.min(Math.max(e.width, e.minWidth), e.maxWidth),
                    e.height = Math.min(Math.max(e.height, e.minHeight), e.maxHeight),
                    this.limitCanvas(!1, !0),
                    e.left = Math.min(Math.max(e.left, e.minLeft), e.maxLeft),
                    e.top = Math.min(Math.max(e.top, e.minTop), e.maxTop),
                    e.oldLeft = e.left,
                    e.oldTop = e.top,
                    this.$canvas.css({
                        width: e.width,
                        height: e.height,
                        transform: c({
                            translateX: e.left,
                            translateY: e.top
                        })
                    }),
                    this.renderImage(t),
                this.cropped && this.limited && this.limitCropBox(!0, !0)
            },
            renderImage: function(i) {
                var e = this.canvas
                    , a = this.image
                    , n = a.naturalWidth * (e.width / e.naturalWidth)
                    , o = a.naturalHeight * (e.height / e.naturalHeight);
                t.extend(a, {
                    width: n,
                    height: o,
                    left: (e.width - n) / 2,
                    top: (e.height - o) / 2
                }),
                    this.$clone.css({
                        width: a.width,
                        height: a.height,
                        transform: c(t.extend({
                            translateX: a.left,
                            translateY: a.top
                        }, a))
                    }),
                i && this.output()
            },
            initCropBox: function() {
                var i = this.options
                    , e = this.canvas
                    , a = i.aspectRatio
                    , n = Number(i.autoCropArea) || .8
                    , o = {
                    width: e.width,
                    height: e.height
                };
                a && (e.height * a > e.width ? o.height = o.width / a : o.width = o.height * a),
                    this.cropBox = o,
                    this.limitCropBox(!0, !0),
                    o.width = Math.min(Math.max(o.width, o.minWidth), o.maxWidth),
                    o.height = Math.min(Math.max(o.height, o.minHeight), o.maxHeight),
                    o.width = Math.max(o.minWidth, o.width * n),
                    o.height = Math.max(o.minHeight, o.height * n),
                    o.left = e.left + (e.width - o.width) / 2,
                    o.top = e.top + (e.height - o.height) / 2,
                    o.oldLeft = o.left,
                    o.oldTop = o.top,
                    this.initialCropBox = t.extend({}, o)
            },
            limitCropBox: function(t, i) {
                var e = this.options
                    , a = this.container
                    , n = this.canvas
                    , o = this.cropBox
                    , r = this.limited
                    , s = e.aspectRatio;
                if (t) {
                    var h = Number(e.minCropBoxWidth) || 0
                        , c = Number(e.minCropBoxHeight) || 0
                        , d = Math.min(a.width, r ? n.width : a.width)
                        , l = Math.min(a.height, r ? n.height : a.height);
                    h = Math.min(h, a.width),
                        c = Math.min(c, a.height),
                    s && (h && c ? c * s > h ? c = h / s : h = c * s : h ? c = h / s : c && (h = c * s),
                        l * s > d ? l = d / s : d = l * s),
                        o.minWidth = Math.min(h, d),
                        o.minHeight = Math.min(c, l),
                        o.maxWidth = d,
                        o.maxHeight = l
                }
                i && (r ? (o.minLeft = Math.max(0, n.left),
                    o.minTop = Math.max(0, n.top),
                    o.maxLeft = Math.min(a.width, n.left + n.width) - o.width,
                    o.maxTop = Math.min(a.height, n.top + n.height) - o.height) : (o.minLeft = 0,
                    o.minTop = 0,
                    o.maxLeft = a.width - o.width,
                    o.maxTop = a.height - o.height))
            },
            renderCropBox: function() {
                var t = this.options
                    , i = this.container
                    , e = this.cropBox;
                (e.width > e.maxWidth || e.width < e.minWidth) && (e.left = e.oldLeft),
                (e.height > e.maxHeight || e.height < e.minHeight) && (e.top = e.oldTop),
                    e.width = Math.min(Math.max(e.width, e.minWidth), e.maxWidth),
                    e.height = Math.min(Math.max(e.height, e.minHeight), e.maxHeight),
                    this.limitCropBox(!1, !0),
                    e.left = Math.min(Math.max(e.left, e.minLeft), e.maxLeft),
                    e.top = Math.min(Math.max(e.top, e.minTop), e.maxTop),
                    e.oldLeft = e.left,
                    e.oldTop = e.top,
                t.movable && t.cropBoxMovable && this.$face.data("action", e.width >= i.width && e.height >= i.height ? "move" : "all"),
                    this.$cropBox.css({
                        width: e.width,
                        height: e.height,
                        transform: c({
                            translateX: e.left,
                            translateY: e.top
                        })
                    }),
                this.cropped && this.limited && this.limitCanvas(!0, !0),
                this.disabled || this.output()
            },
            output: function() {
                this.preview(),
                this.completed && this.trigger("crop", this.getData())
            }
        }
            , S = {
            initPreview: function() {
                var i = this.crossOrigin
                    , e = i ? this.crossOriginUrl : this.url
                    , a = document.createElement("img");
                i && (a.crossOrigin = i),
                    a.src = e;
                var n = t(a);
                this.$preview = t(this.options.preview),
                    this.$clone2 = n,
                    this.$viewBox.html(n),
                    this.$preview.each(function(a, n) {
                        var o = t(n)
                            , r = document.createElement("img");
                        o.data("preview", {
                            width: o.width(),
                            height: o.height(),
                            html: o.html()
                        }),
                        i && (r.crossOrigin = i),
                            r.src = e,
                            r.style.cssText = 'display:block;width:100%;height:auto;min-width:0!important;min-height:0!important;max-width:none!important;max-height:none!important;image-orientation:0deg!important;"',
                            o.html(r)
                    })
            },
            resetPreview: function() {
                this.$preview.each(function(i, e) {
                    var a = t(e)
                        , n = a.data("preview");
                    a.css({
                        width: n.width,
                        height: n.height
                    }).html(n.html).removeData("preview")
                })
            },
            preview: function() {
                var i = this.image
                    , e = this.canvas
                    , a = this.cropBox
                    , n = a.width
                    , o = a.height
                    , r = i.width
                    , s = i.height
                    , h = a.left - e.left - i.left
                    , d = a.top - e.top - i.top;
                this.cropped && !this.disabled && (this.$clone2.css({
                    width: r,
                    height: s,
                    transform: c(t.extend({
                        translateX: -h,
                        translateY: -d
                    }, i))
                }),
                    this.$preview.each(function(e, a) {
                        var l = t(a)
                            , p = l.data("preview")
                            , m = p.width
                            , f = p.height
                            , g = m
                            , u = f
                            , v = 1;
                        n && (v = m / n,
                            u = o * v),
                        o && u > f && (v = f / o,
                            g = n * v,
                            u = f),
                            l.css({
                                width: g,
                                height: u
                            }).find("img").css({
                                width: r * v,
                                height: s * v,
                                transform: c(t.extend({
                                    translateX: -h * v,
                                    translateY: -d * v
                                }, i))
                            })
                    }))
            }
        }
            , _ = {
            bind: function() {
                var i = this.$element
                    , e = this.options
                    , a = this.$cropper;
                t.isFunction(e.cropstart) && i.on("cropstart", e.cropstart),
                t.isFunction(e.cropmove) && i.on("cropmove", e.cropmove),
                t.isFunction(e.cropend) && i.on("cropend", e.cropend),
                t.isFunction(e.crop) && i.on("crop", e.crop),
                t.isFunction(e.zoom) && i.on("zoom", e.zoom),
                    a.on(k, o(this.cropStart, this)),
                e.zoomable && e.zoomOnWheel && a.on("wheel mousewheel DOMMouseScroll", o(this.wheel, this)),
                e.toggleDragModeOnDblclick && a.on("dblclick", o(this.dblclick, this)),
                    t(document).on(D, this.onCropMove = o(this.cropMove, this)).on(W, this.onCropEnd = o(this.cropEnd, this)),
                e.responsive && t(window).on("resize", this.onResize = o(this.resize, this))
            },
            unbind: function() {
                var i = this.$element
                    , e = this.options
                    , a = this.$cropper;
                t.isFunction(e.cropstart) && i.off("cropstart", e.cropstart),
                t.isFunction(e.cropmove) && i.off("cropmove", e.cropmove),
                t.isFunction(e.cropend) && i.off("cropend", e.cropend),
                t.isFunction(e.crop) && i.off("crop", e.crop),
                t.isFunction(e.zoom) && i.off("zoom", e.zoom),
                    a.off(k, this.cropStart),
                e.zoomable && e.zoomOnWheel && a.off("wheel mousewheel DOMMouseScroll", this.wheel),
                e.toggleDragModeOnDblclick && a.off("dblclick", this.dblclick),
                    t(document).off(D, this.onCropMove).off(W, this.onCropEnd),
                e.responsive && t(window).off("resize", this.onResize)
            }
        }
            , Q = {
            resize: function() {
                var i = this.options
                    , e = this.$container
                    , a = this.container
                    , n = Number(i.minContainerWidth) || 200
                    , o = Number(i.minContainerHeight) || 100;
                if (!(this.disabled || a.width <= n || a.height <= o)) {
                    var r = e.width() / a.width;
                    if (1 !== r || e.height() !== a.height) {
                        var s = void 0
                            , h = void 0;
                        i.restore && (s = this.getCanvasData(),
                            h = this.getCropBoxData()),
                            this.render(),
                        i.restore && (this.setCanvasData(t.each(s, function(t, i) {
                            s[t] = i * r
                        })),
                            this.setCropBoxData(t.each(h, function(t, i) {
                                h[t] = i * r
                            })))
                    }
                }
            },
            dblclick: function() {
                this.disabled || "none" === this.options.dragMode || this.setDragMode(this.$dragBox.hasClass("cropper-crop") ? "move" : "crop")
            },
            wheel: function(t) {
                var i = this
                    , e = t.originalEvent || t
                    , a = Number(this.options.wheelZoomRatio) || .1;
                if (!this.disabled && (t.preventDefault(),
                    !this.wheeling)) {
                    this.wheeling = !0,
                        setTimeout(function() {
                            i.wheeling = !1
                        }, 50);
                    var n = 1;
                    e.deltaY ? n = e.deltaY > 0 ? 1 : -1 : e.wheelDelta ? n = -e.wheelDelta / 120 : e.detail && (n = e.detail > 0 ? 1 : -1),
                        this.zoom(-n * a, t)
                }
            },
            cropStart: function(i) {
                if (!this.disabled) {
                    var e = this.options
                        , a = this.pointers
                        , n = i.originalEvent
                        , o = void 0;
                    n && n.changedTouches ? t.each(n.changedTouches, function(t, i) {
                        a[i.identifier] = p(i)
                    }) : a[n && n.pointerId || 0] = p(n || i),
                        o = R(a).length > 1 && e.zoomable && e.zoomOnTouch ? "zoom" : t(i.target).data("action"),
                    P.test(o) && (this.trigger("cropstart", {
                        originalEvent: n,
                        action: o
                    }).isDefaultPrevented() || (i.preventDefault(),
                        this.action = o,
                        this.cropping = !1,
                    "crop" === o && (this.cropping = !0,
                        this.$dragBox.addClass("cropper-modal"))))
                }
            },
            cropMove: function(i) {
                var e = this.action;
                if (!this.disabled && e) {
                    var a = this.pointers
                        , n = i.originalEvent;
                    i.preventDefault(),
                    this.trigger("cropmove", {
                        originalEvent: n,
                        action: e
                    }).isDefaultPrevented() || (n && n.changedTouches ? t.each(n.changedTouches, function(i, e) {
                        t.extend(a[e.identifier], p(e, !0))
                    }) : t.extend(a[n && n.pointerId || 0], p(n || i, !0)),
                        this.change(i))
                }
            },
            cropEnd: function(i) {
                if (!this.disabled) {
                    var e = this.action
                        , a = this.pointers
                        , n = i.originalEvent;
                    n && n.changedTouches ? t.each(n.changedTouches, function(t, i) {
                        delete a[i.identifier]
                    }) : delete a[n && n.pointerId || 0],
                    e && (i.preventDefault(),
                    R(a).length || (this.action = ""),
                    this.cropping && (this.cropping = !1,
                        this.$dragBox.toggleClass("cropper-modal", this.cropped && this.options.modal)),
                        this.trigger("cropend", {
                            originalEvent: n,
                            action: e
                        }))
                }
            }
        }
            , q = {
            change: function(i) {
                var e = this.options
                    , a = this.pointers
                    , n = this.container
                    , o = this.canvas
                    , r = this.cropBox
                    , s = this.action
                    , h = e.aspectRatio
                    , c = r.left
                    , d = r.top
                    , p = r.width
                    , m = r.height
                    , f = c + p
                    , g = d + m
                    , u = 0
                    , v = 0
                    , w = n.width
                    , x = n.height
                    , b = !0
                    , y = void 0;
                !h && i.shiftKey && (h = p && m ? p / m : 1),
                this.limited && (u = r.minLeft,
                    v = r.minTop,
                    w = u + Math.min(n.width, o.width, o.left + o.width),
                    x = v + Math.min(n.height, o.height, o.top + o.height));
                var C = a[R(a)[0]]
                    , $ = {
                    x: C.endX - C.startX,
                    y: C.endY - C.startY
                }
                    , M = function(t) {
                    switch (t) {
                        case "e":
                            f + $.x > w && ($.x = w - f);
                            break;
                        case "w":
                            c + $.x < u && ($.x = u - c);
                            break;
                        case "n":
                            d + $.y < v && ($.y = v - d);
                            break;
                        case "s":
                            g + $.y > x && ($.y = x - g)
                    }
                };
                switch (s) {
                    case "all":
                        c += $.x,
                            d += $.y;
                        break;
                    case "e":
                        if ($.x >= 0 && (f >= w || h && (d <= v || g >= x))) {
                            b = !1;
                            break
                        }
                        M("e"),
                            p += $.x,
                        h && (m = p / h,
                            d -= $.x / h / 2),
                        p < 0 && (s = "w",
                            p = 0);
                        break;
                    case "n":
                        if ($.y <= 0 && (d <= v || h && (c <= u || f >= w))) {
                            b = !1;
                            break
                        }
                        M("n"),
                            m -= $.y,
                            d += $.y,
                        h && (p = m * h,
                            c += $.y * h / 2),
                        m < 0 && (s = "s",
                            m = 0);
                        break;
                    case "w":
                        if ($.x <= 0 && (c <= u || h && (d <= v || g >= x))) {
                            b = !1;
                            break
                        }
                        M("w"),
                            p -= $.x,
                            c += $.x,
                        h && (m = p / h,
                            d += $.x / h / 2),
                        p < 0 && (s = "e",
                            p = 0);
                        break;
                    case "s":
                        if ($.y >= 0 && (g >= x || h && (c <= u || f >= w))) {
                            b = !1;
                            break
                        }
                        M("s"),
                            m += $.y,
                        h && (p = m * h,
                            c -= $.y * h / 2),
                        m < 0 && (s = "n",
                            m = 0);
                        break;
                    case "ne":
                        if (h) {
                            if ($.y <= 0 && (d <= v || f >= w)) {
                                b = !1;
                                break
                            }
                            M("n"),
                                m -= $.y,
                                d += $.y,
                                p = m * h
                        } else
                            M("n"),
                                M("e"),
                                $.x >= 0 ? f < w ? p += $.x : $.y <= 0 && d <= v && (b = !1) : p += $.x,
                                $.y <= 0 ? d > v && (m -= $.y,
                                    d += $.y) : (m -= $.y,
                                    d += $.y);
                        p < 0 && m < 0 ? (s = "sw",
                            m = 0,
                            p = 0) : p < 0 ? (s = "nw",
                            p = 0) : m < 0 && (s = "se",
                            m = 0);
                        break;
                    case "nw":
                        if (h) {
                            if ($.y <= 0 && (d <= v || c <= u)) {
                                b = !1;
                                break
                            }
                            M("n"),
                                m -= $.y,
                                d += $.y,
                                p = m * h,
                                c += $.y * h
                        } else
                            M("n"),
                                M("w"),
                                $.x <= 0 ? c > u ? (p -= $.x,
                                    c += $.x) : $.y <= 0 && d <= v && (b = !1) : (p -= $.x,
                                    c += $.x),
                                $.y <= 0 ? d > v && (m -= $.y,
                                    d += $.y) : (m -= $.y,
                                    d += $.y);
                        p < 0 && m < 0 ? (s = "se",
                            m = 0,
                            p = 0) : p < 0 ? (s = "ne",
                            p = 0) : m < 0 && (s = "sw",
                            m = 0);
                        break;
                    case "sw":
                        if (h) {
                            if ($.x <= 0 && (c <= u || g >= x)) {
                                b = !1;
                                break
                            }
                            M("w"),
                                p -= $.x,
                                c += $.x,
                                m = p / h
                        } else
                            M("s"),
                                M("w"),
                                $.x <= 0 ? c > u ? (p -= $.x,
                                    c += $.x) : $.y >= 0 && g >= x && (b = !1) : (p -= $.x,
                                    c += $.x),
                                $.y >= 0 ? g < x && (m += $.y) : m += $.y;
                        p < 0 && m < 0 ? (s = "ne",
                            m = 0,
                            p = 0) : p < 0 ? (s = "se",
                            p = 0) : m < 0 && (s = "nw",
                            m = 0);
                        break;
                    case "se":
                        if (h) {
                            if ($.x >= 0 && (f >= w || g >= x)) {
                                b = !1;
                                break
                            }
                            M("e"),
                                p += $.x,
                                m = p / h
                        } else
                            M("s"),
                                M("e"),
                                $.x >= 0 ? f < w ? p += $.x : $.y >= 0 && g >= x && (b = !1) : p += $.x,
                                $.y >= 0 ? g < x && (m += $.y) : m += $.y;
                        p < 0 && m < 0 ? (s = "nw",
                            m = 0,
                            p = 0) : p < 0 ? (s = "sw",
                            p = 0) : m < 0 && (s = "ne",
                            m = 0);
                        break;
                    case "move":
                        this.move($.x, $.y),
                            b = !1;
                        break;
                    case "zoom":
                        this.zoom(l(a), i.originalEvent),
                            b = !1;
                        break;
                    case "crop":
                        if (!$.x || !$.y) {
                            b = !1;
                            break
                        }
                        y = this.$cropper.offset(),
                            c = C.startX - y.left,
                            d = C.startY - y.top,
                            p = r.minWidth,
                            m = r.minHeight,
                            $.x > 0 ? s = $.y > 0 ? "se" : "ne" : $.x < 0 && (c -= p,
                                s = $.y > 0 ? "sw" : "nw"),
                        $.y < 0 && (d -= m),
                        this.cropped || (this.$cropBox.removeClass(B),
                            this.cropped = !0,
                        this.limited && this.limitCropBox(!0, !0))
                }
                b && (r.width = p,
                    r.height = m,
                    r.left = c,
                    r.top = d,
                    this.action = s,
                    this.renderCropBox()),
                    t.each(a, function(t, i) {
                        i.startX = i.endX,
                            i.startY = i.endY
                    })
            }
        }
            , G = {
            crop: function() {
                this.ready && !this.disabled && (this.cropped || (this.cropped = !0,
                    this.limitCropBox(!0, !0),
                this.options.modal && this.$dragBox.addClass("cropper-modal"),
                    this.$cropBox.removeClass(B)),
                    this.setCropBoxData(this.initialCropBox))
            },
            reset: function() {
                this.ready && !this.disabled && (this.image = t.extend({}, this.initialImage),
                    this.canvas = t.extend({}, this.initialCanvas),
                    this.cropBox = t.extend({}, this.initialCropBox),
                    this.renderCanvas(),
                this.cropped && this.renderCropBox())
            },
            clear: function() {
                this.cropped && !this.disabled && (t.extend(this.cropBox, {
                    left: 0,
                    top: 0,
                    width: 0,
                    height: 0
                }),
                    this.cropped = !1,
                    this.renderCropBox(),
                    this.limitCanvas(!0, !0),
                    this.renderCanvas(),
                    this.$dragBox.removeClass("cropper-modal"),
                    this.$cropBox.addClass(B))
            },
            replace: function(t, i) {
                !this.disabled && t && (this.isImg && this.$element.attr("src", t),
                    i ? (this.url = t,
                        this.$clone.attr("src", t),
                    this.ready && this.$preview.find("img").add(this.$clone2).attr("src", t)) : (this.isImg && (this.replaced = !0),
                        this.options.data = null,
                        this.load(t)))
            },
            enable: function() {
                this.ready && (this.disabled = !1,
                    this.$cropper.removeClass("cropper-disabled"))
            },
            disable: function() {
                this.ready && (this.disabled = !0,
                    this.$cropper.addClass("cropper-disabled"))
            },
            destroy: function() {
                var t = this.$element;
                this.loaded ? (this.isImg && this.replaced && t.attr("src", this.originalUrl),
                    this.unbuild(),
                    t.removeClass(B)) : this.isImg ? t.off("load", this.start) : this.$clone && this.$clone.remove(),
                    t.removeData("cropper")
            },
            move: function(t, i) {
                var e = this.canvas
                    , a = e.left
                    , o = e.top;
                this.moveTo(n(t) ? t : a + Number(t), n(i) ? i : o + Number(i))
            },
            moveTo: function(t, i) {
                var e = this.canvas
                    , o = !1;
                n(i) && (i = t),
                    t = Number(t),
                    i = Number(i),
                this.ready && !this.disabled && this.options.movable && (a(t) && (e.left = t,
                    o = !0),
                a(i) && (e.top = i,
                    o = !0),
                o && this.renderCanvas(!0))
            },
            zoom: function(t, i) {
                var e = this.canvas;
                t = Number(t),
                    t = t < 0 ? 1 / (1 - t) : 1 + t,
                    this.zoomTo(e.width * t / e.naturalWidth, i)
            },
            zoomTo: function(t, i) {
                var e = this.options
                    , a = this.pointers
                    , n = this.canvas
                    , o = n.width
                    , r = n.height
                    , s = n.naturalWidth
                    , h = n.naturalHeight;
                if ((t = Number(t)) >= 0 && this.ready && !this.disabled && e.zoomable) {
                    var c = s * t
                        , d = h * t
                        , l = void 0;
                    if (i && (l = i.originalEvent),
                        this.trigger("zoom", {
                            originalEvent: l,
                            oldRatio: o / s,
                            ratio: c / s
                        }).isDefaultPrevented())
                        return;
                    if (l) {
                        var p = this.$cropper.offset()
                            , f = a && R(a).length ? m(a) : {
                            pageX: i.pageX || l.pageX || 0,
                            pageY: i.pageY || l.pageY || 0
                        };
                        n.left -= (c - o) * ((f.pageX - p.left - n.left) / o),
                            n.top -= (d - r) * ((f.pageY - p.top - n.top) / r)
                    } else
                        n.left -= (c - o) / 2,
                            n.top -= (d - r) / 2;
                    n.width = c,
                        n.height = d,
                        this.renderCanvas(!0)
                }
            },
            rotate: function(t) {
                this.rotateTo((this.image.rotate || 0) + Number(t))
            },
            rotateTo: function(t) {
                t = Number(t),
                a(t) && this.ready && !this.disabled && this.options.rotatable && (this.image.rotate = t % 360,
                    this.renderCanvas(!0, !0))
            },
            scaleX: function(t) {
                var i = this.image.scaleY;
                this.scale(t, a(i) ? i : 1)
            },
            scaleY: function(t) {
                var i = this.image.scaleX;
                this.scale(a(i) ? i : 1, t)
            },
            scale: function(t) {
                var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : t
                    , e = this.image
                    , n = !1;
                t = Number(t),
                    i = Number(i),
                this.ready && !this.disabled && this.options.scalable && (a(t) && (e.scaleX = t,
                    n = !0),
                a(i) && (e.scaleY = i,
                    n = !0),
                n && this.renderCanvas(!0, !0))
            },
            getData: function() {
                var i = arguments.length > 0 && void 0 !== arguments[0] && arguments[0]
                    , e = this.options
                    , a = this.image
                    , n = this.canvas
                    , o = this.cropBox
                    , r = void 0;
                if (this.ready && this.cropped) {
                    r = {
                        x: o.left - n.left,
                        y: o.top - n.top,
                        width: o.width,
                        height: o.height
                    };
                    var s = a.width / a.naturalWidth;
                    t.each(r, function(t, e) {
                        e /= s,
                            r[t] = i ? Math.round(e) : e
                    })
                } else
                    r = {
                        x: 0,
                        y: 0,
                        width: 0,
                        height: 0
                    };
                return e.rotatable && (r.rotate = a.rotate || 0),
                e.scalable && (r.scaleX = a.scaleX || 1,
                    r.scaleY = a.scaleY || 1),
                    r
            },
            setData: function(i) {
                var e = this.options
                    , n = this.image
                    , o = this.canvas
                    , r = {};
                if (t.isFunction(i) && (i = i.call(this.element)),
                this.ready && !this.disabled && t.isPlainObject(i)) {
                    var s = !1;
                    e.rotatable && a(i.rotate) && i.rotate !== n.rotate && (n.rotate = i.rotate,
                        s = !0),
                    e.scalable && (a(i.scaleX) && i.scaleX !== n.scaleX && (n.scaleX = i.scaleX,
                        s = !0),
                    a(i.scaleY) && i.scaleY !== n.scaleY && (n.scaleY = i.scaleY,
                        s = !0)),
                    s && this.renderCanvas(!0, !0);
                    var h = n.width / n.naturalWidth;
                    a(i.x) && (r.left = i.x * h + o.left),
                    a(i.y) && (r.top = i.y * h + o.top),
                    a(i.width) && (r.width = i.width * h),
                    a(i.height) && (r.height = i.height * h),
                        this.setCropBoxData(r)
                }
            },
            getContainerData: function() {
                return this.ready ? t.extend({}, this.container) : {}
            },
            getImageData: function() {
                return this.loaded ? t.extend({}, this.image) : {}
            },
            getCanvasData: function() {
                var i = this.canvas
                    , e = {};
                return this.ready && t.each(["left", "top", "width", "height", "naturalWidth", "naturalHeight"], function(t, a) {
                    e[a] = i[a]
                }),
                    e
            },
            setCanvasData: function(i) {
                var e = this.canvas
                    , n = e.aspectRatio;
                t.isFunction(i) && (i = i.call(this.$element)),
                this.ready && !this.disabled && t.isPlainObject(i) && (a(i.left) && (e.left = i.left),
                a(i.top) && (e.top = i.top),
                    a(i.width) ? (e.width = i.width,
                        e.height = i.width / n) : a(i.height) && (e.height = i.height,
                        e.width = i.height * n),
                    this.renderCanvas(!0))
            },
            getCropBoxData: function() {
                var t = this.cropBox;
                return this.ready && this.cropped ? {
                    left: t.left,
                    top: t.top,
                    width: t.width,
                    height: t.height
                } : {}
            },
            setCropBoxData: function(i) {
                var e = this.cropBox
                    , n = this.options.aspectRatio
                    , o = void 0
                    , r = void 0;
                t.isFunction(i) && (i = i.call(this.$element)),
                this.ready && this.cropped && !this.disabled && t.isPlainObject(i) && (a(i.left) && (e.left = i.left),
                a(i.top) && (e.top = i.top),
                a(i.width) && i.width !== e.width && (o = !0,
                    e.width = i.width),
                a(i.height) && i.height !== e.height && (r = !0,
                    e.height = i.height),
                n && (o ? e.height = e.width / n : r && (e.width = e.height * n)),
                    this.renderCropBox())
            },
            getCroppedCanvas: function() {
                var i = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                if (!this.ready || !window.HTMLCanvasElement)
                    return null;
                var e = this.canvas
                    , a = u(this.$clone[0], this.image, e, i);
                if (!this.cropped)
                    return a;
                var n = this.getData()
                    , o = n.x
                    , s = n.y
                    , h = n.width
                    , c = n.height
                    , d = h / c
                    , l = f({
                    aspectRatio: d,
                    width: i.maxWidth || 1 / 0,
                    height: i.maxHeight || 1 / 0
                })
                    , p = f({
                    aspectRatio: d,
                    width: i.minWidth || 0,
                    height: i.minHeight || 0
                })
                    , m = f({
                    aspectRatio: d,
                    width: i.width || h,
                    height: i.height || c
                })
                    , g = m.width
                    , v = m.height;
                g = Math.min(l.width, Math.max(p.width, g)),
                    v = Math.min(l.height, Math.max(p.height, v));
                var w = document.createElement("canvas")
                    , x = w.getContext("2d");
                w.width = r(g),
                    w.height = r(v),
                    x.fillStyle = i.fillColor || "transparent",
                    x.fillRect(0, 0, g, v);
                var b = i.imageSmoothingEnabled
                    , y = void 0 === b || b
                    , $ = i.imageSmoothingQuality;
                x.imageSmoothingEnabled = y,
                $ && (x.imageSmoothingQuality = $);
                var M = a.width
                    , B = a.height
                    , k = o
                    , D = s
                    , W = void 0
                    , P = void 0
                    , T = void 0
                    , Y = void 0
                    , H = void 0
                    , X = void 0;
                k <= -h || k > M ? (k = 0,
                    W = 0,
                    T = 0,
                    H = 0) : k <= 0 ? (T = -k,
                    k = 0,
                    W = Math.min(M, h + k),
                    H = W) : k <= M && (T = 0,
                    W = Math.min(h, M - k),
                    H = W),
                    W <= 0 || D <= -c || D > B ? (D = 0,
                        P = 0,
                        Y = 0,
                        X = 0) : D <= 0 ? (Y = -D,
                        D = 0,
                        P = Math.min(B, c + D),
                        X = P) : D <= B && (Y = 0,
                        P = Math.min(c, B - D),
                        X = P);
                var j = [k, D, W, P];
                if (H > 0 && X > 0) {
                    var R = g / h;
                    j.push(T * R, Y * R, H * R, X * R)
                }
                return x.drawImage.apply(x, [a].concat(C(t.map(j, function(t) {
                    return Math.floor(r(t))
                })))),
                    w
            },
            setAspectRatio: function(t) {
                var i = this.options;
                this.disabled || n(t) || (i.aspectRatio = Math.max(0, t) || NaN,
                this.ready && (this.initCropBox(),
                this.cropped && this.renderCropBox()))
            },
            setDragMode: function(t) {
                var i = this.options
                    , e = void 0
                    , a = void 0;
                this.loaded && !this.disabled && (e = "crop" === t,
                    a = i.movable && "move" === t,
                    t = e || a ? t : "none",
                    this.$dragBox.data("action", t).toggleClass("cropper-crop", e).toggleClass("cropper-move", a),
                i.cropBoxMovable || this.$face.data("action", t).toggleClass("cropper-crop", e).toggleClass("cropper-move", a))
            }
        }
            , K = function() {
            function t(t, i) {
                for (var e = 0; e < i.length; e++) {
                    var a = i[e];
                    a.enumerable = a.enumerable || !1,
                        a.configurable = !0,
                    "value"in a && (a.writable = !0),
                        Object.defineProperty(t, a.key, a)
                }
            }
            return function(i, e, a) {
                return e && t(i.prototype, e),
                a && t(i, a),
                    i
            }
        }()
            , Z = function() {
            function i(e) {
                var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                if ($(this, i),
                !e || !H.test(e.tagName))
                    throw new Error("The first argument is required and must be an <img> or <canvas> element.");
                this.element = e,
                    this.$element = t(e),
                    this.options = t.extend({}, X, t.isPlainObject(a) && a),
                    this.completed = !1,
                    this.cropped = !1,
                    this.disabled = !1,
                    this.isImg = !1,
                    this.limited = !1,
                    this.loaded = !1,
                    this.ready = !1,
                    this.replaced = !1,
                    this.wheeling = !1,
                    this.originalUrl = "",
                    this.canvas = null,
                    this.cropBox = null,
                    this.pointers = {},
                    this.init()
            }
            return K(i, [{
                key: "init",
                value: function() {
                    var t = this.$element
                        , i = void 0;
                    if (t.is("img")) {
                        if (this.isImg = !0,
                            i = t.attr("src") || "",
                            this.originalUrl = i,
                            !i)
                            return;
                        i = t.prop("src")
                    } else
                        t.is("canvas") && window.HTMLCanvasElement && (i = t[0].toDataURL());
                    this.load(i)
                }
            }, {
                key: "trigger",
                value: function(i, e) {
                    var a = t.Event(i, e);
                    return this.$element.trigger(a),
                        a
                }
            }, {
                key: "load",
                value: function(t) {
                    var i = this;
                    if (t) {
                        this.url = t,
                            this.image = {};
                        var e = this.$element
                            , a = this.options;
                        if (!a.checkOrientation || !window.ArrayBuffer)
                            return void this.clone();
                        if (T.test(t))
                            return void (Y.test(t) ? this.read(w(t)) : this.clone());
                        var n = new XMLHttpRequest;
                        n.onerror = function() {
                            i.clone()
                        }
                            ,
                            n.onload = function() {
                                i.read(n.response)
                            }
                            ,
                        a.checkCrossOrigin && s(t) && e.prop("crossOrigin") && (t = h(t)),
                            n.open("get", t),
                            n.responseType = "arraybuffer",
                            n.withCredentials = "use-credentials" === e.prop("crossOrigin"),
                            n.send()
                    }
                }
            }, {
                key: "read",
                value: function(t) {
                    var i = this.options
                        , e = this.image
                        , a = b(t)
                        , n = 0
                        , o = 1
                        , r = 1;
                    if (a > 1) {
                        this.url = x(t, "image/jpeg");
                        var s = y(a);
                        n = s.rotate,
                            o = s.scaleX,
                            r = s.scaleY
                    }
                    i.rotatable && (e.rotate = n),
                    i.scalable && (e.scaleX = o,
                        e.scaleY = r),
                        this.clone()
                }
            }, {
                key: "clone",
                value: function() {
                    var i = this.$element
                        , e = this.options
                        , a = this.url
                        , n = ""
                        , o = void 0;
                    e.checkCrossOrigin && s(a) && (n = i.prop("crossOrigin"),
                        n ? o = a : (n = "anonymous",
                            o = h(a))),
                        this.crossOrigin = n,
                        this.crossOriginUrl = o;
                    var r = document.createElement("img");
                    n && (r.crossOrigin = n),
                        r.src = o || a;
                    var c = t(r);
                    this.$clone = c,
                        this.isImg ? this.element.complete ? this.start() : i.one("load", t.proxy(this.start, this)) : c.one("load", t.proxy(this.start, this)).one("error", t.proxy(this.stop, this)).addClass("cropper-hide").insertAfter(i)
                }
            }, {
                key: "start",
                value: function() {
                    var i = this
                        , e = this.$clone
                        , a = this.$element;
                    this.isImg || (e.off("error", this.stop),
                        a = e),
                        d(a[0], function(e, a) {
                            t.extend(i.image, {
                                naturalWidth: e,
                                naturalHeight: a,
                                aspectRatio: e / a
                            }),
                                i.loaded = !0,
                                i.build()
                        })
                }
            }, {
                key: "stop",
                value: function() {
                    this.$clone.remove(),
                        this.$clone = null
                }
            }, {
                key: "build",
                value: function() {
                    var i = this;
                    if (this.loaded) {
                        this.ready && this.unbuild();
                        var e = this.$element
                            , a = this.options
                            , n = this.$clone
                            , o = t('<div class="cropper-container"><div class="cropper-wrap-box"><div class="cropper-canvas"></div></div><div class="cropper-drag-box"></div><div class="cropper-crop-box"><span class="cropper-view-box"></span><span class="cropper-dashed dashed-h"></span><span class="cropper-dashed dashed-v"></span><span class="cropper-center"></span><span class="cropper-face"></span><span class="cropper-line line-e" data-action="e"></span><span class="cropper-line line-n" data-action="n"></span><span class="cropper-line line-w" data-action="w"></span><span class="cropper-line line-s" data-action="s"></span><span class="cropper-point point-e" data-action="e"></span><span class="cropper-point point-n" data-action="n"></span><span class="cropper-point point-w" data-action="w"></span><span class="cropper-point point-s" data-action="s"></span><span class="cropper-point point-ne" data-action="ne"></span><span class="cropper-point point-nw" data-action="nw"></span><span class="cropper-point point-sw" data-action="sw"></span><span class="cropper-point point-se" data-action="se"></span></div></div>')
                            , r = o.find(".cropper-crop-box")
                            , s = r.find(".cropper-face");
                        this.$container = e.parent(),
                            this.$cropper = o,
                            this.$canvas = o.find(".cropper-canvas").append(n),
                            this.$dragBox = o.find(".cropper-drag-box"),
                            this.$cropBox = r,
                            this.$viewBox = o.find(".cropper-view-box"),
                            this.$face = s,
                            e.addClass(B).after(o),
                        this.isImg || n.removeClass("cropper-hide"),
                            this.initPreview(),
                            this.bind(),
                            a.aspectRatio = Math.max(0, a.aspectRatio) || NaN,
                            a.viewMode = Math.max(0, Math.min(3, Math.round(a.viewMode))) || 0,
                            this.cropped = a.autoCrop,
                            a.autoCrop ? a.modal && this.$dragBox.addClass("cropper-modal") : r.addClass(B),
                        a.guides || r.find(".cropper-dashed").addClass(B),
                        a.center || r.find(".cropper-center").addClass(B),
                        a.cropBoxMovable && s.addClass("cropper-move").data("action", "all"),
                        a.highlight || s.addClass("cropper-invisible"),
                        a.background && o.addClass("cropper-bg"),
                        a.cropBoxResizable || r.find(".cropper-line,.cropper-point").addClass(B),
                            this.setDragMode(a.dragMode),
                            this.render(),
                            this.ready = !0,
                            this.setData(a.data),
                            this.completing = setTimeout(function() {
                                t.isFunction(a.ready) && e.one("ready", a.ready),
                                    i.trigger("ready"),
                                    i.trigger("crop", i.getData()),
                                    i.completed = !0
                            }, 0)
                    }
                }
            }, {
                key: "unbuild",
                value: function() {
                    this.ready && (this.completed || clearTimeout(this.completing),
                        this.ready = !1,
                        this.completed = !1,
                        this.initialImage = null,
                        this.initialCanvas = null,
                        this.initialCropBox = null,
                        this.container = null,
                        this.canvas = null,
                        this.cropBox = null,
                        this.unbind(),
                        this.resetPreview(),
                        this.$preview = null,
                        this.$viewBox = null,
                        this.$cropBox = null,
                        this.$dragBox = null,
                        this.$canvas = null,
                        this.$container = null,
                        this.$cropper.remove(),
                        this.$cropper = null)
                }
            }], [{
                key: "setDefaults",
                value: function(i) {
                    t.extend(X, t.isPlainObject(i) && i)
                }
            }]),
                i
        }();
        if (t.extend && t.extend(Z.prototype, F, S, _, Q, q, G),
            t.fn) {
            var J = t.fn.cropper;
            t.fn.cropper = function(i) {
                for (var a = arguments.length, o = Array(a > 1 ? a - 1 : 0), r = 1; r < a; r++)
                    o[r - 1] = arguments[r];
                var s = void 0;
                return this.each(function(a, n) {
                    var r = t(n)
                        , h = r.data("cropper");
                    if (!h) {
                        if (/destroy/.test(i))
                            return;
                        var c = t.extend({}, r.data(), t.isPlainObject(i) && i);
                        h = new Z(n,c),
                            r.data("cropper", h)
                    }
                    if (e(i)) {
                        var d = h[i];
                        t.isFunction(d) && (s = d.apply(h, o))
                    }
                }),
                    n(s) ? this : s
            }
                ,
                t.fn.cropper.Constructor = Z,
                t.fn.cropper.setDefaults = Z.setDefaults,
                t.fn.cropper.noConflict = function() {
                    return t.fn.cropper = J,
                        this
                }
        }
    }(t.jQuery)
}(this),
    define("cropper", function() {}),
    yp.use("cropper", function(t) {
        t.ready(function() {
            t.loader.loadCSS("../j/lib/cropper/cropper.min"),
                {
                    _ui: {
                        $coverDom: $("#js-cover-dom"),
                        $coverBtn: $("#js-cover-btn"),
                        $coverStatus: $("#js-cover-status"),
                        $coverPanal: $("#js-cover-panal"),
                        $coverImg: $("#js-cover-img"),
                        $radioUsed: $("#js-cover-radio-used"),
                        $radioNoUse: $("#js-cover-radio-nouse"),
                        $close: $(".js-cover-close"),
                        $inputCover: $("#js-input-cover")
                    },
                    apis: ["/api/anchor/anchor.upload", "/api/anchor/cover.change", "/api/anchor/cover.info", "/api/anchor/cover.reset"],
                    init: function() {
                        this.view(),
                            this.bindEvent(),
                            this.listenYpEvent()
                    },
                    view: function() {
                        var t = this;
                        t._ui;
                        t.isUpload()
                    },
                    listenYpEvent: function() {
                        var i = this
                            , e = i._ui;
                        t.sub("page/gameselect", function(t, i) {
                            if (1 != i.level)
                                return !1;
                            e.$coverDom.toggleClass("hidden", 65 != i.gameId)
                        })
                    },
                    bindEvent: function() {
                        var t = this
                            , i = t._ui;
                        i.$close.on("click", function(e) {
                            t.initPanal(),
                                i.$coverPanal.hide()
                        }),
                            i.$coverBtn.on("click", function(t) {
                                t.preventDefault(),
                                    i.$coverPanal.show()
                            }),
                            i.$coverPanal.on("change", "input", function() {
                                var e = $(this)
                                    , a = e[0].files[0]
                                    , n = "";
                                i.$coverPanal.find(".ibox-define-btn").off(),
                                    "image/png" != a.type && "image/jpeg" != a.type ? (e.val(""),
                                        i.$coverPanal.find(".titles").text("上传失败，图片格式错误，请上传JPG、PNG格式图片")) : Math.floor(a.size / 1024) > 300 ? (e.val(""),
                                        i.$coverPanal.find(".titles").text("上传失败，请检查图片大小是否超过300k")) : (i.$coverPanal.find(".titles").removeClass("loading"),
                                        i.$coverPanal.find(".titles").text(""),
                                    t.oFReader || (t.oFReader = new FileReader,
                                            t.oFReader.onload = function(t) {
                                                n = t.target.result,
                                                    i.$coverPanal.find("#js-left-view").cropper("destroy"),
                                                    i.$coverPanal.find("#js-left-view").attr("src", n),
                                                    i.$coverPanal.find("#js-left-view").cropper({
                                                        viewMode: 1,
                                                        aspectRatio: 16 / 9,
                                                        preview: $("#js-right-view"),
                                                        crop: function(t) {}
                                                    })
                                            }
                                    ),
                                        i.$coverPanal.find(".ibox-define-btn").off().on("click", function(e) {
                                            if (e.preventDefault(),
                                                $(this).data("ajax"))
                                                return !1;
                                            var a = i.$coverPanal.find("#js-left-view").cropper("getCroppedCanvas", {
                                                width: 520,
                                                height: 293
                                            }).toDataURL("image/jpeg");
                                            t.postUrl($(this), a)
                                        }),
                                        t.oFReader.readAsDataURL(a))
                            }),
                            i.$inputCover.on("click", ":radio", function(i) {
                                t.initCover($(this).data("id"), $(this))
                            })
                    },
                    initPanal: function() {
                        var t = this
                            , i = t._ui;
                        i.$coverPanal.find("#js-left-view").cropper("destroy"),
                            i.$coverPanal.find("img").attr("src", ""),
                            i.$coverPanal.find("input").val(""),
                            i.$coverPanal.find(".titles").text(""),
                            i.$coverPanal.find(".titles").addClass("loading")
                    },
                    initCover: function(i, e) {
                        var a = this
                            , n = a._ui;
                        t.ajax(a.apis[3], {
                            noCache: !0,
                            data: {
                                isUsed: i
                            }
                        }).done(function(i) {
                            0 != i.code && (t.errorNotify(i.message),
                                e.attr("checked", !1))
                        }).fail(function() {
                            t.errorNotify("网络异常，请稍后重试"),
                                e.attr("checked", !1)
                        }).always(function() {
                            n.$inputCover.data("ajax", !1)
                        })
                    },
                    postUrl: function(i, e) {
                        var a = this
                            , n = a._ui;
                        t.ajax(a.apis[0], {
                            noCache: !0,
                            type: "post",
                            data: {
                                type: "anchor_cover",
                                fmt: "base64",
                                picdata: e
                            }
                        }).done(function(e) {
                            e.code ? t.errorNotify(e.message) : t.ajax(a.apis[1], {
                                noCache: !0,
                                type: "post",
                                data: {
                                    coverimg: e.data.file
                                }
                            }).done(function(t) {
                                t.code || (n.$coverPanal.hide(),
                                    n.$coverImg.find("img").attr("src", e.data.file),
                                    n.$coverImg.find("i").show(),
                                    n.$coverImg.find(".title").show(),
                                    n.$coverImg.find("#js-cover-btn").text("重新上传"))
                            }).fail(function() {
                                t.errorNotify("网络异常，请稍后重试~")
                            }).always(function() {
                                i.data("ajax", !1)
                            })
                        }).fail(function() {
                            t.errorNotify("网络异常，请稍后重试~"),
                                i.data("ajax", !1)
                        })
                    },
                    isUpload: function() {
                        var i = this
                            , e = i._ui
                            , a = 0;
                        t.ajax(i.apis[2], {
                            noCache: !0
                        }).done(function(t) {
                            t.code || -1 != (a = t.data.status) && (e.$coverImg.find("img").attr("src", t.data.coverimg),
                            1 == t.data.isUsed && e.$radioUsed.prop("checked", !0),
                            0 == a && e.$coverImg.find(".title").show(),
                                e.$coverImg.find("#js-cover-btn").text("重新上传"),
                                1 == a ? (e.$coverStatus.addClass("success-title"),
                                    e.$coverStatus.find("span").text("封面审核已通过")) : 2 == a && e.$coverStatus.find("span").text("封面审核未通过，请重新上传"))
                        })
                    }
                }.init()
        })
    }, !0),
    define("j/web4.0/usercenter/anchor/cover", function() {});
