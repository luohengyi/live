yp.use("editor, zqSelect, jquery.gameSelect", function() {
    yp.ready(function() {
        var t = yp.ui;
        t.$yp_form = $("#yp-form"),
            t.$yp_live_title = $("#yp_input_live_title"),
            t.$game_select = $("#js-game-select"),
            {
                init: function() {
                    this.view(),
                        this.bindEvent()
                },
                view: function() {
                    t.$yp_live_title_p = t.$yp_live_title.closest(".form-control-wrapper"),
                        t.$yp_live_title_p.popover(),
                        this.afficheEditor = KindEditor.create("#afficheTextarea", {
                            items: ["fontsize", "forecolor", "bold", "|", "image"],
                            minWidth: 500,
                            minHeight: 350,
                            resizeType: 2,
                            uploadJson: oPageConfig.oUrl.editorUploadUrl,
                            allowImageRemote: !1,
                            filterMode: !0,
                            htmlTags: {
                                span: [".color", ".font-size", ".font-weight"],
                                img: ["src", "title"],
                                br: []
                            },
                            newlineTag: "br",
                            pasteType: 1,
                            colorTable: oPageConfig.aSettingColor ? [oPageConfig.aSettingColor] : [["#12b7f5", "#555555"]],
                            fontSizeTable: ["12px", "14px"]
                        }),
                        t.$game_select.gameSelect({
                            postName: "gameId",
                            aGameList: oPageConfig.aGameList,
                            selectedId: oPageConfig.gameSelected,
                            useZqSelect: !0
                        })
                },
                bindEvent: function() {
                    var e = this;
                    t.$yp_form.on("submit", function(i) {
                        i.preventDefault(),
                            e.fSubmitForm(t.$yp_form)
                    }).on("focus", "input", function() {
                        switch ($(this).data("type")) {
                            case "title":
                                t.$yp_live_title_p.popover("hide")
                        }
                    }).on("change", "input", function() {
                        var i, o = $(this), a = o.data("type"), n = o.closest("form"), l = null;
                        switch (a) {
                            case "title":
                                t.$yp_live_title_p.popover("hide"),
                                    i = e.checkTitleFormat(o.val()),
                                    n.data("isok", i.isok && n.data("isok")),
                                    l = t.$yp_live_title_p;
                                break;
                            default:
                                i = {
                                    isok: !0
                                }
                        }
                        i.isok || (l.attr("data-content", i.tip),
                            l.popover("show"))
                    })
                },
                checkTitleFormat: function(t) {
                    var e = ["请输入直播标题", "仅限15~60位的中英文，每个中文字算3位字符", ""]
                        , i = this
                        , o = i.fGetStrLen(t);
                    return "" == t ? {
                        isok: !1,
                        tip: e[0]
                    } : 15 > o || 60 < o ? {
                        isok: !1,
                        tip: e[1]
                    } : {
                        isok: !0,
                        tip: e[2]
                    }
                },
                fSubmitForm: function(e) {
                    var i = this
                        , o = [];
                    e.data("isok", !0),
                        e.find("[data-type]").each(function(a, n) {
                            var l, r = $(n).data("type"), p = null;
                            switch (r) {
                                case "title":
                                    l = i.checkTitleFormat($(n).val()),
                                        e.data("isok", l.isok && e.data("isok")),
                                        p = t.$yp_live_title_p
                            }
                            l.isok || (o.push("title" == r ? "标题" + l.tip : l.tip),
                                p.attr("data-content", l.tip),
                                p.popover("show"))
                        }),
                    o.length && yp.errorNotify(o.join("<br />")),
                    !e.data("ing") && e.data("isok") && (e.data("ing", !0),
                        i.afficheEditor.sync(),
                        e.ajaxSubmit().ajax.done(function(t) {
                            yp.ajaxNotice(t)
                        }).always(function() {
                            e.data("ing", !1)
                        }))
                },
                fGetStrLen: function(t) {
                    for (var e = 0, i = t.split(""), o = 0; o < i.length; o++)
                        i[o].charCodeAt(0) < 299 ? e++ : e += 3;
                    return e
                }
            }.init()
    })
}),
    define("j/web4.0/usercenter/anchor/live-set", function() {});
