+function() {
    var e = {
        zqIM: null,
        _ui: {},
        init: function(e, t) {
            var n = this;
            window.WSDK = t,
                e.init(),
                n.zqIM = e,
                n.view(),
                n.bindEvent(),
                n.fListenYpEvent(),
                n.fUpdateRedPoint()
        },
        view: function() {
            var e = this
                , t = e._ui;
            t.$guide_tips_one = $("#js-im-guide-topbar-one"),
                t.$guide_tips_two = $("#js-im-guide-topbar-two"),
                t.$go_im = $("#userInfo-panel .js-topbar-go-im")
        },
        bindEvent: function() {
            var e = this;
            e._ui.$go_im.on("click", function(t) {
                0 == (2 & (yp.localStorage.getItem("im_guide_topbar") || 0)) && (e.fHideGuide("two"),
                    e.fSetGuideCookie("two"))
            })
        },
        fListenYpEvent: function() {
            var e = this;
            yp.sub("page/login/done", function(t) {
                e.fUpdateRedPoint(),
                    e.fShowGuide()
            }),
                yp.sub("page/logout/done", function(t) {
                    e.fUpdateRedPoint(),
                    e.zqIM.bLogined && e.zqIM.fIMLogout(),
                        e.fHideGuide()
                }),
                yp.sub("page/regist/done", function() {
                    e.fUpdateRedPoint()
                }),
                yp.sub("page/zqim/get", function(t, n) {
                    switch (n.type) {
                        case "login":
                            e.zqIM.fGetUnread({
                                count: 1
                            });
                            break;
                        case "unread":
                            e.fGetUnread(n.data);
                            break;
                        case "listenall":
                            e.fGetNewMsg(n)
                    }
                }),
                yp.sub("page/topbar/navhover", function(t, n) {
                    if ("userinfo" == n.type) {
                        0 == (1 & (yp.localStorage.getItem("im_guide_topbar") || 0)) && (e.fHideGuide("one"),
                            e.fSetGuideCookie("one"))
                    }
                })
        },
        fUpdateRedPoint: function() {
            var e = this;
            if (!oPageConfig.oLoginInfo || e.fShowGuide())
                return yp.pub("page/zqim/unread", {
                    cnt: 0,
                    showRedPoint: !1
                }),
                    !1;
            e.fGetBBAndIACnt()
        },
        fGetBBAndIACnt: function() {
            var e = this;
            e.zqIM.fGetBBAndIAUnRead().done(function(t) {
                if (0 == t.code) {
                    var n = 0;
                    t.data.qiniang && 0 < t.data.qiniang.newCount ? yp.pub("page/zqim/unread", {
                        showRedPoint: !0
                    }) : e.fLoginIm(),
                    t.data.comment && 0 < t.data.comment.newCount && (n += parseInt(t.data.comment.newCount)),
                    t.data.awesome && 0 < t.data.awesome.newCount && (n += parseInt(t.data.awesome.newCount)),
                        yp.pub("page/zqim/unread", {
                            cnt: n
                        })
                }
            })
        },
        fLoginIm: function() {
            this.zqIM.fNewWSDK(),
                yp.pub("page/zqim/login")
        },
        fGetUnread: function(e) {
            var t = this
                , n = 0;
            0 < e.length && yp.each(e, function(e) {
                -1 == e.contact.indexOf("chntribe") && (n += e.msgCount)
            }),
                0 < n ? (yp.pub("page/zqim/unread", {
                    showRedPoint: !0
                }),
                    t.zqIM.fIMLogout()) : t.zqIM.fStartListenAllMsg()
        },
        fGetNewMsg: function(e) {
            var t = this;
            yp.pub("page/zqim/unread", {
                showRedPoint: !0
            }),
                t.zqIM.fIMLogout()
        },
        fShowGuide: function() {
            var e = this
                , t = e._ui;
            if (!oPageConfig.oLoginInfo)
                return !1;
            var n = yp.localStorage.getItem("im_guide_topbar") || 0;
            return 0 == (1 & n) && t.$guide_tips_one.show(),
            0 == (2 & n) && t.$guide_tips_two.show(),
            n < 3 && n >= 0
        },
        fHideGuide: function(e) {
            var t = this
                , n = t._ui;
            "two" != e && n.$guide_tips_one.hide(),
            "one" != e && n.$guide_tips_two.hide()
        },
        fSetGuideCookie: function(e) {
            var t = this
                , n = yp.localStorage.getItem("im_guide_topbar") || 0;
            n |= "one" == e ? 1 : 2,
                yp.localStorage.setItem("im_guide_topbar", n),
                t.fUpdateRedPoint()
        }
    };
    "undefined" != typeof module ? module.exports = e : "function" == typeof define && define.amd ? define([], function() {
        return e
    }) : window.oSimpleIm = e
}();
