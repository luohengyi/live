var requirejs, require, define;
!function(global, setTimeout) {
    function commentReplace(e, t) {
        return t || ""
    }
    function isFunction(e) {
        return "[object Function]" === ostring.call(e)
    }
    function isArray(e) {
        return "[object Array]" === ostring.call(e)
    }
    function each(e, t) {
        if (e) {
            var n;
            for (n = 0; n < e.length && (!e[n] || !t(e[n], n, e)); n += 1)
                ;
        }
    }
    function eachReverse(e, t) {
        if (e) {
            var n;
            for (n = e.length - 1; n > -1 && (!e[n] || !t(e[n], n, e)); n -= 1)
                ;
        }
    }
    function hasProp(e, t) {
        return hasOwn.call(e, t)
    }
    function getOwn(e, t) {
        return hasProp(e, t) && e[t]
    }
    function eachProp(e, t) {
        var n;
        for (n in e)
            if (hasProp(e, n) && t(e[n], n))
                break
    }
    function mixin(e, t, n, i) {
        return t && eachProp(t, function(t, o) {
            (n || !hasProp(e, o)) && (!i || "object" != typeof t || !t || isArray(t) || isFunction(t) || t instanceof RegExp ? e[o] = t : (e[o] || (e[o] = {}),
                mixin(e[o], t, n, i)))
        }),
            e
    }
    function bind(e, t) {
        return function() {
            return t.apply(e, arguments)
        }
    }
    function scripts() {
        return document.getElementsByTagName("script")
    }
    function defaultOnError(e) {
        throw e
    }
    function getGlobal(e) {
        if (!e)
            return e;
        var t = global;
        return each(e.split("."), function(e) {
            t = t[e]
        }),
            t
    }
    function makeError(e, t, n, i) {
        var o = new Error(t + "\nhttp://requirejs.org/docs/errors.html#" + e);
        return o.requireType = e,
            o.requireModules = i,
        n && (o.originalError = n),
            o
    }
    function newContext(e) {
        function t(e) {
            var t, n;
            for (t = 0; t < e.length; t++)
                if ("." === (n = e[t]))
                    e.splice(t, 1),
                        t -= 1;
                else if (".." === n) {
                    if (0 === t || 1 === t && ".." === e[2] || ".." === e[t - 1])
                        continue;
                    t > 0 && (e.splice(t - 1, 2),
                        t -= 2)
                }
        }
        function n(e, n, i) {
            var o, a, r, s, c, l, u, d, f, p, h, m = n && n.split("/"), g = C.map, v = g && g["*"];
            if (e && (e = e.split("/"),
                l = e.length - 1,
            C.nodeIdCompat && jsSuffixRegExp.test(e[l]) && (e[l] = e[l].replace(jsSuffixRegExp, "")),
            "." === e[0].charAt(0) && m && (h = m.slice(0, m.length - 1),
                e = h.concat(e)),
                t(e),
                e = e.join("/")),
            i && g && (m || v)) {
                a = e.split("/");
                e: for (r = a.length; r > 0; r -= 1) {
                    if (c = a.slice(0, r).join("/"),
                        m)
                        for (s = m.length; s > 0; s -= 1)
                            if ((o = getOwn(g, m.slice(0, s).join("/"))) && (o = getOwn(o, c))) {
                                u = o,
                                    d = r;
                                break e
                            }
                    !f && v && getOwn(v, c) && (f = getOwn(v, c),
                        p = r)
                }
                !u && f && (u = f,
                    d = p),
                u && (a.splice(0, d, u),
                    e = a.join("/"))
            }
            return getOwn(C.pkgs, e) || e
        }
        function i(e) {
            isBrowser && each(scripts(), function(t) {
                return t.getAttribute("data-requiremodule") === e && t.getAttribute("data-requirecontext") === x.contextName ? (t.parentNode.removeChild(t),
                    !0) : void 0
            })
        }
        function o(e) {
            var t = getOwn(C.paths, e);
            return t && isArray(t) && t.length > 1 ? (t.shift(),
                x.require.undef(e),
                x.makeRequire(null, {
                    skipMap: !0
                })([e]),
                !0) : void 0
        }
        function a(e) {
            var t, n = e ? e.indexOf("!") : -1;
            return n > -1 && (t = e.substring(0, n),
                e = e.substring(n + 1, e.length)),
                [t, e]
        }
        function r(e, t, i, o) {
            var r, s, c, l, u = null, d = t ? t.name : null, f = e, p = !0, h = "";
            return e || (p = !1,
                e = "_@r" + (D += 1)),
                l = a(e),
                u = l[0],
                e = l[1],
            u && (u = n(u, d, o),
                s = getOwn(_, u)),
            e && (u ? h = i ? e : s && s.normalize ? s.normalize(e, function(e) {
                return n(e, d, o)
            }) : -1 === e.indexOf("!") ? n(e, d, o) : e : (h = n(e, d, o),
                l = a(h),
                u = l[0],
                h = l[1],
                i = !0,
                r = x.nameToUrl(h))),
                c = !u || s || i ? "" : "_unnormalized" + (A += 1),
                {
                    prefix: u,
                    name: h,
                    parentMap: t,
                    unnormalized: !!c,
                    url: r,
                    originalName: f,
                    isDefine: p,
                    id: (u ? u + "!" + h : h) + c
                }
        }
        function s(e) {
            var t = e.id
                , n = getOwn(k, t);
            return n || (n = k[t] = new x.Module(e)),
                n
        }
        function c(e, t, n) {
            var i = e.id
                , o = getOwn(k, i);
            !hasProp(_, i) || o && !o.defineEmitComplete ? (o = s(e),
                o.error && "error" === t ? n(o.error) : o.on(t, n)) : "defined" === t && n(_[i])
        }
        function l(e, t) {
            var n = e.requireModules
                , i = !1;
            t ? t(e) : (each(n, function(t) {
                var n = getOwn(k, t);
                n && (n.error = e,
                n.events.error && (i = !0,
                    n.emit("error", e)))
            }),
            i || req.onError(e))
        }
        function u() {
            globalDefQueue.length && (each(globalDefQueue, function(e) {
                var t = e[0];
                "string" == typeof t && (x.defQueueMap[t] = !0),
                    $.push(e)
            }),
                globalDefQueue = [])
        }
        function d(e) {
            delete k[e],
                delete T[e]
        }
        function f(e, t, n) {
            var i = e.map.id;
            e.error ? e.emit("error", e.error) : (t[i] = !0,
                each(e.depMaps, function(i, o) {
                    var a = i.id
                        , r = getOwn(k, a);
                    !r || e.depMatched[o] || n[a] || (getOwn(t, a) ? (e.defineDep(o, _[a]),
                        e.check()) : f(r, t, n))
                }),
                n[i] = !0)
        }
        function p() {
            var e, t, n = 1e3 * C.waitSeconds, a = n && x.startTime + n < (new Date).getTime(), r = [], s = [], c = !1, u = !0;
            if (!y) {
                if (y = !0,
                    eachProp(T, function(e) {
                        var n = e.map
                            , l = n.id;
                        if (e.enabled && (n.isDefine || s.push(e),
                            !e.error))
                            if (!e.inited && a)
                                o(l) ? (t = !0,
                                    c = !0) : (r.push(l),
                                    i(l));
                            else if (!e.inited && e.fetched && n.isDefine && (c = !0,
                                !n.prefix))
                                return u = !1
                    }),
                a && r.length)
                    return e = makeError("timeout", "Load timeout for modules: " + r, null, r),
                        e.contextName = x.contextName,
                        l(e);
                u && each(s, function(e) {
                    f(e, {}, {})
                }),
                a && !t || !c || !isBrowser && !isWebWorker || j || (j = setTimeout(function() {
                    j = 0,
                        p()
                }, 50)),
                    y = !1
            }
        }
        function h(e) {
            hasProp(_, e[0]) || s(r(e[0], null, !0)).init(e[1], e[2])
        }
        function m(e, t, n, i) {
            e.detachEvent && !isOpera ? i && e.detachEvent(i, t) : e.removeEventListener(n, t, !1)
        }
        function g(e) {
            var t = e.currentTarget || e.srcElement;
            return m(t, x.onScriptLoad, "load", "onreadystatechange"),
                m(t, x.onScriptError, "error"),
                {
                    node: t,
                    id: t && t.getAttribute("data-requiremodule")
                }
        }
        function v() {
            var e;
            for (u(); $.length; ) {
                if (e = $.shift(),
                null === e[0])
                    return l(makeError("mismatch", "Mismatched anonymous define() module: " + e[e.length - 1]));
                h(e)
            }
            x.defQueueMap = {}
        }
        var y, b, x, w, j, C = {
            waitSeconds: 7,
            baseUrl: "./",
            paths: {},
            bundles: {},
            pkgs: {},
            shim: {},
            config: {}
        }, k = {}, T = {}, S = {}, $ = [], _ = {}, E = {}, q = {}, D = 1, A = 1;
        return w = {
            require: function(e) {
                return e.require ? e.require : e.require = x.makeRequire(e.map)
            },
            exports: function(e) {
                return e.usingExports = !0,
                    e.map.isDefine ? e.exports ? _[e.map.id] = e.exports : e.exports = _[e.map.id] = {} : void 0
            },
            module: function(e) {
                return e.module ? e.module : e.module = {
                    id: e.map.id,
                    uri: e.map.url,
                    config: function() {
                        return getOwn(C.config, e.map.id) || {}
                    },
                    exports: e.exports || (e.exports = {})
                }
            }
        },
            b = function(e) {
                this.events = getOwn(S, e.id) || {},
                    this.map = e,
                    this.shim = getOwn(C.shim, e.id),
                    this.depExports = [],
                    this.depMaps = [],
                    this.depMatched = [],
                    this.pluginMaps = {},
                    this.depCount = 0
            }
            ,
            b.prototype = {
                init: function(e, t, n, i) {
                    i = i || {},
                    this.inited || (this.factory = t,
                        n ? this.on("error", n) : this.events.error && (n = bind(this, function(e) {
                            this.emit("error", e)
                        })),
                        this.depMaps = e && e.slice(0),
                        this.errback = n,
                        this.inited = !0,
                        this.ignore = i.ignore,
                        i.enabled || this.enabled ? this.enable() : this.check())
                },
                defineDep: function(e, t) {
                    this.depMatched[e] || (this.depMatched[e] = !0,
                        this.depCount -= 1,
                        this.depExports[e] = t)
                },
                fetch: function() {
                    if (!this.fetched) {
                        this.fetched = !0,
                            x.startTime = (new Date).getTime();
                        var e = this.map;
                        return this.shim ? void x.makeRequire(this.map, {
                            enableBuildCallback: !0
                        })(this.shim.deps || [], bind(this, function() {
                            return e.prefix ? this.callPlugin() : this.load()
                        })) : e.prefix ? this.callPlugin() : this.load()
                    }
                },
                load: function() {
                    var e = this.map.url;
                    E[e] || (E[e] = !0,
                        x.load(this.map.id, e))
                },
                check: function() {
                    if (this.enabled && !this.enabling) {
                        var e, t, n = this.map.id, i = this.depExports, o = this.exports, a = this.factory;
                        if (this.inited) {
                            if (this.error)
                                this.emit("error", this.error);
                            else if (!this.defining) {
                                if (this.defining = !0,
                                this.depCount < 1 && !this.defined) {
                                    if (isFunction(a)) {
                                        if (this.events.error && this.map.isDefine || req.onError !== defaultOnError)
                                            try {
                                                o = x.execCb(n, a, i, o)
                                            } catch (t) {
                                                e = t
                                            }
                                        else
                                            o = x.execCb(n, a, i, o);
                                        if (this.map.isDefine && void 0 === o && (t = this.module,
                                            t ? o = t.exports : this.usingExports && (o = this.exports)),
                                            e)
                                            return e.requireMap = this.map,
                                                e.requireModules = this.map.isDefine ? [this.map.id] : null,
                                                e.requireType = this.map.isDefine ? "define" : "require",
                                                l(this.error = e)
                                    } else
                                        o = a;
                                    if (this.exports = o,
                                    this.map.isDefine && !this.ignore && (_[n] = o,
                                        req.onResourceLoad)) {
                                        var r = [];
                                        each(this.depMaps, function(e) {
                                            r.push(e.normalizedMap || e)
                                        }),
                                            req.onResourceLoad(x, this.map, r)
                                    }
                                    d(n),
                                        this.defined = !0
                                }
                                this.defining = !1,
                                this.defined && !this.defineEmitted && (this.defineEmitted = !0,
                                    this.emit("defined", this.exports),
                                    this.defineEmitComplete = !0)
                            }
                        } else
                            hasProp(x.defQueueMap, n) || this.fetch()
                    }
                },
                callPlugin: function() {
                    var e = this.map
                        , t = e.id
                        , i = r(e.prefix);
                    this.depMaps.push(i),
                        c(i, "defined", bind(this, function(i) {
                            var o, a, u, f = getOwn(q, this.map.id), p = this.map.name, h = this.map.parentMap ? this.map.parentMap.name : null, m = x.makeRequire(e.parentMap, {
                                enableBuildCallback: !0
                            });
                            return this.map.unnormalized ? (i.normalize && (p = i.normalize(p, function(e) {
                                return n(e, h, !0)
                            }) || ""),
                                a = r(e.prefix + "!" + p, this.map.parentMap, !0),
                                c(a, "defined", bind(this, function(e) {
                                    this.map.normalizedMap = a,
                                        this.init([], function() {
                                            return e
                                        }, null, {
                                            enabled: !0,
                                            ignore: !0
                                        })
                                })),
                                void ((u = getOwn(k, a.id)) && (this.depMaps.push(a),
                                this.events.error && u.on("error", bind(this, function(e) {
                                    this.emit("error", e)
                                })),
                                    u.enable()))) : f ? (this.map.url = x.nameToUrl(f),
                                void this.load()) : (o = bind(this, function(e) {
                                this.init([], function() {
                                    return e
                                }, null, {
                                    enabled: !0
                                })
                            }),
                                o.error = bind(this, function(e) {
                                    this.inited = !0,
                                        this.error = e,
                                        e.requireModules = [t],
                                        eachProp(k, function(e) {
                                            0 === e.map.id.indexOf(t + "_unnormalized") && d(e.map.id)
                                        }),
                                        l(e)
                                }),
                                o.fromText = bind(this, function(n, i) {
                                    var a = e.name
                                        , c = r(a)
                                        , u = useInteractive;
                                    i && (n = i),
                                    u && (useInteractive = !1),
                                        s(c),
                                    hasProp(C.config, t) && (C.config[a] = C.config[t]);
                                    try {
                                        req.exec(n)
                                    } catch (e) {
                                        return l(makeError("fromtexteval", "fromText eval for " + t + " failed: " + e, e, [t]))
                                    }
                                    u && (useInteractive = !0),
                                        this.depMaps.push(c),
                                        x.completeLoad(a),
                                        m([a], o)
                                }),
                                void i.load(e.name, m, o, C))
                        })),
                        x.enable(i, this),
                        this.pluginMaps[i.id] = i
                },
                enable: function() {
                    T[this.map.id] = this,
                        this.enabled = !0,
                        this.enabling = !0,
                        each(this.depMaps, bind(this, function(e, t) {
                            var n, i, o;
                            if ("string" == typeof e) {
                                if (e = r(e, this.map.isDefine ? this.map : this.map.parentMap, !1, !this.skipMap),
                                    this.depMaps[t] = e,
                                    o = getOwn(w, e.id))
                                    return void (this.depExports[t] = o(this));
                                this.depCount += 1,
                                    c(e, "defined", bind(this, function(e) {
                                        this.undefed || (this.defineDep(t, e),
                                            this.check())
                                    })),
                                    this.errback ? c(e, "error", bind(this, this.errback)) : this.events.error && c(e, "error", bind(this, function(e) {
                                        this.emit("error", e)
                                    }))
                            }
                            n = e.id,
                                i = k[n],
                            hasProp(w, n) || !i || i.enabled || x.enable(e, this)
                        })),
                        eachProp(this.pluginMaps, bind(this, function(e) {
                            var t = getOwn(k, e.id);
                            t && !t.enabled && x.enable(e, this)
                        })),
                        this.enabling = !1,
                        this.check()
                },
                on: function(e, t) {
                    var n = this.events[e];
                    n || (n = this.events[e] = []),
                        n.push(t)
                },
                emit: function(e, t) {
                    each(this.events[e], function(e) {
                        e(t)
                    }),
                    "error" === e && delete this.events[e]
                }
            },
            x = {
                config: C,
                contextName: e,
                registry: k,
                defined: _,
                urlFetched: E,
                defQueue: $,
                defQueueMap: {},
                Module: b,
                makeModuleMap: r,
                nextTick: req.nextTick,
                onError: l,
                configure: function(e) {
                    if (e.baseUrl && "/" !== e.baseUrl.charAt(e.baseUrl.length - 1) && (e.baseUrl += "/"),
                    "string" == typeof e.urlArgs) {
                        var t = e.urlArgs;
                        e.urlArgs = function(e, n) {
                            return (-1 === n.indexOf("?") ? "?" : "&") + t
                        }
                    }
                    var n = C.shim
                        , i = {
                        paths: !0,
                        bundles: !0,
                        config: !0,
                        map: !0
                    };
                    eachProp(e, function(e, t) {
                        i[t] ? (C[t] || (C[t] = {}),
                            mixin(C[t], e, !0, !0)) : C[t] = e
                    }),
                    e.bundles && eachProp(e.bundles, function(e, t) {
                        each(e, function(e) {
                            e !== t && (q[e] = t)
                        })
                    }),
                    e.shim && (eachProp(e.shim, function(e, t) {
                        isArray(e) && (e = {
                            deps: e
                        }),
                        !e.exports && !e.init || e.exportsFn || (e.exportsFn = x.makeShimExports(e)),
                            n[t] = e
                    }),
                        C.shim = n),
                    e.packages && each(e.packages, function(e) {
                        var t, n;
                        e = "string" == typeof e ? {
                            name: e
                        } : e,
                            n = e.name,
                            t = e.location,
                        t && (C.paths[n] = e.location),
                            C.pkgs[n] = e.name + "/" + (e.main || "main").replace(currDirRegExp, "").replace(jsSuffixRegExp, "")
                    }),
                        eachProp(k, function(e, t) {
                            e.inited || e.map.unnormalized || (e.map = r(t, null, !0))
                        }),
                    (e.deps || e.callback) && x.require(e.deps || [], e.callback)
                },
                makeShimExports: function(e) {
                    function t() {
                        var t;
                        return e.init && (t = e.init.apply(global, arguments)),
                        t || e.exports && getGlobal(e.exports)
                    }
                    return t
                },
                makeRequire: function(t, o) {
                    function a(n, i, c) {
                        var u, d, f;
                        return o.enableBuildCallback && i && isFunction(i) && (i.__requireJsBuild = !0),
                            "string" == typeof n ? isFunction(i) ? l(makeError("requireargs", "Invalid require call"), c) : t && hasProp(w, n) ? w[n](k[t.id]) : req.get ? req.get(x, n, t, a) : (d = r(n, t, !1, !0),
                                u = d.id,
                                hasProp(_, u) ? _[u] : l(makeError("notloaded", 'Module name "' + u + '" has not been loaded yet for context: ' + e + (t ? "" : ". Use require([])")))) : (v(),
                                x.nextTick(function() {
                                    v(),
                                        f = s(r(null, t)),
                                        f.skipMap = o.skipMap,
                                        f.init(n, i, c, {
                                            enabled: !0
                                        }),
                                        p()
                                }),
                                a)
                    }
                    return o = o || {},
                        mixin(a, {
                            isBrowser: isBrowser,
                            toUrl: function(e) {
                                var i, o = e.lastIndexOf("."), a = e.split("/")[0], r = "." === a || ".." === a;
                                return -1 !== o && (!r || o > 1) && (i = e.substring(o, e.length),
                                    e = e.substring(0, o)),
                                    x.nameToUrl(n(e, t && t.id, !0), i, !0)
                            },
                            defined: function(e) {
                                return hasProp(_, r(e, t, !1, !0).id)
                            },
                            specified: function(e) {
                                return e = r(e, t, !1, !0).id,
                                hasProp(_, e) || hasProp(k, e)
                            }
                        }),
                    t || (a.undef = function(e) {
                            u();
                            var n = r(e, t, !0)
                                , o = getOwn(k, e);
                            o.undefed = !0,
                                i(e),
                                delete _[e],
                                delete E[n.url],
                                delete S[e],
                                eachReverse($, function(t, n) {
                                    t[0] === e && $.splice(n, 1)
                                }),
                                delete x.defQueueMap[e],
                            o && (o.events.defined && (S[e] = o.events),
                                d(e))
                        }
                    ),
                        a
                },
                enable: function(e) {
                    getOwn(k, e.id) && s(e).enable()
                },
                completeLoad: function(e) {
                    var t, n, i, a = getOwn(C.shim, e) || {}, r = a.exports;
                    for (u(); $.length; ) {
                        if (n = $.shift(),
                        null === n[0]) {
                            if (n[0] = e,
                                t)
                                break;
                            t = !0
                        } else
                            n[0] === e && (t = !0);
                        h(n)
                    }
                    if (x.defQueueMap = {},
                        i = getOwn(k, e),
                    !t && !hasProp(_, e) && i && !i.inited) {
                        if (!(!C.enforceDefine || r && getGlobal(r)))
                            return o(e) ? void 0 : l(makeError("nodefine", "No define call for " + e, null, [e]));
                        h([e, a.deps || [], a.exportsFn])
                    }
                    p()
                },
                nameToUrl: function(e, t, n) {
                    var i, o, a, r, s, c, l, u = getOwn(C.pkgs, e);
                    if (u && (e = u),
                        l = getOwn(q, e))
                        return x.nameToUrl(l, t, n);
                    if (req.jsExtRegExp.test(e))
                        s = e + (t || "");
                    else {
                        for (i = C.paths,
                                 o = e.split("/"),
                                 a = o.length; a > 0; a -= 1)
                            if (r = o.slice(0, a).join("/"),
                                c = getOwn(i, r)) {
                                isArray(c) && (c = c[0]),
                                    o.splice(0, a, c);
                                break
                            }
                        s = o.join("/"),
                            s += t || (/^data\:|^blob\:|\?/.test(s) || n ? "" : ".js"),
                            s = ("/" === s.charAt(0) || s.match(/^[\w\+\.\-]+:/) ? "" : C.baseUrl) + s
                    }
                    return C.urlArgs && !/^blob\:/.test(s) ? s + C.urlArgs(e, s) : s
                },
                load: function(e, t) {
                    req.load(x, e, t)
                },
                execCb: function(e, t, n, i) {
                    return t.apply(i, n)
                },
                onScriptLoad: function(e) {
                    if ("load" === e.type || readyRegExp.test((e.currentTarget || e.srcElement).readyState)) {
                        interactiveScript = null;
                        var t = g(e);
                        x.completeLoad(t.id)
                    }
                },
                onScriptError: function(e) {
                    var t = g(e);
                    if (!o(t.id)) {
                        var n = [];
                        return eachProp(k, function(e, i) {
                            0 !== i.indexOf("_@r") && each(e.depMaps, function(e) {
                                return e.id === t.id ? (n.push(i),
                                    !0) : void 0
                            })
                        }),
                            l(makeError("scripterror", 'Script error for "' + t.id + (n.length ? '", needed by: ' + n.join(", ") : '"'), e, [t.id]))
                    }
                }
            },
            x.require = x.makeRequire(),
            x
    }
    function getInteractiveScript() {
        return interactiveScript && "interactive" === interactiveScript.readyState ? interactiveScript : (eachReverse(scripts(), function(e) {
            return "interactive" === e.readyState ? interactiveScript = e : void 0
        }),
            interactiveScript)
    }
    var req, s, head, baseElement, dataMain, src, interactiveScript, currentlyAddingScript, mainScript, subPath, version = "2.3.5", commentRegExp = /\/\*[\s\S]*?\*\/|([^:"'=]|^)\/\/.*$/gm, cjsRequireRegExp = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g, jsSuffixRegExp = /\.js$/, currDirRegExp = /^\.\//, op = Object.prototype, ostring = op.toString, hasOwn = op.hasOwnProperty, isBrowser = !("undefined" == typeof window || "undefined" == typeof navigator || !window.document), isWebWorker = !isBrowser && "undefined" != typeof importScripts, readyRegExp = isBrowser && "PLAYSTATION 3" === navigator.platform ? /^complete$/ : /^(complete|loaded)$/, defContextName = "_", isOpera = "undefined" != typeof opera && "[object Opera]" === opera.toString(), contexts = {}, cfg = {}, globalDefQueue = [], useInteractive = !1;
    if (void 0 === define) {
        if (void 0 !== requirejs) {
            if (isFunction(requirejs))
                return;
            cfg = requirejs,
                requirejs = void 0
        }
        void 0 === require || isFunction(require) || (cfg = require,
            require = void 0),
            req = requirejs = function(e, t, n, i) {
                var o, a, r = defContextName;
                return isArray(e) || "string" == typeof e || (a = e,
                    isArray(t) ? (e = t,
                        t = n,
                        n = i) : e = []),
                a && a.context && (r = a.context),
                    o = getOwn(contexts, r),
                o || (o = contexts[r] = req.s.newContext(r)),
                a && o.configure(a),
                    o.require(e, t, n)
            }
            ,
            req.config = function(e) {
                return req(e)
            }
            ,
            req.nextTick = void 0 !== setTimeout ? function(e) {
                    setTimeout(e, 4)
                }
                : function(e) {
                    e()
                }
            ,
        require || (require = req),
            req.version = version,
            req.jsExtRegExp = /^\/|:|\?|\.js$/,
            req.isBrowser = isBrowser,
            s = req.s = {
                contexts: contexts,
                newContext: newContext
            },
            req({}),
            each(["toUrl", "undef", "defined", "specified"], function(e) {
                req[e] = function() {
                    var t = contexts[defContextName];
                    return t.require[e].apply(t, arguments)
                }
            }),
        isBrowser && (head = s.head = document.getElementsByTagName("head")[0],
        (baseElement = document.getElementsByTagName("base")[0]) && (head = s.head = baseElement.parentNode)),
            req.onError = defaultOnError,
            req.createNode = function(e, t, n) {
                var i = e.xhtml ? document.createElementNS("http://www.w3.org/1999/xhtml", "html:script") : document.createElement("script");
                return i.type = e.scriptType || "text/javascript",
                    i.charset = "utf-8",
                    i.async = !0,
                    i
            }
            ,
            req.load = function(e, t, n) {
                var i, o = e && e.config || {};
                if (isBrowser)
                    return i = req.createNode(o, t, n),
                        i.setAttribute("data-requirecontext", e.contextName),
                        i.setAttribute("data-requiremodule", t),
                        !i.attachEvent || i.attachEvent.toString && i.attachEvent.toString().indexOf("[native code") < 0 || isOpera ? (i.addEventListener("load", e.onScriptLoad, !1),
                            i.addEventListener("error", e.onScriptError, !1)) : (useInteractive = !0,
                            i.attachEvent("onreadystatechange", e.onScriptLoad)),
                        i.src = n,
                    o.onNodeCreated && o.onNodeCreated(i, o, t, n),
                        currentlyAddingScript = i,
                        baseElement ? head.insertBefore(i, baseElement) : head.appendChild(i),
                        currentlyAddingScript = null,
                        i;
                if (isWebWorker)
                    try {
                        setTimeout(function() {}, 0),
                            importScripts(n),
                            e.completeLoad(t)
                    } catch (i) {
                        e.onError(makeError("importscripts", "importScripts failed for " + t + " at " + n, i, [t]))
                    }
            }
            ,
        isBrowser && !cfg.skipDataMain && eachReverse(scripts(), function(e) {
            return head || (head = e.parentNode),
                dataMain = e.getAttribute("data-main"),
                dataMain ? (mainScript = dataMain,
                cfg.baseUrl || -1 !== mainScript.indexOf("!") || (src = mainScript.split("/"),
                    mainScript = src.pop(),
                    subPath = src.length ? src.join("/") + "/" : "./",
                    cfg.baseUrl = subPath),
                    mainScript = mainScript.replace(jsSuffixRegExp, ""),
                req.jsExtRegExp.test(mainScript) && (mainScript = dataMain),
                    cfg.deps = cfg.deps ? cfg.deps.concat(mainScript) : [mainScript],
                    !0) : void 0
        }),
            define = function(e, t, n) {
                var i, o;
                "string" != typeof e && (n = t,
                    t = e,
                    e = null),
                isArray(t) || (n = t,
                    t = null),
                !t && isFunction(n) && (t = [],
                n.length && (n.toString().replace(commentRegExp, commentReplace).replace(cjsRequireRegExp, function(e, n) {
                    t.push(n)
                }),
                    t = (1 === n.length ? ["require"] : ["require", "exports", "module"]).concat(t))),
                useInteractive && (i = currentlyAddingScript || getInteractiveScript()) && (e || (e = i.getAttribute("data-requiremodule")),
                    o = contexts[i.getAttribute("data-requirecontext")]),
                    o ? (o.defQueue.push([e, t, n]),
                        o.defQueueMap[e] = !0) : globalDefQueue.push([e, t, n])
            }
            ,
            define.amd = {
                jQuery: !0
            },
            req.exec = function(text) {
                return eval(text)
            }
            ,
            req(cfg)
    }
}(this, "undefined" == typeof setTimeout ? void 0 : setTimeout),
    function(e, t) {
        "object" == typeof module && "object" == typeof module.exports ? module.exports = function(n) {
                if (n = n || e,
                    !n.document)
                    throw new Error("jQuery requires a window with a document");
                return t(n)
            }
            : t(e)
    }(this, function(e) {
        function t(e) {
            var t = e.length
                , n = re.type(e);
            return "function" !== n && !re.isWindow(e) && (!(1 !== e.nodeType || !t) || ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e))
        }
        function n(e, t, n) {
            if (re.isFunction(t))
                return re.grep(e, function(e, i) {
                    return !!t.call(e, i, e) !== n
                });
            if (t.nodeType)
                return re.grep(e, function(e) {
                    return e === t !== n
                });
            if ("string" == typeof t) {
                if (he.test(t))
                    return re.filter(t, e, n);
                t = re.filter(t, e)
            }
            return re.grep(e, function(e) {
                return re.inArray(e, t) >= 0 !== n
            })
        }
        function i(e, t) {
            do {
                e = e[t]
            } while (e && 1 !== e.nodeType);return e
        }
        function o(e) {
            var t = we[e] = {};
            return re.each(e.match(xe) || [], function(e, n) {
                t[n] = !0
            }),
                t
        }
        function a() {
            ge.addEventListener ? (ge.removeEventListener("DOMContentLoaded", r, !1),
                e.removeEventListener("load", r, !1)) : (ge.detachEvent("onreadystatechange", r),
                e.detachEvent("onload", r))
        }
        function r() {
            (ge.addEventListener || "load" === event.type || "complete" === ge.readyState) && (a(),
                re.ready())
        }
        function s(e, t, n) {
            if (void 0 === n && 1 === e.nodeType) {
                var i = "data-" + t.replace(Se, "-$1").toLowerCase();
                if ("string" == typeof (n = e.getAttribute(i))) {
                    try {
                        n = "true" === n || "false" !== n && ("null" === n ? null : +n + "" === n ? +n : Te.test(n) ? re.parseJSON(n) : n)
                    } catch (e) {}
                    re.data(e, t, n)
                } else
                    n = void 0
            }
            return n
        }
        function c(e) {
            var t;
            for (t in e)
                if (("data" !== t || !re.isEmptyObject(e[t])) && "toJSON" !== t)
                    return !1;
            return !0
        }
        function l(e, t, n, i) {
            if (re.acceptData(e)) {
                var o, a, r = re.expando, s = e.nodeType, c = s ? re.cache : e, l = s ? e[r] : e[r] && r;
                if (l && c[l] && (i || c[l].data) || void 0 !== n || "string" != typeof t)
                    return l || (l = s ? e[r] = V.pop() || re.guid++ : r),
                    c[l] || (c[l] = s ? {} : {
                        toJSON: re.noop
                    }),
                    ("object" == typeof t || "function" == typeof t) && (i ? c[l] = re.extend(c[l], t) : c[l].data = re.extend(c[l].data, t)),
                        a = c[l],
                    i || (a.data || (a.data = {}),
                        a = a.data),
                    void 0 !== n && (a[re.camelCase(t)] = n),
                        "string" == typeof t ? null == (o = a[t]) && (o = a[re.camelCase(t)]) : o = a,
                        o
            }
        }
        function u(e, t, n) {
            if (re.acceptData(e)) {
                var i, o, a = e.nodeType, r = a ? re.cache : e, s = a ? e[re.expando] : re.expando;
                if (r[s]) {
                    if (t && (i = n ? r[s] : r[s].data)) {
                        re.isArray(t) ? t = t.concat(re.map(t, re.camelCase)) : t in i ? t = [t] : (t = re.camelCase(t),
                            t = t in i ? [t] : t.split(" ")),
                            o = t.length;
                        for (; o--; )
                            delete i[t[o]];
                        if (n ? !c(i) : !re.isEmptyObject(i))
                            return
                    }
                    (n || (delete r[s].data,
                        c(r[s]))) && (a ? re.cleanData([e], !0) : ne.deleteExpando || r != r.window ? delete r[s] : r[s] = null)
                }
            }
        }
        function d() {
            return !0
        }
        function f() {
            return !1
        }
        function p() {
            try {
                return ge.activeElement
            } catch (e) {}
        }
        function h(e) {
            var t = ze.split("|")
                , n = e.createDocumentFragment();
            if (n.createElement)
                for (; t.length; )
                    n.createElement(t.pop());
            return n
        }
        function m(e, t) {
            var n, i, o = 0, a = typeof e.getElementsByTagName !== ke ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== ke ? e.querySelectorAll(t || "*") : void 0;
            if (!a)
                for (a = [],
                         n = e.childNodes || e; null != (i = n[o]); o++)
                    !t || re.nodeName(i, t) ? a.push(i) : re.merge(a, m(i, t));
            return void 0 === t || t && re.nodeName(e, t) ? re.merge([e], a) : a
        }
        function g(e) {
            De.test(e.type) && (e.defaultChecked = e.checked)
        }
        function v(e, t) {
            return re.nodeName(e, "table") && re.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
        }
        function y(e) {
            return e.type = (null !== re.find.attr(e, "type")) + "/" + e.type,
                e
        }
        function b(e) {
            var t = Ge.exec(e.type);
            return t ? e.type = t[1] : e.removeAttribute("type"),
                e
        }
        function x(e, t) {
            for (var n, i = 0; null != (n = e[i]); i++)
                re._data(n, "globalEval", !t || re._data(t[i], "globalEval"))
        }
        function w(e, t) {
            if (1 === t.nodeType && re.hasData(e)) {
                var n, i, o, a = re._data(e), r = re._data(t, a), s = a.events;
                if (s) {
                    delete r.handle,
                        r.events = {};
                    for (n in s)
                        for (i = 0,
                                 o = s[n].length; o > i; i++)
                            re.event.add(t, n, s[n][i])
                }
                r.data && (r.data = re.extend({}, r.data))
            }
        }
        function j(e, t) {
            var n, i, o;
            if (1 === t.nodeType) {
                if (n = t.nodeName.toLowerCase(),
                !ne.noCloneEvent && t[re.expando]) {
                    o = re._data(t);
                    for (i in o.events)
                        re.removeEvent(t, i, o.handle);
                    t.removeAttribute(re.expando)
                }
                "script" === n && t.text !== e.text ? (y(t).text = e.text,
                    b(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML),
                ne.html5Clone && e.innerHTML && !re.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && De.test(e.type) ? (t.defaultChecked = t.checked = e.checked,
                t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
            }
        }
        function C(t, n) {
            var i = re(n.createElement(t)).appendTo(n.body)
                , o = e.getDefaultComputedStyle ? e.getDefaultComputedStyle(i[0]).display : re.css(i[0], "display");
            return i.detach(),
                o
        }
        function k(e) {
            var t = ge
                , n = et[e];
            return n || (n = C(e, t),
            "none" !== n && n || (Ze = (Ze || re("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement),
                t = (Ze[0].contentWindow || Ze[0].contentDocument).document,
                t.write(),
                t.close(),
                n = C(e, t),
                Ze.detach()),
                et[e] = n),
                n
        }
        function T(e, t) {
            return {
                get: function() {
                    var n = e();
                    return null != n ? n ? void delete this.get : (this.get = t).apply(this, arguments) : void 0
                }
            }
        }
        function S(e, t) {
            if (t in e)
                return t;
            for (var n = t.charAt(0).toUpperCase() + t.slice(1), i = t, o = pt.length; o--; )
                if ((t = pt[o] + n)in e)
                    return t;
            return i
        }
        function $(e, t) {
            for (var n, i, o, a = [], r = 0, s = e.length; s > r; r++)
                i = e[r],
                i.style && (a[r] = re._data(i, "olddisplay"),
                    n = i.style.display,
                    t ? (a[r] || "none" !== n || (i.style.display = ""),
                    "" === i.style.display && Ee(i) && (a[r] = re._data(i, "olddisplay", k(i.nodeName)))) : a[r] || (o = Ee(i),
                    (n && "none" !== n || !o) && re._data(i, "olddisplay", o ? n : re.css(i, "display"))));
            for (r = 0; s > r; r++)
                i = e[r],
                i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? a[r] || "" : "none"));
            return e
        }
        function _(e, t, n) {
            var i = lt.exec(t);
            return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : t
        }
        function E(e, t, n, i, o) {
            for (var a = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, r = 0; 4 > a; a += 2)
                "margin" === n && (r += re.css(e, n + _e[a], !0, o)),
                    i ? ("content" === n && (r -= re.css(e, "padding" + _e[a], !0, o)),
                    "margin" !== n && (r -= re.css(e, "border" + _e[a] + "Width", !0, o))) : (r += re.css(e, "padding" + _e[a], !0, o),
                    "padding" !== n && (r += re.css(e, "border" + _e[a] + "Width", !0, o)));
            return r
        }
        function q(e, t, n) {
            var i = !0
                , o = "width" === t ? e.offsetWidth : e.offsetHeight
                , a = tt(e)
                , r = ne.boxSizing() && "border-box" === re.css(e, "boxSizing", !1, a);
            if (0 >= o || null == o) {
                if (o = nt(e, t, a),
                (0 > o || null == o) && (o = e.style[t]),
                    ot.test(o))
                    return o;
                i = r && (ne.boxSizingReliable() || o === e.style[t]),
                    o = parseFloat(o) || 0
            }
            return o + E(e, t, n || (r ? "border" : "content"), i, a) + "px"
        }
        function D(e, t, n, i, o) {
            return new D.prototype.init(e,t,n,i,o)
        }
        function A() {
            return setTimeout(function() {
                ht = void 0
            }),
                ht = re.now()
        }
        function L(e, t) {
            var n, i = {
                height: e
            }, o = 0;
            for (t = t ? 1 : 0; 4 > o; o += 2 - t)
                n = _e[o],
                    i["margin" + n] = i["padding" + n] = e;
            return t && (i.opacity = i.width = e),
                i
        }
        function N(e, t, n) {
            for (var i, o = (xt[t] || []).concat(xt["*"]), a = 0, r = o.length; r > a; a++)
                if (i = o[a].call(n, t, e))
                    return i
        }
        function M(e, t, n) {
            var i, o, a, r, s, c, l, u, d = this, f = {}, p = e.style, h = e.nodeType && Ee(e), m = re._data(e, "fxshow");
            n.queue || (s = re._queueHooks(e, "fx"),
            null == s.unqueued && (s.unqueued = 0,
                    c = s.empty.fire,
                    s.empty.fire = function() {
                        s.unqueued || c()
                    }
            ),
                s.unqueued++,
                d.always(function() {
                    d.always(function() {
                        s.unqueued--,
                        re.queue(e, "fx").length || s.empty.fire()
                    })
                })),
            1 === e.nodeType && ("height"in t || "width"in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY],
                l = re.css(e, "display"),
                u = k(e.nodeName),
            "none" === l && (l = u),
            "inline" === l && "none" === re.css(e, "float") && (ne.inlineBlockNeedsLayout && "inline" !== u ? p.zoom = 1 : p.display = "inline-block")),
            n.overflow && (p.overflow = "hidden",
            ne.shrinkWrapBlocks() || d.always(function() {
                p.overflow = n.overflow[0],
                    p.overflowX = n.overflow[1],
                    p.overflowY = n.overflow[2]
            }));
            for (i in t)
                if (o = t[i],
                    gt.exec(o)) {
                    if (delete t[i],
                        a = a || "toggle" === o,
                    o === (h ? "hide" : "show")) {
                        if ("show" !== o || !m || void 0 === m[i])
                            continue;
                        h = !0
                    }
                    f[i] = m && m[i] || re.style(e, i)
                }
            if (!re.isEmptyObject(f)) {
                m ? "hidden"in m && (h = m.hidden) : m = re._data(e, "fxshow", {}),
                a && (m.hidden = !h),
                    h ? re(e).show() : d.done(function() {
                        re(e).hide()
                    }),
                    d.done(function() {
                        var t;
                        re._removeData(e, "fxshow");
                        for (t in f)
                            re.style(e, t, f[t])
                    });
                for (i in f)
                    r = N(h ? m[i] : 0, i, d),
                    i in m || (m[i] = r.start,
                    h && (r.end = r.start,
                        r.start = "width" === i || "height" === i ? 1 : 0))
            }
        }
        function P(e, t) {
            var n, i, o, a, r;
            for (n in e)
                if (i = re.camelCase(n),
                    o = t[i],
                    a = e[n],
                re.isArray(a) && (o = a[1],
                    a = e[n] = a[0]),
                n !== i && (e[i] = a,
                    delete e[n]),
                (r = re.cssHooks[i]) && "expand"in r) {
                    a = r.expand(a),
                        delete e[i];
                    for (n in a)
                        n in e || (e[n] = a[n],
                            t[n] = o)
                } else
                    t[i] = o
        }
        function z(e, t, n) {
            var i, o, a = 0, r = bt.length, s = re.Deferred().always(function() {
                delete c.elem
            }), c = function() {
                if (o)
                    return !1;
                for (var t = ht || A(), n = Math.max(0, l.startTime + l.duration - t), i = n / l.duration || 0, a = 1 - i, r = 0, c = l.tweens.length; c > r; r++)
                    l.tweens[r].run(a);
                return s.notifyWith(e, [l, a, n]),
                    1 > a && c ? n : (s.resolveWith(e, [l]),
                        !1)
            }, l = s.promise({
                elem: e,
                props: re.extend({}, t),
                opts: re.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: ht || A(),
                duration: n.duration,
                tweens: [],
                createTween: function(t, n) {
                    var i = re.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
                    return l.tweens.push(i),
                        i
                },
                stop: function(t) {
                    var n = 0
                        , i = t ? l.tweens.length : 0;
                    if (o)
                        return this;
                    for (o = !0; i > n; n++)
                        l.tweens[n].run(1);
                    return t ? s.resolveWith(e, [l, t]) : s.rejectWith(e, [l, t]),
                        this
                }
            }), u = l.props;
            for (P(u, l.opts.specialEasing); r > a; a++)
                if (i = bt[a].call(l, e, u, l.opts))
                    return i;
            return re.map(u, N, l),
            re.isFunction(l.opts.start) && l.opts.start.call(e, l),
                re.fx.timer(re.extend(c, {
                    elem: e,
                    anim: l,
                    queue: l.opts.queue
                })),
                l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
        }
        function R(e) {
            return function(t, n) {
                "string" != typeof t && (n = t,
                    t = "*");
                var i, o = 0, a = t.toLowerCase().match(xe) || [];
                if (re.isFunction(n))
                    for (; i = a[o++]; )
                        "+" === i.charAt(0) ? (i = i.slice(1) || "*",
                            (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
            }
        }
        function H(e, t, n, i) {
            function o(s) {
                var c;
                return a[s] = !0,
                    re.each(e[s] || [], function(e, s) {
                        var l = s(t, n, i);
                        return "string" != typeof l || r || a[l] ? r ? !(c = l) : void 0 : (t.dataTypes.unshift(l),
                            o(l),
                            !1)
                    }),
                    c
            }
            var a = {}
                , r = e === Ut;
            return o(t.dataTypes[0]) || !a["*"] && o("*")
        }
        function I(e, t) {
            var n, i, o = re.ajaxSettings.flatOptions || {};
            for (i in t)
                void 0 !== t[i] && ((o[i] ? e : n || (n = {}))[i] = t[i]);
            return n && re.extend(!0, e, n),
                e
        }
        function O(e, t, n) {
            for (var i, o, a, r, s = e.contents, c = e.dataTypes; "*" === c[0]; )
                c.shift(),
                void 0 === o && (o = e.mimeType || t.getResponseHeader("Content-Type"));
            if (o)
                for (r in s)
                    if (s[r] && s[r].test(o)) {
                        c.unshift(r);
                        break
                    }
            if (c[0]in n)
                a = c[0];
            else {
                for (r in n) {
                    if (!c[0] || e.converters[r + " " + c[0]]) {
                        a = r;
                        break
                    }
                    i || (i = r)
                }
                a = a || i
            }
            return a ? (a !== c[0] && c.unshift(a),
                n[a]) : void 0
        }
        function B(e, t, n, i) {
            var o, a, r, s, c, l = {}, u = e.dataTypes.slice();
            if (u[1])
                for (r in e.converters)
                    l[r.toLowerCase()] = e.converters[r];
            for (a = u.shift(); a; )
                if (e.responseFields[a] && (n[e.responseFields[a]] = t),
                !c && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)),
                    c = a,
                    a = u.shift())
                    if ("*" === a)
                        a = c;
                    else if ("*" !== c && c !== a) {
                        if (!(r = l[c + " " + a] || l["* " + a]))
                            for (o in l)
                                if (s = o.split(" "),
                                s[1] === a && (r = l[c + " " + s[0]] || l["* " + s[0]])) {
                                    !0 === r ? r = l[o] : !0 !== l[o] && (a = s[0],
                                        u.unshift(s[1]));
                                    break
                                }
                        if (!0 !== r)
                            if (r && e.throws)
                                t = r(t);
                            else
                                try {
                                    t = r(t)
                                } catch (e) {
                                    return {
                                        state: "parsererror",
                                        error: r ? e : "No conversion from " + c + " to " + a
                                    }
                                }
                    }
            return {
                state: "success",
                data: t
            }
        }
        function F(e, t, n, i) {
            var o;
            if (re.isArray(t))
                re.each(t, function(t, o) {
                    n || Vt.test(e) ? i(e, o) : F(e + "[" + ("object" == typeof o ? t : "") + "]", o, n, i)
                });
            else if (n || "object" !== re.type(t))
                i(e, t);
            else
                for (o in t)
                    F(e + "[" + o + "]", t[o], n, i)
        }
        function U() {
            try {
                return new e.XMLHttpRequest
            } catch (e) {}
        }
        function W() {
            try {
                return new e.ActiveXObject("Microsoft.XMLHTTP")
            } catch (e) {}
        }
        function Q(e) {
            return re.isWindow(e) ? e : 9 === e.nodeType && (e.defaultView || e.parentWindow)
        }
        var V = []
            , G = V.slice
            , Y = V.concat
            , X = V.push
            , K = V.indexOf
            , J = {}
            , Z = J.toString
            , ee = J.hasOwnProperty
            , te = "".trim
            , ne = {}
            , ie = e.jQuery
            , oe = e.$
            , ae = "1.11.0-beta2"
            , re = function(e, t) {
            return new re.fn.init(e,t)
        }
            , se = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g
            , ce = /^-ms-/
            , le = /-([\da-z])/gi
            , ue = function(e, t) {
            return t.toUpperCase()
        };
        re.fn = re.prototype = {
            jquery: ae,
            constructor: re,
            selector: "",
            length: 0,
            toArray: function() {
                return G.call(this)
            },
            get: function(e) {
                return null != e ? 0 > e ? this[e + this.length] : this[e] : G.call(this)
            },
            pushStack: function(e) {
                var t = re.merge(this.constructor(), e);
                return t.prevObject = this,
                    t.context = this.context,
                    t
            },
            each: function(e, t) {
                return re.each(this, e, t)
            },
            map: function(e) {
                return this.pushStack(re.map(this, function(t, n) {
                    return e.call(t, n, t)
                }))
            },
            slice: function() {
                return this.pushStack(G.apply(this, arguments))
            },
            first: function() {
                return this.eq(0)
            },
            last: function() {
                return this.eq(-1)
            },
            eq: function(e) {
                var t = this.length
                    , n = +e + (0 > e ? t : 0);
                return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
            },
            end: function() {
                return this.prevObject || this.constructor(null)
            },
            push: X,
            sort: V.sort,
            splice: V.splice
        },
            re.extend = re.fn.extend = function() {
                var e, t, n, i, o, a, r = arguments[0] || {}, s = 1, c = arguments.length, l = !1;
                for ("boolean" == typeof r && (l = r,
                    r = arguments[s] || {},
                    s++),
                     "object" == typeof r || re.isFunction(r) || (r = {}),
                     s === c && (r = this,
                         s--); c > s; s++)
                    if (null != (o = arguments[s]))
                        for (i in o)
                            e = r[i],
                                n = o[i],
                            r !== n && (l && n && (re.isPlainObject(n) || (t = re.isArray(n))) ? (t ? (t = !1,
                                a = e && re.isArray(e) ? e : []) : a = e && re.isPlainObject(e) ? e : {},
                                r[i] = re.extend(l, a, n)) : void 0 !== n && (r[i] = n));
                return r
            }
            ,
            re.extend({
                expando: "jQuery" + (ae + Math.random()).replace(/\D/g, ""),
                isReady: !0,
                error: function(e) {
                    throw new Error(e)
                },
                noop: function() {},
                noConflict: function(t) {
                    return e.$ === re && (e.$ = oe),
                    t && e.jQuery === re && (e.jQuery = ie),
                        re
                },
                isFunction: function(e) {
                    return "function" === re.type(e)
                },
                isArray: Array.isArray || function(e) {
                    return "array" === re.type(e)
                }
                ,
                isWindow: function(e) {
                    return null != e && e == e.window
                },
                isNumeric: function(e) {
                    return e - parseFloat(e) >= 0
                },
                isEmptyObject: function(e) {
                    var t;
                    for (t in e)
                        return !1;
                    return !0
                },
                isPlainObject: function(e) {
                    var t;
                    if (!e || "object" !== re.type(e) || e.nodeType || re.isWindow(e))
                        return !1;
                    try {
                        if (e.constructor && !ee.call(e, "constructor") && !ee.call(e.constructor.prototype, "isPrototypeOf"))
                            return !1
                    } catch (e) {
                        return !1
                    }
                    if (ne.ownLast)
                        for (t in e)
                            return ee.call(e, t);
                    for (t in e)
                        ;
                    return void 0 === t || ee.call(e, t)
                },
                type: function(e) {
                    return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? J[Z.call(e)] || "object" : typeof e
                },
                globalEval: function(t) {
                    t && re.trim(t) && (e.execScript || function(t) {
                            e.eval.call(e, t)
                        }
                    )(t)
                },
                camelCase: function(e) {
                    return e.replace(ce, "ms-").replace(le, ue)
                },
                nodeName: function(e, t) {
                    return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
                },
                each: function(e, n, i) {
                    var o = 0
                        , a = e.length
                        , r = t(e);
                    if (i) {
                        if (r)
                            for (; a > o && !1 !== n.apply(e[o], i); o++)
                                ;
                        else
                            for (o in e)
                                if (!1 === n.apply(e[o], i))
                                    break
                    } else if (r)
                        for (; a > o && !1 !== n.call(e[o], o, e[o]); o++)
                            ;
                    else
                        for (o in e)
                            if (!1 === n.call(e[o], o, e[o]))
                                break;
                    return e
                },
                trim: te && !te.call("\ufeff ") ? function(e) {
                        return null == e ? "" : te.call(e)
                    }
                    : function(e) {
                        return null == e ? "" : (e + "").replace(se, "")
                    }
                ,
                makeArray: function(e, n) {
                    var i = n || [];
                    return null != e && (t(Object(e)) ? re.merge(i, "string" == typeof e ? [e] : e) : X.call(i, e)),
                        i
                },
                inArray: function(e, t, n) {
                    var i;
                    if (t) {
                        if (K)
                            return K.call(t, e, n);
                        for (i = t.length,
                                 n = n ? 0 > n ? Math.max(0, i + n) : n : 0; i > n; n++)
                            if (n in t && t[n] === e)
                                return n
                    }
                    return -1
                },
                merge: function(e, t) {
                    for (var n = +t.length, i = 0, o = e.length; n > i; )
                        e[o++] = t[i++];
                    if (n !== n)
                        for (; void 0 !== t[i]; )
                            e[o++] = t[i++];
                    return e.length = o,
                        e
                },
                grep: function(e, t, n) {
                    for (var i = [], o = 0, a = e.length, r = !n; a > o; o++)
                        !t(e[o], o) !== r && i.push(e[o]);
                    return i
                },
                map: function(e, n, i) {
                    var o, a = 0, r = e.length, s = t(e), c = [];
                    if (s)
                        for (; r > a; a++)
                            null != (o = n(e[a], a, i)) && c.push(o);
                    else
                        for (a in e)
                            null != (o = n(e[a], a, i)) && c.push(o);
                    return Y.apply([], c)
                },
                guid: 1,
                proxy: function(e, t) {
                    var n, i, o;
                    return "string" == typeof t && (o = e[t],
                        t = e,
                        e = o),
                        re.isFunction(e) ? (n = G.call(arguments, 2),
                            i = function() {
                                return e.apply(t || this, n.concat(G.call(arguments)))
                            }
                            ,
                            i.guid = e.guid = e.guid || re.guid++,
                            i) : void 0
                },
                now: function() {
                    return +new Date
                },
                support: ne
            }),
            re.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
                J["[object " + t + "]"] = t.toLowerCase()
            });
        var de = function(e) {
            function t(e, t, n, i) {
                var o, a, r, s, l, f, p, h, m, g;
                if ((t ? t.ownerDocument || t : H) !== D && q(t),
                    t = t || D,
                    n = n || [],
                !e || "string" != typeof e)
                    return n;
                if (1 !== (s = t.nodeType) && 9 !== s)
                    return [];
                if (L && !i) {
                    if (o = ve.exec(e))
                        if (r = o[1]) {
                            if (9 === s) {
                                if (!(a = t.getElementById(r)) || !a.parentNode)
                                    return n;
                                if (a.id === r)
                                    return n.push(a),
                                        n
                            } else if (t.ownerDocument && (a = t.ownerDocument.getElementById(r)) && z(t, a) && a.id === r)
                                return n.push(a),
                                    n
                        } else {
                            if (o[2])
                                return J.apply(n, t.getElementsByTagName(e)),
                                    n;
                            if ((r = o[3]) && w.getElementsByClassName && t.getElementsByClassName)
                                return J.apply(n, t.getElementsByClassName(r)),
                                    n
                        }
                    if (w.qsa && (!N || !N.test(e))) {
                        if (h = p = R,
                            m = t,
                            g = 9 === s && e,
                        1 === s && "object" !== t.nodeName.toLowerCase()) {
                            for (f = u(e),
                                     (p = t.getAttribute("id")) ? h = p.replace(be, "\\$&") : t.setAttribute("id", h),
                                     h = "[id='" + h + "'] ",
                                     l = f.length; l--; )
                                f[l] = h + d(f[l]);
                            m = ye.test(e) && c(t.parentNode) || t,
                                g = f.join(",")
                        }
                        if (g)
                            try {
                                return J.apply(n, m.querySelectorAll(g)),
                                    n
                            } catch (e) {} finally {
                                p || t.removeAttribute("id")
                            }
                    }
                }
                return b(e.replace(se, "$1"), t, n, i)
            }
            function n() {
                function e(n, i) {
                    return t.push(n + " ") > C.cacheLength && delete e[t.shift()],
                        e[n + " "] = i
                }
                var t = [];
                return e
            }
            function i(e) {
                return e[R] = !0,
                    e
            }
            function o(e) {
                var t = D.createElement("div");
                try {
                    return !!e(t)
                } catch (e) {
                    return !1
                } finally {
                    t.parentNode && t.parentNode.removeChild(t),
                        t = null
                }
            }
            function a(e, t) {
                for (var n = e.split("|"), i = e.length; i--; )
                    C.attrHandle[n[i]] = t
            }
            function r(e, t) {
                var n = t && e
                    , i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || V) - (~e.sourceIndex || V);
                if (i)
                    return i;
                if (n)
                    for (; n = n.nextSibling; )
                        if (n === t)
                            return -1;
                return e ? 1 : -1
            }
            function s(e) {
                return i(function(t) {
                    return t = +t,
                        i(function(n, i) {
                            for (var o, a = e([], n.length, t), r = a.length; r--; )
                                n[o = a[r]] && (n[o] = !(i[o] = n[o]))
                        })
                })
            }
            function c(e) {
                return e && typeof e.getElementsByTagName !== Q && e
            }
            function l() {}
            function u(e, n) {
                var i, o, a, r, s, c, l, u = F[e + " "];
                if (u)
                    return n ? 0 : u.slice(0);
                for (s = e,
                         c = [],
                         l = C.preFilter; s; ) {
                    (!i || (o = ce.exec(s))) && (o && (s = s.slice(o[0].length) || s),
                        c.push(a = [])),
                        i = !1,
                    (o = le.exec(s)) && (i = o.shift(),
                        a.push({
                            value: i,
                            type: o[0].replace(se, " ")
                        }),
                        s = s.slice(i.length));
                    for (r in C.filter)
                        !(o = pe[r].exec(s)) || l[r] && !(o = l[r](o)) || (i = o.shift(),
                            a.push({
                                value: i,
                                type: r,
                                matches: o
                            }),
                            s = s.slice(i.length));
                    if (!i)
                        break
                }
                return n ? s.length : s ? t.error(e) : F(e, c).slice(0)
            }
            function d(e) {
                for (var t = 0, n = e.length, i = ""; n > t; t++)
                    i += e[t].value;
                return i
            }
            function f(e, t, n) {
                var i = t.dir
                    , o = n && "parentNode" === i
                    , a = O++;
                return t.first ? function(t, n, a) {
                        for (; t = t[i]; )
                            if (1 === t.nodeType || o)
                                return e(t, n, a)
                    }
                    : function(t, n, r) {
                        var s, c, l, u = I + " " + a;
                        if (r) {
                            for (; t = t[i]; )
                                if ((1 === t.nodeType || o) && e(t, n, r))
                                    return !0
                        } else
                            for (; t = t[i]; )
                                if (1 === t.nodeType || o)
                                    if (l = t[R] || (t[R] = {}),
                                    (c = l[i]) && c[0] === u) {
                                        if (!0 === (s = c[1]) || s === j)
                                            return !0 === s
                                    } else if (c = l[i] = [u],
                                        c[1] = e(t, n, r) || j,
                                    !0 === c[1])
                                        return !0
                    }
            }
            function p(e) {
                return e.length > 1 ? function(t, n, i) {
                        for (var o = e.length; o--; )
                            if (!e[o](t, n, i))
                                return !1;
                        return !0
                    }
                    : e[0]
            }
            function h(e, t, n, i, o) {
                for (var a, r = [], s = 0, c = e.length, l = null != t; c > s; s++)
                    (a = e[s]) && (!n || n(a, i, o)) && (r.push(a),
                    l && t.push(s));
                return r
            }
            function m(e, t, n, o, a, r) {
                return o && !o[R] && (o = m(o)),
                a && !a[R] && (a = m(a, r)),
                    i(function(i, r, s, c) {
                        var l, u, d, f = [], p = [], m = r.length, g = i || y(t || "*", s.nodeType ? [s] : s, []), v = !e || !i && t ? g : h(g, f, e, s, c), b = n ? a || (i ? e : m || o) ? [] : r : v;
                        if (n && n(v, b, s, c),
                            o)
                            for (l = h(b, p),
                                     o(l, [], s, c),
                                     u = l.length; u--; )
                                (d = l[u]) && (b[p[u]] = !(v[p[u]] = d));
                        if (i) {
                            if (a || e) {
                                if (a) {
                                    for (l = [],
                                             u = b.length; u--; )
                                        (d = b[u]) && l.push(v[u] = d);
                                    a(null, b = [], l, c)
                                }
                                for (u = b.length; u--; )
                                    (d = b[u]) && (l = a ? ee.call(i, d) : f[u]) > -1 && (i[l] = !(r[l] = d))
                            }
                        } else
                            b = h(b === r ? b.splice(m, b.length) : b),
                                a ? a(null, r, b, c) : J.apply(r, b)
                    })
            }
            function g(e) {
                for (var t, n, i, o = e.length, a = C.relative[e[0].type], r = a || C.relative[" "], s = a ? 1 : 0, c = f(function(e) {
                    return e === t
                }, r, !0), l = f(function(e) {
                    return ee.call(t, e) > -1
                }, r, !0), u = [function(e, n, i) {
                    return !a && (i || n !== $) || ((t = n).nodeType ? c(e, n, i) : l(e, n, i))
                }
                ]; o > s; s++)
                    if (n = C.relative[e[s].type])
                        u = [f(p(u), n)];
                    else {
                        if (n = C.filter[e[s].type].apply(null, e[s].matches),
                            n[R]) {
                            for (i = ++s; o > i && !C.relative[e[i].type]; i++)
                                ;
                            return m(s > 1 && p(u), s > 1 && d(e.slice(0, s - 1).concat({
                                value: " " === e[s - 2].type ? "*" : ""
                            })).replace(se, "$1"), n, i > s && g(e.slice(s, i)), o > i && g(e = e.slice(i)), o > i && d(e))
                        }
                        u.push(n)
                    }
                return p(u)
            }
            function v(e, n) {
                var o = 0
                    , a = n.length > 0
                    , r = e.length > 0
                    , s = function(i, s, c, l, u) {
                    var d, f, p, m = 0, g = "0", v = i && [], y = [], b = $, x = i || r && C.find.TAG("*", u), w = I += null == b ? 1 : Math.random() || .1, k = x.length;
                    for (u && ($ = s !== D && s,
                        j = o); g !== k && null != (d = x[g]); g++) {
                        if (r && d) {
                            for (f = 0; p = e[f++]; )
                                if (p(d, s, c)) {
                                    l.push(d);
                                    break
                                }
                            u && (I = w,
                                j = ++o)
                        }
                        a && ((d = !p && d) && m--,
                        i && v.push(d))
                    }
                    if (m += g,
                    a && g !== m) {
                        for (f = 0; p = n[f++]; )
                            p(v, y, s, c);
                        if (i) {
                            if (m > 0)
                                for (; g--; )
                                    v[g] || y[g] || (y[g] = X.call(l));
                            y = h(y)
                        }
                        J.apply(l, y),
                        u && !i && y.length > 0 && m + n.length > 1 && t.uniqueSort(l)
                    }
                    return u && (I = w,
                        $ = b),
                        v
                };
                return a ? i(s) : s
            }
            function y(e, n, i) {
                for (var o = 0, a = n.length; a > o; o++)
                    t(e, n[o], i);
                return i
            }
            function b(e, t, n, i) {
                var o, a, r, s, l, f = u(e);
                if (!i && 1 === f.length) {
                    if (a = f[0] = f[0].slice(0),
                    a.length > 2 && "ID" === (r = a[0]).type && w.getById && 9 === t.nodeType && L && C.relative[a[1].type]) {
                        if (!(t = (C.find.ID(r.matches[0].replace(xe, we), t) || [])[0]))
                            return n;
                        e = e.slice(a.shift().value.length)
                    }
                    for (o = pe.needsContext.test(e) ? 0 : a.length; o-- && (r = a[o],
                        !C.relative[s = r.type]); )
                        if ((l = C.find[s]) && (i = l(r.matches[0].replace(xe, we), ye.test(a[0].type) && c(t.parentNode) || t))) {
                            if (a.splice(o, 1),
                                !(e = i.length && d(a)))
                                return J.apply(n, i),
                                    n;
                            break
                        }
                }
                return S(e, f)(i, t, !L, n, ye.test(e) && c(t.parentNode) || t),
                    n
            }
            var x, w, j, C, k, T, S, $, _, E, q, D, A, L, N, M, P, z, R = "sizzle" + -new Date, H = e.document, I = 0, O = 0, B = n(), F = n(), U = n(), W = function(e, t) {
                return e === t && (E = !0),
                    0
            }, Q = "undefined", V = 1 << 31, G = {}.hasOwnProperty, Y = [], X = Y.pop, K = Y.push, J = Y.push, Z = Y.slice, ee = Y.indexOf || function(e) {
                for (var t = 0, n = this.length; n > t; t++)
                    if (this[t] === e)
                        return t;
                return -1
            }
                , te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", ne = "[\\x20\\t\\r\\n\\f]", ie = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", oe = ie.replace("w", "w#"), ae = "\\[" + ne + "*(" + ie + ")" + ne + "*(?:([*^$|!~]?=)" + ne + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + oe + ")|)|)" + ne + "*\\]", re = ":(" + ie + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + ae.replace(3, 8) + ")*)|.*)\\)|)", se = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$","g"), ce = new RegExp("^" + ne + "*," + ne + "*"), le = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"), ue = new RegExp("=" + ne + "*([^\\]'\"]*)" + ne + "*\\]","g"), de = new RegExp(re), fe = new RegExp("^" + oe + "$"), pe = {
                ID: new RegExp("^#(" + ie + ")"),
                CLASS: new RegExp("^\\.(" + ie + ")"),
                TAG: new RegExp("^(" + ie.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + ae),
                PSEUDO: new RegExp("^" + re),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)","i"),
                bool: new RegExp("^(?:" + te + ")$","i"),
                needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)","i")
            }, he = /^(?:input|select|textarea|button)$/i, me = /^h\d$/i, ge = /^[^{]+\{\s*\[native \w/, ve = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ye = /[+~]/, be = /'|\\/g, xe = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)","ig"), we = function(e, t, n) {
                var i = "0x" + t - 65536;
                return i !== i || n ? t : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            };
            try {
                J.apply(Y = Z.call(H.childNodes), H.childNodes),
                    Y[H.childNodes.length].nodeType
            } catch (e) {
                J = {
                    apply: Y.length ? function(e, t) {
                            K.apply(e, Z.call(t))
                        }
                        : function(e, t) {
                            for (var n = e.length, i = 0; e[n++] = t[i++]; )
                                ;
                            e.length = n - 1
                        }
                }
            }
            w = t.support = {},
                T = t.isXML = function(e) {
                    var t = e && (e.ownerDocument || e).documentElement;
                    return !!t && "HTML" !== t.nodeName
                }
                ,
                q = t.setDocument = function(e) {
                    var t, n = e ? e.ownerDocument || e : H, i = n.defaultView;
                    return n !== D && 9 === n.nodeType && n.documentElement ? (D = n,
                        A = n.documentElement,
                        L = !T(n),
                    i && i.attachEvent && i !== i.top && i.attachEvent("onbeforeunload", function() {
                        q()
                    }),
                        w.attributes = o(function(e) {
                            return e.className = "i",
                                !e.getAttribute("className")
                        }),
                        w.getElementsByTagName = o(function(e) {
                            return e.appendChild(n.createComment("")),
                                !e.getElementsByTagName("*").length
                        }),
                        w.getElementsByClassName = ge.test(n.getElementsByClassName) && o(function(e) {
                            return e.innerHTML = "<div class='a'></div><div class='a i'></div>",
                                e.firstChild.className = "i",
                            2 === e.getElementsByClassName("i").length
                        }),
                        w.getById = o(function(e) {
                            return A.appendChild(e).id = R,
                            !n.getElementsByName || !n.getElementsByName(R).length
                        }),
                        w.getById ? (C.find.ID = function(e, t) {
                                if (typeof t.getElementById !== Q && L) {
                                    var n = t.getElementById(e);
                                    return n && n.parentNode ? [n] : []
                                }
                            }
                                ,
                                C.filter.ID = function(e) {
                                    var t = e.replace(xe, we);
                                    return function(e) {
                                        return e.getAttribute("id") === t
                                    }
                                }
                        ) : (delete C.find.ID,
                                C.filter.ID = function(e) {
                                    var t = e.replace(xe, we);
                                    return function(e) {
                                        var n = typeof e.getAttributeNode !== Q && e.getAttributeNode("id");
                                        return n && n.value === t
                                    }
                                }
                        ),
                        C.find.TAG = w.getElementsByTagName ? function(e, t) {
                                return typeof t.getElementsByTagName !== Q ? t.getElementsByTagName(e) : void 0
                            }
                            : function(e, t) {
                                var n, i = [], o = 0, a = t.getElementsByTagName(e);
                                if ("*" === e) {
                                    for (; n = a[o++]; )
                                        1 === n.nodeType && i.push(n);
                                    return i
                                }
                                return a
                            }
                        ,
                        C.find.CLASS = w.getElementsByClassName && function(e, t) {
                            return typeof t.getElementsByClassName !== Q && L ? t.getElementsByClassName(e) : void 0
                        }
                        ,
                        M = [],
                        N = [],
                    (w.qsa = ge.test(n.querySelectorAll)) && (o(function(e) {
                        e.innerHTML = "<select><option selected=''></option></select>",
                        e.querySelectorAll("[selected]").length || N.push("\\[" + ne + "*(?:value|" + te + ")"),
                        e.querySelectorAll(":checked").length || N.push(":checked")
                    }),
                        o(function(e) {
                            var t = n.createElement("input");
                            t.setAttribute("type", "hidden"),
                                e.appendChild(t).setAttribute("t", ""),
                            e.querySelectorAll("[t^='']").length && N.push("[*^$]=" + ne + "*(?:''|\"\")"),
                            e.querySelectorAll(":enabled").length || N.push(":enabled", ":disabled"),
                                e.querySelectorAll("*,:x"),
                                N.push(",.*:")
                        })),
                    (w.matchesSelector = ge.test(P = A.webkitMatchesSelector || A.mozMatchesSelector || A.oMatchesSelector || A.msMatchesSelector)) && o(function(e) {
                        w.disconnectedMatch = P.call(e, "div"),
                            P.call(e, "[s!='']:x"),
                            M.push("!=", re)
                    }),
                        N = N.length && new RegExp(N.join("|")),
                        M = M.length && new RegExp(M.join("|")),
                        t = ge.test(A.compareDocumentPosition),
                        z = t || ge.test(A.contains) ? function(e, t) {
                                var n = 9 === e.nodeType ? e.documentElement : e
                                    , i = t && t.parentNode;
                                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
                            }
                            : function(e, t) {
                                if (t)
                                    for (; t = t.parentNode; )
                                        if (t === e)
                                            return !0;
                                return !1
                            }
                        ,
                        W = t ? function(e, t) {
                                if (e === t)
                                    return E = !0,
                                        0;
                                var i = !e.compareDocumentPosition - !t.compareDocumentPosition;
                                return i || (i = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1,
                                    1 & i || !w.sortDetached && t.compareDocumentPosition(e) === i ? e === n || e.ownerDocument === H && z(H, e) ? -1 : t === n || t.ownerDocument === H && z(H, t) ? 1 : _ ? ee.call(_, e) - ee.call(_, t) : 0 : 4 & i ? -1 : 1)
                            }
                            : function(e, t) {
                                if (e === t)
                                    return E = !0,
                                        0;
                                var i, o = 0, a = e.parentNode, s = t.parentNode, c = [e], l = [t];
                                if (!a || !s)
                                    return e === n ? -1 : t === n ? 1 : a ? -1 : s ? 1 : _ ? ee.call(_, e) - ee.call(_, t) : 0;
                                if (a === s)
                                    return r(e, t);
                                for (i = e; i = i.parentNode; )
                                    c.unshift(i);
                                for (i = t; i = i.parentNode; )
                                    l.unshift(i);
                                for (; c[o] === l[o]; )
                                    o++;
                                return o ? r(c[o], l[o]) : c[o] === H ? -1 : l[o] === H ? 1 : 0
                            }
                        ,
                        n) : D
                }
                ,
                t.matches = function(e, n) {
                    return t(e, null, null, n)
                }
                ,
                t.matchesSelector = function(e, n) {
                    if ((e.ownerDocument || e) !== D && q(e),
                        n = n.replace(ue, "='$1']"),
                        !(!w.matchesSelector || !L || M && M.test(n) || N && N.test(n)))
                        try {
                            var i = P.call(e, n);
                            if (i || w.disconnectedMatch || e.document && 11 !== e.document.nodeType)
                                return i
                        } catch (e) {}
                    return t(n, D, null, [e]).length > 0
                }
                ,
                t.contains = function(e, t) {
                    return (e.ownerDocument || e) !== D && q(e),
                        z(e, t)
                }
                ,
                t.attr = function(e, t) {
                    (e.ownerDocument || e) !== D && q(e);
                    var n = C.attrHandle[t.toLowerCase()]
                        , i = n && G.call(C.attrHandle, t.toLowerCase()) ? n(e, t, !L) : void 0;
                    return void 0 !== i ? i : w.attributes || !L ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
                }
                ,
                t.error = function(e) {
                    throw new Error("Syntax error, unrecognized expression: " + e)
                }
                ,
                t.uniqueSort = function(e) {
                    var t, n = [], i = 0, o = 0;
                    if (E = !w.detectDuplicates,
                        _ = !w.sortStable && e.slice(0),
                        e.sort(W),
                        E) {
                        for (; t = e[o++]; )
                            t === e[o] && (i = n.push(o));
                        for (; i--; )
                            e.splice(n[i], 1)
                    }
                    return _ = null,
                        e
                }
                ,
                k = t.getText = function(e) {
                    var t, n = "", i = 0, o = e.nodeType;
                    if (o) {
                        if (1 === o || 9 === o || 11 === o) {
                            if ("string" == typeof e.textContent)
                                return e.textContent;
                            for (e = e.firstChild; e; e = e.nextSibling)
                                n += k(e)
                        } else if (3 === o || 4 === o)
                            return e.nodeValue
                    } else
                        for (; t = e[i++]; )
                            n += k(t);
                    return n
                }
                ,
                C = t.selectors = {
                    cacheLength: 50,
                    createPseudo: i,
                    match: pe,
                    attrHandle: {},
                    find: {},
                    relative: {
                        ">": {
                            dir: "parentNode",
                            first: !0
                        },
                        " ": {
                            dir: "parentNode"
                        },
                        "+": {
                            dir: "previousSibling",
                            first: !0
                        },
                        "~": {
                            dir: "previousSibling"
                        }
                    },
                    preFilter: {
                        ATTR: function(e) {
                            return e[1] = e[1].replace(xe, we),
                                e[3] = (e[4] || e[5] || "").replace(xe, we),
                            "~=" === e[2] && (e[3] = " " + e[3] + " "),
                                e.slice(0, 4)
                        },
                        CHILD: function(e) {
                            return e[1] = e[1].toLowerCase(),
                                "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]),
                                    e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])),
                                    e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]),
                                e
                        },
                        PSEUDO: function(e) {
                            var t, n = !e[5] && e[2];
                            return pe.CHILD.test(e[0]) ? null : (e[3] && void 0 !== e[4] ? e[2] = e[4] : n && de.test(n) && (t = u(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t),
                                e[2] = n.slice(0, t)),
                                e.slice(0, 3))
                        }
                    },
                    filter: {
                        TAG: function(e) {
                            var t = e.replace(xe, we).toLowerCase();
                            return "*" === e ? function() {
                                    return !0
                                }
                                : function(e) {
                                    return e.nodeName && e.nodeName.toLowerCase() === t
                                }
                        },
                        CLASS: function(e) {
                            var t = B[e + " "];
                            return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && B(e, function(e) {
                                return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== Q && e.getAttribute("class") || "")
                            })
                        },
                        ATTR: function(e, n, i) {
                            return function(o) {
                                var a = t.attr(o, e);
                                return null == a ? "!=" === n : !n || (a += "",
                                    "=" === n ? a === i : "!=" === n ? a !== i : "^=" === n ? i && 0 === a.indexOf(i) : "*=" === n ? i && a.indexOf(i) > -1 : "$=" === n ? i && a.slice(-i.length) === i : "~=" === n ? (" " + a + " ").indexOf(i) > -1 : "|=" === n && (a === i || a.slice(0, i.length + 1) === i + "-"))
                            }
                        },
                        CHILD: function(e, t, n, i, o) {
                            var a = "nth" !== e.slice(0, 3)
                                , r = "last" !== e.slice(-4)
                                , s = "of-type" === t;
                            return 1 === i && 0 === o ? function(e) {
                                    return !!e.parentNode
                                }
                                : function(t, n, c) {
                                    var l, u, d, f, p, h, m = a !== r ? "nextSibling" : "previousSibling", g = t.parentNode, v = s && t.nodeName.toLowerCase(), y = !c && !s;
                                    if (g) {
                                        if (a) {
                                            for (; m; ) {
                                                for (d = t; d = d[m]; )
                                                    if (s ? d.nodeName.toLowerCase() === v : 1 === d.nodeType)
                                                        return !1;
                                                h = m = "only" === e && !h && "nextSibling"
                                            }
                                            return !0
                                        }
                                        if (h = [r ? g.firstChild : g.lastChild],
                                        r && y) {
                                            for (u = g[R] || (g[R] = {}),
                                                     l = u[e] || [],
                                                     p = l[0] === I && l[1],
                                                     f = l[0] === I && l[2],
                                                     d = p && g.childNodes[p]; d = ++p && d && d[m] || (f = p = 0) || h.pop(); )
                                                if (1 === d.nodeType && ++f && d === t) {
                                                    u[e] = [I, p, f];
                                                    break
                                                }
                                        } else if (y && (l = (t[R] || (t[R] = {}))[e]) && l[0] === I)
                                            f = l[1];
                                        else
                                            for (; (d = ++p && d && d[m] || (f = p = 0) || h.pop()) && ((s ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++f || (y && ((d[R] || (d[R] = {}))[e] = [I, f]),
                                            d !== t)); )
                                                ;
                                        return (f -= o) === i || f % i == 0 && f / i >= 0
                                    }
                                }
                        },
                        PSEUDO: function(e, n) {
                            var o, a = C.pseudos[e] || C.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                            return a[R] ? a(n) : a.length > 1 ? (o = [e, e, "", n],
                                    C.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function(e, t) {
                                        for (var i, o = a(e, n), r = o.length; r--; )
                                            i = ee.call(e, o[r]),
                                                e[i] = !(t[i] = o[r])
                                    }) : function(e) {
                                        return a(e, 0, o)
                                    }
                            ) : a
                        }
                    },
                    pseudos: {
                        not: i(function(e) {
                            var t = []
                                , n = []
                                , o = S(e.replace(se, "$1"));
                            return o[R] ? i(function(e, t, n, i) {
                                for (var a, r = o(e, null, i, []), s = e.length; s--; )
                                    (a = r[s]) && (e[s] = !(t[s] = a))
                            }) : function(e, i, a) {
                                return t[0] = e,
                                    o(t, null, a, n),
                                    !n.pop()
                            }
                        }),
                        has: i(function(e) {
                            return function(n) {
                                return t(e, n).length > 0
                            }
                        }),
                        contains: i(function(e) {
                            return function(t) {
                                return (t.textContent || t.innerText || k(t)).indexOf(e) > -1
                            }
                        }),
                        lang: i(function(e) {
                            return fe.test(e || "") || t.error("unsupported lang: " + e),
                                e = e.replace(xe, we).toLowerCase(),
                                function(t) {
                                    var n;
                                    do {
                                        if (n = L ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang"))
                                            return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-")
                                    } while ((t = t.parentNode) && 1 === t.nodeType);return !1
                                }
                        }),
                        target: function(t) {
                            var n = e.location && e.location.hash;
                            return n && n.slice(1) === t.id
                        },
                        root: function(e) {
                            return e === A
                        },
                        focus: function(e) {
                            return e === D.activeElement && (!D.hasFocus || D.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                        },
                        enabled: function(e) {
                            return !1 === e.disabled
                        },
                        disabled: function(e) {
                            return !0 === e.disabled
                        },
                        checked: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && !!e.checked || "option" === t && !!e.selected
                        },
                        selected: function(e) {
                            return e.parentNode && e.parentNode.selectedIndex,
                            !0 === e.selected
                        },
                        empty: function(e) {
                            for (e = e.firstChild; e; e = e.nextSibling)
                                if (e.nodeType < 6)
                                    return !1;
                            return !0
                        },
                        parent: function(e) {
                            return !C.pseudos.empty(e)
                        },
                        header: function(e) {
                            return me.test(e.nodeName)
                        },
                        input: function(e) {
                            return he.test(e.nodeName)
                        },
                        button: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && "button" === e.type || "button" === t
                        },
                        text: function(e) {
                            var t;
                            return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                        },
                        first: s(function() {
                            return [0]
                        }),
                        last: s(function(e, t) {
                            return [t - 1]
                        }),
                        eq: s(function(e, t, n) {
                            return [0 > n ? n + t : n]
                        }),
                        even: s(function(e, t) {
                            for (var n = 0; t > n; n += 2)
                                e.push(n);
                            return e
                        }),
                        odd: s(function(e, t) {
                            for (var n = 1; t > n; n += 2)
                                e.push(n);
                            return e
                        }),
                        lt: s(function(e, t, n) {
                            for (var i = 0 > n ? n + t : n; --i >= 0; )
                                e.push(i);
                            return e
                        }),
                        gt: s(function(e, t, n) {
                            for (var i = 0 > n ? n + t : n; ++i < t; )
                                e.push(i);
                            return e
                        })
                    }
                },
                C.pseudos.nth = C.pseudos.eq;
            for (x in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            })
                C.pseudos[x] = function(e) {
                    return function(t) {
                        return "input" === t.nodeName.toLowerCase() && t.type === e
                    }
                }(x);
            for (x in {
                submit: !0,
                reset: !0
            })
                C.pseudos[x] = function(e) {
                    return function(t) {
                        var n = t.nodeName.toLowerCase();
                        return ("input" === n || "button" === n) && t.type === e
                    }
                }(x);
            return l.prototype = C.filters = C.pseudos,
                C.setFilters = new l,
                S = t.compile = function(e, t) {
                    var n, i = [], o = [], a = U[e + " "];
                    if (!a) {
                        for (t || (t = u(e)),
                                 n = t.length; n--; )
                            a = g(t[n]),
                                a[R] ? i.push(a) : o.push(a);
                        a = U(e, v(o, i))
                    }
                    return a
                }
                ,
                w.sortStable = R.split("").sort(W).join("") === R,
                w.detectDuplicates = !!E,
                q(),
                w.sortDetached = o(function(e) {
                    return 1 & e.compareDocumentPosition(D.createElement("div"))
                }),
            o(function(e) {
                return e.innerHTML = "<a href='#'></a>",
                "#" === e.firstChild.getAttribute("href")
            }) || a("type|href|height|width", function(e, t, n) {
                return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
            }),
            w.attributes && o(function(e) {
                return e.innerHTML = "<input/>",
                    e.firstChild.setAttribute("value", ""),
                "" === e.firstChild.getAttribute("value")
            }) || a("value", function(e, t, n) {
                return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
            }),
            o(function(e) {
                return null == e.getAttribute("disabled")
            }) || a(te, function(e, t, n) {
                var i;
                return n ? void 0 : !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
            }),
                t
        }(e);
        re.find = de,
            re.expr = de.selectors,
            re.expr[":"] = re.expr.pseudos,
            re.unique = de.uniqueSort,
            re.text = de.getText,
            re.isXMLDoc = de.isXML,
            re.contains = de.contains;
        var fe = re.expr.match.needsContext
            , pe = /^<(\w+)\s*\/?>(?:<\/\1>|)$/
            , he = /^.[^:#\[\.,]*$/;
        re.filter = function(e, t, n) {
            var i = t[0];
            return n && (e = ":not(" + e + ")"),
                1 === t.length && 1 === i.nodeType ? re.find.matchesSelector(i, e) ? [i] : [] : re.find.matches(e, re.grep(t, function(e) {
                    return 1 === e.nodeType
                }))
        }
            ,
            re.fn.extend({
                find: function(e) {
                    var t, n = [], i = this, o = i.length;
                    if ("string" != typeof e)
                        return this.pushStack(re(e).filter(function() {
                            for (t = 0; o > t; t++)
                                if (re.contains(i[t], this))
                                    return !0
                        }));
                    for (t = 0; o > t; t++)
                        re.find(e, i[t], n);
                    return n = this.pushStack(o > 1 ? re.unique(n) : n),
                        n.selector = this.selector ? this.selector + " " + e : e,
                        n
                },
                filter: function(e) {
                    return this.pushStack(n(this, e || [], !1))
                },
                not: function(e) {
                    return this.pushStack(n(this, e || [], !0))
                },
                is: function(e) {
                    return !!n(this, "string" == typeof e && fe.test(e) ? re(e) : e || [], !1).length
                }
            });
        var me, ge = e.document, ve = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
        (re.fn.init = function(e, t) {
                var n, i;
                if (!e)
                    return this;
                if ("string" == typeof e) {
                    if (!(n = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : ve.exec(e)) || !n[1] && t)
                        return !t || t.jquery ? (t || me).find(e) : this.constructor(t).find(e);
                    if (n[1]) {
                        if (t = t instanceof re ? t[0] : t,
                            re.merge(this, re.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : ge, !0)),
                        pe.test(n[1]) && re.isPlainObject(t))
                            for (n in t)
                                re.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                        return this
                    }
                    if ((i = ge.getElementById(n[2])) && i.parentNode) {
                        if (i.id !== n[2])
                            return me.find(e);
                        this.length = 1,
                            this[0] = i
                    }
                    return this.context = ge,
                        this.selector = e,
                        this
                }
                return e.nodeType ? (this.context = this[0] = e,
                    this.length = 1,
                    this) : re.isFunction(e) ? void 0 !== me.ready ? me.ready(e) : e(re) : (void 0 !== e.selector && (this.selector = e.selector,
                    this.context = e.context),
                    re.makeArray(e, this))
            }
        ).prototype = re.fn,
            me = re(ge);
        var ye = /^(?:parents|prev(?:Until|All))/
            , be = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
        re.extend({
            dir: function(e, t, n) {
                for (var i = [], o = e[t]; o && 9 !== o.nodeType && (void 0 === n || 1 !== o.nodeType || !re(o).is(n)); )
                    1 === o.nodeType && i.push(o),
                        o = o[t];
                return i
            },
            sibling: function(e, t) {
                for (var n = []; e; e = e.nextSibling)
                    1 === e.nodeType && e !== t && n.push(e);
                return n
            }
        }),
            re.fn.extend({
                has: function(e) {
                    var t, n = re(e, this), i = n.length;
                    return this.filter(function() {
                        for (t = 0; i > t; t++)
                            if (re.contains(this, n[t]))
                                return !0
                    })
                },
                closest: function(e, t) {
                    for (var n, i = 0, o = this.length, a = [], r = fe.test(e) || "string" != typeof e ? re(e, t || this.context) : 0; o > i; i++)
                        for (n = this[i]; n && n !== t; n = n.parentNode)
                            if (n.nodeType < 11 && (r ? r.index(n) > -1 : 1 === n.nodeType && re.find.matchesSelector(n, e))) {
                                a.push(n);
                                break
                            }
                    return this.pushStack(a.length > 1 ? re.unique(a) : a)
                },
                index: function(e) {
                    return e ? "string" == typeof e ? re.inArray(this[0], re(e)) : re.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
                },
                add: function(e, t) {
                    var n = "string" == typeof e ? re(e, t) : re.makeArray(e && e.nodeType ? [e] : e)
                        , i = re.merge(this.get(), n);
                    return this.pushStack(re.unique(i))
                },
                addBack: function(e) {
                    return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
                }
            }),
            re.each({
                parent: function(e) {
                    var t = e.parentNode;
                    return t && 11 !== t.nodeType ? t : null
                },
                parents: function(e) {
                    return re.dir(e, "parentNode")
                },
                parentsUntil: function(e, t, n) {
                    return re.dir(e, "parentNode", n)
                },
                next: function(e) {
                    return i(e, "nextSibling")
                },
                prev: function(e) {
                    return i(e, "previousSibling")
                },
                nextAll: function(e) {
                    return re.dir(e, "nextSibling")
                },
                prevAll: function(e) {
                    return re.dir(e, "previousSibling")
                },
                nextUntil: function(e, t, n) {
                    return re.dir(e, "nextSibling", n)
                },
                prevUntil: function(e, t, n) {
                    return re.dir(e, "previousSibling", n)
                },
                siblings: function(e) {
                    return re.sibling((e.parentNode || {}).firstChild, e)
                },
                children: function(e) {
                    return re.sibling(e.firstChild)
                },
                contents: function(e) {
                    return re.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : re.merge([], e.childNodes)
                }
            }, function(e, t) {
                re.fn[e] = function(n, i) {
                    var o = re.map(this, t, n);
                    return "Until" !== e.slice(-5) && (i = n),
                    i && "string" == typeof i && (o = re.filter(i, o)),
                    this.length > 1 && (be[e] || (o = re.unique(o)),
                    ye.test(e) && (o = o.reverse())),
                        this.pushStack(o)
                }
            });
        var xe = /\S+/g
            , we = {};
        re.Callbacks = function(e) {
            e = "string" == typeof e ? we[e] || o(e) : re.extend({}, e);
            var t, n, i, a, r, s, c = [], l = !e.once && [], u = function(o) {
                for (n = e.memory && o,
                         i = !0,
                         r = s || 0,
                         s = 0,
                         a = c.length,
                         t = !0; c && a > r; r++)
                    if (!1 === c[r].apply(o[0], o[1]) && e.stopOnFalse) {
                        n = !1;
                        break
                    }
                t = !1,
                c && (l ? l.length && u(l.shift()) : n ? c = [] : d.disable())
            }, d = {
                add: function() {
                    if (c) {
                        var i = c.length;
                        !function t(n) {
                            re.each(n, function(n, i) {
                                var o = re.type(i);
                                "function" === o ? e.unique && d.has(i) || c.push(i) : i && i.length && "string" !== o && t(i)
                            })
                        }(arguments),
                            t ? a = c.length : n && (s = i,
                                u(n))
                    }
                    return this
                },
                remove: function() {
                    return c && re.each(arguments, function(e, n) {
                        for (var i; (i = re.inArray(n, c, i)) > -1; )
                            c.splice(i, 1),
                            t && (a >= i && a--,
                            r >= i && r--)
                    }),
                        this
                },
                has: function(e) {
                    return e ? re.inArray(e, c) > -1 : !(!c || !c.length)
                },
                empty: function() {
                    return c = [],
                        a = 0,
                        this
                },
                disable: function() {
                    return c = l = n = void 0,
                        this
                },
                disabled: function() {
                    return !c
                },
                lock: function() {
                    return l = void 0,
                    n || d.disable(),
                        this
                },
                locked: function() {
                    return !l
                },
                fireWith: function(e, n) {
                    return !c || i && !l || (n = n || [],
                        n = [e, n.slice ? n.slice() : n],
                        t ? l.push(n) : u(n)),
                        this
                },
                fire: function() {
                    return d.fireWith(this, arguments),
                        this
                },
                fired: function() {
                    return !!i
                }
            };
            return d
        }
            ,
            re.extend({
                Deferred: function(e) {
                    var t = [["resolve", "done", re.Callbacks("once memory"), "resolved"], ["reject", "fail", re.Callbacks("once memory"), "rejected"], ["notify", "progress", re.Callbacks("memory")]]
                        , n = "pending"
                        , i = {
                        state: function() {
                            return n
                        },
                        always: function() {
                            return o.done(arguments).fail(arguments),
                                this
                        },
                        then: function() {
                            var e = arguments;
                            return re.Deferred(function(n) {
                                re.each(t, function(t, a) {
                                    var r = re.isFunction(e[t]) && e[t];
                                    o[a[1]](function() {
                                        var e = r && r.apply(this, arguments);
                                        e && re.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[a[0] + "With"](this === i ? n.promise() : this, r ? [e] : arguments)
                                    })
                                }),
                                    e = null
                            }).promise()
                        },
                        promise: function(e) {
                            return null != e ? re.extend(e, i) : i
                        }
                    }
                        , o = {};
                    return i.pipe = i.then,
                        re.each(t, function(e, a) {
                            var r = a[2]
                                , s = a[3];
                            i[a[1]] = r.add,
                            s && r.add(function() {
                                n = s
                            }, t[1 ^ e][2].disable, t[2][2].lock),
                                o[a[0]] = function() {
                                    return o[a[0] + "With"](this === o ? i : this, arguments),
                                        this
                                }
                                ,
                                o[a[0] + "With"] = r.fireWith
                        }),
                        i.promise(o),
                    e && e.call(o, o),
                        o
                },
                when: function(e) {
                    var t, n, i, o = 0, a = G.call(arguments), r = a.length, s = 1 !== r || e && re.isFunction(e.promise) ? r : 0, c = 1 === s ? e : re.Deferred(), l = function(e, n, i) {
                        return function(o) {
                            n[e] = this,
                                i[e] = arguments.length > 1 ? G.call(arguments) : o,
                                i === t ? c.notifyWith(n, i) : --s || c.resolveWith(n, i)
                        }
                    };
                    if (r > 1)
                        for (t = new Array(r),
                                 n = new Array(r),
                                 i = new Array(r); r > o; o++)
                            a[o] && re.isFunction(a[o].promise) ? a[o].promise().done(l(o, i, a)).fail(c.reject).progress(l(o, n, t)) : --s;
                    return s || c.resolveWith(i, a),
                        c.promise()
                }
            });
        var je;
        re.fn.ready = function(e) {
            return re.ready.promise().done(e),
                this
        }
            ,
            re.extend({
                isReady: !1,
                readyWait: 1,
                holdReady: function(e) {
                    e ? re.readyWait++ : re.ready(!0)
                },
                ready: function(e) {
                    if (!0 === e ? !--re.readyWait : !re.isReady) {
                        if (!ge.body)
                            return setTimeout(re.ready);
                        re.isReady = !0,
                        !0 !== e && --re.readyWait > 0 || (je.resolveWith(ge, [re]),
                        re.fn.trigger && re(ge).trigger("ready").off("ready"))
                    }
                }
            }),
            re.ready.promise = function(t) {
                if (!je)
                    if (je = re.Deferred(),
                    "complete" === ge.readyState)
                        setTimeout(re.ready);
                    else if (ge.addEventListener)
                        ge.addEventListener("DOMContentLoaded", r, !1),
                            e.addEventListener("load", r, !1);
                    else {
                        ge.attachEvent("onreadystatechange", r),
                            e.attachEvent("onload", r);
                        var n = !1;
                        try {
                            n = null == e.frameElement && ge.documentElement
                        } catch (e) {}
                        n && n.doScroll && function e() {
                            if (!re.isReady) {
                                try {
                                    n.doScroll("left")
                                } catch (t) {
                                    return setTimeout(e, 50)
                                }
                                a(),
                                    re.ready()
                            }
                        }()
                    }
                return je.promise(t)
            }
        ;
        var Ce, ke = "undefined";
        for (Ce in re(ne))
            break;
        ne.ownLast = "0" !== Ce,
            ne.inlineBlockNeedsLayout = !1,
            re(function() {
                var e, t, n = ge.getElementsByTagName("body")[0];
                n && (e = ge.createElement("div"),
                    e.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px",
                    t = ge.createElement("div"),
                    n.appendChild(e).appendChild(t),
                typeof t.style.zoom !== ke && (t.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1",
                (ne.inlineBlockNeedsLayout = 3 === t.offsetWidth) && (n.style.zoom = 1)),
                    n.removeChild(e),
                    e = t = null)
            }),
            function() {
                var e = ge.createElement("div");
                if (null == ne.deleteExpando) {
                    ne.deleteExpando = !0;
                    try {
                        delete e.test
                    } catch (e) {
                        ne.deleteExpando = !1
                    }
                }
                e = null
            }(),
            re.acceptData = function(e) {
                var t = re.noData[(e.nodeName + " ").toLowerCase()]
                    , n = +e.nodeType || 1;
                return (1 === n || 9 === n) && (!t || !0 !== t && e.getAttribute("classid") === t)
            }
        ;
        var Te = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/
            , Se = /([A-Z])/g;
        re.extend({
            cache: {},
            noData: {
                "applet ": !0,
                "embed ": !0,
                "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            },
            hasData: function(e) {
                return !!(e = e.nodeType ? re.cache[e[re.expando]] : e[re.expando]) && !c(e)
            },
            data: function(e, t, n) {
                return l(e, t, n)
            },
            removeData: function(e, t) {
                return u(e, t)
            },
            _data: function(e, t, n) {
                return l(e, t, n, !0)
            },
            _removeData: function(e, t) {
                return u(e, t, !0)
            }
        }),
            re.fn.extend({
                data: function(e, t) {
                    var n, i, o, a = this[0], r = a && a.attributes;
                    if (void 0 === e) {
                        if (this.length && (o = re.data(a),
                        1 === a.nodeType && !re._data(a, "parsedAttrs"))) {
                            for (n = r.length; n--; )
                                i = r[n].name,
                                0 === i.indexOf("data-") && (i = re.camelCase(i.slice(5)),
                                    s(a, i, o[i]));
                            re._data(a, "parsedAttrs", !0)
                        }
                        return o
                    }
                    return "object" == typeof e ? this.each(function() {
                        re.data(this, e)
                    }) : arguments.length > 1 ? this.each(function() {
                        re.data(this, e, t)
                    }) : a ? s(a, e, re.data(a, e)) : void 0
                },
                removeData: function(e) {
                    return this.each(function() {
                        re.removeData(this, e)
                    })
                }
            }),
            re.extend({
                queue: function(e, t, n) {
                    var i;
                    return e ? (t = (t || "fx") + "queue",
                        i = re._data(e, t),
                    n && (!i || re.isArray(n) ? i = re._data(e, t, re.makeArray(n)) : i.push(n)),
                    i || []) : void 0
                },
                dequeue: function(e, t) {
                    t = t || "fx";
                    var n = re.queue(e, t)
                        , i = n.length
                        , o = n.shift()
                        , a = re._queueHooks(e, t)
                        , r = function() {
                        re.dequeue(e, t)
                    };
                    "inprogress" === o && (o = n.shift(),
                        i--),
                    o && ("fx" === t && n.unshift("inprogress"),
                        delete a.stop,
                        o.call(e, r, a)),
                    !i && a && a.empty.fire()
                },
                _queueHooks: function(e, t) {
                    var n = t + "queueHooks";
                    return re._data(e, n) || re._data(e, n, {
                        empty: re.Callbacks("once memory").add(function() {
                            re._removeData(e, t + "queue"),
                                re._removeData(e, n)
                        })
                    })
                }
            }),
            re.fn.extend({
                queue: function(e, t) {
                    var n = 2;
                    return "string" != typeof e && (t = e,
                        e = "fx",
                        n--),
                        arguments.length < n ? re.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                            var n = re.queue(this, e, t);
                            re._queueHooks(this, e),
                            "fx" === e && "inprogress" !== n[0] && re.dequeue(this, e)
                        })
                },
                dequeue: function(e) {
                    return this.each(function() {
                        re.dequeue(this, e)
                    })
                },
                clearQueue: function(e) {
                    return this.queue(e || "fx", [])
                },
                promise: function(e, t) {
                    var n, i = 1, o = re.Deferred(), a = this, r = this.length, s = function() {
                        --i || o.resolveWith(a, [a])
                    };
                    for ("string" != typeof e && (t = e,
                        e = void 0),
                             e = e || "fx"; r--; )
                        (n = re._data(a[r], e + "queueHooks")) && n.empty && (i++,
                            n.empty.add(s));
                    return s(),
                        o.promise(t)
                }
            });
        var $e = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source
            , _e = ["Top", "Right", "Bottom", "Left"]
            , Ee = function(e, t) {
            return e = t || e,
            "none" === re.css(e, "display") || !re.contains(e.ownerDocument, e)
        }
            , qe = re.access = function(e, t, n, i, o, a, r) {
            var s = 0
                , c = e.length
                , l = null == n;
            if ("object" === re.type(n)) {
                o = !0;
                for (s in n)
                    re.access(e, t, s, n[s], !0, a, r)
            } else if (void 0 !== i && (o = !0,
            re.isFunction(i) || (r = !0),
            l && (r ? (t.call(e, i),
                t = null) : (l = t,
                    t = function(e, t, n) {
                        return l.call(re(e), n)
                    }
            )),
                t))
                for (; c > s; s++)
                    t(e[s], n, r ? i : i.call(e[s], s, t(e[s], n)));
            return o ? e : l ? t.call(e) : c ? t(e[0], n) : a
        }
            , De = /^(?:checkbox|radio)$/i;
        !function() {
            var e = ge.createDocumentFragment()
                , t = ge.createElement("div")
                , n = ge.createElement("input");
            if (n.type = "checkbox",
                t.setAttribute("className", "t"),
                t.innerHTML = "  <link/><table></table><a href='/a'>a</a>",
                ne.leadingWhitespace = 3 === t.firstChild.nodeType,
                ne.tbody = !t.getElementsByTagName("tbody").length,
                ne.htmlSerialize = !!t.getElementsByTagName("link").length,
                ne.html5Clone = "<:nav></:nav>" !== ge.createElement("nav").cloneNode(!0).outerHTML,
                n.checked = !0,
                ne.noCloneChecked = n.cloneNode(!0).checked,
                e.appendChild(n),
                ne.appendChecked = n.checked,
                e.appendChild(t),
                t.innerHTML = "<input type='radio' checked='checked' name='t'/>",
                ne.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked,
                ne.noCloneEvent = !0,
            t.attachEvent && (t.attachEvent("onclick", function() {
                ne.noCloneEvent = !1
            }),
                t.cloneNode(!0).click()),
            null == ne.deleteExpando) {
                ne.deleteExpando = !0;
                try {
                    delete t.test
                } catch (e) {
                    ne.deleteExpando = !1
                }
            }
            e = t = n = null
        }(),
            function() {
                var t, n, i = ge.createElement("div");
                for (t in {
                    submit: !0,
                    change: !0,
                    focusin: !0
                })
                    n = "on" + t,
                    (ne[t + "Bubbles"] = n in e) || (i.setAttribute(n, "t"),
                        ne[t + "Bubbles"] = !1 === i.attributes[n].expando);
                i = null
            }();
        var Ae = /^(?:input|select|textarea)$/i
            , Le = /^key/
            , Ne = /^(?:mouse|contextmenu)|click/
            , Me = /^(?:focusinfocus|focusoutblur)$/
            , Pe = /^([^.]*)(?:\.(.+)|)$/;
        re.event = {
            global: {},
            add: function(e, t, n, i, o) {
                var a, r, s, c, l, u, d, f, p, h, m, g = re._data(e);
                if (g) {
                    for (n.handler && (c = n,
                        n = c.handler,
                        o = c.selector),
                         n.guid || (n.guid = re.guid++),
                         (r = g.events) || (r = g.events = {}),
                         (u = g.handle) || (u = g.handle = function(e) {
                             return typeof re === ke || e && re.event.triggered === e.type ? void 0 : re.event.dispatch.apply(u.elem, arguments)
                         }
                             ,
                             u.elem = e),
                             t = (t || "").match(xe) || [""],
                             s = t.length; s--; )
                        a = Pe.exec(t[s]) || [],
                            p = m = a[1],
                            h = (a[2] || "").split(".").sort(),
                        p && (l = re.event.special[p] || {},
                            p = (o ? l.delegateType : l.bindType) || p,
                            l = re.event.special[p] || {},
                            d = re.extend({
                                type: p,
                                origType: m,
                                data: i,
                                handler: n,
                                guid: n.guid,
                                selector: o,
                                needsContext: o && re.expr.match.needsContext.test(o),
                                namespace: h.join(".")
                            }, c),
                        (f = r[p]) || (f = r[p] = [],
                            f.delegateCount = 0,
                        l.setup && !1 !== l.setup.call(e, i, h, u) || (e.addEventListener ? e.addEventListener(p, u, !1) : e.attachEvent && e.attachEvent("on" + p, u))),
                        l.add && (l.add.call(e, d),
                        d.handler.guid || (d.handler.guid = n.guid)),
                            o ? f.splice(f.delegateCount++, 0, d) : f.push(d),
                            re.event.global[p] = !0);
                    e = null
                }
            },
            remove: function(e, t, n, i, o) {
                var a, r, s, c, l, u, d, f, p, h, m, g = re.hasData(e) && re._data(e);
                if (g && (u = g.events)) {
                    for (t = (t || "").match(xe) || [""],
                             l = t.length; l--; )
                        if (s = Pe.exec(t[l]) || [],
                            p = m = s[1],
                            h = (s[2] || "").split(".").sort(),
                            p) {
                            for (d = re.event.special[p] || {},
                                     p = (i ? d.delegateType : d.bindType) || p,
                                     f = u[p] || [],
                                     s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                                     c = a = f.length; a--; )
                                r = f[a],
                                !o && m !== r.origType || n && n.guid !== r.guid || s && !s.test(r.namespace) || i && i !== r.selector && ("**" !== i || !r.selector) || (f.splice(a, 1),
                                r.selector && f.delegateCount--,
                                d.remove && d.remove.call(e, r));
                            c && !f.length && (d.teardown && !1 !== d.teardown.call(e, h, g.handle) || re.removeEvent(e, p, g.handle),
                                delete u[p])
                        } else
                            for (p in u)
                                re.event.remove(e, p + t[l], n, i, !0);
                    re.isEmptyObject(u) && (delete g.handle,
                        re._removeData(e, "events"))
                }
            },
            trigger: function(t, n, i, o) {
                var a, r, s, c, l, u, d, f = [i || ge], p = ee.call(t, "type") ? t.type : t, h = ee.call(t, "namespace") ? t.namespace.split(".") : [];
                if (s = u = i = i || ge,
                3 !== i.nodeType && 8 !== i.nodeType && !Me.test(p + re.event.triggered) && (p.indexOf(".") >= 0 && (h = p.split("."),
                    p = h.shift(),
                    h.sort()),
                    r = p.indexOf(":") < 0 && "on" + p,
                    t = t[re.expando] ? t : new re.Event(p,"object" == typeof t && t),
                    t.isTrigger = o ? 2 : 3,
                    t.namespace = h.join("."),
                    t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null,
                    t.result = void 0,
                t.target || (t.target = i),
                    n = null == n ? [t] : re.makeArray(n, [t]),
                    l = re.event.special[p] || {},
                o || !l.trigger || !1 !== l.trigger.apply(i, n))) {
                    if (!o && !l.noBubble && !re.isWindow(i)) {
                        for (c = l.delegateType || p,
                             Me.test(c + p) || (s = s.parentNode); s; s = s.parentNode)
                            f.push(s),
                                u = s;
                        u === (i.ownerDocument || ge) && f.push(u.defaultView || u.parentWindow || e)
                    }
                    for (d = 0; (s = f[d++]) && !t.isPropagationStopped(); )
                        t.type = d > 1 ? c : l.bindType || p,
                            a = (re._data(s, "events") || {})[t.type] && re._data(s, "handle"),
                        a && a.apply(s, n),
                        (a = r && s[r]) && a.apply && re.acceptData(s) && (t.result = a.apply(s, n),
                        !1 === t.result && t.preventDefault());
                    if (t.type = p,
                    !o && !t.isDefaultPrevented() && (!l._default || !1 === l._default.apply(f.pop(), n)) && re.acceptData(i) && r && i[p] && !re.isWindow(i)) {
                        u = i[r],
                        u && (i[r] = null),
                            re.event.triggered = p;
                        try {
                            i[p]()
                        } catch (e) {}
                        re.event.triggered = void 0,
                        u && (i[r] = u)
                    }
                    return t.result
                }
            },
            dispatch: function(e) {
                e = re.event.fix(e);
                var t, n, i, o, a, r = [], s = G.call(arguments), c = (re._data(this, "events") || {})[e.type] || [], l = re.event.special[e.type] || {};
                if (s[0] = e,
                    e.delegateTarget = this,
                !l.preDispatch || !1 !== l.preDispatch.call(this, e)) {
                    for (r = re.event.handlers.call(this, e, c),
                             t = 0; (o = r[t++]) && !e.isPropagationStopped(); )
                        for (e.currentTarget = o.elem,
                                 a = 0; (i = o.handlers[a++]) && !e.isImmediatePropagationStopped(); )
                            (!e.namespace_re || e.namespace_re.test(i.namespace)) && (e.handleObj = i,
                                e.data = i.data,
                            void 0 !== (n = ((re.event.special[i.origType] || {}).handle || i.handler).apply(o.elem, s)) && !1 === (e.result = n) && (e.preventDefault(),
                                e.stopPropagation()));
                    return l.postDispatch && l.postDispatch.call(this, e),
                        e.result
                }
            },
            handlers: function(e, t) {
                var n, i, o, a, r = [], s = t.delegateCount, c = e.target;
                if (s && c.nodeType && (!e.button || "click" !== e.type))
                    for (; c != this; c = c.parentNode || this)
                        if (1 === c.nodeType && (!0 !== c.disabled || "click" !== e.type)) {
                            for (o = [],
                                     a = 0; s > a; a++)
                                i = t[a],
                                    n = i.selector + " ",
                                void 0 === o[n] && (o[n] = i.needsContext ? re(n, this).index(c) >= 0 : re.find(n, this, null, [c]).length),
                                o[n] && o.push(i);
                            o.length && r.push({
                                elem: c,
                                handlers: o
                            })
                        }
                return s < t.length && r.push({
                    elem: this,
                    handlers: t.slice(s)
                }),
                    r
            },
            fix: function(e) {
                if (e[re.expando])
                    return e;
                var t, n, i, o = e.type, a = e, r = this.fixHooks[o];
                for (r || (this.fixHooks[o] = r = Ne.test(o) ? this.mouseHooks : Le.test(o) ? this.keyHooks : {}),
                         i = r.props ? this.props.concat(r.props) : this.props,
                         e = new re.Event(a),
                         t = i.length; t--; )
                    n = i[t],
                        e[n] = a[n];
                return e.target || (e.target = a.srcElement || ge),
                3 === e.target.nodeType && (e.target = e.target.parentNode),
                    e.metaKey = !!e.metaKey,
                    r.filter ? r.filter(e, a) : e
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode),
                        e
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(e, t) {
                    var n, i, o, a = t.button, r = t.fromElement;
                    return null == e.pageX && null != t.clientX && (i = e.target.ownerDocument || ge,
                        o = i.documentElement,
                        n = i.body,
                        e.pageX = t.clientX + (o && o.scrollLeft || n && n.scrollLeft || 0) - (o && o.clientLeft || n && n.clientLeft || 0),
                        e.pageY = t.clientY + (o && o.scrollTop || n && n.scrollTop || 0) - (o && o.clientTop || n && n.clientTop || 0)),
                    !e.relatedTarget && r && (e.relatedTarget = r === e.target ? t.toElement : r),
                    e.which || void 0 === a || (e.which = 1 & a ? 1 : 2 & a ? 3 : 4 & a ? 2 : 0),
                        e
                }
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function() {
                        if (this !== p() && this.focus)
                            try {
                                return this.focus(),
                                    !1
                            } catch (e) {}
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        return this === p() && this.blur ? (this.blur(),
                            !1) : void 0
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        return re.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(),
                            !1) : void 0
                    },
                    _default: function(e) {
                        return re.nodeName(e.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function(e) {
                        void 0 !== e.result && (e.originalEvent.returnValue = e.result)
                    }
                }
            },
            simulate: function(e, t, n, i) {
                var o = re.extend(new re.Event, n, {
                    type: e,
                    isSimulated: !0,
                    originalEvent: {}
                });
                i ? re.event.trigger(o, null, t) : re.event.dispatch.call(t, o),
                o.isDefaultPrevented() && n.preventDefault()
            }
        },
            re.removeEvent = ge.removeEventListener ? function(e, t, n) {
                    e.removeEventListener && e.removeEventListener(t, n, !1)
                }
                : function(e, t, n) {
                    var i = "on" + t;
                    e.detachEvent && (typeof e[i] === ke && (e[i] = null),
                        e.detachEvent(i, n))
                }
            ,
            re.Event = function(e, t) {
                return this instanceof re.Event ? (e && e.type ? (this.originalEvent = e,
                    this.type = e.type,
                    this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && (!1 === e.returnValue || e.getPreventDefault && e.getPreventDefault()) ? d : f) : this.type = e,
                t && re.extend(this, t),
                    this.timeStamp = e && e.timeStamp || re.now(),
                    void (this[re.expando] = !0)) : new re.Event(e,t)
            }
            ,
            re.Event.prototype = {
                isDefaultPrevented: f,
                isPropagationStopped: f,
                isImmediatePropagationStopped: f,
                preventDefault: function() {
                    var e = this.originalEvent;
                    this.isDefaultPrevented = d,
                    e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
                },
                stopPropagation: function() {
                    var e = this.originalEvent;
                    this.isPropagationStopped = d,
                    e && (e.stopPropagation && e.stopPropagation(),
                        e.cancelBubble = !0)
                },
                stopImmediatePropagation: function() {
                    this.isImmediatePropagationStopped = d,
                        this.stopPropagation()
                }
            },
            re.each({
                mouseenter: "mouseover",
                mouseleave: "mouseout"
            }, function(e, t) {
                re.event.special[e] = {
                    delegateType: t,
                    bindType: t,
                    handle: function(e) {
                        var n, i = this, o = e.relatedTarget, a = e.handleObj;
                        return (!o || o !== i && !re.contains(i, o)) && (e.type = a.origType,
                            n = a.handler.apply(this, arguments),
                            e.type = t),
                            n
                    }
                }
            }),
        ne.submitBubbles || (re.event.special.submit = {
            setup: function() {
                return !re.nodeName(this, "form") && void re.event.add(this, "click._submit keypress._submit", function(e) {
                    var t = e.target
                        , n = re.nodeName(t, "input") || re.nodeName(t, "button") ? t.form : void 0;
                    n && !re._data(n, "submitBubbles") && (re.event.add(n, "submit._submit", function(e) {
                        e._submit_bubble = !0
                    }),
                        re._data(n, "submitBubbles", !0))
                })
            },
            postDispatch: function(e) {
                e._submit_bubble && (delete e._submit_bubble,
                this.parentNode && !e.isTrigger && re.event.simulate("submit", this.parentNode, e, !0))
            },
            teardown: function() {
                return !re.nodeName(this, "form") && void re.event.remove(this, "._submit")
            }
        }),
        ne.changeBubbles || (re.event.special.change = {
            setup: function() {
                return Ae.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (re.event.add(this, "propertychange._change", function(e) {
                    "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
                }),
                    re.event.add(this, "click._change", function(e) {
                        this._just_changed && !e.isTrigger && (this._just_changed = !1),
                            re.event.simulate("change", this, e, !0)
                    })),
                    !1) : void re.event.add(this, "beforeactivate._change", function(e) {
                    var t = e.target;
                    Ae.test(t.nodeName) && !re._data(t, "changeBubbles") && (re.event.add(t, "change._change", function(e) {
                        !this.parentNode || e.isSimulated || e.isTrigger || re.event.simulate("change", this.parentNode, e, !0)
                    }),
                        re._data(t, "changeBubbles", !0))
                })
            },
            handle: function(e) {
                var t = e.target;
                return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
            },
            teardown: function() {
                return re.event.remove(this, "._change"),
                    !Ae.test(this.nodeName)
            }
        }),
        ne.focusinBubbles || re.each({
            focus: "focusin",
            blur: "focusout"
        }, function(e, t) {
            var n = function(e) {
                re.event.simulate(t, e.target, re.event.fix(e), !0)
            };
            re.event.special[t] = {
                setup: function() {
                    var i = this.ownerDocument || this
                        , o = re._data(i, t);
                    o || i.addEventListener(e, n, !0),
                        re._data(i, t, (o || 0) + 1)
                },
                teardown: function() {
                    var i = this.ownerDocument || this
                        , o = re._data(i, t) - 1;
                    o ? re._data(i, t, o) : (i.removeEventListener(e, n, !0),
                        re._removeData(i, t))
                }
            }
        }),
            re.fn.extend({
                on: function(e, t, n, i, o) {
                    var a, r;
                    if ("object" == typeof e) {
                        "string" != typeof t && (n = n || t,
                            t = void 0);
                        for (a in e)
                            this.on(a, t, n, e[a], o);
                        return this
                    }
                    if (null == n && null == i ? (i = t,
                        n = t = void 0) : null == i && ("string" == typeof t ? (i = n,
                        n = void 0) : (i = n,
                        n = t,
                        t = void 0)),
                    !1 === i)
                        i = f;
                    else if (!i)
                        return this;
                    return 1 === o && (r = i,
                        i = function(e) {
                            return re().off(e),
                                r.apply(this, arguments)
                        }
                        ,
                        i.guid = r.guid || (r.guid = re.guid++)),
                        this.each(function() {
                            re.event.add(this, e, i, n, t)
                        })
                },
                one: function(e, t, n, i) {
                    return this.on(e, t, n, i, 1)
                },
                off: function(e, t, n) {
                    var i, o;
                    if (e && e.preventDefault && e.handleObj)
                        return i = e.handleObj,
                            re(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler),
                            this;
                    if ("object" == typeof e) {
                        for (o in e)
                            this.off(o, t, e[o]);
                        return this
                    }
                    return (!1 === t || "function" == typeof t) && (n = t,
                        t = void 0),
                    !1 === n && (n = f),
                        this.each(function() {
                            re.event.remove(this, e, n, t)
                        })
                },
                trigger: function(e, t) {
                    return this.each(function() {
                        re.event.trigger(e, t, this)
                    })
                },
                triggerHandler: function(e, t) {
                    var n = this[0];
                    return n ? re.event.trigger(e, t, n, !0) : void 0
                }
            });
        var ze = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video"
            , Re = / jQuery\d+="(?:null|\d+)"/g
            , He = new RegExp("<(?:" + ze + ")[\\s/>]","i")
            , Ie = /^\s+/
            , Oe = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi
            , Be = /<([\w:]+)/
            , Fe = /<tbody/i
            , Ue = /<|&#?\w+;/
            , We = /<(?:script|style|link)/i
            , Qe = /checked\s*(?:[^=]|=\s*.checked.)/i
            , Ve = /^$|\/(?:java|ecma)script/i
            , Ge = /^true\/(.*)/
            , Ye = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g
            , Xe = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ne.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }
            , Ke = h(ge)
            , Je = Ke.appendChild(ge.createElement("div"));
        Xe.optgroup = Xe.option,
            Xe.tbody = Xe.tfoot = Xe.colgroup = Xe.caption = Xe.thead,
            Xe.th = Xe.td,
            re.extend({
                clone: function(e, t, n) {
                    var i, o, a, r, s, c = re.contains(e.ownerDocument, e);
                    if (ne.html5Clone || re.isXMLDoc(e) || !He.test("<" + e.nodeName + ">") ? a = e.cloneNode(!0) : (Je.innerHTML = e.outerHTML,
                        Je.removeChild(a = Je.firstChild)),
                        !(ne.noCloneEvent && ne.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || re.isXMLDoc(e)))
                        for (i = m(a),
                                 s = m(e),
                                 r = 0; null != (o = s[r]); ++r)
                            i[r] && j(o, i[r]);
                    if (t)
                        if (n)
                            for (s = s || m(e),
                                     i = i || m(a),
                                     r = 0; null != (o = s[r]); r++)
                                w(o, i[r]);
                        else
                            w(e, a);
                    return i = m(a, "script"),
                    i.length > 0 && x(i, !c && m(e, "script")),
                        i = s = o = null,
                        a
                },
                buildFragment: function(e, t, n, i) {
                    for (var o, a, r, s, c, l, u, d = e.length, f = h(t), p = [], v = 0; d > v; v++)
                        if ((a = e[v]) || 0 === a)
                            if ("object" === re.type(a))
                                re.merge(p, a.nodeType ? [a] : a);
                            else if (Ue.test(a)) {
                                for (s = s || f.appendChild(t.createElement("div")),
                                         c = (Be.exec(a) || ["", ""])[1].toLowerCase(),
                                         u = Xe[c] || Xe._default,
                                         s.innerHTML = u[1] + a.replace(Oe, "<$1></$2>") + u[2],
                                         o = u[0]; o--; )
                                    s = s.lastChild;
                                if (!ne.leadingWhitespace && Ie.test(a) && p.push(t.createTextNode(Ie.exec(a)[0])),
                                    !ne.tbody)
                                    for (a = "table" !== c || Fe.test(a) ? "<table>" !== u[1] || Fe.test(a) ? 0 : s : s.firstChild,
                                             o = a && a.childNodes.length; o--; )
                                        re.nodeName(l = a.childNodes[o], "tbody") && !l.childNodes.length && a.removeChild(l);
                                for (re.merge(p, s.childNodes),
                                         s.textContent = ""; s.firstChild; )
                                    s.removeChild(s.firstChild);
                                s = f.lastChild
                            } else
                                p.push(t.createTextNode(a));
                    for (s && f.removeChild(s),
                         ne.appendChecked || re.grep(m(p, "input"), g),
                             v = 0; a = p[v++]; )
                        if ((!i || -1 === re.inArray(a, i)) && (r = re.contains(a.ownerDocument, a),
                            s = m(f.appendChild(a), "script"),
                        r && x(s),
                            n))
                            for (o = 0; a = s[o++]; )
                                Ve.test(a.type || "") && n.push(a);
                    return s = null,
                        f
                },
                cleanData: function(e, t) {
                    for (var n, i, o, a, r = 0, s = re.expando, c = re.cache, l = ne.deleteExpando, u = re.event.special; null != (n = e[r]); r++)
                        if ((t || re.acceptData(n)) && (o = n[s],
                            a = o && c[o])) {
                            if (a.events)
                                for (i in a.events)
                                    u[i] ? re.event.remove(n, i) : re.removeEvent(n, i, a.handle);
                            c[o] && (delete c[o],
                                l ? delete n[s] : typeof n.removeAttribute !== ke ? n.removeAttribute(s) : n[s] = null,
                                V.push(o))
                        }
                }
            }),
            re.fn.extend({
                text: function(e) {
                    return qe(this, function(e) {
                        return void 0 === e ? re.text(this) : this.empty().append((this[0] && this[0].ownerDocument || ge).createTextNode(e))
                    }, null, e, arguments.length)
                },
                append: function() {
                    return this.domManip(arguments, function(e) {
                        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                            v(this, e).appendChild(e)
                        }
                    })
                },
                prepend: function() {
                    return this.domManip(arguments, function(e) {
                        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                            var t = v(this, e);
                            t.insertBefore(e, t.firstChild)
                        }
                    })
                },
                before: function() {
                    return this.domManip(arguments, function(e) {
                        this.parentNode && this.parentNode.insertBefore(e, this)
                    })
                },
                after: function() {
                    return this.domManip(arguments, function(e) {
                        this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                    })
                },
                remove: function(e, t) {
                    for (var n, i = e ? re.filter(e, this) : this, o = 0; null != (n = i[o]); o++)
                        t || 1 !== n.nodeType || re.cleanData(m(n)),
                        n.parentNode && (t && re.contains(n.ownerDocument, n) && x(m(n, "script")),
                            n.parentNode.removeChild(n));
                    return this
                },
                empty: function() {
                    for (var e, t = 0; null != (e = this[t]); t++) {
                        for (1 === e.nodeType && re.cleanData(m(e, !1)); e.firstChild; )
                            e.removeChild(e.firstChild);
                        e.options && re.nodeName(e, "select") && (e.options.length = 0)
                    }
                    return this
                },
                clone: function(e, t) {
                    return e = null != e && e,
                        t = null == t ? e : t,
                        this.map(function() {
                            return re.clone(this, e, t)
                        })
                },
                html: function(e) {
                    return qe(this, function(e) {
                        var t = this[0] || {}
                            , n = 0
                            , i = this.length;
                        if (void 0 === e)
                            return 1 === t.nodeType ? t.innerHTML.replace(Re, "") : void 0;
                        if (!("string" != typeof e || We.test(e) || !ne.htmlSerialize && He.test(e) || !ne.leadingWhitespace && Ie.test(e) || Xe[(Be.exec(e) || ["", ""])[1].toLowerCase()])) {
                            e = e.replace(Oe, "<$1></$2>");
                            try {
                                for (; i > n; n++)
                                    t = this[n] || {},
                                    1 === t.nodeType && (re.cleanData(m(t, !1)),
                                        t.innerHTML = e);
                                t = 0
                            } catch (e) {}
                        }
                        t && this.empty().append(e)
                    }, null, e, arguments.length)
                },
                replaceWith: function() {
                    var e = arguments[0];
                    return this.domManip(arguments, function(t) {
                        e = this.parentNode,
                            re.cleanData(m(this)),
                        e && e.replaceChild(t, this)
                    }),
                        e && (e.length || e.nodeType) ? this : this.remove()
                },
                detach: function(e) {
                    return this.remove(e, !0)
                },
                domManip: function(e, t) {
                    e = Y.apply([], e);
                    var n, i, o, a, r, s, c = 0, l = this.length, u = this, d = l - 1, f = e[0], p = re.isFunction(f);
                    if (p || l > 1 && "string" == typeof f && !ne.checkClone && Qe.test(f))
                        return this.each(function(n) {
                            var i = u.eq(n);
                            p && (e[0] = f.call(this, n, i.html())),
                                i.domManip(e, t)
                        });
                    if (l && (s = re.buildFragment(e, this[0].ownerDocument, !1, this),
                        n = s.firstChild,
                    1 === s.childNodes.length && (s = n),
                        n)) {
                        for (a = re.map(m(s, "script"), y),
                                 o = a.length; l > c; c++)
                            i = s,
                            c !== d && (i = re.clone(i, !0, !0),
                            o && re.merge(a, m(i, "script"))),
                                t.call(this[c], i, c);
                        if (o)
                            for (r = a[a.length - 1].ownerDocument,
                                     re.map(a, b),
                                     c = 0; o > c; c++)
                                i = a[c],
                                Ve.test(i.type || "") && !re._data(i, "globalEval") && re.contains(r, i) && (i.src ? re._evalUrl && re._evalUrl(i.src) : re.globalEval((i.text || i.textContent || i.innerHTML || "").replace(Ye, "")));
                        s = n = null
                    }
                    return this
                }
            }),
            re.each({
                appendTo: "append",
                prependTo: "prepend",
                insertBefore: "before",
                insertAfter: "after",
                replaceAll: "replaceWith"
            }, function(e, t) {
                re.fn[e] = function(e) {
                    for (var n, i = 0, o = [], a = re(e), r = a.length - 1; r >= i; i++)
                        n = i === r ? this : this.clone(!0),
                            re(a[i])[t](n),
                            X.apply(o, n.get());
                    return this.pushStack(o)
                }
            });
        var Ze, et = {};
        !function() {
            var e, t, n = ge.createElement("div");
            n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",
                e = n.getElementsByTagName("a")[0],
                e.style.cssText = "float:left;opacity:.5",
                ne.opacity = /^0.5/.test(e.style.opacity),
                ne.cssFloat = !!e.style.cssFloat,
                n.style.backgroundClip = "content-box",
                n.cloneNode(!0).style.backgroundClip = "",
                ne.clearCloneStyle = "content-box" === n.style.backgroundClip,
                e = n = null,
                ne.shrinkWrapBlocks = function() {
                    var e, n, i;
                    if (null == t) {
                        if (!(e = ge.getElementsByTagName("body")[0]))
                            return;
                        "border:0;width:0;height:0;position:absolute;top:0;left:-9999px",
                            n = ge.createElement("div"),
                            i = ge.createElement("div"),
                            e.appendChild(n).appendChild(i),
                            t = !1,
                        typeof i.style.zoom !== ke && (i.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0;width:1px;padding:1px;zoom:1",
                            i.innerHTML = "<div></div>",
                            i.firstChild.style.width = "5px",
                            t = 3 !== i.offsetWidth),
                            e.removeChild(n),
                            e = n = i = null
                    }
                    return t
                }
        }();
        var tt, nt, it = /^margin/, ot = new RegExp("^(" + $e + ")(?!px)[a-z%]+$","i"), at = /^(top|right|bottom|left)$/;
        e.getComputedStyle ? (tt = function(e) {
                return e.ownerDocument.defaultView.getComputedStyle(e, null)
            }
                ,
                nt = function(e, t, n) {
                    var i, o, a, r, s = e.style;
                    return n = n || tt(e),
                        r = n ? n.getPropertyValue(t) || n[t] : void 0,
                    n && ("" !== r || re.contains(e.ownerDocument, e) || (r = re.style(e, t)),
                    ot.test(r) && it.test(t) && (i = s.width,
                        o = s.minWidth,
                        a = s.maxWidth,
                        s.minWidth = s.maxWidth = s.width = r,
                        r = n.width,
                        s.width = i,
                        s.minWidth = o,
                        s.maxWidth = a)),
                        void 0 === r ? r : r + ""
                }
        ) : ge.documentElement.currentStyle && (tt = function(e) {
                return e.currentStyle
            }
                ,
                nt = function(e, t, n) {
                    var i, o, a, r, s = e.style;
                    return n = n || tt(e),
                        r = n ? n[t] : void 0,
                    null == r && s && s[t] && (r = s[t]),
                    ot.test(r) && !at.test(t) && (i = s.left,
                        o = e.runtimeStyle,
                        a = o && o.left,
                    a && (o.left = e.currentStyle.left),
                        s.left = "fontSize" === t ? "1em" : r,
                        r = s.pixelLeft + "px",
                        s.left = i,
                    a && (o.left = a)),
                        void 0 === r ? r : r + "" || "auto"
                }
        ),
            function() {
                function t() {
                    var t, n, i = ge.getElementsByTagName("body")[0];
                    i && (t = ge.createElement("div"),
                        n = ge.createElement("div"),
                        t.style.cssText = l,
                        i.appendChild(t).appendChild(n),
                        n.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;display:block;padding:1px;border:1px;width:4px;margin-top:1%;top:1%",
                        re.swap(i, null != i.style.zoom ? {
                            zoom: 1
                        } : {}, function() {
                            o = 4 === n.offsetWidth
                        }),
                        a = !0,
                        r = !1,
                        s = !0,
                    e.getComputedStyle && (r = "1%" !== (e.getComputedStyle(n, null) || {}).top,
                        a = "4px" === (e.getComputedStyle(n, null) || {
                            width: "4px"
                        }).width),
                        i.removeChild(t),
                        n = i = null)
                }
                var n, i, o, a, r, s, c = ge.createElement("div"), l = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px";
                c.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",
                    n = c.getElementsByTagName("a")[0],
                    n.style.cssText = "float:left;opacity:.5",
                    ne.opacity = /^0.5/.test(n.style.opacity),
                    ne.cssFloat = !!n.style.cssFloat,
                    c.style.backgroundClip = "content-box",
                    c.cloneNode(!0).style.backgroundClip = "",
                    ne.clearCloneStyle = "content-box" === c.style.backgroundClip,
                    n = c = null,
                    re.extend(ne, {
                        reliableHiddenOffsets: function() {
                            if (null != i)
                                return i;
                            var e, t, n, o = ge.createElement("div"), a = ge.getElementsByTagName("body")[0];
                            return a ? (o.setAttribute("className", "t"),
                                o.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",
                                e = ge.createElement("div"),
                                e.style.cssText = l,
                                a.appendChild(e).appendChild(o),
                                o.innerHTML = "<table><tr><td></td><td>t</td></tr></table>",
                                t = o.getElementsByTagName("td"),
                                t[0].style.cssText = "padding:0;margin:0;border:0;display:none",
                                n = 0 === t[0].offsetHeight,
                                t[0].style.display = "",
                                t[1].style.display = "none",
                                i = n && 0 === t[0].offsetHeight,
                                a.removeChild(e),
                                o = a = null,
                                i) : void 0
                        },
                        boxSizing: function() {
                            return null == o && t(),
                                o
                        },
                        boxSizingReliable: function() {
                            return null == a && t(),
                                a
                        },
                        pixelPosition: function() {
                            return null == r && t(),
                                r
                        },
                        reliableMarginRight: function() {
                            var t, n, i, o;
                            if (null == s && e.getComputedStyle) {
                                if (!(t = ge.getElementsByTagName("body")[0]))
                                    return;
                                n = ge.createElement("div"),
                                    i = ge.createElement("div"),
                                    n.style.cssText = l,
                                    t.appendChild(n).appendChild(i),
                                    o = i.appendChild(ge.createElement("div")),
                                    o.style.cssText = i.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0",
                                    o.style.marginRight = o.style.width = "0",
                                    i.style.width = "1px",
                                    s = !parseFloat((e.getComputedStyle(o, null) || {}).marginRight),
                                    t.removeChild(n)
                            }
                            return s
                        }
                    })
            }(),
            re.swap = function(e, t, n, i) {
                var o, a, r = {};
                for (a in t)
                    r[a] = e.style[a],
                        e.style[a] = t[a];
                o = n.apply(e, i || []);
                for (a in t)
                    e.style[a] = r[a];
                return o
            }
        ;
        var rt = /alpha\([^)]*\)/i
            , st = /opacity\s*=\s*([^)]*)/
            , ct = /^(none|table(?!-c[ea]).+)/
            , lt = new RegExp("^(" + $e + ")(.*)$","i")
            , ut = new RegExp("^([+-])=(" + $e + ")","i")
            , dt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        }
            , ft = {
            letterSpacing: 0,
            fontWeight: 400
        }
            , pt = ["Webkit", "O", "Moz", "ms"];
        re.extend({
            cssHooks: {
                opacity: {
                    get: function(e, t) {
                        if (t) {
                            var n = nt(e, "opacity");
                            return "" === n ? "1" : n
                        }
                    }
                }
            },
            cssNumber: {
                columnCount: !0,
                fillOpacity: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {
                float: ne.cssFloat ? "cssFloat" : "styleFloat"
            },
            style: function(e, t, n, i) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var o, a, r, s = re.camelCase(t), c = e.style;
                    if (t = re.cssProps[s] || (re.cssProps[s] = S(c, s)),
                        r = re.cssHooks[t] || re.cssHooks[s],
                    void 0 === n)
                        return r && "get"in r && void 0 !== (o = r.get(e, !1, i)) ? o : c[t];
                    if (a = typeof n,
                    "string" === a && (o = ut.exec(n)) && (n = (o[1] + 1) * o[2] + parseFloat(re.css(e, t)),
                        a = "number"),
                    null != n && n === n && ("number" !== a || re.cssNumber[s] || (n += "px"),
                    ne.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"),
                        !(r && "set"in r && void 0 === (n = r.set(e, n, i)))))
                        try {
                            c[t] = "",
                                c[t] = n
                        } catch (e) {}
                }
            },
            css: function(e, t, n, i) {
                var o, a, r, s = re.camelCase(t);
                return t = re.cssProps[s] || (re.cssProps[s] = S(e.style, s)),
                    r = re.cssHooks[t] || re.cssHooks[s],
                r && "get"in r && (a = r.get(e, !0, n)),
                void 0 === a && (a = nt(e, t, i)),
                "normal" === a && t in ft && (a = ft[t]),
                    "" === n || n ? (o = parseFloat(a),
                        !0 === n || re.isNumeric(o) ? o || 0 : a) : a
            }
        }),
            re.each(["height", "width"], function(e, t) {
                re.cssHooks[t] = {
                    get: function(e, n, i) {
                        return n ? 0 === e.offsetWidth && ct.test(re.css(e, "display")) ? re.swap(e, dt, function() {
                            return q(e, t, i)
                        }) : q(e, t, i) : void 0
                    },
                    set: function(e, n, i) {
                        var o = i && tt(e);
                        return _(e, n, i ? E(e, t, i, ne.boxSizing() && "border-box" === re.css(e, "boxSizing", !1, o), o) : 0)
                    }
                }
            }),
        ne.opacity || (re.cssHooks.opacity = {
            get: function(e, t) {
                return st.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
            },
            set: function(e, t) {
                var n = e.style
                    , i = e.currentStyle
                    , o = re.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : ""
                    , a = i && i.filter || n.filter || "";
                n.zoom = 1,
                (t >= 1 || "" === t) && "" === re.trim(a.replace(rt, "")) && n.removeAttribute && (n.removeAttribute("filter"),
                "" === t || i && !i.filter) || (n.filter = rt.test(a) ? a.replace(rt, o) : a + " " + o)
            }
        }),
            re.cssHooks.marginRight = T(ne.reliableMarginRight, function(e, t) {
                return t ? re.swap(e, {
                    display: "inline-block"
                }, nt, [e, "marginRight"]) : void 0
            }),
            re.each({
                margin: "",
                padding: "",
                border: "Width"
            }, function(e, t) {
                re.cssHooks[e + t] = {
                    expand: function(n) {
                        for (var i = 0, o = {}, a = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++)
                            o[e + _e[i] + t] = a[i] || a[i - 2] || a[0];
                        return o
                    }
                },
                it.test(e) || (re.cssHooks[e + t].set = _)
            }),
            re.fn.extend({
                css: function(e, t) {
                    return qe(this, function(e, t, n) {
                        var i, o, a = {}, r = 0;
                        if (re.isArray(t)) {
                            for (i = tt(e),
                                     o = t.length; o > r; r++)
                                a[t[r]] = re.css(e, t[r], !1, i);
                            return a
                        }
                        return void 0 !== n ? re.style(e, t, n) : re.css(e, t)
                    }, e, t, arguments.length > 1)
                },
                show: function() {
                    return $(this, !0)
                },
                hide: function() {
                    return $(this)
                },
                toggle: function(e) {
                    return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                        Ee(this) ? re(this).show() : re(this).hide()
                    })
                }
            }),
            re.Tween = D,
            D.prototype = {
                constructor: D,
                init: function(e, t, n, i, o, a) {
                    this.elem = e,
                        this.prop = n,
                        this.easing = o || "swing",
                        this.options = t,
                        this.start = this.now = this.cur(),
                        this.end = i,
                        this.unit = a || (re.cssNumber[n] ? "" : "px")
                },
                cur: function() {
                    var e = D.propHooks[this.prop];
                    return e && e.get ? e.get(this) : D.propHooks._default.get(this)
                },
                run: function(e) {
                    var t, n = D.propHooks[this.prop];
                    return this.pos = t = this.options.duration ? re.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e,
                        this.now = (this.end - this.start) * t + this.start,
                    this.options.step && this.options.step.call(this.elem, this.now, this),
                        n && n.set ? n.set(this) : D.propHooks._default.set(this),
                        this
                }
            },
            D.prototype.init.prototype = D.prototype,
            D.propHooks = {
                _default: {
                    get: function(e) {
                        var t;
                        return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = re.css(e.elem, e.prop, ""),
                            t && "auto" !== t ? t : 0) : e.elem[e.prop]
                    },
                    set: function(e) {
                        re.fx.step[e.prop] ? re.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[re.cssProps[e.prop]] || re.cssHooks[e.prop]) ? re.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
                    }
                }
            },
            D.propHooks.scrollTop = D.propHooks.scrollLeft = {
                set: function(e) {
                    e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
                }
            },
            re.easing = {
                linear: function(e) {
                    return e
                },
                swing: function(e) {
                    return .5 - Math.cos(e * Math.PI) / 2
                }
            },
            re.fx = D.prototype.init,
            re.fx.step = {};
        var ht, mt, gt = /^(?:toggle|show|hide)$/, vt = new RegExp("^(?:([+-])=|)(" + $e + ")([a-z%]*)$","i"), yt = /queueHooks$/, bt = [M], xt = {
            "*": [function(e, t) {
                var n = this.createTween(e, t)
                    , i = n.cur()
                    , o = vt.exec(t)
                    , a = o && o[3] || (re.cssNumber[e] ? "" : "px")
                    , r = (re.cssNumber[e] || "px" !== a && +i) && vt.exec(re.css(n.elem, e))
                    , s = 1
                    , c = 20;
                if (r && r[3] !== a) {
                    a = a || r[3],
                        o = o || [],
                        r = +i || 1;
                    do {
                        s = s || ".5",
                            r /= s,
                            re.style(n.elem, e, r + a)
                    } while (s !== (s = n.cur() / i) && 1 !== s && --c)
                }
                return o && (r = n.start = +r || +i || 0,
                    n.unit = a,
                    n.end = o[1] ? r + (o[1] + 1) * o[2] : +o[2]),
                    n
            }
            ]
        };
        re.Animation = re.extend(z, {
            tweener: function(e, t) {
                re.isFunction(e) ? (t = e,
                    e = ["*"]) : e = e.split(" ");
                for (var n, i = 0, o = e.length; o > i; i++)
                    n = e[i],
                        xt[n] = xt[n] || [],
                        xt[n].unshift(t)
            },
            prefilter: function(e, t) {
                t ? bt.unshift(e) : bt.push(e)
            }
        }),
            re.speed = function(e, t, n) {
                var i = e && "object" == typeof e ? re.extend({}, e) : {
                    complete: n || !n && t || re.isFunction(e) && e,
                    duration: e,
                    easing: n && t || t && !re.isFunction(t) && t
                };
                return i.duration = re.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in re.fx.speeds ? re.fx.speeds[i.duration] : re.fx.speeds._default,
                (null == i.queue || !0 === i.queue) && (i.queue = "fx"),
                    i.old = i.complete,
                    i.complete = function() {
                        re.isFunction(i.old) && i.old.call(this),
                        i.queue && re.dequeue(this, i.queue)
                    }
                    ,
                    i
            }
            ,
            re.fn.extend({
                fadeTo: function(e, t, n, i) {
                    return this.filter(Ee).css("opacity", 0).show().end().animate({
                        opacity: t
                    }, e, n, i)
                },
                animate: function(e, t, n, i) {
                    var o = re.isEmptyObject(e)
                        , a = re.speed(t, n, i)
                        , r = function() {
                        var t = z(this, re.extend({}, e), a);
                        (o || re._data(this, "finish")) && t.stop(!0)
                    };
                    return r.finish = r,
                        o || !1 === a.queue ? this.each(r) : this.queue(a.queue, r)
                },
                stop: function(e, t, n) {
                    var i = function(e) {
                        var t = e.stop;
                        delete e.stop,
                            t(n)
                    };
                    return "string" != typeof e && (n = t,
                        t = e,
                        e = void 0),
                    t && !1 !== e && this.queue(e || "fx", []),
                        this.each(function() {
                            var t = !0
                                , o = null != e && e + "queueHooks"
                                , a = re.timers
                                , r = re._data(this);
                            if (o)
                                r[o] && r[o].stop && i(r[o]);
                            else
                                for (o in r)
                                    r[o] && r[o].stop && yt.test(o) && i(r[o]);
                            for (o = a.length; o--; )
                                a[o].elem !== this || null != e && a[o].queue !== e || (a[o].anim.stop(n),
                                    t = !1,
                                    a.splice(o, 1));
                            (t || !n) && re.dequeue(this, e)
                        })
                },
                finish: function(e) {
                    return !1 !== e && (e = e || "fx"),
                        this.each(function() {
                            var t, n = re._data(this), i = n[e + "queue"], o = n[e + "queueHooks"], a = re.timers, r = i ? i.length : 0;
                            for (n.finish = !0,
                                     re.queue(this, e, []),
                                 o && o.stop && o.stop.call(this, !0),
                                     t = a.length; t--; )
                                a[t].elem === this && a[t].queue === e && (a[t].anim.stop(!0),
                                    a.splice(t, 1));
                            for (t = 0; r > t; t++)
                                i[t] && i[t].finish && i[t].finish.call(this);
                            delete n.finish
                        })
                }
            }),
            re.each(["toggle", "show", "hide"], function(e, t) {
                var n = re.fn[t];
                re.fn[t] = function(e, i, o) {
                    return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(L(t, !0), e, i, o)
                }
            }),
            re.each({
                slideDown: L("show"),
                slideUp: L("hide"),
                slideToggle: L("toggle"),
                fadeIn: {
                    opacity: "show"
                },
                fadeOut: {
                    opacity: "hide"
                },
                fadeToggle: {
                    opacity: "toggle"
                }
            }, function(e, t) {
                re.fn[e] = function(e, n, i) {
                    return this.animate(t, e, n, i)
                }
            }),
            re.timers = [],
            re.fx.tick = function() {
                var e, t = re.timers, n = 0;
                for (ht = re.now(); n < t.length; n++)
                    (e = t[n])() || t[n] !== e || t.splice(n--, 1);
                t.length || re.fx.stop(),
                    ht = void 0
            }
            ,
            re.fx.timer = function(e) {
                e() && re.timers.push(e) && re.fx.start()
            }
            ,
            re.fx.interval = 13,
            re.fx.start = function() {
                mt || (mt = setInterval(re.fx.tick, re.fx.interval))
            }
            ,
            re.fx.stop = function() {
                clearInterval(mt),
                    mt = null
            }
            ,
            re.fx.speeds = {
                slow: 600,
                fast: 200,
                _default: 400
            },
            re.fn.delay = function(e, t) {
                return e = re.fx ? re.fx.speeds[e] || e : e,
                    t = t || "fx",
                    this.queue(t, function(t, n) {
                        var i = setTimeout(t, e);
                        n.stop = function() {
                            clearTimeout(i)
                        }
                    })
            }
            ,
            function() {
                var e, t, n, i, o = ge.createElement("div");
                o.setAttribute("className", "t"),
                    o.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",
                    e = o.getElementsByTagName("a")[0],
                    n = ge.createElement("select"),
                    i = n.appendChild(ge.createElement("option")),
                    t = o.getElementsByTagName("input")[0],
                    e.style.cssText = "top:1px",
                    ne.getSetAttribute = "t" !== o.className,
                    ne.style = /top/.test(e.getAttribute("style")),
                    ne.hrefNormalized = "/a" === e.getAttribute("href"),
                    ne.checkOn = !!t.value,
                    ne.optSelected = i.selected,
                    ne.enctype = !!ge.createElement("form").enctype,
                    n.disabled = !0,
                    ne.optDisabled = !i.disabled,
                    t = ge.createElement("input"),
                    t.setAttribute("value", ""),
                    ne.input = "" === t.getAttribute("value"),
                    t.value = "t",
                    t.setAttribute("type", "radio"),
                    ne.radioValue = "t" === t.value,
                    e = t = n = i = o = null
            }();
        var wt = /\r/g;
        re.fn.extend({
            val: function(e) {
                var t, n, i, o = this[0];
                return arguments.length ? (i = re.isFunction(e),
                    this.each(function(n) {
                        var o;
                        1 === this.nodeType && (o = i ? e.call(this, n, re(this).val()) : e,
                            null == o ? o = "" : "number" == typeof o ? o += "" : re.isArray(o) && (o = re.map(o, function(e) {
                                return null == e ? "" : e + ""
                            })),
                        (t = re.valHooks[this.type] || re.valHooks[this.nodeName.toLowerCase()]) && "set"in t && void 0 !== t.set(this, o, "value") || (this.value = o))
                    })) : o ? (t = re.valHooks[o.type] || re.valHooks[o.nodeName.toLowerCase()],
                    t && "get"in t && void 0 !== (n = t.get(o, "value")) ? n : (n = o.value,
                        "string" == typeof n ? n.replace(wt, "") : null == n ? "" : n)) : void 0
            }
        }),
            re.extend({
                valHooks: {
                    option: {
                        get: function(e) {
                            var t = re.find.attr(e, "value");
                            return null != t ? t : e.text
                        }
                    },
                    select: {
                        get: function(e) {
                            for (var t, n, i = e.options, o = e.selectedIndex, a = "select-one" === e.type || 0 > o, r = a ? null : [], s = a ? o + 1 : i.length, c = 0 > o ? s : a ? o : 0; s > c; c++)
                                if (n = i[c],
                                    !(!n.selected && c !== o || (ne.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && re.nodeName(n.parentNode, "optgroup"))) {
                                    if (t = re(n).val(),
                                        a)
                                        return t;
                                    r.push(t)
                                }
                            return r
                        },
                        set: function(e, t) {
                            for (var n, i, o = e.options, a = re.makeArray(t), r = o.length; r--; )
                                i = o[r],
                                (i.selected = re.inArray(re(i).val(), a) >= 0) && (n = !0);
                            return n || (e.selectedIndex = -1),
                                a
                        }
                    }
                }
            }),
            re.each(["radio", "checkbox"], function() {
                re.valHooks[this] = {
                    set: function(e, t) {
                        return re.isArray(t) ? e.checked = re.inArray(re(e).val(), t) >= 0 : void 0
                    }
                },
                ne.checkOn || (re.valHooks[this].get = function(e) {
                        return null === e.getAttribute("value") ? "on" : e.value
                    }
                )
            });
        var jt, Ct, kt = re.expr.attrHandle, Tt = /^(?:checked|selected)$/i, St = ne.getSetAttribute, $t = ne.input;
        re.fn.extend({
            attr: function(e, t) {
                return qe(this, re.attr, e, t, arguments.length > 1)
            },
            removeAttr: function(e) {
                return this.each(function() {
                    re.removeAttr(this, e)
                })
            }
        }),
            re.extend({
                attr: function(e, t, n) {
                    var i, o, a = e.nodeType;
                    return e && 3 !== a && 8 !== a && 2 !== a ? typeof e.getAttribute === ke ? re.prop(e, t, n) : (1 === a && re.isXMLDoc(e) || (t = t.toLowerCase(),
                        i = re.attrHooks[t] || (re.expr.match.bool.test(t) ? Ct : jt)),
                        void 0 === n ? i && "get"in i && null !== (o = i.get(e, t)) ? o : (o = re.find.attr(e, t),
                            null == o ? void 0 : o) : null !== n ? i && "set"in i && void 0 !== (o = i.set(e, n, t)) ? o : (e.setAttribute(t, n + ""),
                            n) : void re.removeAttr(e, t)) : void 0
                },
                removeAttr: function(e, t) {
                    var n, i, o = 0, a = t && t.match(xe);
                    if (a && 1 === e.nodeType)
                        for (; n = a[o++]; )
                            i = re.propFix[n] || n,
                                re.expr.match.bool.test(n) ? $t && St || !Tt.test(n) ? e[i] = !1 : e[re.camelCase("default-" + n)] = e[i] = !1 : re.attr(e, n, ""),
                                e.removeAttribute(St ? n : i)
                },
                attrHooks: {
                    type: {
                        set: function(e, t) {
                            if (!ne.radioValue && "radio" === t && re.nodeName(e, "input")) {
                                var n = e.value;
                                return e.setAttribute("type", t),
                                n && (e.value = n),
                                    t
                            }
                        }
                    }
                }
            }),
            Ct = {
                set: function(e, t, n) {
                    return !1 === t ? re.removeAttr(e, n) : $t && St || !Tt.test(n) ? e.setAttribute(!St && re.propFix[n] || n, n) : e[re.camelCase("default-" + n)] = e[n] = !0,
                        n
                }
            },
            re.each(re.expr.match.bool.source.match(/\w+/g), function(e, t) {
                var n = kt[t] || re.find.attr;
                kt[t] = $t && St || !Tt.test(t) ? function(e, t, i) {
                        var o, a;
                        return i || (a = kt[t],
                            kt[t] = o,
                            o = null != n(e, t, i) ? t.toLowerCase() : null,
                            kt[t] = a),
                            o
                    }
                    : function(e, t, n) {
                        return n ? void 0 : e[re.camelCase("default-" + t)] ? t.toLowerCase() : null
                    }
            }),
        $t && St || (re.attrHooks.value = {
            set: function(e, t, n) {
                return re.nodeName(e, "input") ? void (e.defaultValue = t) : jt && jt.set(e, t, n)
            }
        }),
        St || (jt = {
            set: function(e, t, n) {
                var i = e.getAttributeNode(n);
                return i || e.setAttributeNode(i = e.ownerDocument.createAttribute(n)),
                    i.value = t += "",
                    "value" === n || t === e.getAttribute(n) ? t : void 0
            }
        },
            kt.id = kt.name = kt.coords = function(e, t, n) {
                var i;
                return n ? void 0 : (i = e.getAttributeNode(t)) && "" !== i.value ? i.value : null
            }
            ,
            re.valHooks.button = {
                get: function(e, t) {
                    var n = e.getAttributeNode(t);
                    return n && n.specified ? n.value : void 0
                },
                set: jt.set
            },
            re.attrHooks.contenteditable = {
                set: function(e, t, n) {
                    jt.set(e, "" !== t && t, n)
                }
            },
            re.each(["width", "height"], function(e, t) {
                re.attrHooks[t] = {
                    set: function(e, n) {
                        return "" === n ? (e.setAttribute(t, "auto"),
                            n) : void 0
                    }
                }
            })),
        ne.style || (re.attrHooks.style = {
            get: function(e) {
                return e.style.cssText || void 0
            },
            set: function(e, t) {
                return e.style.cssText = t + ""
            }
        });
        var _t = /^(?:input|select|textarea|button|object)$/i
            , Et = /^(?:a|area)$/i;
        re.fn.extend({
            prop: function(e, t) {
                return qe(this, re.prop, e, t, arguments.length > 1)
            },
            removeProp: function(e) {
                return e = re.propFix[e] || e,
                    this.each(function() {
                        try {
                            this[e] = void 0,
                                delete this[e]
                        } catch (e) {}
                    })
            }
        }),
            re.extend({
                propFix: {
                    for: "htmlFor",
                    class: "className"
                },
                prop: function(e, t, n) {
                    var i, o, a, r = e.nodeType;
                    return e && 3 !== r && 8 !== r && 2 !== r ? (a = 1 !== r || !re.isXMLDoc(e),
                    a && (t = re.propFix[t] || t,
                        o = re.propHooks[t]),
                        void 0 !== n ? o && "set"in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get"in o && null !== (i = o.get(e, t)) ? i : e[t]) : void 0
                },
                propHooks: {
                    tabIndex: {
                        get: function(e) {
                            var t = re.find.attr(e, "tabindex");
                            return t ? parseInt(t, 10) : _t.test(e.nodeName) || Et.test(e.nodeName) && e.href ? 0 : -1
                        }
                    }
                }
            }),
        ne.hrefNormalized || re.each(["href", "src"], function(e, t) {
            re.propHooks[t] = {
                get: function(e) {
                    return e.getAttribute(t, 4)
                }
            }
        }),
        ne.optSelected || (re.propHooks.selected = {
            get: function(e) {
                var t = e.parentNode;
                return t && (t.selectedIndex,
                t.parentNode && t.parentNode.selectedIndex),
                    null
            }
        }),
            re.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
                re.propFix[this.toLowerCase()] = this
            }),
        ne.enctype || (re.propFix.enctype = "encoding");
        var qt = /[\t\r\n\f]/g;
        re.fn.extend({
            addClass: function(e) {
                var t, n, i, o, a, r, s = 0, c = this.length, l = "string" == typeof e && e;
                if (re.isFunction(e))
                    return this.each(function(t) {
                        re(this).addClass(e.call(this, t, this.className))
                    });
                if (l)
                    for (t = (e || "").match(xe) || []; c > s; s++)
                        if (n = this[s],
                            i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(qt, " ") : " ")) {
                            for (a = 0; o = t[a++]; )
                                i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                            r = re.trim(i),
                            n.className !== r && (n.className = r)
                        }
                return this
            },
            removeClass: function(e) {
                var t, n, i, o, a, r, s = 0, c = this.length, l = 0 === arguments.length || "string" == typeof e && e;
                if (re.isFunction(e))
                    return this.each(function(t) {
                        re(this).removeClass(e.call(this, t, this.className))
                    });
                if (l)
                    for (t = (e || "").match(xe) || []; c > s; s++)
                        if (n = this[s],
                            i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(qt, " ") : "")) {
                            for (a = 0; o = t[a++]; )
                                for (; i.indexOf(" " + o + " ") >= 0; )
                                    i = i.replace(" " + o + " ", " ");
                            r = e ? re.trim(i) : "",
                            n.className !== r && (n.className = r)
                        }
                return this
            },
            toggleClass: function(e, t) {
                var n = typeof e;
                return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : re.isFunction(e) ? this.each(function(n) {
                    re(this).toggleClass(e.call(this, n, this.className, t), t)
                }) : this.each(function() {
                    if ("string" === n)
                        for (var t, i = 0, o = re(this), a = e.match(xe) || []; t = a[i++]; )
                            o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
                    else
                        (n === ke || "boolean" === n) && (this.className && re._data(this, "__className__", this.className),
                            this.className = this.className || !1 === e ? "" : re._data(this, "__className__") || "")
                })
            },
            hasClass: function(e) {
                for (var t = " " + e + " ", n = 0, i = this.length; i > n; n++)
                    if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(qt, " ").indexOf(t) >= 0)
                        return !0;
                return !1
            }
        }),
            re.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
                re.fn[t] = function(e, n) {
                    return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
                }
            }),
            re.fn.extend({
                hover: function(e, t) {
                    return this.mouseenter(e).mouseleave(t || e)
                },
                bind: function(e, t, n) {
                    return this.on(e, null, t, n)
                },
                unbind: function(e, t) {
                    return this.off(e, null, t)
                },
                delegate: function(e, t, n, i) {
                    return this.on(t, e, n, i)
                },
                undelegate: function(e, t, n) {
                    return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
                }
            });
        var Dt = re.now()
            , At = /\?/
            , Lt = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
        re.parseJSON = function(t) {
            if (e.JSON && e.JSON.parse)
                return e.JSON.parse(t + "");
            var n, i = null, o = re.trim(t + "");
            return o && !re.trim(o.replace(Lt, function(e, t, o, a) {
                return n && t && (i = 0),
                    0 === i ? e : (n = o || t,
                        i += !a - !o,
                        "")
            })) ? Function("return " + o)() : re.error("Invalid JSON: " + t)
        }
            ,
            re.parseXML = function(t) {
                var n, i;
                if (!t || "string" != typeof t)
                    return null;
                try {
                    e.DOMParser ? (i = new DOMParser,
                        n = i.parseFromString(t, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"),
                        n.async = "false",
                        n.loadXML(t))
                } catch (e) {
                    n = void 0
                }
                return n && n.documentElement && !n.getElementsByTagName("parsererror").length || re.error("Invalid XML: " + t),
                    n
            }
        ;
        var Nt, Mt, Pt = /#.*$/, zt = /([?&])_=[^&]*/, Rt = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, Ht = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, It = /^(?:GET|HEAD)$/, Ot = /^\/\//, Bt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Ft = {}, Ut = {}, Wt = "*/".concat("*");
        try {
            Mt = location.href
        } catch (e) {
            Mt = ge.createElement("a"),
                Mt.href = "",
                Mt = Mt.href
        }
        Nt = Bt.exec(Mt.toLowerCase()) || [],
            re.extend({
                active: 0,
                lastModified: {},
                etag: {},
                ajaxSettings: {
                    url: Mt,
                    type: "GET",
                    isLocal: Ht.test(Nt[1]),
                    global: !0,
                    processData: !0,
                    async: !0,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    accepts: {
                        "*": Wt,
                        text: "text/plain",
                        html: "text/html",
                        xml: "application/xml, text/xml",
                        json: "application/json, text/javascript"
                    },
                    contents: {
                        xml: /xml/,
                        html: /html/,
                        json: /json/
                    },
                    responseFields: {
                        xml: "responseXML",
                        text: "responseText",
                        json: "responseJSON"
                    },
                    converters: {
                        "* text": String,
                        "text html": !0,
                        "text json": re.parseJSON,
                        "text xml": re.parseXML
                    },
                    flatOptions: {
                        url: !0,
                        context: !0
                    }
                },
                ajaxSetup: function(e, t) {
                    return t ? I(I(e, re.ajaxSettings), t) : I(re.ajaxSettings, e)
                },
                ajaxPrefilter: R(Ft),
                ajaxTransport: R(Ut),
                ajax: function(e, t) {
                    function n(e, t, n, i) {
                        var o, u, v, y, x, j = t;
                        2 !== b && (b = 2,
                        s && clearTimeout(s),
                            l = void 0,
                            r = i || "",
                            w.readyState = e > 0 ? 4 : 0,
                            o = e >= 200 && 300 > e || 304 === e,
                        n && (y = O(d, w, n)),
                            y = B(d, y, w, o),
                            o ? (d.ifModified && (x = w.getResponseHeader("Last-Modified"),
                            x && (re.lastModified[a] = x),
                            (x = w.getResponseHeader("etag")) && (re.etag[a] = x)),
                                204 === e || "HEAD" === d.type ? j = "nocontent" : 304 === e ? j = "notmodified" : (j = y.state,
                                    u = y.data,
                                    v = y.error,
                                    o = !v)) : (v = j,
                            (e || !j) && (j = "error",
                            0 > e && (e = 0))),
                            w.status = e,
                            w.statusText = (t || j) + "",
                            o ? h.resolveWith(f, [u, j, w]) : h.rejectWith(f, [w, j, v]),
                            w.statusCode(g),
                            g = void 0,
                        c && p.trigger(o ? "ajaxSuccess" : "ajaxError", [w, d, o ? u : v]),
                            m.fireWith(f, [w, j]),
                        c && (p.trigger("ajaxComplete", [w, d]),
                        --re.active || re.event.trigger("ajaxStop")))
                    }
                    "object" == typeof e && (t = e,
                        e = void 0),
                        t = t || {};
                    var i, o, a, r, s, c, l, u, d = re.ajaxSetup({}, t), f = d.context || d, p = d.context && (f.nodeType || f.jquery) ? re(f) : re.event, h = re.Deferred(), m = re.Callbacks("once memory"), g = d.statusCode || {}, v = {}, y = {}, b = 0, x = "canceled", w = {
                        readyState: 0,
                        getResponseHeader: function(e) {
                            var t;
                            if (2 === b) {
                                if (!u)
                                    for (u = {}; t = Rt.exec(r); )
                                        u[t[1].toLowerCase()] = t[2];
                                t = u[e.toLowerCase()]
                            }
                            return null == t ? null : t
                        },
                        getAllResponseHeaders: function() {
                            return 2 === b ? r : null
                        },
                        setRequestHeader: function(e, t) {
                            var n = e.toLowerCase();
                            return b || (e = y[n] = y[n] || e,
                                v[e] = t),
                                this
                        },
                        overrideMimeType: function(e) {
                            return b || (d.mimeType = e),
                                this
                        },
                        statusCode: function(e) {
                            var t;
                            if (e)
                                if (2 > b)
                                    for (t in e)
                                        g[t] = [g[t], e[t]];
                                else
                                    w.always(e[w.status]);
                            return this
                        },
                        abort: function(e) {
                            var t = e || x;
                            return l && l.abort(t),
                                n(0, t),
                                this
                        }
                    };
                    if (h.promise(w).complete = m.add,
                        w.success = w.done,
                        w.error = w.fail,
                        d.url = ((e || d.url || Mt) + "").replace(Pt, "").replace(Ot, Nt[1] + "//"),
                        d.type = t.method || t.type || d.method || d.type,
                        d.dataTypes = re.trim(d.dataType || "*").toLowerCase().match(xe) || [""],
                    null == d.crossDomain && (i = Bt.exec(d.url.toLowerCase()),
                        d.crossDomain = !(!i || i[1] === Nt[1] && i[2] === Nt[2] && (i[3] || ("http:" === i[1] ? "80" : "443")) === (Nt[3] || ("http:" === Nt[1] ? "80" : "443")))),
                    d.data && d.processData && "string" != typeof d.data && (d.data = re.param(d.data, d.traditional)),
                        H(Ft, d, t, w),
                    2 === b)
                        return w;
                    c = d.global,
                    c && 0 == re.active++ && re.event.trigger("ajaxStart"),
                        d.type = d.type.toUpperCase(),
                        d.hasContent = !It.test(d.type),
                        a = d.url,
                    d.hasContent || (d.data && (a = d.url += (At.test(a) ? "&" : "?") + d.data,
                        delete d.data),
                    !1 === d.cache && (d.url = zt.test(a) ? a.replace(zt, "$1_=" + Dt++) : a + (At.test(a) ? "&" : "?") + "_=" + Dt++)),
                    d.ifModified && (re.lastModified[a] && w.setRequestHeader("If-Modified-Since", re.lastModified[a]),
                    re.etag[a] && w.setRequestHeader("If-None-Match", re.etag[a])),
                    (d.data && d.hasContent && !1 !== d.contentType || t.contentType) && w.setRequestHeader("Content-Type", d.contentType),
                        w.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + Wt + "; q=0.01" : "") : d.accepts["*"]);
                    for (o in d.headers)
                        w.setRequestHeader(o, d.headers[o]);
                    if (d.beforeSend && (!1 === d.beforeSend.call(f, w, d) || 2 === b))
                        return w.abort();
                    x = "abort";
                    for (o in {
                        success: 1,
                        error: 1,
                        complete: 1
                    })
                        w[o](d[o]);
                    if (l = H(Ut, d, t, w)) {
                        w.readyState = 1,
                        c && p.trigger("ajaxSend", [w, d]),
                        d.async && d.timeout > 0 && (s = setTimeout(function() {
                            w.abort("timeout")
                        }, d.timeout));
                        try {
                            b = 1,
                                l.send(v, n)
                        } catch (e) {
                            if (!(2 > b))
                                throw e;
                            n(-1, e)
                        }
                    } else
                        n(-1, "No Transport");
                    return w
                },
                getJSON: function(e, t, n) {
                    return re.get(e, t, n, "json")
                },
                getScript: function(e, t) {
                    return re.get(e, void 0, t, "script")
                }
            }),
            re.each(["get", "post"], function(e, t) {
                re[t] = function(e, n, i, o) {
                    return re.isFunction(n) && (o = o || i,
                        i = n,
                        n = void 0),
                        re.ajax({
                            url: e,
                            type: t,
                            dataType: o,
                            data: n,
                            success: i
                        })
                }
            }),
            re.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
                re.fn[t] = function(e) {
                    return this.on(t, e)
                }
            }),
            re._evalUrl = function(e) {
                return re.ajax({
                    url: e,
                    type: "GET",
                    dataType: "script",
                    async: !1,
                    global: !1,
                    throws: !0
                })
            }
            ,
            re.fn.extend({
                wrapAll: function(e) {
                    if (re.isFunction(e))
                        return this.each(function(t) {
                            re(this).wrapAll(e.call(this, t))
                        });
                    if (this[0]) {
                        var t = re(e, this[0].ownerDocument).eq(0).clone(!0);
                        this[0].parentNode && t.insertBefore(this[0]),
                            t.map(function() {
                                for (var e = this; e.firstChild && 1 === e.firstChild.nodeType; )
                                    e = e.firstChild;
                                return e
                            }).append(this)
                    }
                    return this
                },
                wrapInner: function(e) {
                    return re.isFunction(e) ? this.each(function(t) {
                        re(this).wrapInner(e.call(this, t))
                    }) : this.each(function() {
                        var t = re(this)
                            , n = t.contents();
                        n.length ? n.wrapAll(e) : t.append(e)
                    })
                },
                wrap: function(e) {
                    var t = re.isFunction(e);
                    return this.each(function(n) {
                        re(this).wrapAll(t ? e.call(this, n) : e)
                    })
                },
                unwrap: function() {
                    return this.parent().each(function() {
                        re.nodeName(this, "body") || re(this).replaceWith(this.childNodes)
                    }).end()
                }
            }),
            re.expr.filters.hidden = function(e) {
                return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !ne.reliableHiddenOffsets() && "none" === (e.style && e.style.display || re.css(e, "display"))
            }
            ,
            re.expr.filters.visible = function(e) {
                return !re.expr.filters.hidden(e)
            }
        ;
        var Qt = /%20/g
            , Vt = /\[\]$/
            , Gt = /\r?\n/g
            , Yt = /^(?:submit|button|image|reset|file)$/i
            , Xt = /^(?:input|select|textarea|keygen)/i;
        re.param = function(e, t) {
            var n, i = [], o = function(e, t) {
                t = re.isFunction(t) ? t() : null == t ? "" : t,
                    i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
            if (void 0 === t && (t = re.ajaxSettings && re.ajaxSettings.traditional),
            re.isArray(e) || e.jquery && !re.isPlainObject(e))
                re.each(e, function() {
                    o(this.name, this.value)
                });
            else
                for (n in e)
                    F(n, e[n], t, o);
            return i.join("&").replace(Qt, "+")
        }
            ,
            re.fn.extend({
                serialize: function() {
                    return re.param(this.serializeArray())
                },
                serializeArray: function() {
                    return this.map(function() {
                        var e = re.prop(this, "elements");
                        return e ? re.makeArray(e) : this
                    }).filter(function() {
                        var e = this.type;
                        return this.name && !re(this).is(":disabled") && Xt.test(this.nodeName) && !Yt.test(e) && (this.checked || !De.test(e))
                    }).map(function(e, t) {
                        var n = re(this).val();
                        return null == n ? null : re.isArray(n) ? re.map(n, function(e) {
                            return {
                                name: t.name,
                                value: e.replace(Gt, "\r\n")
                            }
                        }) : {
                            name: t.name,
                            value: n.replace(Gt, "\r\n")
                        }
                    }).get()
                }
            }),
            re.ajaxSettings.xhr = void 0 !== e.ActiveXObject ? function() {
                    return !this.isLocal && U() || W()
                }
                : U;
        var Kt = 0
            , Jt = {}
            , Zt = re.ajaxSettings.xhr();
        e.ActiveXObject && re(e).on("unload", function() {
            for (var e in Jt)
                Jt[e](void 0, !0)
        }),
            ne.cors = !!Zt && "withCredentials"in Zt,
            Zt = ne.ajax = !!Zt,
        Zt && re.ajaxTransport(function(e) {
            if (!e.crossDomain || ne.cors) {
                var t;
                return {
                    send: function(n, i) {
                        var o, a = e.xhr(), r = ++Kt;
                        if (a.open(e.type, e.url, e.async, e.username, e.password),
                            e.xhrFields)
                            for (o in e.xhrFields)
                                a[o] = e.xhrFields[o];
                        e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType),
                        e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                        for (o in n)
                            void 0 !== n[o] && a.setRequestHeader(o, n[o] + "");
                        a.send(e.hasContent && e.data || null),
                            t = function(n, o) {
                                var s, c, l;
                                if (t && (o || 4 === a.readyState))
                                    if (delete Jt[r],
                                        t = void 0,
                                        a.onreadystatechange = re.noop,
                                        o)
                                        4 !== a.readyState && a.abort();
                                    else {
                                        l = {},
                                            s = a.status,
                                        "string" == typeof a.responseText && (l.text = a.responseText);
                                        try {
                                            c = a.statusText
                                        } catch (e) {
                                            c = ""
                                        }
                                        s || !e.isLocal || e.crossDomain ? 1223 === s && (s = 204) : s = l.text ? 200 : 404
                                    }
                                l && i(s, c, l, a.getAllResponseHeaders())
                            }
                            ,
                            e.async ? 4 === a.readyState ? setTimeout(t) : a.onreadystatechange = Jt[r] = t : t()
                    },
                    abort: function() {
                        t && t(void 0, !0)
                    }
                }
            }
        }),
            re.ajaxSetup({
                accepts: {
                    script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
                },
                contents: {
                    script: /(?:java|ecma)script/
                },
                converters: {
                    "text script": function(e) {
                        return re.globalEval(e),
                            e
                    }
                }
            }),
            re.ajaxPrefilter("script", function(e) {
                void 0 === e.cache && (e.cache = !1),
                e.crossDomain && (e.type = "GET",
                    e.global = !1)
            }),
            re.ajaxTransport("script", function(e) {
                if (e.crossDomain) {
                    var t, n = ge.head || re("head")[0] || ge.documentElement;
                    return {
                        send: function(i, o) {
                            t = ge.createElement("script"),
                                t.async = !0,
                            e.scriptCharset && (t.charset = e.scriptCharset),
                                t.src = e.url,
                                t.onload = t.onreadystatechange = function(e, n) {
                                    (n || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null,
                                    t.parentNode && t.parentNode.removeChild(t),
                                        t = null,
                                    n || o(200, "success"))
                                }
                                ,
                                n.insertBefore(t, n.firstChild)
                        },
                        abort: function() {
                            t && t.onload(void 0, !0)
                        }
                    }
                }
            });
        var en = []
            , tn = /(=)\?(?=&|$)|\?\?/;
        re.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var e = en.pop() || re.expando + "_" + Dt++;
                return this[e] = !0,
                    e
            }
        }),
            re.ajaxPrefilter("json jsonp", function(t, n, i) {
                var o, a, r, s = !1 !== t.jsonp && (tn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && tn.test(t.data) && "data");
                return s || "jsonp" === t.dataTypes[0] ? (o = t.jsonpCallback = re.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback,
                    s ? t[s] = t[s].replace(tn, "$1" + o) : !1 !== t.jsonp && (t.url += (At.test(t.url) ? "&" : "?") + t.jsonp + "=" + o),
                    t.converters["script json"] = function() {
                        return r || re.error(o + " was not called"),
                            r[0]
                    }
                    ,
                    t.dataTypes[0] = "json",
                    a = e[o],
                    e[o] = function() {
                        r = arguments
                    }
                    ,
                    i.always(function() {
                        e[o] = a,
                        t[o] && (t.jsonpCallback = n.jsonpCallback,
                            en.push(o)),
                        r && re.isFunction(a) && a(r[0]),
                            r = a = void 0
                    }),
                    "script") : void 0
            }),
            re.parseHTML = function(e, t, n) {
                if (!e || "string" != typeof e)
                    return null;
                "boolean" == typeof t && (n = t,
                    t = !1),
                    t = t || ge;
                var i = pe.exec(e)
                    , o = !n && [];
                return i ? [t.createElement(i[1])] : (i = re.buildFragment([e], t, o),
                o && o.length && re(o).remove(),
                    re.merge([], i.childNodes))
            }
        ;
        var nn = re.fn.load;
        re.fn.load = function(e, t, n) {
            if ("string" != typeof e && nn)
                return nn.apply(this, arguments);
            var i, o, a, r = this, s = e.indexOf(" ");
            return s >= 0 && (i = e.slice(s, e.length),
                e = e.slice(0, s)),
                re.isFunction(t) ? (n = t,
                    t = void 0) : t && "object" == typeof t && (a = "POST"),
            r.length > 0 && re.ajax({
                url: e,
                type: a,
                dataType: "html",
                data: t
            }).done(function(e) {
                o = arguments,
                    r.html(i ? re("<div>").append(re.parseHTML(e)).find(i) : e)
            }).complete(n && function(e, t) {
                r.each(n, o || [e.responseText, t, e])
            }
            ),
                this
        }
            ,
            re.expr.filters.animated = function(e) {
                return re.grep(re.timers, function(t) {
                    return e === t.elem
                }).length
            }
        ;
        var on = e.document.documentElement;
        return re.offset = {
            setOffset: function(e, t, n) {
                var i, o, a, r, s, c, l, u = re.css(e, "position"), d = re(e), f = {};
                "static" === u && (e.style.position = "relative"),
                    s = d.offset(),
                    a = re.css(e, "top"),
                    c = re.css(e, "left"),
                    l = ("absolute" === u || "fixed" === u) && re.inArray("auto", [a, c]) > -1,
                    l ? (i = d.position(),
                        r = i.top,
                        o = i.left) : (r = parseFloat(a) || 0,
                        o = parseFloat(c) || 0),
                re.isFunction(t) && (t = t.call(e, n, s)),
                null != t.top && (f.top = t.top - s.top + r),
                null != t.left && (f.left = t.left - s.left + o),
                    "using"in t ? t.using.call(e, f) : d.css(f)
            }
        },
            re.fn.extend({
                offset: function(e) {
                    if (arguments.length)
                        return void 0 === e ? this : this.each(function(t) {
                            re.offset.setOffset(this, e, t)
                        });
                    var t, n, i = {
                        top: 0,
                        left: 0
                    }, o = this[0], a = o && o.ownerDocument;
                    return a ? (t = a.documentElement,
                        re.contains(t, o) ? (typeof o.getBoundingClientRect !== ke && (i = o.getBoundingClientRect()),
                            n = Q(a),
                            {
                                top: i.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                                left: i.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
                            }) : i) : void 0
                },
                position: function() {
                    if (this[0]) {
                        var e, t, n = {
                            top: 0,
                            left: 0
                        }, i = this[0];
                        return "fixed" === re.css(i, "position") ? t = i.getBoundingClientRect() : (e = this.offsetParent(),
                            t = this.offset(),
                        re.nodeName(e[0], "html") || (n = e.offset()),
                            n.top += re.css(e[0], "borderTopWidth", !0),
                            n.left += re.css(e[0], "borderLeftWidth", !0)),
                            {
                                top: t.top - n.top - re.css(i, "marginTop", !0),
                                left: t.left - n.left - re.css(i, "marginLeft", !0)
                            }
                    }
                },
                offsetParent: function() {
                    return this.map(function() {
                        for (var e = this.offsetParent || on; e && !re.nodeName(e, "html") && "static" === re.css(e, "position"); )
                            e = e.offsetParent;
                        return e || on
                    })
                }
            }),
            re.each({
                scrollLeft: "pageXOffset",
                scrollTop: "pageYOffset"
            }, function(e, t) {
                var n = /Y/.test(t);
                re.fn[e] = function(i) {
                    return qe(this, function(e, i, o) {
                        var a = Q(e);
                        return void 0 === o ? a ? t in a ? a[t] : a.document.documentElement[i] : e[i] : void (a ? a.scrollTo(n ? re(a).scrollLeft() : o, n ? o : re(a).scrollTop()) : e[i] = o)
                    }, e, i, arguments.length, null)
                }
            }),
            re.each(["top", "left"], function(e, t) {
                re.cssHooks[t] = T(ne.pixelPosition, function(e, n) {
                    return n ? (n = nt(e, t),
                        ot.test(n) ? re(e).position()[t] + "px" : n) : void 0
                })
            }),
            re.each({
                Height: "height",
                Width: "width"
            }, function(e, t) {
                re.each({
                    padding: "inner" + e,
                    content: t,
                    "": "outer" + e
                }, function(n, i) {
                    re.fn[i] = function(i, o) {
                        var a = arguments.length && (n || "boolean" != typeof i)
                            , r = n || (!0 === i || !0 === o ? "margin" : "border");
                        return qe(this, function(t, n, i) {
                            var o;
                            return re.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement,
                                Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? re.css(t, n, r) : re.style(t, n, i, r)
                        }, t, a ? i : void 0, a, null)
                    }
                })
            }),
            re.fn.size = function() {
                return this.length
            }
            ,
            re.fn.andSelf = re.fn.addBack,
        "function" == typeof define && define.amd && define("jquery", [], function() {
            return re
        }),
            e.jQuery = e.$ = re
    }),
    function(e, t, n) {
        function i(e) {
            return e
        }
        function o(e) {
            var t = "";
            try {
                t = decodeURIComponent(e.replace(a, " "))
            } catch (n) {
                t = unescape(e.replace(a, " "))
            }
            return t
        }
        var a = /\+/g
            , r = e.cookie = function(n, a, s) {
                if (void 0 !== a) {
                    if (s = e.extend({}, r.defaults, s),
                    null === a && (s.expires = -1),
                    "number" == typeof s.expires) {
                        var c = s.expires
                            , l = s.expires = new Date;
                        l.setDate(l.getDate() + c)
                    }
                    return a = r.json ? JSON.stringify(a) : String(a),
                        t.cookie = [encodeURIComponent(n), "=", r.raw ? a : encodeURIComponent(a), s.expires ? "; expires=" + s.expires.toUTCString() : "", s.path ? "; path=" + s.path : "", s.domain ? "; domain=" + s.domain : "", s.secure ? "; secure" : ""].join("")
                }
                for (var u = r.raw ? i : o, d = t.cookie.split("; "), f = 0, p = d.length; p > f; f++) {
                    var h = d[f].split("=");
                    if (u(h.shift()) === n) {
                        var m = u(h.join("="));
                        return r.json ? JSON.parse(m) : m
                    }
                }
                return null
            }
        ;
        r.defaults = {},
            e.removeCookie = function(t, n) {
                return null !== e.cookie(t) && (e.cookie(t, null, n),
                    !0)
            }
    }(jQuery, document),
    jQuery.easing.jswing = jQuery.easing.swing,
    jQuery.extend(jQuery.easing, {
        def: "easeOutQuad",
        swing: function(e, t, n, i, o) {
            return jQuery.easing[jQuery.easing.def](e, t, n, i, o)
        },
        easeInQuad: function(e, t, n, i, o) {
            return i * (t /= o) * t + n
        },
        easeOutQuad: function(e, t, n, i, o) {
            return -i * (t /= o) * (t - 2) + n
        },
        easeInOutQuad: function(e, t, n, i, o) {
            return (t /= o / 2) < 1 ? i / 2 * t * t + n : -i / 2 * (--t * (t - 2) - 1) + n
        },
        easeInCubic: function(e, t, n, i, o) {
            return i * (t /= o) * t * t + n
        },
        easeOutCubic: function(e, t, n, i, o) {
            return i * ((t = t / o - 1) * t * t + 1) + n
        },
        easeInOutCubic: function(e, t, n, i, o) {
            return (t /= o / 2) < 1 ? i / 2 * t * t * t + n : i / 2 * ((t -= 2) * t * t + 2) + n
        },
        easeInQuart: function(e, t, n, i, o) {
            return i * (t /= o) * t * t * t + n
        },
        easeOutQuart: function(e, t, n, i, o) {
            return -i * ((t = t / o - 1) * t * t * t - 1) + n
        },
        easeInOutQuart: function(e, t, n, i, o) {
            return (t /= o / 2) < 1 ? i / 2 * t * t * t * t + n : -i / 2 * ((t -= 2) * t * t * t - 2) + n
        },
        easeInQuint: function(e, t, n, i, o) {
            return i * (t /= o) * t * t * t * t + n
        },
        easeOutQuint: function(e, t, n, i, o) {
            return i * ((t = t / o - 1) * t * t * t * t + 1) + n
        },
        easeInOutQuint: function(e, t, n, i, o) {
            return (t /= o / 2) < 1 ? i / 2 * t * t * t * t * t + n : i / 2 * ((t -= 2) * t * t * t * t + 2) + n
        },
        easeInSine: function(e, t, n, i, o) {
            return -i * Math.cos(t / o * (Math.PI / 2)) + i + n
        },
        easeOutSine: function(e, t, n, i, o) {
            return i * Math.sin(t / o * (Math.PI / 2)) + n
        },
        easeInOutSine: function(e, t, n, i, o) {
            return -i / 2 * (Math.cos(Math.PI * t / o) - 1) + n
        },
        easeInExpo: function(e, t, n, i, o) {
            return 0 == t ? n : i * Math.pow(2, 10 * (t / o - 1)) + n
        },
        easeOutExpo: function(e, t, n, i, o) {
            return t == o ? n + i : i * (1 - Math.pow(2, -10 * t / o)) + n
        },
        easeInOutExpo: function(e, t, n, i, o) {
            return 0 == t ? n : t == o ? n + i : (t /= o / 2) < 1 ? i / 2 * Math.pow(2, 10 * (t - 1)) + n : i / 2 * (2 - Math.pow(2, -10 * --t)) + n
        },
        easeInCirc: function(e, t, n, i, o) {
            return -i * (Math.sqrt(1 - (t /= o) * t) - 1) + n
        },
        easeOutCirc: function(e, t, n, i, o) {
            return i * Math.sqrt(1 - (t = t / o - 1) * t) + n
        },
        easeInOutCirc: function(e, t, n, i, o) {
            return (t /= o / 2) < 1 ? -i / 2 * (Math.sqrt(1 - t * t) - 1) + n : i / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + n
        },
        easeInElastic: function(e, t, n, i, o) {
            var a = 1.70158
                , r = 0
                , s = i;
            if (0 == t)
                return n;
            if (1 == (t /= o))
                return n + i;
            if (r || (r = .3 * o),
            s < Math.abs(i)) {
                s = i;
                var a = r / 4
            } else
                var a = r / (2 * Math.PI) * Math.asin(i / s);
            return -s * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t * o - a) * Math.PI / r) + n
        },
        easeOutElastic: function(e, t, n, i, o) {
            var a = 1.70158
                , r = 0
                , s = i;
            if (0 == t)
                return n;
            if (1 == (t /= o))
                return n + i;
            if (r || (r = .3 * o),
            s < Math.abs(i)) {
                s = i;
                var a = r / 4
            } else
                var a = r / (2 * Math.PI) * Math.asin(i / s);
            return s * Math.pow(2, -10 * t) * Math.sin(2 * (t * o - a) * Math.PI / r) + i + n
        },
        easeInOutElastic: function(e, t, n, i, o) {
            var a = 1.70158
                , r = 0
                , s = i;
            if (0 == t)
                return n;
            if (2 == (t /= o / 2))
                return n + i;
            if (r || (r = .3 * o * 1.5),
            s < Math.abs(i)) {
                s = i;
                var a = r / 4
            } else
                var a = r / (2 * Math.PI) * Math.asin(i / s);
            return 1 > t ? -.5 * s * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t * o - a) * Math.PI / r) + n : s * Math.pow(2, -10 * (t -= 1)) * Math.sin(2 * (t * o - a) * Math.PI / r) * .5 + i + n
        },
        easeInBack: function(e, t, n, i, o, a) {
            return void 0 == a && (a = 1.70158),
            i * (t /= o) * t * ((a + 1) * t - a) + n
        },
        easeOutBack: function(e, t, n, i, o, a) {
            return void 0 == a && (a = 1.70158),
            i * ((t = t / o - 1) * t * ((a + 1) * t + a) + 1) + n
        },
        easeInOutBack: function(e, t, n, i, o, a) {
            return void 0 == a && (a = 1.70158),
                (t /= o / 2) < 1 ? i / 2 * t * t * ((1 + (a *= 1.525)) * t - a) + n : i / 2 * ((t -= 2) * t * ((1 + (a *= 1.525)) * t + a) + 2) + n
        },
        easeInBounce: function(e, t, n, i, o) {
            return i - jQuery.easing.easeOutBounce(e, o - t, 0, i, o) + n
        },
        easeOutBounce: function(e, t, n, i, o) {
            return (t /= o) < 1 / 2.75 ? 7.5625 * i * t * t + n : 2 / 2.75 > t ? i * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + n : 2.5 / 2.75 > t ? i * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + n : i * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + n
        },
        easeInOutBounce: function(e, t, n, i, o) {
            return o / 2 > t ? .5 * jQuery.easing.easeInBounce(e, 2 * t, 0, i, o) + n : .5 * jQuery.easing.easeOutBounce(e, 2 * t - o, 0, i, o) + .5 * i + n
        }
    }),
    $.fn.smoothScroll = function(e, t) {
        var n = this;
        return this.step = e ? e.step || 100 : 100,
            this.f = e ? e.f || .1 : .1,
            this.interval = 10,
            this.intervalID = null,
            this.isFF = 0 <= navigator.userAgent.toLowerCase().indexOf("firefox"),
            this.upOrDown = "",
            this.init = function(e) {
                var t = this;
                t.isFF ? e.addEventListener("DOMMouseScroll", function(n) {
                    t.upOrDown = 0 > n.detail ? "up" : "down",
                        t.scrollHandler(e),
                        n.preventDefault()
                }, !1) : e.onmousewheel = function(n) {
                    n = n || window.event,
                        t.upOrDown = 0 < n.wheelDelta ? "up" : "down",
                        t.scrollHandler(e),
                        n.preventDefault ? n.preventDefault() : n.returnValue = !1
                }
            }
            ,
            this.scrollHandler = function(e) {
                var t = this;
                clearInterval(t.intervalID);
                var n = e.scrollTop + t.step * ("up" == t.upOrDown ? -1 : 1);
                t.intervalID = setInterval(function() {
                    e.scrollTop += (n - e.scrollTop) * t.f,
                    n != e.scrollTop && e.scrollTop != t.lastScrollTop || clearInterval(t.intervalID),
                        t.lastScrollTop = e.scrollTop
                }, t.interval)
            }
            ,
            n.each(function(e, t) {
                n.init(t)
            }),
            this
    }
    ,
    define("j/yp/lib/require-jquery-min", function() {}),
    function(e) {
        e.mods = {
            modList: {
                "yp-objList": {
                    js: "../yp/lib/yp-objList"
                },
                "yp-socket": {
                    js: "../yp/yp-socket"
                },
                "yp-socket-avatar": {
                    js: "../yp/yp-socket-avatar"
                },
                "yp-chat": {
                    js: "../yp/lib/yp-chat",
                    shim: ["yp-objList"]
                },
                mvvm: {
                    js: "../yp/lib/avalon"
                },
                ibox: {
                    js: "jquery.ibox"
                },
                flashobject: {
                    js: "swfobject"
                },
                json: {
                    js: "json3.min"
                },
                bootstrap: {
                    js: "bootstrap/js/bootstrap",
                    jq: ["tooltip", "popover"]
                },
                tabs: {
                    js: "tabs.jquery"
                },
                scroll: {
                    js: "jquery.slimscroll.min"
                },
                lazyload: {
                    js: "jquery.lazyload.min"
                },
                mousewheel: {
                    js: "jquery.mousewheel"
                },
                fileupload: {
                    js: "ajaxfileupload"
                },
                colpick: {
                    js: "colpick/js/colpick"
                },
                editor: {
                    js: "editor/kindeditor/kindeditor"
                },
                ZeroClipboard: {
                    js: "ZeroClipboard/ZeroClipboard.min"
                },
                xss: {
                    js: "xss"
                },
                inotify: {
                    js: "jquery.inotify"
                },
                pagination: {
                    js: "pagination"
                },
                pagination2: {
                    js: "pagination2"
                },
                titleTips: {
                    js: "jquery.titleTips"
                },
                WdatePicker: {
                    js: "datepicker/WdatePicker"
                },
                qrcode: {
                    js: "jquery.qrcode"
                },
                lottery: {
                    js: "jquery.lottery"
                },
                jCopy: {
                    js: "jquery.jCopy"
                },
                videoHover: {
                    js: "jquery.videoHover"
                },
                carousel: {
                    js: "jquery.carousel"
                },
                rich: {
                    js: "rich"
                },
                pca: {
                    js: "pca"
                },
                pcaData: {
                    js: "pcaData"
                },
                "room-mainVideo": {
                    js: "room-widget/mainVideo",
                    shim: ["mvvm", "xss"]
                },
                highcharts: {
                    js: "highcharts/highcharts"
                },
                rotate: {
                    js: "jQueryRotate.2.2"
                },
                carousel2: {
                    js: "carousel2"
                },
                "state-machine": {
                    js: "state-machine.min"
                },
                stardesc: {
                    js: "jquery.stardesc"
                },
                newsroll: {
                    js: "jquery.newsroll"
                },
                "velocity.min": {
                    js: "velocity.min"
                },
                "snowfall.jquery": {
                    js: "snowfall.jquery"
                },
                "jquery.vote": {
                    js: "jquery.vote"
                },
                rechargeData: {
                    js: "rechargeData"
                },
                zqSelect: {
                    js: "jquery.select"
                },
                zqSearch: {
                    js: "jquery.search"
                },
                phoneCode: {
                    js: "jquery.phoneCode"
                },
                phoneCheck: {
                    js: "phoneCheck",
                    shim: ["phoneCode"]
                },
                keepCard: {
                    js: "keepCard"
                },
                buyFire: {
                    js: "buyFire"
                },
                logList: {
                    js: "log-common",
                    shim: ["pagination"]
                },
                videoList: {
                    js: "jquery.videoList"
                },
                paginationList: {
                    js: "jquery.paginationList",
                    shim: ["pagination"]
                },
                "jquery.pagetransitions": {
                    js: "jquery.pagetransitions"
                },
                buyCar: {
                    js: "buyCar"
                },
                buyDefend: {
                    js: "buyDefendNew"
                },
                buyProp: {
                    js: "buyProp"
                },
                "jquery.gameSelect": {
                    js: "jquery.gameSelect"
                },
                geetest: {
                    js: "geetest"
                },
                checkMobile: {
                    js: "checkMobile"
                },
                createjs: {
                    js: "createjs.min"
                },
                propList: {
                    js: "propList",
                    shim: ["jquery.carousel3"]
                },
                loginModule: {
                    js: "loginModule"
                },
                registModule: {
                    js: "registModule"
                },
                popRecharge: {
                    js: "popRecharge"
                },
                adminpanel: {
                    js: "room4.0/adminpanel",
                    shim: ["pagination"]
                },
                base: {
                    js: "room4.0/base",
                    shim: ["md5"]
                },
                defendNew: {
                    js: "room4.0/defend"
                },
                forAnchor: {
                    js: "room4.0/forAnchor"
                },
                gift: {
                    js: "room4.0/gift"
                },
                firework: {
                    js: "room4.0/firework"
                },
                welfare: {
                    js: "room4.0/welfare"
                },
                addSpeed: {
                    js: "room4.0/addSpeed"
                },
                googleNew: {
                    js: "room4.0/google"
                },
                guess: {
                    js: "room4.0/guessNew"
                },
                anchorLottery: {
                    js: "room4.0/anchorNewLottery"
                },
                anchorTask: {
                    js: "room4.0/anchorTask"
                },
                danmuSetting: {
                    js: "room4.0/danmuSetting"
                },
                main: {
                    js: "room4.0/main",
                    shim: ["xss"]
                },
                planeTicketNew: {
                    js: "room4.0/planeTicket",
                    shim: ["xss", "jquery.gameSelect"]
                },
                task: {
                    js: "room4.0/task",
                    shim: ["localStorageExpire"]
                },
                taskDataManage: {
                    js: "taskDataManage"
                },
                taskPanel: {
                    js: "room4.0/taskPanel",
                    shim: ["localStorageExpire"]
                },
                gameTaskGroup: {
                    js: "room4.0/task/gameTaskGroup",
                    shim: ["jquery.carousel3"]
                },
                taskGroup: {
                    js: "room4.0/task/taskGroup"
                },
                activationCode: {
                    js: "room4.0/activationCode"
                },
                translateNew: {
                    js: "room4.0/translate"
                },
                tianshuNew: {
                    js: "room4.0/tianshu",
                    shim: ["jquery.gameSelect"]
                },
                guide: {
                    js: "room4.0/guide"
                },
                hongbaoNew: {
                    js: "room4.0/hongbao"
                },
                popRechargeNew: {
                    js: "popRechargeNew"
                },
                roomPackage: {
                    js: "room4.0/roomPackage"
                },
                keyWordDropping: {
                    js: "room4.0/keyWordDropping"
                },
                lotterySlot: {
                    js: "lotterySlot"
                },
                slotmachine: {
                    js: "room4.0/slotmachine"
                },
                artqiyi: {
                    js: "room4.0/artqiyi"
                },
                lotteryTopic: {
                    js: "lottery"
                },
                oRoomMedal: {
                    js: "room4.0/roomMedal"
                },
                roomBannerCamp: {
                    js: "room4.0/roomBannerCamp"
                },
                specialEffects: {
                    js: "room4.0/specialEffects",
                    shim: ["createjs"]
                },
                roomReport: {
                    js: "room4.0/roomReport"
                },
                beanRank: {
                    js: "room4.0/beanRank"
                },
                packageEnter: {
                    js: "room4.0/packageEnter"
                },
                chargeEnter: {
                    js: "room4.0/chargeEnter"
                },
                luckyBag: {
                    js: "room4.0/luckyBag"
                },
                activeCenter: {
                    js: "room4.0/activeCenter"
                },
                videoPlayer: {
                    js: "jquery.videoPlayer"
                },
                videoJS: {
                    js: "video/video"
                },
                videoJSForHLS: {
                    js: "video/videojs-contrib-hls.min"
                },
                dfp: {
                    js: "dfp"
                },
                timegift: {
                    js: "jquery.timegift"
                },
                frameAnimation: {
                    js: "jquery.frameAnimation"
                },
                "jquery.fullPage": {
                    js: "jquery.fullPage",
                    shim: ["scroll"]
                },
                wsdk: {
                    js: "im/wsdk"
                },
                im: {
                    js: "im/base"
                },
                simpleIm: {
                    js: "im/simple"
                },
                fullIm: {
                    js: "im/full"
                },
                roomIm: {
                    js: "im/room"
                },
                friendManage: {
                    js: "im/friend"
                },
                imSetting: {
                    js: "im/setting"
                },
                imBobao: {
                    js: "im/bobao"
                },
                imZan: {
                    js: "im/zan"
                },
                imComment: {
                    js: "im/comment"
                },
                drag: {
                    js: "jquery.drag"
                },
                geetest3: {
                    js: "geetest3"
                },
                verification: {
                    js: "verification"
                },
                localStorageExpire: {
                    js: "localStorageExpire"
                },
                audioPlayer: {
                    js: "audioPlayer"
                },
                loginRegistModule: {
                    js: "loginRegistModule"
                },
                Base64: {
                    js: "base64"
                },
                defendEffect: {
                    js: "defendEffect"
                },
                car2017: {
                    js: "room4.0/car2017"
                },
                "jquery.carousel3": {
                    js: "jquery.carousel3"
                },
                carPanel: {
                    js: "carPanel",
                    shim: ["jquery.carousel3"]
                },
                waterfall: {
                    js: "waterfall"
                },
                makeQrcode: {
                    js: "qrcode.min"
                },
                dbqb: {
                    js: "dbqb",
                    shim: ["pagination"]
                },
                pushNew: {
                    js: "room4.0/pushNew"
                },
                oMedalPanel: {
                    js: "medalPanel"
                },
                guardPanel: {
                    js: "room4.0/guardPanel"
                },
                propPanel2017: {
                    js: "room4.0/propPanel2017",
                    shim: ["pagination"]
                },
                chargePanel: {
                    js: "chargePanel"
                },
                zqCharge: {
                    js: "zqCharge"
                },
                dexie: {
                    js: "dexie.min"
                },
                cropper: {
                    js: "cropper/cropper"
                },
                chatTextarea: {
                    js: "room4.0/chatTextarea"
                },
                businessCard: {
                    js: "room4.0/businessCard",
                    shim: ["jquery.carousel3"]
                },
                bodyMovin: {
                    js: "lottie"
                },
                md5: {
                    js: "md5"
                },
                emotData: {
                    js: "emotList"
                },
                anchorRelay: {
                    js: "room4.0/anchorRelay"
                },
                eatchicken: {
                    js: "room4.0/eatchicken"
                },
                callVideo: {
                    js: "room4.0/callVideo"
                },
                activeGuess: {
                    js: "room4.0/activeGuess"
                },
                appgameSlide: {
                    js: "appgameSlide"
                },
                zqShare: {
                    js: "jquery.share"
                },
                videoComment: {
                    js: "videoPage/comment"
                },
                videoRecommend: {
                    js: "videoPage/recommend"
                },
                videoSendComment: {
                    js: "videoPage/sendComment"
                },
                vipLevel: {
                    js: "room4.0/vipLevel"
                },
                matchGuess: {
                    js: "room4.0/matchGuess"
                },
                xplay: {
                    js: "room4.0/xplay"
                },
                userForbidden: {
                    js: "room4.0/userForbidden"
                },
                apng: {
                    js: "apng-canvas.min"
                },
                roomRecommend: {
                    js: "room4.0/roomRecommend"
                },
                vue: {
                    js: "vue"
                },
                bootstrapNew: {
                    js: "bootstrapNew/bootstrap.min"
                },
                moment: {
                    js: "moment"
                },
                flatpickr: {
                    js: "flatpickr/flatpickr"
                },
                playerMaskProp: {
                    js: "room4.0/playerMaskProp"
                },
                glDatePicker: {
                    js: "glDatePicker.min"
                },
                roomIntoGif: {
                    js: "room4.0/roomAnimation"
                },
                prayPanel: {
                    js: "../active/2018/12/yearParty/prayPanel"
                }
            }
        }
    }(window),
    define("j/yp/yp-mods-config", function() {}),
    function() {
        var e = {
            myRich: {
                coin: 0,
                gold: 0,
                goldLocked: 0,
                goldEnable: 1,
                coinEnable: 1,
                bullet: 0
            },
            bInit: !1,
            options: {
                sGetRichUrl: "/api/user/rich.get"
            },
            init: function(e) {
                var t = this;
                if (t.bInit)
                    return !1;
                yp.module.fRegistModule("oRich", t),
                    t.bInit = !0,
                    t.options = $.extend(!0, t.options, e),
                    t.listenYpEvent()
            },
            listenYpEvent: function() {
                var e = this;
                yp.sub("page/login/done", function() {
                    e.fGetRich()
                }),
                    yp.sub("page/regist/done", function() {
                        e.fGetRich()
                    }),
                    yp.sub("page/logout/done", function() {
                        e.fGetRich()
                    }),
                    yp.sub("page/rich/update", function(t, n) {
                        e.fRichUpdate(n.num, n.type)
                    }),
                    yp.sub("page/rich/set", function(t, n) {
                        e.fRichSet(n.num, n.type)
                    }),
                    yp.sub("page/rich/ipcheck", function(t) {
                        yp.use("checkMobile", function(t, n) {
                            n.init();
                            var i = {};
                            oPageConfig.oRoom && (i.roomId = oPageConfig.oRoom.id),
                                t.pub("page/checkmobile/show", {
                                    bGotCode: !0,
                                    params: i,
                                    ajaxUrl: "/api/user/user.safe_check",
                                    callback: function(e) {
                                        0 == e.code && t.successNotify("验证成功")
                                    },
                                    selfCall: e
                                })
                        })
                    });
                var t;
                yp.sub("page/gold/upcheck", function(e, n) {
                    t && t.close();
                    var i = n.source || "gift"
                        , o = n.callback;
                    if (n = n.data,
                        n.phone) {
                        var a = "";
                        switch (i) {
                            case "gift":
                                a = '<p style="font-size: 14px;text-align: left;line-height: 22px;padding: 0 10px;">消费验证短信已发送到<span style="color: red">' + n.phone + "</span>，请注意查收<br />请根据短信提示进行操作！若手机上已经完成操作，请点击“已完成”！</p>";
                                break;
                            default:
                                a = '<p style="font-size: 14px;text-align: left;line-height: 22px;padding: 0 10px;">为了您的账户安全，我们需要对您本次消费行为进行验证，验证短信已发送到<span style="color: red">' + n.phone + "</span>，请注意查收并根据短信提示进行操作，完成操作后点击“已完成”</p>"
                        }
                        t = $.ibox({
                            title: "提示信息",
                            content: a,
                            btns: [{
                                name: "已完成",
                                callback: function() {
                                    this.close(),
                                    o && o()
                                }
                            }],
                            width: 370
                        })
                    } else
                        t = $.ibox({
                            title: "提示信息",
                            content: '<p style="font-size: 14px;text-align: left;line-height: 22px;padding: 0 10px;">请先前往绑定手机</p>',
                            btns: [{
                                name: "确定",
                                callback: function() {
                                    this.close(),
                                        window.open("/user/bind/mobile")
                                }
                            }],
                            width: 370
                        })
                }),
                    yp.sub("page/gold/upcheck/return", function(e, n) {
                        t && t.close(),
                            n = n.data,
                            1 == n.status ? alert("财富安全校验失败") : 2 == n.status && alert("财富安全校验成功")
                    })
            },
            fRichInit: function() {
                var e = this;
                e.myRich = {
                    coin: 0,
                    gold: 0,
                    goldLocked: 0,
                    goldEnable: 1,
                    coinEnable: 1,
                    bullet: 0
                },
                    yp.pub("page/rich/change", e.myRich)
            },
            fGetRich: function() {
                var e = this;
                oPageConfig.oLoginInfo ? oPageConfig.oLoginInfo.rich ? e.fSetRich({
                    coin: {
                        count: oPageConfig.oLoginInfo.rich.coin,
                        enable: 1
                    },
                    gold: {
                        count: oPageConfig.oLoginInfo.rich.gold,
                        lock: 0,
                        enable: 1
                    },
                    bullet: {
                        count: oPageConfig.oLoginInfo.rich.bullet || 0
                    }
                }) : e.fGetRichAjax() : e.fRichInit()
            },
            fGetRichAjax: function() {
                var e = this;
                yp.ajax(e.options.sGetRichUrl, {
                    dataType: "json"
                }).done(function(t) {
                    0 == t.code ? e.fSetRich(t.data) : e.fRichInit()
                })
            },
            fSetRich: function(e) {
                var t = this;
                t.myRich.coin = e.coin.count,
                    t.myRich.gold = e.gold.count,
                    t.myRich.goldLocked = e.gold.lock,
                    t.myRich.goldEnable = +e.gold.enable,
                    t.myRich.coinEnable = +e.coin.enable,
                    t.myRich.bullet = e.bullet ? +e.bullet.count : 0,
                    yp.pub("page/rich/change", t.myRich)
            },
            fReturnRich: function(e) {
                return e ? this.myRich[e] : this.myRich
            },
            fRichUpdate: function(e, t) {
                if (!t || void 0 == this.myRich[t])
                    return !1;
                this.myRich[t] = +this.myRich[t] + +e,
                    yp.pub("page/rich/change", this.myRich)
            },
            fRichSet: function(e, t) {
                if (!t || void 0 == this.myRich[t])
                    return !1;
                this.myRich[t] = +e,
                    yp.pub("page/rich/change", this.myRich)
            }
        };
        "undefined" != typeof module ? module.exports = e : "function" == typeof define && define.amd ? define("rich", [], function() {
            return e
        }) : window.oRich = e
    }(),
    function() {
        var e = {};
        e.TaskTypeDic = {
            1: {
                key: "newbie",
                name: "新手任务"
            },
            2: {
                key: "daily",
                name: "日常任务"
            }
        };
        var t = {
            taskFormIdIndex: {
                danmu: 2,
                dingyue: 3,
                liwu: 4,
                fenxiang: 5
            },
            data: {
                newbie: {
                    defaultGet: !0,
                    dfd: null,
                    list: []
                },
                daily: {
                    defaultGet: !1,
                    dfd: null,
                    list: []
                }
            },
            init: function() {
                this.listenYpEvent()
            },
            listenYpEvent: function() {
                var e = this;
                yp.sub("page/logout/done", function() {
                    e.data.newbie.dfd = null,
                        e.data.newbie.list = [],
                        e.data.daily.dfd = null,
                        e.data.daily.list = []
                }),
                    yp.sub("page/task/part_complete", function(t, n) {
                        var i = e.taskFormIdIndex[n.type];
                        i && (e.fUpdateTaskData(i, n.num, 1),
                            e.fUpdateTaskData(i, n.num, 2),
                            yp.pub("page/task/part_complete/after", n))
                    })
            },
            fGetTaskInfo: function(t, n) {
                var i = this
                    , o = e.TaskTypeDic[t];
                if (void 0 === o) {
                    var a = $.Deferred();
                    return a.reject("类型不存在"),
                        a
                }
                var r = i.data[o.key];
                return r.dfd && "pending" === r.dfd.state() && (n = !1),
                !n && r.dfd && "rejected" !== r.dfd.state() || (r.dfd = $.Deferred(),
                    yp.ajax("/api/user/task.get", {
                        noCache: !0,
                        data: {
                            type: t
                        }
                    }).done(function(e) {
                        0 == +e.code ? (r.list = e.data,
                        n && yp.pub("page/task/data/update", {
                            type: t
                        }),
                            r.dfd.resolve()) : r.dfd.reject(e.message)
                    }).fail(function() {
                        r.dfd.reject("旗娘正忙，请稍后再试~")
                    })),
                    r.dfd.promise()
            },
            fUpdateTaskData: function(e, t, n) {
                var i = this
                    , o = (i._ui,
                    1 == n ? i.data.newbie.list : i.data.daily.list);
                if (0 == o.length)
                    return !1;
                yp.each(o, function(n) {
                    n.formId == e && n.progress.total > n.progress.current && (n.progress.current = n.progress.current + t > n.progress.total ? n.progress.total : n.progress.current + t)
                })
            }
        };
        "undefined" != typeof module ? module.exports = t : "function" == typeof define && define.amd ? define("taskDataManage", [], function() {
            return t
        }) : window.oTaskDataManage = t
    }(),
    function(e, t, n) {
        var i = function(e, t) {
            return "object" != typeof t && (t = [].slice.call(arguments, 1)),
                e.replace(/{(\w+)}/g, function(e, n) {
                    return t[n]
                })
        }
            , o = {
            zIndex: 10,
            maskContainer: "body",
            classes: {
                ibox: "ibox",
                cur: "active",
                maskLayer: "maskLayer",
                iboxProxy: "iboxProxy",
                hd: "hd",
                closeBox: "closeBox",
                bd: "bd",
                ibox_msg: "ibox_msg",
                ft: "ft",
                dom: {},
                them: {
                    iboxThemDefault: {}
                },
                ico: {
                    msg: "ico_32_color",
                    btnY: "greenIt",
                    btnN: "basic",
                    greenIt: "ico_16_white i37",
                    basic: "ico_16_gray i36",
                    suc: " i1",
                    err: " i2",
                    warn: " i3",
                    cfm: " i4"
                }
            }
        }
            , a = (e("body"),
                function(e) {
                    return new a.fn.init(e)
                }
        );
        a.fn = a.prototype = {
            constructor: a,
            ibox: "1.7",
            init: function(t) {
                "string" === e.type(t) && (t = {
                    content: t
                }),
                    t.id = t.id || "ibox_" + o.zIndex + "_" + e.now();
                var n = {
                    width: 260,
                    title: "标题",
                    x: null,
                    y: null,
                    animate: !1,
                    hasMask: !1,
                    maskColor: "",
                    drag: !1,
                    dragHandle: ".hd",
                    them: "iboxThemDefault",
                    data: {},
                    btns: null,
                    tmpl: {
                        div: '<div class="{0}"></div>',
                        ico: '<i class="{0}"></i>',
                        title: "<strong>{0}</strong>"
                    }
                };
                return t = e.extend(!0, n, e.ibox.data().defaults, t),
                    this._view(t),
                    this._bindEvent(),
                    this.dom.data("ibox", this),
                    this._render(),
                    this
            },
            _view: function(t) {
                var n, a, s;
                return n = this.data("conf", t),
                    a = t.tmpl,
                t.hasMask && n.data("domMask", e(i(a.div, r.maskLayer + " " + t.maskColor)).appendTo(o.maskContainer)),
                    s = e(i(a.ibox, r.ibox, i(a.div, r.bd))).attr("tabindex", "-1"),
                    n.data("dom", s).head(t.title, t.ico).foot(t.btns).content(t.content).size(t.width, t.height, t.minHeight),
                    s.appendTo(e("body")),
                    n.position(t.x, t.y, t.animate),
                    n
            },
            _bindEvent: function() {
                var t = this
                    , n = t.data()
                    , i = t.dom;
                n.drag && i.on("mousedown", n.dragHandle, function(n) {
                    if (n.target.className === r.closeBox)
                        return !1;
                    i.data("toggleClass", "move", !0);
                    var o, a, s, c;
                    o = n.clientX,
                        a = n.clientY,
                        s = t.data("left"),
                        c = t.data("top"),
                        e("body").on("mousemove.ibox_move", function(e) {
                            t.data("offset", {
                                left: s - (o - e.clientX),
                                top: c - (a - e.clientY)
                            })
                        }),
                        i.on("mouseup.ibox_move", function() {
                            e("body").off("mousemove.ibox_move"),
                                i.off("mouseup.ibox_move"),
                                i.data("toggleClass", "move", !1)
                        })
                })
            },
            _render: function() {
                var e = this;
                return e.data("content") ? e.data("toggle", !0).focus() : e.data("toggle", !1),
                    e
            },
            render: function(e) {
                var t = this;
                return e && t.data("content", e).content(e).position(conf.x, conf.y, conf.animate),
                    t._render(),
                    t
            },
            close: function() {
                var t = this;
                t.triggerEvent("dom.destroy", t.getDom(r.bd));
                var n = t.data("siblings", "." + r.ibox);
                t.data("domWithMask").remove();
                var i, o = 0;
                return e.each(n, function(e, t) {
                    var n = t.style.zIndex;
                    o < n && (o = n,
                        i = t)
                }),
                o && e(i).ibox().focus(!1),
                    t
            },
            focus: function(e) {
                var t = this;
                return t.data("toggleClass", r.cur, !0),
                    t.zIndex(),
                    t.getDom("btnY").focus(),
                    t
            },
            zIndex: function(e) {
                var t = this;
                e = e || ++o.zIndex,
                    t.dom.css("zIndex", e),
                    t.data("domWithMask").data("css", "zIndex", e)
            },
            size: function(e, t, n) {
                var i = this;
                return e && i.data("css", "width", e),
                t && i.data("height", t),
                n && i.data("css", "min-height", n),
                    i
            },
            position: function(n, i, o) {
                var a, r = this, s = r.getDom(), c = e(t) || e("body");
                return a = {
                    left: null != n ? n : (e("body").outerWidth() - s.outerWidth()) / 2,
                    top: null != i ? i : (c.outerHeight() - s.outerHeight()) / 2
                },
                    o || s.is(":animated") ? (s.stop().css({
                        top: 0,
                        left: a.left
                    }),
                        s.animate(a, o, function() {})) : s.css(a),
                    r
            },
            head: function(t, n) {
                var o = this;
                if (t || n) {
                    var a, s;
                    a = o.data("tmpl"),
                        s = e(i(a.hd, r.hd)),
                    n && s.append(i(a.ico, n)),
                    t && s.append(i(a.title, t)),
                        s.append(i(a.closeBox, r.closeBox)),
                        s.prependTo(o.dom)
                }
                return o
            },
            content: function(e) {
                var t = this;
                t.data();
                return t.triggerEvent("dom.create", t.getDom(r.bd).html(e)),
                    t
            },
            foot: function(t) {
                var n = this;
                if (t) {
                    var o, a, s;
                    o = n.data("tmpl"),
                        a = r.ico,
                        s = e(i(o.div, r.ft)),
                        e.each(t, function(t, r) {
                            e.isFunction(r) && (r = {
                                callback: r
                            }),
                                r.name = r.name || (t ? "取消" : "确定"),
                                r.them = r.them || a[t ? "btnN" : "btnY"],
                                r.ico = r.ico || a[r.them],
                                r.callback = r.callback || function() {
                                    this.close()
                                }
                                ,
                                e("<button>", {
                                    html: (r.ico ? i(o.ico, r.ico) : "") + r.name,
                                    click: e.proxy(r.callback, n)
                                }).addClass("btn " + r.them).appendTo(s)
                        }),
                        s.appendTo(n.dom)
                }
                return n
            },
            getDom: function(e, t) {
                var n = this;
                return arguments.length ? "btnY" === e ? n.dom.find(":button:first") : "btnN" === e ? n.getDom(r.ft).find(":button:last") : n.dom.find("." + e) : n.dom
            },
            data: function(t, n) {
                var i = this;
                if (!arguments.length)
                    return i.conf;
                if (1 === arguments.length)
                    return e.inArray(t, ["conf", "dom"]) > -1 ? i[t] : e.inArray(t, ["left", "top"]) > -1 ? parseInt(i.data("css", "margin-" + t)) : "domWithMask" === t ? i.dom.add(i.data("domMask")) : e.inArray(t, ["btnY", "btnN"]) > -1 ? i.getDom(t) : "isCur" === t ? i.dom.hasClass(r.cur) : "isFocus" === t ? i.dom.find(":focus").length : "isMsg" === t ? i.getDom(r.ibox_msg).length : i.conf[t];
                if (2 === arguments.length && e.inArray(t, ["css", "attr", "outerWidth", "outerHeight", "siblings"]) > -1)
                    return i.dom[t](n);
                if (e.inArray(t, ["conf", "dom"]) > -1)
                    i[t] = n;
                else if (e.inArray(t, ["toggleClass", "css", "attr", "removeAttr", "height"]) > -1)
                    i.dom[t].apply(i.dom, [].slice.call(arguments, 1)),
                    "toggleClass" === t && "move" === n && (arguments[2] ? (i.getDom(r.bd).css("visibility", "hidden"),
                        i.dom.css("opacity", "0.6")) : (i.getDom(r.bd).css("visibility", ""),
                        i.dom.css("opacity", "")));
                else if (e.inArray(t, ["toggle"]) > -1)
                    i.data(n ? "removeAttr" : "attr", "hidden");
                else if (e.inArray(t, ["left", "top"]) > -1)
                    i.data("css", "margin-" + t, n);
                else if (e.inArray(t, ["offset"]) > -1)
                    for (var o in n)
                        i.data(o, n[o]);
                else
                    e.inArray(t, ["btnY", "btnN"]) > -1 ? i.getDom(t).click() : i.conf[t] = n;
                return i
            },
            triggerEvent: function(e, t) {
                var n = this;
                return t = t || n.dom,
                    t.trigger({
                        type: e,
                        domTarget: t
                    }),
                    n
            }
        },
            a.fn.init.prototype = a.fn,
            a.extend = a.fn.extend = function(t) {
                e.extend(this, t)
            }
            ,
            a.extend({
                init: function() {
                    var t = {
                        ESC: 27,
                        ENTER: 13
                    };
                    e("body").on("keyup", "." + r.ibox + "." + r.cur, function(n) {
                        var i;
                        n.which === t.ESC ? (i = e(this).ibox(),
                            i.close()) : n.which === t.ENTER && (e(n.target).is(":button") || (i = e(this).ibox(),
                            i.data("btnY", !0)))
                    }).on({
                        mousedown: function(t) {
                            var n = e(this).ibox();
                            n.data("isCur") || n.focus()
                        },
                        focusin: function(t) {
                            var n = e(this).ibox();
                            n && !n.data("isCur") && n.data("toggleClass", r.cur, !0)
                        },
                        focusout: function(t) {
                            var n = e(this).ibox();
                            n && !n.data("isFocus") && n.data("toggleClass", r.cur, !1)
                        }
                    }, "." + r.ibox).on("click", "." + r.ibox + " ." + r.closeBox, function() {
                        e(this).ibox().close()
                    })
                },
                data: function(e, t) {
                    return arguments.length ? 1 === arguments.length ? o[e] : (o[e] = t,
                        this) : o
                }
            }),
            e.ibox = a;
        var r = e.ibox.data("classes");
        e.fn.ibox = function(e) {
            return this.closest("." + r.ibox).data("ibox")
        }
            ,
            e.ibox.fn.modelTo = function(e) {
                return this.data("toggleClass", e, !0).position()
            }
            ,
            e.ibox.extend({
                loading: function(e) {
                    return a({
                        title: !1,
                        content: '<i class="ico i28 mr6"></i>' + (e || "加载中,请稍候..."),
                        width: "auto",
                        minHeight: 0,
                        them: "iboxThemLoading",
                        animate: !1,
                        btns: null
                    })
                },
                msg: function(t, n, i, o) {
                    var s = r.ico.msg + " i" + (n || "1");
                    return i && (t += '(<em class="timer"></em>)'),
                        o = e.extend({
                            title: !1,
                            content: '<div class="' + r.ibox_msg + '"><i class="' + s + '"></i>' + t + "</div>",
                            time: i,
                            them: "iboxThemMsg",
                            btns: null
                        }, o),
                        a(o)
                },
                alert: function(t, n, i, o) {
                    if (!t)
                        return !1;
                    "function" == e.type(n) && (i = n,
                        n = null);
                    var s = r.ico.msg + " i" + (n || "2");
                    return o = e.extend({
                        title: "提示信息",
                        content: '<div class="' + r.ibox_msg + '"><i class="' + s + '"></i><div class="info">' + t + "</div></div>",
                        them: "iboxThemAlert",
                        btns: [{
                            name: "确定",
                            callback: i && function() {
                                i.call(this),
                                    this.close()
                            }
                        }],
                        drag: !0,
                        them: "iboxThemAlert"
                    }, o),
                        a(o)
                },
                confirm: function(t, n, i, o) {
                    if (!t)
                        return !1;
                    var s = r.ico.msg + " i4";
                    return o = e.extend({
                        title: "确认信息",
                        content: '<div class="' + r.ibox_msg + '"><i class="' + s + '"></i>' + t + "</div>",
                        them: "iboxThemAlert",
                        btns: [{
                            name: "确定",
                            callback: n && function() {
                                var e = this;
                                n.call(e, !0),
                                    e.close()
                            }
                        }, {
                            name: "取消",
                            them: "disable",
                            callback: i && function() {
                                var e = this;
                                i.call(e),
                                    e.close()
                            }
                        }],
                        drag: !0,
                        them: "iboxThemAlert"
                    }, o),
                        a(o)
                },
                form: function(t, n, i, o, r) {
                    i = i || function() {
                        var e = this
                            , t = e.dom.find("form");
                        t.length && t.ajaxSubmit({
                            loaderBar: !0
                        }, function(e) {
                            e.code ? e.message && a.alert("NO[" + e.code + "] " + e.message) : e.message && a.alert(e.message, 1)
                        }),
                            e.close()
                    }
                    ;
                    var s, c;
                    if (c = s = e(n),
                    !c.is("form") && (c = c.find("form")),
                    c.length && (!c.find(":submit").length && c.append('<button type="submit" style="display:none;">'),
                        n = s),
                        s.find(":submit").length) {
                        var l = i;
                        i = function() {
                            var e = this;
                            l.call(e) && e.dom.find("form :submit").click()
                        }
                    }
                    "object" === e.type(o) && (r = o,
                        o = null),
                        r = e.extend({
                            title: t,
                            content: n,
                            btns: [{
                                name: "确定",
                                callback: i
                            }, {
                                name: "取消",
                                them: "disable",
                                callback: o
                            }]
                        }, r);
                    var u = a(r);
                    return r && r.width && u.size(r.width).position(),
                        u
                },
                ajax: function(t, n) {
                    var i;
                    return i = n && n.type ? a[n.type].apply(this, n.data) : a(n.data),
                        e.ajax(t, {
                            success: function(e) {
                                n.success && n.success(e)
                            }
                        }),
                        i
                }
            });
        e.ibox.data("classes");
        !1 !== t.ibox_init && e.ibox.init()
    }(jQuery, window),
    function(e, t, n) {
        e.inotify = function(n) {
            function i(e) {
                e.fadeOut(function() {
                    e.remove(),
                    r.callback && r.callback(),
                        o()
                })
            }
            function o() {
                var n = e(".yp-notify-pop")
                    , i = 47 * n.length
                    , o = e(t)
                    , a = o.height()
                    , r = o.width()
                    , s = e(t).scrollTop()
                    , c = Math.floor((2 * a / 3 - i) / 2);
                50 > c && (c = 50),
                    c += s,
                    yp.each(n, function(t, n) {
                        var i = e(t)
                            , o = Math.floor((r - i.outerWidth()) / 2);
                        i.css({
                            top: c,
                            left: o
                        }),
                            c += 47
                    })
            }
            var a = {
                callback: null,
                hideMode: "all"
            }
                , r = e.extend({}, a, n)
                , s = "success" == r.type ? "notice" : "notice-error"
                , c = "";
            c = '<div class="yp-notify-pop ' + s + '" style="display: none;"><i></i><span>' + r.content + "</span></div>";
            var l = e(c).appendTo("body")
                , u = 0;
            return l.show(),
                o(),
            "all" != r.hideMode && "auto" != r.hideMode || (u = setTimeout(function() {
                i(l)
            }, r.delay)),
            "all" != r.hideMode && "click" != r.hideMode || l.on("click.notify", function() {
                l.off("click.notify"),
                u && clearTimeout(u),
                    i(l)
            }),
                l
        }
    }(jQuery, window),
    function(e) {
        "use strict";
        var t = function(t, n) {
            this.$element = e(t),
                this.options = n,
                this.init()
        }
            , n = null;
        t.DEFAULTS = {
            title: "",
            $tips: null
        },
            t.prototype = {
                init: function() {
                    this.view(),
                        this.bindEvent()
                },
                view: function() {
                    this.$element.attr("title", "")
                },
                bindEvent: function() {
                    var t = this
                        , i = t.options;
                    t.$element.hover(function() {
                        var o = t.$element.offset()
                            , a = t.$element.width()
                            , r = 0
                            , s = 0
                            , c = i.$tips || n;
                        c || (i.$tips = c = n = e('<div class="tips titleTips" style="display: none;"><div class="tipsArea"><span class="txt-area"></span></div><i class="tips-arrow"></i></div>').appendTo("body")),
                        i.$tips || (i.$tips = c),
                        t.$element.attr("title") && (i.title = t.$element.attr("title"),
                            t.$element.attr("title", "")),
                        "" != i.title && (c.find(".txt-area").html(i.title),
                            i.width ? (c.width(i.width),
                                c.find(".tipsArea").height("auto")) : (c.css({
                                width: ""
                            }),
                                c.find(".tipsArea").css({
                                    height: ""
                                })),
                            r = c.width(),
                            s = c.height(),
                            o.top = o.top - s - 8,
                            o.left = o.left - (r - a) / 2,
                            c.css(o).show(),
                            i.location ? (c.find("i").css({
                                borderBottomColor: "rgba(0,0,0,.7)",
                                borderTopColor: "rgba(0,0,0,0)",
                                borderWidth: "5px",
                                top: "-10px"
                            }),
                                c.css({
                                    top: o.top + s + t.$element.outerHeight(!0) + 15
                                })) : c.find("i").css({
                                top: s,
                                borderBottomColor: "rgba(0,0,0,0)",
                                borderTopColor: "rgba(0,0,0,.7)"
                            }))
                    }, function() {
                        i.$tips.hide()
                    })
                },
                fSetTitle: function(e) {
                    this.options.title = e
                }
            };
        var i = e.fn.titleTips;
        e.fn.titleTips = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("titleTips")
                    , r = e.extend({}, t.DEFAULTS, o.data(), {
                    title: o.attr("title")
                }, "object" == typeof n && n);
                a || o.data("titleTips", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.titleTips.Constructor = t,
            e.fn.titleTips.noConflict = function() {
                return e.fn.titleTips = i,
                    this
            }
    }(window.jQuery),
    function(e) {
        "use strict";
        var t = function(n) {
            return n = e.extend({}, t.DEFAULTS, n),
                this.$dom = this.open(n),
                this
        };
        t.DEFAULTS = {
            hasBorder: !0,
            hasClose: !0,
            uniqueDialog: !1,
            formDialog: !1,
            formAction: "",
            uniqueClass: "",
            dialogType: "alert",
            zIndex: 70,
            bHide: !1
        },
            t.prototype = {
                open: function(t) {
                    var n, i, o, a, r = this;
                    if (t.uniqueDialog && (n = e('.common-ibox[data-dialog="' + t.dialogType + '"]'),
                        r.close(n)),
                        n = e('<div class="common-ibox message-tit-ibox" style="z-index: ' + t.zIndex + ';"><div class="content"><div class="hd"></div><div class="bd"></div></div></div>'),
                    t.bHide && n.css({
                        display: "none"
                    }),
                        i = n.find(".content"),
                    t.width && (n.css({
                        width: t.width
                    }),
                        i.css({
                            width: t.width
                        })),
                        o = i.find(".hd"),
                        a = i.find(".bd"),
                    t.title && o.append(t.title),
                    t.hasClose && (n.addClass("dialog-close"),
                        o.append('<a href="javascript:;" class="close js-close"></a>'),
                        n.on("click.dialogClose", ".js-close", function(e) {
                            e.preventDefault(),
                            t.fCloseClick && t.fCloseClick.call(r),
                                r.close(n)
                        })),
                    t.formDialog && (a.append('<form class="dialog-form" action="' + t.formAction + '"></form>'),
                        a = a.find(".dialog-form")),
                    t.hasBorder && n.addClass("dialog-border"),
                        a.append(t.content),
                    t.btns && t.btns.length) {
                        a.append('<div class="ibox-btn-area textC"></div>');
                        var s = a.find(".ibox-btn-area")
                            , c = {
                            blue: "ibox-define-btn",
                            gray: "ibox-shut-down-btn"
                        };
                        yp.each(t.btns, function(i) {
                            var o;
                            switch (i.color || (i.color = "blue"),
                            i.type || (i.type = "btn"),
                            i.text || (i.text = "确定"),
                                i.type) {
                                case "submit":
                                    o = e(t.formDialog ? '<button type="submit" class="dialog-btn dv ' + c[i.color] + '">' + i.text + "</button>" : '<a href="javascript:;" class="dialog-btn dv ' + c[i.color] + '">' + i.text + "</a>");
                                    break;
                                case "btn":
                                    o = e('<a href="javascript:;" class="dialog-btn dv ' + c[i.color] + '">' + i.text + "</a>");
                                    break;
                                case "link":
                                    o = e('<a href="' + i.href + '" target="_blank" class="title">' + i.text + "</a>")
                            }
                            o.on("click.dialogBtn", function(e) {
                                "link" == i.type || "submit" == i.type && t.formDialog || e.preventDefault(),
                                "submit" != i.type && r.close(n),
                                i.callback && i.callback.call(o, e)
                            }),
                                s.append(o)
                        })
                    }
                    e("body").append(n);
                    var l = n.outerWidth()
                        , u = n.outerHeight();
                    return n.css({
                        marginLeft: -l / 2,
                        marginTop: -u / 2
                    }),
                        n
                },
                close: function(e) {
                    e && e.length && (e.trigger("dialog.remove"),
                        e.remove())
                }
            },
            window.popDialog = t
    }(window.jQuery),
    define("ibox", function() {}),
    function(e) {
        jQuery.fn.extend({
            slimScroll: function(n) {
                var i = e.extend({
                    width: "auto",
                    height: "250px",
                    size: "7px",
                    color: "#000",
                    position: "right",
                    distance: "1px",
                    start: "top",
                    opacity: 1,
                    alwaysVisible: !1,
                    disableFadeOut: !1,
                    railVisible: !1,
                    railColor: "#333",
                    railOpacity: .2,
                    railDraggable: !0,
                    railClass: "slimScrollRail",
                    barClass: "slimScrollBar",
                    wrapperClass: "slimScrollDiv",
                    allowPageScroll: !1,
                    wheelStep: 20,
                    touchScrollStep: 200,
                    borderRadius: "7px",
                    railBorderRadius: "7px"
                }, n);
                return this.each(function() {
                    function o(t) {
                        if (l) {
                            t = t || window.event;
                            var n = 0;
                            t.wheelDelta && (n = -t.wheelDelta / 120),
                            t.detail && (n = t.detail / 3),
                            e(t.target || t.srcTarget || t.srcElement).closest("." + i.wrapperClass).is(b.parent()) && a(n, !0),
                            t.preventDefault && !y && t.preventDefault(),
                            y || (t.returnValue = !1)
                        }
                    }
                    function a(e, t, n) {
                        y = !1;
                        var o = e
                            , a = b.outerHeight() - w.outerHeight();
                        t && (o = parseInt(w.css("top")) + e * parseInt(i.wheelStep) / 100 * w.outerHeight(),
                            o = Math.min(Math.max(o, 0), a),
                            o = 0 < e ? Math.ceil(o) : Math.floor(o),
                            w.css({
                                top: o + "px"
                            })),
                            m = parseInt(w.css("top")) / (b.outerHeight() - w.outerHeight()),
                            o = m * (b[0].scrollHeight - b.outerHeight()),
                        n && (o = e,
                            e = o / b[0].scrollHeight * b.outerHeight(),
                            e = Math.min(Math.max(e, 0), a),
                            w.css({
                                top: e + "px"
                            })),
                            b.scrollTop(o),
                            b.trigger("slimscrolling", ~~o),
                            s(),
                            c()
                    }
                    function r() {
                        h = Math.max(b.outerHeight() / b[0].scrollHeight * b.outerHeight(), v),
                            w.css({
                                height: h + "px"
                            });
                        var e = h == b.outerHeight() ? "none" : "block";
                        w.css({
                            display: e
                        })
                    }
                    function s() {
                        r(),
                            clearTimeout(f),
                            m == ~~m ? (y = i.allowPageScroll,
                            g != m && b.trigger("slimscroll", 0 == ~~m ? "top" : "bottom")) : y = !1,
                            g = m,
                            h >= b.outerHeight() ? y = !0 : (w.stop(!0, !0).fadeIn("fast"),
                            i.railVisible && j.stop(!0, !0).fadeIn("fast"))
                    }
                    function c() {
                        i.alwaysVisible || (f = setTimeout(function() {
                            i.disableFadeOut && l || u || d || (w.fadeOut("slow"),
                                j.fadeOut("slow"))
                        }, 1e3))
                    }
                    var l, u, d, f, p, h, m, g, v = 30, y = !1, b = e(this);
                    if (b.parent().hasClass(i.wrapperClass)) {
                        var x = b.scrollTop()
                            , w = b.parent().find("." + i.barClass)
                            , j = b.parent().find("." + i.railClass);
                        if (r(),
                            e.isPlainObject(n)) {
                            if ("height"in n && "auto" == n.height) {
                                b.parent().css("height", "auto"),
                                    b.css("height", "auto");
                                var C = b.parent().parent().height();
                                b.parent().css("height", C),
                                    b.css("height", C)
                            }
                            if ("scrollTo"in n)
                                x = parseInt(i.scrollTo);
                            else if ("scrollBy"in n)
                                x += parseInt(i.scrollBy);
                            else if ("destroy"in n)
                                return w.remove(),
                                    j.remove(),
                                    void b.unwrap();
                            a(x, !1, !0)
                        }
                    } else {
                        i.height = "auto" == i.height ? b.parent().height() : i.height,
                            x = e("<div></div>").addClass(i.wrapperClass).css({
                                position: "relative",
                                overflow: "hidden",
                                width: i.width,
                                height: i.height
                            }),
                            b.css({
                                overflow: "hidden",
                                width: i.width,
                                height: i.height
                            });
                        var j = e("<div></div>").addClass(i.railClass).css({
                            width: i.size,
                            height: "100%",
                            position: "absolute",
                            top: 0,
                            display: i.alwaysVisible && i.railVisible ? "block" : "none",
                            "border-radius": i.railBorderRadius,
                            background: i.railColor,
                            opacity: i.railOpacity,
                            zIndex: 9
                        })
                            , w = e("<div></div>").addClass(i.barClass).css({
                            background: i.color,
                            width: i.size,
                            position: "absolute",
                            top: 0,
                            opacity: i.opacity,
                            display: i.alwaysVisible ? "block" : "none",
                            "border-radius": i.borderRadius,
                            BorderRadius: i.borderRadius,
                            MozBorderRadius: i.borderRadius,
                            WebkitBorderRadius: i.borderRadius,
                            zIndex: 10
                        })
                            , C = "right" == i.position ? {
                            right: i.distance
                        } : {
                            left: i.distance
                        };
                        j.css(C),
                            w.css(C),
                            b.wrap(x),
                            b.parent().append(w),
                            b.parent().append(j),
                        i.railDraggable && w.bind("mousedown", function(n) {
                            var i = e(document);
                            return d = !0,
                                t = parseFloat(w.css("top")),
                                pageY = n.pageY,
                                i.bind("mousemove.slimscroll", function(e) {
                                    currTop = t + e.pageY - pageY,
                                        w.css("top", currTop),
                                        a(0, w.position().top, !1)
                                }),
                                i.bind("mouseup.slimscroll", function(e) {
                                    d = !1,
                                        c(),
                                        i.unbind(".slimscroll")
                                }),
                                !1
                        }).bind("selectstart.slimscroll", function(e) {
                            return e.stopPropagation(),
                                e.preventDefault(),
                                !1
                        }),
                            j.hover(function() {
                                s()
                            }, function() {
                                c()
                            }),
                            w.hover(function() {
                                u = !0
                            }, function() {
                                u = !1
                            }),
                            b.hover(function() {
                                l = !0,
                                    s(),
                                    c()
                            }, function() {
                                l = !1,
                                    c()
                            }),
                            b.bind("touchstart", function(e, t) {
                                e.originalEvent.touches.length && (p = e.originalEvent.touches[0].pageY)
                            }),
                            b.bind("touchmove", function(e) {
                                y || e.originalEvent.preventDefault(),
                                e.originalEvent.touches.length && (a((p - e.originalEvent.touches[0].pageY) / i.touchScrollStep, !0),
                                    p = e.originalEvent.touches[0].pageY)
                            }),
                            "bottom" === i.start ? (w.css({
                                top: b.outerHeight() - w.outerHeight()
                            }),
                                a(0, !0)) : "top" !== i.start && (a(e(i.start).position().top, null, !0),
                            i.alwaysVisible || w.hide()),
                            function() {
                                window.addEventListener ? (this.addEventListener("DOMMouseScroll", o, !1),
                                    this.addEventListener("mousewheel", o, !1),
                                    this.addEventListener("MozMousePixelScroll", o, !1)) : document.attachEvent("onmousewheel", o)
                            }()
                    }
                }),
                    this
            }
        }),
            jQuery.fn.extend({
                slimscroll: jQuery.fn.slimScroll
            })
    }(jQuery),
    define("scroll", function() {}),
    function(e) {
        "use strict";
        var t = function(e, t) {
            this.init("tabs", e, t)
        };
        t.prototype = {
            constructor: t,
            init: function(t, n, i) {
                this.type = t,
                    this.$element = e(n),
                    this.options = this.getOptions(i);
                var o = this.options
                    , a = this.$element
                    , t = this.type;
                o.triggerPanel && (this.$triggerPanel = e(o.triggerPanel, a)),
                o.targetPanel && (this.$targetPanel = e(o.targetPanel, a)),
                    this.$tabActive = e(null),
                    this.refresh(o.active),
                    a.on(o.eventType + "." + t, o.trigger, e.proxy(this.enter, this)),
                    a.on("click." + t, o.close, e.proxy(this.destroy, this))
            },
            getOptions: function(t) {
                return t = e.extend({}, e.fn[this.type].defaults, t, this.$element.data()),
                    t.trigger = t.trigger || t.tab,
                    t.target = t.target || t.tabc,
                    t
            },
            refresh: function(t) {
                var n = this.options
                    , i = this.$element;
                this.$eTriggers = e(n.trigger, i),
                    this.$eTargets = e(n.target, i),
                this.$eTriggers.length && null != t && this.active(t)
            },
            enter: function(t) {
                var n = this
                    , i = e(t.currentTarget)
                    , o = (n.options,
                    n.$eTriggers.index(i));
                -1 == o && (o = i.data("index")),
                i.hasClass(n.options.disable) || (n.active(o, i),
                    t.preventDefault())
            },
            active: function(e, t) {
                var t = t || this.$eTriggers.eq(e)
                    , n = this.$eTargets.eq(e)
                    , i = this.options.cur;
                this.$tabActive.removeClass(i),
                    this.$tabActive = t.add(n).addClass(i),
                    this.$element.trigger("tabsactive", {
                        index: e
                    })
            },
            destroy: function(t) {
                var n = this
                    , i = n.options
                    , o = i.trigger
                    , a = e(t.currentTarget).closest(o)
                    , r = n.$eTriggers.index(a);
                return n.$element.trigger("tabsclose", {
                    index: r
                }),
                    n.close(r, a),
                    !1
            },
            close: function(e, t) {
                var n, t = t || this.$eTriggers.eq(e), i = this.$eTargets.eq(e);
                t.hasClass(this.options.cur) && this.$eTriggers.length - 1 > 0 && (n = e < this.$eTriggers.length - 1 ? e : e - 1),
                    t.add(i).remove(),
                    this.refresh(n)
            }
        },
            e.fn.tabs = function(n) {
                var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
                return this.each(function() {
                    var o = e(this)
                        , a = o.data("tabs")
                        , r = "object" == typeof n && n;
                    a || o.data("tabs", a = new t(this,r)),
                    "string" == typeof n && a[n].apply(a, i)
                })
            }
            ,
            e.fn.tabs.Constructor = t,
            e.fn.tabs.defaults = {
                tab: ".tab",
                tabc: ".tabc",
                close: ".close",
                closeTo: "-1",
                trigger: "",
                target: "",
                triggerPanel: "",
                targetPanel: "",
                cur: "active",
                active: "0",
                eventType: "click",
                disable: "disable-tab"
            }
    }(jQuery),
    define("tabs", function() {}),
    function(e) {
        "use strict";
        var t = function(t, n) {
            this.$element = e(t),
                this.aInputAssociation = [],
                this.options = e.extend({}, this.DEFAULTS, n),
                this.init()
        };
        t.DEFAULTS = {
            focusClass: "search-focus",
            q: "",
            bHotSearch: !0,
            sDefaultSearch: "all",
            sHotSearchItemHtml: '<li class="js-search-hover">                          <a href="${url}" class="a clearfix js-search-key-item" data-type="hot">                            <i style="${style}" class="num ${indexClass}">${index}</i>                            <span style="${style}" class="search-hot-name js-search-key-item-txt">${txt}</span>                          </a>                        </li>',
            sSearchHistoryItemHtml: '<li class="js-search-hover">                              <a href="javascript:;" class="close js-history-del-item" data-index="${index}"></a>                              <a href="javascript:;" class="a js-search-key-item" data-type="history">                                <p class="js-search-key-item-txt" data-type="association">${txt}</p>                              </a>                            </li>',
            sAssociationItemHtml: '<li class="js-association-hover" data-index="${index}">                <a href="javascript:;" class="js-search-key-item" data-type="association">${txt}</a>              </li>',
            sSearchHost: "/search",
            aHotSearch: [],
            randomHot: ""
        },
            t.prototype = {
                _ui: {},
                init: function() {
                    var e = this;
                    e.options;
                    e.view(),
                        e.bindEvent(),
                        e.listenYpEvent(),
                        e.fLoatHotSearch()
                },
                view: function() {
                    var t = this
                        , n = t.options
                        , i = t._ui;
                    i.$body = e("body"),
                        i.$search_panel = t.$element,
                        i.$search_input = i.$search_panel.find(".js-search-input"),
                        i.$search_input_label = i.$search_panel.find(".js-search-input-label"),
                        i.$search_btn = i.$search_panel.find(".js-search-btn"),
                    n.q && (t.fSetInputVal(n.q),
                        i.$search_input_label.hide())
                },
                bindEvent: function() {
                    var t = this
                        , n = t._ui
                        , i = t.options
                        , o = window.ie678 ? "propertychange" : "input";
                    n.$search_input.on("focus", function() {
                        i.fAfterFocus && i.fAfterFocus.call(n.$search_input),
                            n.$search_panel.addClass(i.focusClass),
                            n.$search_input_label.hide(),
                            n.$search_input.attr("placeholder", "直播/主播/分类/视频"),
                        "" == n.$search_input.val() && t.fShowHotSearch(),
                            mfTongji("search_input_focus", "pc")
                    }).on("blur", function() {
                        i.fAfterBlur && i.fAfterBlur.call(n.$search_input),
                            setTimeout(function() {
                                n.$search_panel.removeClass(i.focusClass)
                            }, 100);
                        var e = n.$search_input.val();
                        n.$search_input.attr("placeholder", ""),
                        "" == e && n.$search_input_label.show()
                    }).on("keypress", function(e) {
                        switch (e.keyCode) {
                            case 13:
                                e.preventDefault(),
                                    t.fDoSearch(n.$search_input.val(), "input")
                        }
                    }).on("keydown", function(e) {
                        switch (e.keyCode) {
                            case 38:
                                e.preventDefault(),
                                    t.fSelectAssociationWithKey(!0);
                                break;
                            case 40:
                                e.preventDefault(),
                                    t.fSelectAssociationWithKey(!1)
                        }
                    }).on(o, function(i) {
                        var o = n.$search_input.val();
                        t.fClearAssociationCD(),
                            "" == o ? (t.fShowHotSearch(),
                                t.fHideInputAssociation()) : (o = e.trim(o),
                                t.fLoadInputAssociation(o),
                                t.fHideHotSearch())
                    }),
                        n.$search_input_label.on("click", function(e) {
                            e.preventDefault(),
                                n.$search_input.focus()
                        }),
                        n.$search_btn.on("click", function(e) {
                            e.preventDefault();
                            var i = "" == n.$search_input.val() && t.options.hotWord ? t.options.hotWord : n.$search_input.val();
                            t.fDoSearch(i, "input")
                        })
                },
                listenYpEvent: function() {
                    var e = this
                        , t = e._ui;
                    yp.sub("page/searchhistory/change", function() {
                        t.$search_hot_tips && t.$search_hot_tips.is(":visible") && e.fRanderHistorySearchHtml()
                    })
                },
                fLoatHotSearch: function() {
                    var e = this
                        , t = e._ui
                        , n = e.options;
                    yp.ajax("/api/touch/search/v2.1/hotwords.json", {
                        noCache: !0
                    }).done(function(e) {
                        if (0 == e.code) {
                            oPageConfig.aHotSearch = e.data,
                                n.aHotSearch = e.data;
                            var i = n.aHotSearch[fRandomRange(0, n.aHotSearch.length)];
                            i && t.$search_input_label.html(i.hotWord),
                                n.hotWord = i.hotWord,
                                yp.sub("page/hostsearch/got", {
                                    aHotSearch: n.aHotSearch
                                })
                        }
                    })
                },
                fDoSearch: function(t, n) {
                    var i = this
                        , o = i.options
                        , a = i._ui;
                    t = e.trim(t);
                    var r = o.sSearchHost
                        , s = "search_do_without_key";
                    if (t) {
                        yp.oHistorySearch.fAddHistorySearch(t),
                            t = encodeURIComponent(t);
                        var c = a.$search_panel.find(".js-search-type-hidden").length ? a.$search_panel.find(".js-search-type-hidden").val() : o.sDefaultSearch;
                        r = "all" == c ? o.sSearchHost + "?q=" + t : o.sSearchHost + "/" + c + "?q=" + t,
                            s = "search_do_with_key_" + n
                    }
                    mfTongji(s, "pc"),
                    "hot" == n && mfTongji("search_do_hot_item", t),
                        o.bCurPage ? window.location.href = r : window.open(r)
                },
                fShowHotSearch: function() {
                    var t = this
                        , n = t.options
                        , i = t._ui;
                    if (!n.bHotSearch)
                        return !1;
                    i.$search_hot_tips || t.fAppendHotTips(),
                        t.fRanderHotSearchHtml(),
                        t.fRanderHistorySearchHtml(),
                        i.$search_hot_tips.show(),
                        i.$body.off("click.hotsearch").on("click.hotsearch", function(n) {
                            var i = e(n.target);
                            i.hasClass("js-for-hot-search") || i.closest(".js-for-hot-search").length || t.fHideHotSearch()
                        })
                },
                fHideHotSearch: function() {
                    var e = this
                        , t = e._ui;
                    if (!t.$search_hot_tips)
                        return !1;
                    t.$body.off("click.hotsearch"),
                        t.$search_hot_tips.hide()
                },
                fRanderHotSearchHtml: function() {
                    var e = this
                        , t = e.options
                        , n = e._ui;
                    if (!n.$search_hot_tips)
                        return !1;
                    var i = "";
                    yp.each(t.aHotSearch, function(e, n) {
                        e.pcAdUrl ? i += yp.format(t.sHotSearchItemHtml, {
                            index: n + 1 + ".",
                            indexClass: 3 > n ? "first" : "",
                            txt: e.pcAdWord,
                            url: e.pcAdUrl,
                            style: "color:#e82a25;"
                        }) : i += yp.format(t.sHotSearchItemHtml, {
                            index: n + 1 + ".",
                            indexClass: 3 > n ? "first" : "",
                            txt: e.hotWord,
                            url: "javascript:;",
                            style: ""
                        })
                    }),
                        i ? n.$search_hot_tips.find(".js-hot-search-list-panel").show().find(".js-hot-search-list").html(i) : n.$search_hot_tips.find(".js-hot-search-list-panel").hide().find(".js-hot-search-list").html("")
                },
                fRanderHistorySearchHtml: function() {
                    var e = this
                        , t = e.options
                        , n = e._ui;
                    if (!n.$search_hot_tips)
                        return !1;
                    var i = ""
                        , o = yp.oHistorySearch.fGetHistorySearch();
                    yp.each(o, function(e, n) {
                        i += yp.format(t.sSearchHistoryItemHtml, {
                            txt: e,
                            index: n
                        })
                    }),
                        i ? (n.$search_hot_tips.find(".js-history-search-list-panel").show().find(".js-history-search-list").html(i),
                            n.$search_hot_tips.find(".js-history-search-list-panel").removeClass("no-history")) : (n.$search_hot_tips.find(".js-history-search-list-panel").find(".js-history-search-list").html(""),
                            n.$search_hot_tips.find(".js-history-search-list-panel").addClass("no-history"))
                },
                fAppendHotTips: function() {
                    var t = this
                        , n = t._ui;
                    n.$search_hot_tips = e('<div class="seach-hot-ibox js-for-hot-search">                                \x3c!-- 搜索历史 --\x3e                                <div class="search-history">                                  <div class="hd">                                    <i class="icon"></i>                                    搜索历史                                  </div>                                  <a href="javascript:;" class="clear js-history-del-all"></a>                                  \x3c!-- 没有搜索历史加类no-history --\x3e                                  <div class="content no-history1 js-history-search-list-panel">                                    <div class="no-content"></div>                                    <ul class="clearfix js-history-search-list"></ul>                                  </div>                                </div>                                \x3c!-- 搜索历史End --\x3e                                \x3c!-- 热搜 --\x3e                                <div class="hot-saerch js-hot-search-list-panel">                                  <div class="hd">                                    <i class="icon"></i>                                    热门搜索                                  </div>                                  <ul class="js-hot-search-list"></ul>                                </div>                                \x3c!-- 热搜End --\x3e                              </div>'),
                        n.$search_panel.append(n.$search_hot_tips),
                        n.$search_hot_tips.on("click", ".js-search-key-item", function(i) {
                            i.preventDefault();
                            var o = e(this)
                                , a = o.attr("href")
                                , r = o.find(".js-search-key-item-txt").text()
                                , s = o.data("type");
                            "javascript:;" == a ? (t.fSetInputVal(r),
                                n.$search_input_label.hide(),
                                t.fDoSearch(r, s)) : (window.open(a),
                                mfTongji("search_hot_ads_click", encodeURIComponent(r))),
                                t.fHideHotSearch()
                        }),
                        n.$search_hot_tips.on("mouseenter", ".js-search-hover", function() {
                            e(this).addClass("hover")
                        }).on("mouseleave", ".js-search-hover", function() {
                            e(this).removeClass("hover")
                        }),
                        n.$search_hot_tips.on("click", ".js-history-del-item", function(t) {
                            return yp.oHistorySearch.fClearHistorySearch(e(this).data("index")),
                                !1
                        }),
                        n.$search_hot_tips.on("click", ".js-history-del-all", function(e) {
                            e.preventDefault(),
                                yp.oHistorySearch.fClearHistorySearch()
                        })
                },
                nAssociationCD: 0,
                nAssociationLast: 0,
                associationActiveIndex: 0,
                fLoadInputAssociation: function(t) {
                    var n = this
                        , i = e.now();
                    return !n.bStopAssociation && (1e3 > i - n.nAssociationLast ? (n.nAssociationCD = setTimeout(function() {
                        n.fLoadInputAssociation(t)
                    }, 500),
                        !1) : (n.nAssociationLast = i,
                        void yp.ajax("/api/zsearch/suggest?q=" + encodeURIComponent(t) + "&num=10", {
                            type: "get",
                            data: {
                                _v: Math.floor(e.now() / 6e4)
                            },
                            dataType: "json"
                        }).done(function(e) {
                            0 == e.code && (n.fShowInputAssociation(),
                                n.fRenderAssociationList(e.data))
                        })))
                },
                fShowInputAssociation: function() {
                    var t = this
                        , n = (t.options,
                        t._ui);
                    n.$search_input_association_tips || t.fAppendAssonciationTips(),
                        n.$search_input_association_tips.show(),
                        n.$body.off("click.association").on("click.association", function(n) {
                            var i = e(n.target);
                            i.hasClass("js-for-search-association") || i.closest(".js-for-search-association").length || (t.fClearAssociationCD(),
                                t.fHideInputAssociation())
                        })
                },
                fRenderAssociationList: function(e) {
                    var t = this
                        , n = t.options
                        , i = t._ui;
                    if (!i.$search_input_association_tips)
                        return !1;
                    if (e && e.length) {
                        var o = "";
                        yp.each(e, function(e, t) {
                            o += yp.format(n.sAssociationItemHtml, {
                                txt: e,
                                index: t + 1
                            })
                        }),
                            o ? i.$search_input_association_tips.find(".js-assocation-list").html(o) : i.$search_input_association_tips.find(".js-assocation-list").html("")
                    } else
                        t.fHideInputAssociation()
                },
                fHideInputAssociation: function() {
                    var e = this
                        , t = e._ui;
                    if (!t.$search_input_association_tips)
                        return !1;
                    t.$body.off("click.association"),
                        t.$search_input_association_tips.hide(),
                        e.fRemoveAllHighLightAssociation()
                },
                fClearAssociationCD: function() {
                    var e = this;
                    e.nAssociationCD && clearTimeout(e.nAssociationCD)
                },
                fAppendAssonciationTips: function() {
                    var t = this
                        , n = (t.options,
                        t._ui);
                    n.$search_input_association_tips = e('<div class="association-search-ibox js-for-search-association">            <ul class="js-assocation-list"></ul>          </div>'),
                        n.$search_panel.append(n.$search_input_association_tips),
                        n.$search_input_association_tips.on("click", ".js-search-key-item", function(n) {
                            n.preventDefault();
                            var i = e(this).text()
                                , o = e(this).data("type");
                            t.fSetInputVal(i),
                                t.fClearAssociationCD(),
                                t.fHideInputAssociation(),
                                t.fDoSearch(i, o)
                        }),
                        n.$search_input_association_tips.on("mouseenter", ".js-association-hover", function() {
                            t.fHighLightAssociation(e(this).data("index"))
                        }).on("mouseleave", ".js-association-hover", function() {
                            t.fRemoveAllHighLightAssociation()
                        })
                },
                fHighLightAssociation: function(e) {
                    var t, n = this, i = n._ui;
                    return e != n.associationActiveIndex && (n.associationActiveIndex = e,
                        i.$search_input_association_tips.find(".js-assocation-list .js-association-hover.active").removeClass("active"),
                        t = i.$search_input_association_tips.find('.js-assocation-list .js-association-hover[data-index="' + e + '"]'),
                        t.addClass("active")),
                        t
                },
                fRemoveAllHighLightAssociation: function() {
                    var e = this;
                    e._ui.$search_input_association_tips.find(".js-assocation-list .js-association-hover.active").removeClass("active"),
                        e.associationActiveIndex = 0
                },
                bStopAssociation: !1,
                fSelectAssociationWithKey: function(e) {
                    var t = this
                        , n = t._ui
                        , i = n.$search_input_association_tips.find(".js-assocation-list .js-association-hover").length
                        , o = t.associationActiveIndex;
                    e ? o-- : o++,
                        i < o ? o = 1 : 1 > o && (o = i);
                    var a = t.fHighLightAssociation(o);
                    a && (t.fClearAssociationCD(),
                        t.fSetInputVal(a.find(".js-search-key-item").text()))
                },
                fSetInputVal: function(e) {
                    var t = this
                        , n = t._ui;
                    t.bStopAssociation = !0,
                        n.$search_input.val(e),
                        t.bStopAssociation = !1
                }
            };
        var n = e.fn.zqSearch;
        e.fn.zqSearch = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("Search")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("Search", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.zqSearch.Constructor = t,
            e.fn.zqSearch.noConflict = function() {
                return e.fn.zqSearch = n,
                    this
            }
    }(window.jQuery),
    define("zqSearch", function() {}),
    function(e) {
        "use strict";
        function t(e) {
            this._obj = e
        }
        function n(e) {
            var n = this;
            new t(e)._each(function(e, t) {
                n[e] = t
            })
        }
        if (void 0 === e)
            throw new Error("Geetest requires browser environment");
        var i = e.document
            , o = e.Math
            , a = i.getElementsByTagName("head")[0];
        t.prototype = {
            _each: function(e) {
                var t = this._obj;
                for (var n in t)
                    t.hasOwnProperty(n) && e(n, t[n]);
                return this
            }
        },
            n.prototype = {
                api_server: "api.geetest.com",
                protocol: "http://",
                typePath: "/gettype.php",
                fallback_config: {
                    slide: {
                        static_servers: ["static.geetest.com", "dn-staticdown.qbox.me"],
                        type: "slide",
                        slide: "/static/js/geetest.0.0.0.js"
                    },
                    fullpage: {
                        static_servers: ["static.geetest.com", "dn-staticdown.qbox.me"],
                        type: "fullpage",
                        fullpage: "/static/js/fullpage.0.0.0.js"
                    }
                },
                _get_fallback_config: function() {
                    var e = this;
                    return s(e.type) ? e.fallback_config[e.type] : e.new_captcha ? e.fallback_config.fullpage : e.fallback_config.slide
                },
                _extend: function(e) {
                    var n = this;
                    new t(e)._each(function(e, t) {
                        n[e] = t
                    })
                }
            };
        var r = function(e) {
            return "number" == typeof e
        }
            , s = function(e) {
            return "string" == typeof e
        }
            , c = function(e) {
            return "boolean" == typeof e
        }
            , l = function(e) {
            return "object" == typeof e && null !== e
        }
            , u = function(e) {
            return "function" == typeof e
        }
            , d = {}
            , f = {}
            , p = function() {
            return parseInt(1e4 * o.random()) + (new Date).valueOf()
        }
            , h = function(e, t) {
            var n = i.createElement("script");
            n.charset = "UTF-8",
                n.async = !0,
                n.onerror = function() {
                    t(!0)
                }
            ;
            var o = !1;
            n.onload = n.onreadystatechange = function() {
                o || n.readyState && "loaded" !== n.readyState && "complete" !== n.readyState || (o = !0,
                    setTimeout(function() {
                        t(!1)
                    }, 0))
            }
                ,
                n.src = e,
                a.appendChild(n)
        }
            , m = function(e) {
            return e.replace(/^https?:\/\/|\/$/g, "")
        }
            , g = function(e) {
            return e = e.replace(/\/+/g, "/"),
            0 !== e.indexOf("/") && (e = "/" + e),
                e
        }
            , v = function(e) {
            if (!e)
                return "";
            var n = "?";
            return new t(e)._each(function(e, t) {
                (s(t) || r(t) || c(t)) && (n = n + encodeURIComponent(e) + "=" + encodeURIComponent(t) + "&")
            }),
            "?" === n && (n = ""),
                n.replace(/&$/, "")
        }
            , y = function(e, t, n, i) {
            t = m(t);
            var o = g(n) + v(i);
            return t && (o = e + t + o),
                o
        }
            , b = function(e, t, n, i, o) {
            var a = function(r) {
                var s = y(e, t[r], n, i);
                h(s, function(e) {
                    e ? r >= t.length - 1 ? o(!0) : a(r + 1) : o(!1)
                })
            };
            a(0)
        }
            , x = function(t, n, i, o) {
            if (l(i.getLib))
                return i._extend(i.getLib),
                    void o(i);
            if (i.offline)
                return void o(i._get_fallback_config());
            var a = "geetest_" + p();
            e[a] = function(t) {
                o("success" == t.status ? t.data : t.status ? i._get_fallback_config() : t);
                try {
                    e[a] = void 0,
                        delete e[a]
                } catch (e) {}
            }
                ,
                b(i.protocol, t, n, {
                    gt: i.gt,
                    callback: a
                }, function(e) {
                    e && o(i._get_fallback_config())
                })
        }
            , w = function(e, t) {
            var n = {
                networkError: "网络错误",
                gtTypeError: "gt字段不是字符串类型"
            };
            if ("function" != typeof t.onError)
                throw new Error(n[e]);
            t.onError(n[e])
        };
        (function() {
                return e.Geetest || i.getElementById("gt_lib")
            }
        )() && (f.slide = "loaded"),
            e.initGeetest = function(t, i) {
                var o = new n(t);
                t.https ? o.protocol = "https://" : t.protocol || (o.protocol = e.location.protocol + "//"),
                "050cffef4ae57b5d5e529fea9540b0d1" !== t.gt && "3bd38408ae4af923ed36e13819b14d42" !== t.gt || (o.apiserver = "yumchina.geetest.com/",
                    o.api_server = "yumchina.geetest.com"),
                l(t.getType) && o._extend(t.getType),
                    x([o.api_server || o.apiserver], o.typePath, o, function(t) {
                        var n = t.type
                            , a = function() {
                            o._extend(t),
                                i(new e.Geetest(o))
                        };
                        d[n] = d[n] || [];
                        var r = f[n] || "init";
                        "init" === r ? (f[n] = "loading",
                            d[n].push(a),
                            b(o.protocol, t.static_servers || t.domains, t[n] || t.path, null, function(e) {
                                if (e)
                                    f[n] = "fail",
                                        w("networkError", o);
                                else {
                                    f[n] = "loaded";
                                    for (var t = d[n], i = 0, a = t.length; i < a; i += 1) {
                                        var r = t[i];
                                        u(r) && r()
                                    }
                                    d[n] = []
                                }
                            })) : "loaded" === r ? a() : "fail" === r ? w("networkError", o) : "loading" === r && d[n].push(a)
                    })
            }
    }(window),
    function() {
        var e = {
            captchaObj: null,
            verifyType: "",
            beVerifyIng: !1,
            init: function(e) {
                var t = this;
                t.fInitGeet(e),
                    t.fListenYpEvent()
            },
            bListenYpEvent: !1,
            fListenYpEvent: function() {
                var e = this;
                if (e.bListenYpEvent)
                    return !1;
                e.bListenYpEvent = !0,
                    yp.sub("page/geet3.0/reset", function() {}),
                    yp.sub("page/geet3.0/verify", function(t, n) {
                        e.geetReady ? e.beVerifyIng || (mfTongji("geetest3_verify", "pc_" + n.type),
                            e.beVerifyIng = !0,
                            e.verifyType = n.type,
                        e.captchaObj && e.captchaObj.verify()) : (yp.pub("page/geet3.0/noready"),
                            yp.errorNotify("验证失败，请刷新或稍后重试"),
                            mfTongji("geetest3_noready", "pc_" + n.type),
                        "loading" != e.captchaObj && (e.captchaObj = null,
                            e.fInitGeet(n.type)))
                    }),
                    yp.sub("page/geet3.0/clear", function(t, n) {
                        e.verifyType = ""
                    })
            },
            geetReady: !1,
            fInitGeet: function(e) {
                var t = this;
                if (t.captchaObj)
                    return mfTongji("geetest3_get_exist", "pc_" + e),
                    "loading" != t.captchaObj && yp.pub("page/geet3.0/exist", {
                        type: e
                    }),
                        !1;
                t.geetReady = !1,
                    mfTongji("geetest3_get_str", "pc_" + e),
                    t.captchaObj = "loading",
                    yp.ajax("/api/auth/user.geetest_str?geetest_ver=3.0&os=0&platform=1", {
                        noCache: !0
                    }).done(function(n) {
                        mfTongji("geetest3_init", "pc_" + e),
                            initGeetest({
                                gt: n.gt,
                                challenge: n.challenge,
                                offline: !n.success,
                                new_captcha: n.new_captcha,
                                product: "bind",
                                width: "300px",
                                pure: 1
                            }, function(n) {
                                mfTongji("geetest3_init_success", "pc_" + e),
                                    t.fInitCallback(n, e)
                            })
                    })
            },
            fInitCallback: function(e, t) {
                var n = this;
                n.captchaObj = e,
                    n.captchaObj.onSuccess(function() {
                        var e = n.captchaObj.getValidate();
                        mfTongji("geetest3_verify_success", "pc_" + t),
                            yp.pub("page/geet3.0/success", {
                                geetestResult: e,
                                type: n.verifyType || t
                            }),
                            n.verifyType = "",
                            n.fAfterVerify(t)
                    }),
                    n.captchaObj.onReady(function() {
                        mfTongji("geetest3_init_ready", "pc_" + t),
                            yp.pub("page/geet3.0/ready", {
                                type: t
                            }),
                            n.geetReady = !0
                    }),
                    n.captchaObj.onError(function(e) {
                        yp.errorNotify("验证失败，请稍后或刷新重试");
                        var i = "pc_" + n.verifyType || t;
                        e && e.code && (i += "_" + e.code),
                            mfTongji("geetest3_verify_error", i),
                            yp.pub("page/geet3.0/verifyerror", {
                                type: n.verifyType || t
                            }),
                            n.fAfterVerify(t)
                    }),
                    n.captchaObj.onClose(function() {
                        mfTongji("geetest3_verify_close", "pc_" + t),
                            n.fAfterVerify(t)
                    })
            },
            fAfterVerify: function(e) {
                var t = this;
                t.beVerifyIng = !1,
                    yp.pub("page/geet3.0/afterverify", {
                        type: t.verifyType || e
                    })
            }
        };
        "undefined" != typeof module ? module.exports = e : "function" == typeof define && define.amd ? define("geetest3", [], function() {
            return e
        }) : window.oGeetest3 = e
    }(),
    function() {
        var e = {
            bInit: !1,
            oCallback: null,
            bReadyForShow: !1,
            type: "rob",
            init: function(e) {
                var t = this;
                if (e && (t.type = e),
                    t.bInit)
                    return !1;
                yp.use("geetest3", function(e, n) {
                    n.init(t.type)
                }, !0),
                    t.bInit = !0,
                    t.listenYpEvent()
            },
            view: function() {},
            bindEvent: function() {},
            listenYpEvent: function() {
                var e = this;
                yp.sub("page/geetest/show", function(t, n) {
                    e.bReadyForShow || (e.bReadyForShow = !0,
                        e.oCallback = n,
                        e.type = n.type,
                        yp.pub("page/geet3.0/verify", {
                            type: e.type
                        }))
                }),
                    yp.sub("page/geet3.0/success", function(t, n) {
                        e.type == n.type && e.bReadyForShow && (e.oCallback.fun.call(e.oCallback.self, n.geetestResult, n.type),
                            e.fRemoveGeet())
                    }),
                    yp.sub("page/geet3.0/afterverify", function(t, n) {
                        e.type == n.type && e.bReadyForShow && (e.oCallback.remove && e.oCallback.remove.call(e.oCallback.self, !e.bReadyForShow),
                            e.fRemoveGeet())
                    }),
                    yp.sub("page/geet3.0/noready", function(t) {
                        e.bReadyForShow = !1,
                        e.oCallback.remove && e.oCallback.remove.call(e.oCallback.self, !1)
                    })
            },
            fRemoveGeet: function() {
                this.bReadyForShow = !1,
                    yp.pub("page/geet3.0/clear")
            }
        };
        "undefined" != typeof module ? module.exports = e : "function" == typeof define && define.amd ? define("geetest", [], function() {
            return e
        }) : window.oGeetest = e
    }(),
    function(e) {
        "use strict";
        var t = function(t, n) {
            this.$element = e(t),
                this.options = n,
                this.init()
        };
        t.DEFAULTS = {
            defaultVal: ""
        },
            t.prototype = {
                init: function() {
                    this.view(),
                        this.bindEvent(),
                        this.fSelectVal(this.options.defaultVal, !0)
                },
                view: function() {
                    var t, n = this, i = "", o = "", a = n.$element.find("select option"), r = n.options;
                    yp.each(a, function(n, i) {
                        t = e(n),
                            t.data("hide") ? o += '<li class="js-select-item-li" style="display:none;"><a href="javascript:;" class="js-select-item" data-value="' + t.attr("value") + '">' + t.html() + "</a></li>" : o += '<li class="js-select-item-li"><a href="javascript:;" class="js-select-item" data-value="' + t.attr("value") + '">' + t.html() + "</a></li>"
                    }),
                    r.inputName || (r.inputName = n.$element.find("select").attr("name") ? n.$element.find("select").attr("name") : "countryCode"),
                        i = '<i class="arrow js-arrow"></i>              <span class="js-regist-country-select js-select-txt"></span>              <input type="hidden" class="js-select-input" name="' + r.inputName + '" />              <div class="option ' + (r.optsClass ? r.optsClass : "") + ' js-select-list">                <ul>' + o + "</ul>              </div>",
                        n.$element.html(i),
                    r.autoComplete && (n.$element.find(".js-select-txt").hide().before('<input type="text" class="select-input js-select-auto-complete" placeholder="' + r.placeholder + '" />'),
                        n.$element.find(".js-arrow").hide())
                },
                bindEvent: function() {
                    var t = this;
                    t.$element.on("click", function(e) {
                        if (e.preventDefault(),
                            t.$element.data("selectDisable"))
                            return !1;
                        t.fShowSelectList()
                    }),
                        t.$element.on("click", ".js-select-item", function(n) {
                            n.preventDefault(),
                                t.fSetSelevtVal(e(this))
                        }),
                        t.$element.on("selectReset", function() {
                            t.fSelectVal(t.options.defaultVal, !0)
                        }),
                        t.$element.data("selectVal", t.fSelectVal.bind(t)),
                    t.options.autoComplete && t.fAutoCompleteEvent()
                },
                fSelectVal: function(t, n) {
                    var i, o = this, a = o.$element;
                    return t || n ? yp.each(a.find(".js-select-item"), function(a, r) {
                        if (i = e(a),
                        !t && n && 0 == r || t == i.data("value"))
                            return o.fSetSelevtVal(i, n),
                                !1
                    }) : t = a.find(".js-select-input").val(),
                        t
                },
                fSetSelevtVal: function(e, t) {
                    var n = this
                        , i = n.$element
                        , o = i.find(".js-select-txt")
                        , a = i.find(".js-select-input")
                        , r = i.find(".js-select-auto-complete")
                        , s = ""
                        , c = "";
                    a.val() != e.data("value") || t ? (s = n.options.autoComplete && t && !n.options.defaultVal ? "" : e.html(),
                        c = n.options.autoComplete && t && !n.options.defaultVal ? "" : e.data("value"),
                        a.val(c),
                        o.html(n.options.bShowVal ? c : s),
                        r.val(s),
                        i.trigger("selectChange")) : n.options.autoComplete && r.val(e.html())
                },
                setDisable: function(e) {
                    var t = this;
                    t.$element.data("selectDisable", e),
                        t.$element.find(".js-arrow").toggle(!e)
                },
                fAddOptions: function(e) {
                    var t = this
                        , n = "";
                    n = '<li><a href="javascript:;" class="js-select-item" data-value="' + e.value + '">' + e.txt + "</a></li>",
                        t.$element.find(".js-select-list ul").append(n),
                    e.bSelect && t.fSelectVal(e.value)
                },
                fShowOption: function(e) {
                    this.$element.find('.js-select-item[data-value="' + e + '"]').closest(".js-select-item-li").show()
                },
                fhideOption: function(e) {
                    this.$element.find('.js-select-item[data-value="' + e + '"]').closest(".js-select-item-li").hide()
                },
                fShowSelectList: function() {
                    var t = this
                        , n = t.$element.find(".js-select-list");
                    if (n.is(":visible"))
                        return !1;
                    n.show(),
                        t.$element.trigger("selectShow"),
                        setTimeout(function() {
                            e("body").off("click.select").on("click.select", function(e) {
                                t.fHideSelectList()
                            })
                        }, 0)
                },
                fHideSelectList: function() {
                    var t = this;
                    e("body").off("click.select"),
                        t.$element.find(".js-select-list").hide(),
                        t.$element.trigger("selectHide")
                },
                fAutoCompleteEvent: function() {
                    var t = this
                        , n = "input";
                    window.ie678 && (n = "propertychange"),
                        t.$element.find(".js-select-auto-complete").on("focus", function() {
                            t.fToggleSelectItem(e(this).val())
                        }).on(n, function() {
                            t.fToggleSelectItem(e(this).val())
                        }).on("blur", function() {
                            var e = t.$element.find(".js-select-item:visible").eq(0);
                            "" != t.$element.find(".js-select-auto-complete").val() && e.length ? t.fSetSelevtVal(e) : t.fSelectVal(t.options.defaultVal, !0)
                        })
                },
                fToggleSelectItem: function(t) {
                    var n, i = this, o = "";
                    yp.each(i.$element.find(".js-select-item"), function(i) {
                        n = e(i),
                            o = n.html(),
                            n.closest(".js-select-item-li").toggle(!t || -1 < o.indexOf(t))
                    })
                }
            };
        var n = e.fn.zqSelect;
        e.fn.zqSelect = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("oSelect")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("oSelect", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.zqSelect.Constructor = t,
            e.fn.zqSelect.noConflict = function() {
                return e.fn.zqSelect = n,
                    this
            }
    }(window.jQuery),
    define("zqSelect", function() {}),
    function(e, t, n) {
        var i, o = function(t, n) {
            this.$element = e(t),
                this.options = n,
                this.init()
        };
        o.prototype = {
            init: function() {
                this.bindEvent()
            },
            bindEvent: function() {
                var n = this
                    , o = n.$element
                    , a = n.options;
                !t.ie678 && t.clipboardData ? o.on("click", function() {
                    var n = e("#" + o.data("clipboardTarget")).val() || e("#" + o.data("clipboardTarget")).text();
                    t.clipboardData.setData("text", n),
                        yp.successNotify("已成功复制链接到您的剪切板"),
                    "function" == typeof a.callback && a.callback()
                }) : (o.on("mouseenter", function() {
                    i = o
                }),
                    o.on("mouseover", function() {
                        i && i.trigger("copyenter")
                    }),
                    o.on("mouseout", function() {
                        i && i.trigger("copyleave")
                    }),
                    o.on("click", function() {
                        var t = e("#" + o.data("clipboardTarget")).val() || e("#" + o.data("clipboardTarget")).text();
                        n.copyInit(t)
                    }))
            },
            copyInit: function(e) {
                var n, o, a, r, s, c, l = this, u = !1;
                l.options || (l.options = {}),
                    n = l.options.debug || !1;
                try {
                    a = l.deselectCurrent(),
                        r = document.createRange(),
                        s = document.getSelection(),
                        c = document.createElement("span"),
                        c.textContent = e,
                        c.style.all = "unset",
                        c.style.position = "fixed",
                        c.style.top = 0,
                        c.style.clip = "rect(0, 0, 0, 0)",
                        c.style.whiteSpace = "pre",
                        c.style.webkitUserSelect = "text",
                        c.style.MozUserSelect = "text",
                        c.style.msUserSelect = "text",
                        c.style.userSelect = "text",
                        document.body.appendChild(c),
                        r.selectNode(c),
                        s.addRange(r);
                    if (!document.execCommand("copy"))
                        throw new Error("copy command was unsuccessful");
                    u = !0
                } catch (i) {
                    n && console.error("unable to copy using execCommand: ", i),
                    n && console.warn("trying IE specific stuff");
                    try {
                        t.clipboardData.setData("text", e),
                            u = !0
                    } catch (i) {
                        n && console.error("unable to copy using clipboardData: ", i),
                        n && console.error("falling back to prompt");
                        o = l.format("message"in l.options ? l.options.message : "Copy to clipboard: #{key}, Enter"),
                            t.prompt(o, e)
                    }
                } finally {
                    s && ("function" == typeof s.removeRange ? s.removeRange(r) : s.removeAllRanges()),
                    c && document.body.removeChild(c),
                        a()
                }
                if (u) {
                    yp.successNotify("已成功复制链接到您的剪切板"),
                    i && i.trigger("copycomplete");
                    var d = i.data("jcopy").options;
                    d && "function" == typeof d.callback && d.callback()
                }
            },
            format: function(e) {
                var t = (/mac os x/i.test(navigator.userAgent) ? "⌘" : "Ctrl") + "+C";
                return e.replace(/#{\s*key\s*}/g, t)
            },
            deselectCurrent: function() {
                var e = document.getSelection();
                if (!e.rangeCount)
                    return function() {}
                        ;
                for (var t = document.activeElement, n = [], i = 0; i < e.rangeCount; i++)
                    n.push(e.getRangeAt(i));
                switch (t.tagName.toUpperCase()) {
                    case "INPUT":
                    case "TEXTAREA":
                        t.blur();
                        break;
                    default:
                        t = null
                }
                return e.removeAllRanges(),
                    function() {
                        "Caret" === e.type && e.removeAllRanges(),
                        e.rangeCount || n.forEach(function(t) {
                            e.addRange(t)
                        }),
                        t && t.focus()
                    }
            }
        };
        var a = e.fn.jcopy;
        e.fn.jcopy = function(t) {
            return this.each(function() {
                var n = e(this)
                    , i = n.data("jcopy")
                    , a = e.extend({}, n.data(), "object" == typeof t && t);
                i || n.data("jcopy", i = new o(this,a))
            })
        }
            ,
            e.fn.jcopy.Constructor = o,
            e.fn.jcopy.noConflict = function() {
                return e.fn.jcopy = a,
                    this
            }
    }(jQuery, window),
    define("jCopy", function() {}),
    function(e) {
        "use strict";
        var t = function(t, n) {
            this.$element = e(t),
                this.options = n,
                this.init()
        };
        t.DEFAULTS = {
            count: 0,
            prePage: 20,
            curPage: 1,
            onPageChange: null,
            defaultUrl: "",
            urlParam: "page",
            prevText: "上一页",
            nextText: "下一页",
            ellipseText: "...",
            fullHtml: !0
        },
            t.prototype = {
                init: function() {
                    this.view(),
                        this.bindEvent()
                },
                view: function() {
                    var e = this
                        , t = e.options;
                    t.totalPage = Math.ceil(t.count / t.prePage),
                    6 >= t.totalPage && (t.showGO = !1),
                        e.$element.html(e.fGetPagination())
                },
                bindEvent: function() {
                    var t = this
                        , n = t.options;
                    t.$element.on("click", ".pageItem", function() {
                        var i, o = e(this);
                        return o.hasClass("pre") || o.hasClass("next") ? n.curPage = o.data("page") : n.curPage = o.closest("li").data("page"),
                            i = o.closest("li"),
                        !i.hasClass("disabled") && !i.hasClass("active") && (n.onPageChange ? (t.$element.html(t.fGetPagination()),
                            n.onPageChange(n.curPage, o.attr("href"), t.$element),
                            !1) : void 0)
                    })
                },
                fGetPagination: function() {
                    var e = this
                        , t = e.options
                        , n = t.curPage
                        , i = t.totalPage
                        , o = function(e, t) {
                        var n = [+t - 2, +t + 2];
                        if (n[0] = Math.max(n[0], 1),
                            n[1] = Math.min(n[1], e),
                        4 > n[1] - n[0] && 4 < e) {
                            var i, o = 4 - (n[1] - n[0]);
                            if (1 == n[0])
                                for (i = 0; i < o && (n[1]++,
                                e != n[1]); i++)
                                    ;
                            else if (e = n[1])
                                for (i = 0; i < o && 1 != --n[0]; i++)
                                    ;
                        }
                        return n
                    }(i, n)
                        , a = ""
                        , r = "";
                    if (0 == i)
                        return r;
                    var s = 1 == n ? 1 : n - 1
                        , c = i == n ? i : n + 1;
                    if (t.fullHtml) {
                        a += '<li data-page="1" class="' + (1 == n ? "active" : "") + '"><a class="pageItem" href="' + e.fGetUrl(1) + '">1</a></li>',
                        1 < o[0] - 1 && (a += "<li><span>" + t.ellipseText + "</span></li>");
                        for (var l = o[0]; l < o[1] + 1; l++)
                            1 != l && i != l && (a += '<li data-page="' + l + '" class="' + (l == n ? "active" : "") + '"><a class="pageItem" href="' + e.fGetUrl(l) + '">' + l + "</a></li>");
                        i > o[1] + 1 && (a += "<li><span>" + t.ellipseText + "</span></li>"),
                        1 < i && (a += '<li data-page="' + i + '" class="' + (i == n ? "active" : "") + '"><a class="pageItem" href="' + e.fGetUrl(i) + '">' + t.totalPage + "</a></li>")
                    }
                    return r = '<ul class="page-ul"><li class="' + (s == n ? "disabled" : "") + '"><a data-page="' + s + '" class="pageItem pre" href="' + e.fGetUrl(s) + '">' + t.prevText + "</a></li>" + a + '<li class="' + (c == n ? "disabled" : "") + '"><a data-page="' + c + '" class="pageItem next" href="' + e.fGetUrl(c) + '">' + t.nextText + "</a></li></ul>"
                },
                fGetUrl: function(e) {
                    var t = this;
                    return t.options.defaultUrl ? yp.format(t.options.defaultUrl, {
                        page: e,
                        per_page: t.options.prePage
                    }) : "javascript:;"
                },
                fGetListRange: function() {
                    var e, t, n = this;
                    return e = (n.options.curPage - 1) * n.options.prePage + 1,
                        t = n.options.curPage != n.options.totalPage ? n.options.curPage * n.options.prePage : n.options.count,
                        {
                            nForm: e,
                            nTo: t
                        }
                },
                fReRender: function(t) {
                    var n = this;
                    n.options = e.extend(n.options, t),
                        n.$element.html(n.fGetPagination())
                }
            };
        var n = e.fn.pagination;
        e.fn.pagination = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("pagination")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("pagination", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.pagination.Constructor = t,
            e.fn.pagination.noConflict = function() {
                return e.fn.pagination = n,
                    this
            }
            ,
        "function" == typeof define && define.amd && define("pagination", [], function() {
            return t
        })
    }(window.jQuery),
    function(e) {
        "use strict";
        var t = function(t, n) {
            this.$element = e(t),
                this.options = n,
                this.list = {
                    curPage: 0,
                    reload: !0
                },
                this.init()
        };
        t.DEFAULTS = {
            defaultVal: "",
            autoLoad: !0,
            paginationOpt: null,
            dataByAjax: !0
        },
            t.prototype = {
                init: function() {
                    this.view(),
                    this.options.autoLoad && this.fLoadPage(1)
                },
                view: function() {
                    var e = this
                        , t = e.options.paginationOpt || {};
                    t.onPageChange = function(t, n, i) {
                        e.fListPatination(t, n, i)
                    }
                        ,
                        e.options.$pagination.pagination(t)
                },
                fListPatination: function(e, t, n) {
                    var i = this;
                    i.options.dataByAjax ? i.fGetDataByAjax(e, t, n) : i.fGetDataByLocal(e, n)
                },
                fGetDataByAjax: function(t, n, i) {
                    var o = this;
                    if (i.data("ajaxing"))
                        return !1;
                    var a;
                    if (o.options.$form)
                        var a = o.options.$form.serializeArray();
                    else
                        a = [];
                    a.push({
                        name: "_v",
                        value: Math.floor(e.now() / 6e4)
                    }),
                        i.data("ajaxing", !0),
                    o.options.fShowLoading && o.options.fShowLoading(),
                        o.list.curPage = t,
                        yp.ajax(n, {
                            type: "get",
                            data: a,
                            dataType: "json"
                        }).always(function() {
                            i.data("ajaxing", !1),
                            o.options.fHideLoading && o.options.fHideLoading()
                        }).done(function(e) {
                            o.options.fDataGet && o.options.fDataGet(e, t),
                                0 == e.code ? o.fRenderByData(e.data, t, i) : e.message && yp.errorNotify(e.message)
                        })
                },
                fGetDataByLocal: function(e, t) {
                    var n, i = this, o = i.options.aLocalData || [], a = {
                        cnt: o.length,
                        local: []
                    };
                    if (i.list.curPage = e,
                        i.list.reload) {
                        var r = t.data("pagination").options;
                        t.pagination("fReRender", {
                            count: a.cnt,
                            curPage: e,
                            totalPage: Math.ceil(a.cnt / r.prePage)
                        }),
                            i.list.reload = !1
                    }
                    n = t.data("pagination").fGetListRange(),
                        a.local = o.slice(n.nForm - 1, n.nTo),
                        i.fRenderByData(a, e, t)
                },
                fReload: function(e) {
                    var t = this
                        , n = t.options.$pagination.data("pagination").options
                        , i = n.curPage;
                    e && i--,
                    0 >= i && i++,
                        t.fLoadPage(i)
                },
                fLoadPage: function(e) {
                    var t = this
                        , n = t.options.$pagination.data("pagination").fGetUrl(e);
                    t.list.reload = !0,
                        t.fListPatination(e, n, t.options.$pagination)
                },
                fRenderByData: function(e, t, n) {
                    var e, i = this, o = "", a = i.options.dataByAjax ? i.options.dataKey || "videos" : "local", r = i.options.dataByAjax ? i.options.cntKey || "cnt" : "cnt", s = e[a];
                    if (!s)
                        return i.options.fNoData && i.options.fNoData.call(i.$element, t),
                            !1;
                    var c = e[r];
                    if (i.options.$total && i.options.$total.html(c),
                    i.list.reload || i.list.curPage != t) {
                        var l = n.data("pagination").options;
                        n.pagination("fReRender", {
                            count: c,
                            curPage: t,
                            totalPage: Math.ceil(c / l.prePage)
                        }),
                            i.list.reload = !1
                    }
                    yp.each(s, function(n, a) {
                        (e = i.options.fDataFormat ? i.options.fDataFormat(n, a, t) : n) && (o += yp.format(i.options.tmplHtml, e))
                    }),
                        o ? (i.$element.html(o),
                        i.options.fAfterRander && i.options.fAfterRander.call(i.$element)) : i.options.fNoData && i.options.fNoData.call(i.$element, t)
                },
                fResetLocalData: function(e) {
                    var t = this;
                    !t.options.dataByAjax && (t.options.aLocalData = e)
                }
            };
        var n = e.fn.zqPagination;
        e.fn.zqPagination = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("oPaginationList")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("oPaginationList", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.zqPagination.Constructor = t,
            e.fn.zqPagination.noConflict = function() {
                return e.fn.zqPagination = n,
                    this
            }
    }(window.jQuery),
    define("paginationList", ["pagination"], function() {}),
    jQuery.extend({
        createUploadIframe: function(e, t) {
            var n = "jUploadFrame" + e;
            if (window.ActiveXObject) {
                if (window.ie678) {
                    var i = document.createElement('<iframe id="' + n + '" name="' + n + '" />');
                    "boolean" == typeof t ? i.src = "javascript:false" : "string" == typeof t && (i.src = t)
                } else {
                    var i = document.createElement("iframe");
                    i.id = n,
                        i.name = n
                }
            } else {
                var i = document.createElement("iframe");
                i.id = n,
                    i.name = n
            }
            return i.style.position = "absolute",
                i.style.top = "-1000px",
                i.style.left = "-1000px",
                document.body.appendChild(i),
                i
        },
        createUploadForm: function(e, t) {
            var n = "jUploadForm" + e
                , i = "jUploadFile" + e
                , o = $('<form  action="" method="POST" name="' + n + '" id="' + n + '" enctype="multipart/form-data"></form>')
                , a = $("#" + t)
                , r = $(a).clone(!0);
            return $(a).attr("id", i),
                $(a).before(r),
                $(a).appendTo(o),
                $(o).css("position", "absolute"),
                $(o).css("top", "-1200px"),
                $(o).css("left", "-1200px"),
                $(o).appendTo("body"),
                o
        },
        ajaxFileUpload: function(e) {
            e = jQuery.extend({}, jQuery.ajaxSettings, e);
            var t = e.fileElementId
                , n = jQuery.createUploadForm(t, e.fileElementId)
                , i = (jQuery.createUploadIframe(t, e.secureuri),
            "jUploadFrame" + t)
                , o = "jUploadForm" + t;
            e.global && !jQuery.active++ && jQuery.event.trigger("ajaxStart");
            var a = !1
                , r = {};
            e.global && jQuery.event.trigger("ajaxSend", [r, e]);
            var s = function(t) {
                var o = document.getElementById(i);
                try {
                    o.contentWindow ? (r.responseText = o.contentWindow.document.body ? o.contentWindow.document.body.innerHTML : null,
                        r.responseXML = o.contentWindow.document.XMLDocument ? o.contentWindow.document.XMLDocument : o.contentWindow.document) : o.contentDocument && (r.responseText = o.contentDocument.document.body ? o.contentDocument.document.body.innerHTML : null,
                        r.responseXML = o.contentDocument.document.XMLDocument ? o.contentDocument.document.XMLDocument : o.contentDocument.document)
                } catch (t) {
                    jQuery.handleError(e, r, null, t)
                }
                if (r || "timeout" == t) {
                    a = !0;
                    var s;
                    try {
                        if ("error" != (s = "timeout" != t ? "success" : "error")) {
                            var c = jQuery.uploadHttpData(r, e.dataType);
                            e.success && e.success(c, s),
                            e.global && jQuery.event.trigger("ajaxSuccess", [r, e])
                        } else
                            jQuery.handleError(e, r, s)
                    } catch (t) {
                        s = "error",
                            jQuery.handleError(e, r, s, t)
                    }
                    e.global && jQuery.event.trigger("ajaxComplete", [r, e]),
                    e.global && !--jQuery.active && jQuery.event.trigger("ajaxStop"),
                    e.complete && e.complete(r, s),
                        jQuery(o).unbind(),
                        setTimeout(function() {
                            try {
                                $(o).remove(),
                                    $(n).remove()
                            } catch (t) {
                                jQuery.handleError(e, r, null, t)
                            }
                        }, 100),
                        r = null
                }
            };
            e.timeout > 0 && setTimeout(function() {
                a || s("timeout")
            }, e.timeout),
                window.attachEvent ? document.getElementById(i).attachEvent("onload", s) : document.getElementById(i).addEventListener("load", s, !1);
            try {
                var n = $("#" + o);
                $(n).attr("action", e.url),
                    $(n).attr("method", "POST"),
                    $(n).attr("target", i),
                    n.encoding ? n.encoding = "multipart/form-data" : n.enctype = "multipart/form-data",
                    $(n)[0].submit()
            } catch (t) {
                jQuery.handleError(e, r, null, t)
            }
            return {
                abort: function() {}
            }
        },
        uploadHttpData: function(e, t) {
            var n = !t;
            return n = "xml" == t || n ? e.responseXML : e.responseText,
            "script" == t && jQuery.globalEval(n),
            "json" == t && (n = jQuery.parseJSON(n)),
            "html" == t && jQuery("<div>").html(n).evalScripts(),
                n
        },
        handleError: function(e, t, n, i) {
            e.error && e.error.call(e.context || window, t, n, i),
            e.global && (e.context ? jQuery(e.context) : jQuery.event).trigger("ajaxError", [t, e, i])
        }
    }),
    define("fileupload", function() {}),
    function(e) {
        "use strict";
        window.requestAnimationFrame || (window.requestAnimationFrame = window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame || function(e) {
                return window.setTimeout(e, 17)
            }
        );
        var t = function(t, n) {
            this.$element = e(t),
                this.options = n,
                this.then = e.now(),
                this.now = 0,
                this.delta = 0,
                this.pause = !1,
                this.nowPosY = 0,
                this.roundCnt = 0,
            n.bgImg && this.$element.css({
                background: "url(" + n.bgImg + ") left top no-repeat"
            })
        };
        t.DEFAULTS = {
            interval: 125,
            oneStep: 139,
            totalStep: 1390,
            roundCnt: 0,
            sleep: 0,
            loopCallback: null
        },
            t.prototype = {
                animate: function() {
                    var t = this
                        , n = t.options;
                    if (t.pause)
                        return !1;
                    window.requestAnimationFrame && (requestAnimationFrame(function() {
                        t.animate()
                    }),
                        t.now = e.now(),
                        t.delta = t.now - t.then,
                    t.delta > n.interval && (t.then = t.now - t.delta % n.interval,
                        t.draw()))
                },
                draw: function() {
                    var e = this
                        , t = e.options
                        , n = !1;
                    e.nowPosY = e.nowPosY + t.oneStep,
                    e.nowPosY == t.totalStep && (e.roundCnt++,
                        e.nowPosY = 0,
                        n = !0);
                    var i = {
                        backgroundPosition: "0px -" + e.nowPosY + "px"
                    };
                    e.$element.css(i),
                    n && t.roundCnt && t.roundCnt == e.roundCnt && (e.roundCnt = 0,
                        e.pause = !0,
                    t.loopCallback && t.loopCallback(),
                    t.sleep && setTimeout(function() {
                        e.play()
                    }, t.sleep))
                },
                stop: function() {
                    this.pause = !0
                },
                play: function(e) {
                    var t = this;
                    e && t.$element.css({
                        backgroundPosition: "0px 0px"
                    }),
                        t.pause = !1,
                        t.animate()
                }
            };
        var n = e.fn.frameAnimation;
        e.fn.frameAnimation = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("frameAnimation")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("frameAnimation", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.frameAnimation.Constructor = t,
            e.fn.frameAnimation.noConflict = function() {
                return e.fn.frameAnimation = n,
                    this
            }
    }(window.jQuery),
    define("frameAnimation", function() {}),
    function(e) {
        "use strict";
        var t = function(t, n) {
            this.$element = e(t),
                this.options = e.extend({}, this.DEFAULTS, n),
                this.init()
        };
        t.DEFAULTS = {
            handle: "",
            positionType: "fixed",
            $containor: e("body"),
            leftOrRight: "left",
            topOrBottom: "top",
            posSaveKey: "",
            dragDirection: "all"
        },
            t.prototype = {
                winOpts: {},
                init: function() {
                    function e(e) {
                        e.preventDefault();
                        var t = e.clientY - c.clientY
                            , n = e.clientX - c.clientX;
                        c.clientY = e.clientY,
                            c.clientX = e.clientX,
                        "all" != o.dragDirection && "horizontal" != o.dragDirection || ("left" == o.leftOrRight ? (s.left += n,
                            s.left = Math.min(Math.max(s.left, 0), a.horizontal)) : (s.right -= n,
                            s.right = Math.min(Math.max(s.right, 0), a.horizontal))),
                        "all" != o.dragDirection && "vertical" != o.dragDirection || ("top" == o.topOrBottom ? (s.top += t,
                            s.top = Math.min(Math.max(s.top, 0), a.vertical)) : (s.bottom -= t,
                            s.bottom = Math.min(Math.max(s.bottom, 0), a.vertical))),
                            i.$element.css(s)
                    }
                    function t(n) {
                        o.$containor.off("mousemove", e),
                            o.$containor.off("mouseup", t),
                            o.$containor.off("mouseleave", t),
                            i.fSaveDragPos(s)
                    }
                    function n(n) {
                        o.$containor.on("mousemove", e),
                            o.$containor.on("mouseup", t),
                            o.$containor.on("mouseleave", t)
                    }
                    var i = this
                        , o = i.options
                        , a = i.fGetMaxPosition()
                        , r = o.handle ? i.$element.find(o.handle) : i.$element;
                    yp.sub("ui/resize", function() {
                        a = i.fGetMaxPosition()
                    });
                    var s = {}
                        , c = {};
                    r.on("mousedown", function(e) {
                        c.clientY = e.clientY,
                            c.clientX = e.clientX,
                            s = i.fGetCurDragPos(),
                            n(e)
                    }),
                        i.fResetPos()
                },
                fSaveDragPos: function(e) {
                    var t = this
                        , n = t.options;
                    n.posSaveKey && yp.localStorage.setItem(n.posSaveKey, JSON.stringify(e))
                },
                fGetCurDragPos: function() {
                    var e, t = this, n = t.options;
                    if (n.posSaveKey)
                        try {
                            e = JSON.parse(yp.localStorage.getItem(n.posSaveKey))
                        } catch (e) {}
                    if (!e) {
                        e = {},
                            e[n.leftOrRight] = 0,
                            e[n.topOrBottom] = 0;
                        try {
                            e[n.leftOrRight] = parseInt(t.$element.css(n.leftOrRight)) || 0,
                                e[n.topOrBottom] = parseInt(t.$element.css(n.topOrBottom)) || 0
                        } catch (e) {}
                    }
                    return e
                },
                fGetMaxPosition: function() {
                    var t = this
                        , n = t.options
                        , i = t.$element.outerWidth(!0)
                        , o = t.$element.outerHeight(!0)
                        , a = {
                        horizontal: 0,
                        vertical: 0
                    };
                    if ("fixed" == n.positionType) {
                        var r = e(window);
                        a.horizontal = r.width(),
                            a.vertical = r.height()
                    } else
                        a.horizontal = n.$containor.width(),
                            a.vertical = n.$containor.height();
                    return a.horizontal = a.horizontal - i,
                        a.vertical = a.vertical - o,
                        a
                },
                fResetPos: function() {
                    var e = this
                        , t = e.fGetCurDragPos();
                    e.$element.css(t)
                }
            };
        var n = e.fn.zqDrag;
        e.fn.zqDrag = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("Drag")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("Drag", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.zqDrag.Constructor = t,
            e.fn.zqDrag.noConflict = function() {
                return e.fn.zqDrag = n,
                    this
            }
    }(window.jQuery),
    define("drag", function() {}),
    function() {
        "use strict";
        function e(e) {
            this.options = $.extend(!0, {
                number: 6,
                triggle: "click",
                autoPlay: !0,
                speed: 500,
                disableCls: "disabled",
                infinity: !1,
                easing: "linear",
                delay: 3e3,
                leftBtn: "#btn_l",
                rightBtn: "#btn_r",
                elemBox: "ul"
            }, e)
        }
        e.prototype = {
            init: function() {
                var e = this.getDValue()
                    , t = this.getMinLeft(e);
                this.lessChildren(),
                this.options.infinity && (this.setElemBoxHtml(),
                    this.autoPlaying(e, t)),
                    this.leftCarousel(e, t),
                    this.rightCarouse(e, t)
            },
            lessChildren: function() {
                var e = this.options
                    , t = $(e.elemBox).children()
                    , n = e.rightBtn
                    , i = t.first().outerWidth(!0)
                    , o = $(e.elemBox).parent().width();
                i * t.length <= o && $(n).addClass(e.disableCls)
            },
            autoPlaying: function(e, t) {
                var n = this.options
                    , i = null
                    , o = this;
                if (n.autoPlay) {
                    if ($(n.rightBtn).hasClass(n.disableCls))
                        return;
                    $(n.leftBtn).parent().hover(function() {
                        clearInterval(i)
                    }, function() {
                        i = setInterval(function() {
                            o.moveRight(e, $(n.rightBtn), t)
                        }, n.delay)
                    }).trigger("mouseleave")
                }
            },
            setElemBoxHtml: function() {
                var e = this.options
                    , t = $(e.elemBox).html()
                    , e = this.options;
                $(e.rightBtn).hasClass(e.disableCls) || ($(e.elemBox).get(0).innerHTML += t)
            },
            getMinLeft: function(e) {
                var t = this.options
                    , n = $(t.elemBox).children()
                    , i = n.first().outerWidth(!0);
                if (t.infinity)
                    return -e * n.length / t.number;
                var o = $(t.elemBox).parent().outerWidth()
                    , a = Math.floor((n.length * i - o) / i);
                return -e * Math.ceil(a / t.number) + 10
            },
            getDValue: function() {
                var e = this.options
                    , t = e.number;
                return $(e.elemBox).children().eq(0).outerWidth(!0) * t
            },
            leftCarousel: function(e, t) {
                var n = this
                    , i = this.options;
                i.number;
                var o = i.leftBtn;
                $(o).off(i.triggle).on(i.triggle, function() {
                    $(this).data("disabled") || $(this).hasClass(i.disableCls) || ($(this).data("disabled", "disabled"),
                        n.carousel("left", e, $(this), t))
                })
            },
            rightCarouse: function(e, t) {
                var n = this
                    , i = this.options;
                i.number;
                var o = i.rightBtn;
                $(o).off(i.triggle).on(i.triggle, function() {
                    $(this).data("disabled") || $(this).hasClass(i.disableCls) || ($(this).data("disabled", "disabled"),
                        n.carousel("right", e, $(this), t))
                })
            },
            carousel: function(e, t, n, i) {
                switch (e) {
                    case "left":
                        this.moveLeft(t, n, i);
                        break;
                    case "right":
                        this.moveRight(t, n, i)
                }
            },
            moveLeft: function(e, t, n) {
                var i = this.options
                    , o = $(i.elemBox)
                    , a = o.position().left;
                if (a >= 0) {
                    if (!i.infinity)
                        return t.addClass(i.disableCls),
                            void t.removeData("disabled");
                    o.css("left", n),
                        a = n
                }
                $(i.rightBtn).removeClass(i.disableCls),
                    o.animate({
                        left: a + e
                    }, i.speed, i.easing, function() {
                        $(i.leftBtn).trigger("clickleft"),
                            window.setTimeout(function() {
                                t.removeData("disabled"),
                                !i.infinity && o.position().left >= 0 && t.addClass(i.disableCls)
                            }, 200)
                    })
            },
            moveRight: function(e, t, n) {
                var i = this.options
                    , o = $(i.elemBox)
                    , a = o.position().left;
                if (n >= a) {
                    if (!i.infinity)
                        return void t.addClass(i.disableCls);
                    o.css("left", 0),
                        a = 0
                }
                $(i.leftBtn).removeClass(i.disableCls),
                    o.animate({
                        left: a - e
                    }, i.speed, i.easing, function() {
                        $(i.rightBtn).trigger("clickright"),
                            window.setTimeout(function() {
                                t.removeData("disabled"),
                                !i.infinity && o.position().left <= n && t.addClass(i.disableCls)
                            }, 200)
                    })
            }
        },
            "undefined" != typeof module ? module.exports = e : "function" == typeof define && define.amd ? define("carousel", [], function() {
                return e
            }) : window.Carousel = e
    }(),
    function(e) {
        "use strict";
        var t = function(t, n) {
            this.el = t,
                this.$el = e(this.el),
                this.options = n,
                this._ui = {},
                this.init()
        };
        t.DEFAULTS = {
            btns: ".js-btn-share",
            title: document.title,
            url: location.href,
            desc: "",
            pic: "",
            summary: "",
            shortTitle: "",
            searchPic: !1,
            appkey: {
                weibo: "",
                qqweibo: ""
            },
            weiboTag: "",
            size: 200,
            currentLevel: 0,
            zindex: 16
        },
            t.prototype = {
                init: function() {
                    this.view(),
                        this.bindEvent()
                },
                view: function() {
                    var t = this
                        , n = t._ui;
                    n.$body = e("body"),
                        n.$qrcode_dialog = e('<div class="common-ibox message-tit-ibox" id="js-div-share-qrcode-panel" style="display: none;">                                <div class="content">                                  <div class="hd">分享到朋友圈<a href="javascript:;" class="close js-btn-close"></a></div>                                  <div class="bd">                                    <div class="img-box js-qrcode-img""></div>                                  </div>                                </div>                              </div>')
                },
                bindEvent: function() {
                    var t = this
                        , n = t._ui;
                    n.$qrcode_dialog.on("click", ".js-btn-close", function(e) {
                        e.preventDefault(),
                            n.$qrcode_dialog.hide()
                    }),
                        t.$el.off("click.share").on("click.share", t.options.btns, function(i) {
                            i.preventDefault();
                            var o = e(this)
                                , a = o.data("cmd") || o.attr("data-cmd") || o.data("share") || o.attr("data-share")
                                , r = e.extend(!0, t.options);
                            switch (r.onBeforeClick && (r = r.onBeforeClick(a, r)),
                                a) {
                                case "tsina":
                                case "btn_weibo":
                                    t.openShareWindow("http://service.weibo.com/share/share.php", {
                                        url: r.url,
                                        type: "3",
                                        count: "1",
                                        appkey: "",
                                        title: r.title,
                                        pic: r.pic,
                                        searchPic: r.searchPic,
                                        ralateUid: "",
                                        language: "zh_cn",
                                        rnd: (new Date).valueOf()
                                    });
                                    break;
                                case "tqq":
                                    t.openShareWindow("http://v.t.qq.com/share/share.php", {
                                        title: r.title,
                                        url: r.url,
                                        site: "//www.zhanqi.tv/",
                                        appkey: "",
                                        assname: "zhanqi",
                                        pic: r.pic
                                    });
                                    break;
                                case "qzone":
                                case "btn_qqzone":
                                    t.openShareWindow("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey", {
                                        url: r.url,
                                        showcount: 1,
                                        desc: r.desc,
                                        summary: r.summary,
                                        title: r.shortTitle || r.title,
                                        site: "战旗直播",
                                        pics: r.pic,
                                        style: "203",
                                        width: 98,
                                        height: 22
                                    });
                                    break;
                                case "tieba":
                                    t.openShareWindow("http://tieba.baidu.com/f/commit/share/openShareApi", {
                                        title: r.title,
                                        url: r.url,
                                        to: "tieba",
                                        type: "text",
                                        relateUid: "",
                                        pic: r.pic,
                                        key: "",
                                        desc: "",
                                        comment: r.desc
                                    });
                                    break;
                                case "sqq":
                                case "btn_qq":
                                    t.openShareWindow("http://connect.qq.com/widget/shareqq/index.html", {
                                        url: r.url,
                                        desc: r.desc,
                                        title: r.title,
                                        summary: r.summary,
                                        pics: r.pic,
                                        flash: "",
                                        site: "",
                                        style: "201",
                                        width: 32,
                                        height: 32
                                    });
                                    break;
                                case "weixin":
                                    n.$qrcode_dialog.is(":visible") || yp.use("makeQrcode", function() {
                                        var e = r.url
                                            , i = r.size < 150 ? 150 : r.size;
                                        n.$body.find("#js-div-share-qrcode-panel").length || n.$body.append(n.$qrcode_dialog),
                                        t.oQRCode || (t.oQRCode = new QRCode(n.$qrcode_dialog.find(".js-qrcode-img")[0],{
                                            width: i,
                                            height: i,
                                            correctLevel: r.currentLevel
                                        })),
                                            n.$qrcode_dialog.find(".content").width(i + 39),
                                            n.$qrcode_dialog.css({
                                                margin: "-" + (i + 39) / 2 + "px 0 0 -" + (i + 74) / 2 + "px",
                                                "z-index": r.zindex
                                            }),
                                            t.oQRCode.makeCode(e),
                                            n.$qrcode_dialog.show()
                                    })
                            }
                        })
                },
                openShareWindow: function(e, t) {
                    var n = [];
                    return yp.each(t, function(e, t) {
                        null != e && n.push(t + "=" + encodeURIComponent(e))
                    }),
                        window.open(e + "?" + n.join("&"), "", "width=760, height=640, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no"),
                        !1
                }
            };
        var n = e.fn.zqShare;
        e.fn.zqShare = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("oShare")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("oShare", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.zqShare.Constructor = t,
            e.fn.zqShare.noConflict = function() {
                return e.fn.zqShare = n,
                    this
            }
    }(window.jQuery),
    define("zqShare", function() {}),
    function(e) {
        var t, n = e.oYpConfig, i = n.baseUrl + "j/", o = (n.ypUrl,
        n.ver || "yp");
        !function(e, t) {
            if (e) {
                var n;
                for (n = e.length - 1; n > -1 && (!e[n] || !t(e[n], n, e)); n -= 1)
                    ;
            }
        }(function() {
            return document.getElementsByTagName("script")
        }(), function(e) {
            if (t = e.getAttribute("data-main-yp"))
                return !0
        }),
        Function.prototype.bind || (Function.prototype.bind = function(e) {
                if (arguments.length < 2 && void 0 === e)
                    return this;
                var t = this
                    , n = arguments;
                return function() {
                    var i, o = [];
                    for (i = 1; i < n.length; i++)
                        o.push(n[i]);
                    for (i = 0; i < arguments.length; i++)
                        o.push(arguments[i]);
                    return t.apply(e, o)
                }
            }
        ),
        t && function(e, t) {
            -1 == e.indexOf(".js") && (e = e + ".js?v=" + t),
                document.write('<script src="' + e + '"><\/script>')
        }(t, o)
    }(this),
    function(e) {
        var t = e.oYpConfig;
        t.debug = 1 == $.cookie("ypDebug"),
            t.fun = {
                formatSettings: {
                    re: /\${([\s\S]+?)}/g
                }
            },
            t.mods = e.mods,
            t.baseUrl = t.baseUrl || "../",
            t.loader = {
                require: {
                    urlArgs: function(e, n) {
                        var i = t.ver || +new Date;
                        return window.modsHash && window.modsHash[e] && (i = window.modsHash[e]),
                        "?v=" + i
                    },
                    paths: {},
                    shim: {}
                },
                baseList: e.baseList,
                data_pre: ""
            },
            t.loader.baseUrlList = {
                js: t.baseUrl + "j/lib/",
                css: t.baseUrl + "c/",
                html: t.baseUrl + "h/"
            },
            t.loader.require.baseUrl = t.loader.baseUrlList.js,
            t.loader.debug = t.debug || !0
    }(this),
    function(e, t) {
        var n = function(e) {
            return n.mix(!0, new n.fn.create(e), n)
        };
        n.fn = n.prototype = {
            constructor: n,
            yp: "20140223.1",
            create: function(e) {
                return this.config.init(),
                    this.loader.init(),
                    this
            }
        },
            n.fn.create.prototype = n.fn,
            n.create = n.fn.create,
            n.mix = t.extend,
            n.extend = function(e, t) {
                return n.mix({}, e, t)
            }
            ,
            n.each = function(e, n) {
                return t.each(e, function(e, t) {
                    return n(t, e)
                })
            }
            ,
            n.proxy = t.proxy,
            n.format = function(e, t) {
                var i = this.re || n.config.fun.formatSettings.re || /{([\s\S]+?)}/g;
                return "object" != typeof t && (t = [].slice.call(arguments, 1)),
                    e.replace(i, function(e, n) {
                        return null != t[n] ? t[n] : ""
                    })
            }
            ,
            n.noop = t.noop,
            n.mix(n, {
                rword: /[^, ]+/g,
                global: {},
                config: {},
                loader: {},
                loger: {},
                mods: {},
                cache: {},
                event: {},
                ui: {},
                system: {}
            }),
            e.yp = n,
            define("yp", [], function() {
                return n
            })
    }(this, jQuery),
    function(e, t) {
        var n = this
            , i = t.mix(t.config, n.oYpConfig);
        i.data = function(e, t) {}
            ,
            i.init = function() {
                i.mods.css = {},
                    i.mods.jq = {},
                    t.each(i.mods.modList, function(e, n) {
                        i.loader.require.paths[n] = e.js,
                            t.each(["shim"], function(t) {
                                e[t] && (i.loader.require[t][n] = e[t])
                            }),
                            t.each(["css", "jq"], function(t) {
                                e[t] && (i.mods[t][n] = e[t])
                            })
                    })
            }
            ,
            define("yp.config", [], function() {
                return i
            })
    }(jQuery, yp),
    function(e, t) {
        var n = t.event
            , i = e({});
        n.sub = function() {
            var e = arguments[0]
                , t = i.data(e.replace(/\..*/, ""));
            if (t) {
                return void (0,
                    arguments[1])(t)
            }
            i.on.apply(i, arguments)
        }
            ,
            n.unsub = function() {
                i.off.apply(i, arguments)
            }
            ,
            n.pub = function() {
                i.trigger.apply(i, arguments);
                var e = arguments[0];
                return {
                    cache: function(t) {
                        e = "string" == typeof e ? e : e.type + "." + e.namespace,
                            i.data(e, t || !0)
                    }
                }
            }
            ,
            e.sub = n.sub,
            e.unsub = n.unsub,
            e.pub = n.pub,
            t.each(["sub", "unsub", "pub"], function(n) {
                t[n] = function() {
                    return "string" == typeof arguments[0] && (arguments[0] = "yp/" + arguments[0]),
                        e[n].apply(null, arguments)
                }
            }),
            t.sub("page/cookie/oLoginInfo", function(e, t) {}),
            t.sub("page/cookie/oViewer", function(e, t) {}),
            t.sub("page/ip/save", function(t, n) {
                var i = e.cookie("cookie_ip") || ""
                    , o = new Date;
                i = i ? i.split(",") : ["", ""],
                2 < i.length && (i = ["", ""]),
                    i[n.index] = n.ip ? "" + n.ip : "",
                    o.setTime(o.getTime() + 2592e6),
                    e.cookie("cookie_ip", i.join(","), {
                        expires: o,
                        path: "/"
                    })
            })
    }(jQuery, yp),
    function(e, t) {
        var n = t.config
            , i = t.loader
            , o = function(e, t) {
            return function() {
                var n = [e];
                return [].push.apply(n, arguments),
                    t.apply(this, n)
            }
        }
            , a = function(e) {
            return o(t, e)
        }
            , r = function(e) {
            var t = document
                , n = t.getElementsByTagName("head")[0]
                , i = e.replace(/[?#].*/, "");
            if (!t.getElementById(i)) {
                var o = t.createElement("link");
                o.rel = "stylesheet",
                    o.href = e,
                    o.id = i,
                    n.appendChild(o, n.firstChild)
            }
        };
        i.loadCSS = function() {
            -1 == arguments[0].indexOf("http") && (arguments[0] = n.loader.baseUrlList.css + arguments[0] + ".css?v=" + n.ver),
                r.apply(this, arguments)
        }
            ,
            i.use = function(n, i, r) {
                n && (n = String(n).match(t.rword),
                i && (i = o(i, function(t) {
                    var i = [].slice.call(arguments, 1);
                    e.sub("base-ready", function() {
                        a(t).apply(null, i),
                            e.pub("loader-ready-page", n)
                    })
                })),
                    require(n, i, function(e) {
                        r && "function" == typeof r && r(e)
                    }))
            }
            ,
            t.use = i.use,
            i.ready = function() {
                var t = 3;
                return e.sub("base-ready doc-ready login-ready", function() {
                    --t <= 0 && i.ready()
                }),
                    function(t) {
                        t ? (e.pub("yp/init").cache(),
                            e.sub("yp/ready", t)) : e.pub("yp/ready").cache()
                    }
            }(),
            t.init = t.ready = i.ready,
            i.config = function(e) {
                t.mix(n.loader, e),
                    require.config(n.loader.require)
            }
            ,
            i.init = function() {
                require.config(n.loader.require),
                    require(["rich", "taskDataManage", "ibox", "scroll", "tabs", "zqSearch", "geetest", "zqSelect", "jCopy", "paginationList", "fileupload", "frameAnimation", "drag", "carousel", "zqShare"], function(n, i) {
                        t.each(["rich", "ibox", "scroll", "tabs", "zqSearch", "geetest", "zqSelect", "jCopy", "paginationList", "fileupload", "frameAnimation", "drag", "carousel", "zqShare", "taskDataManage"], function(t) {
                            e.pub("loader/ready", [t])
                        }),
                            window.oRich = t.oRich = n,
                            i.init(),
                            t.oTaskDataManage = i,
                            t.oRich.init(),
                            e.pub("base-ready").cache()
                    }),
                    e(function() {
                        t.ui.$body = e("body"),
                            e.pub("doc-ready").cache()
                    }),
                    e.ajax({
                        url: oPageConfig.oUrl.sGetLoginInfoUrl,
                        dataType: "json",
                        success: function(n) {
                            0 == n.code ? (oPageConfig.oLoginInfo = n.data,
                                t.pub("page/ip/save", {
                                    ip: n.data.clientIp,
                                    index: 0
                                })) : oPageConfig.oLoginInfo = null,
                                t.pub("page/cookie/oLoginInfo", {
                                    cookie_value: oPageConfig.oLoginInfo || 1
                                }),
                                e.pub("login-ready").cache()
                        },
                        error: function() {
                            oPageConfig.oLoginInfo = null,
                                t.pub("page/cookie/oLoginInfo", {
                                    cookie_value: 1
                                }),
                                e.pub("login-ready").cache()
                        }
                    })
            }
    }(jQuery, yp),
    yp.create(),
    function(e, t) {
        var n = t
            , i = n.config
            , o = n.loader
            , a = {}
            , r = function(n, a) {
            "string" != typeof n ? (a = n,
                n = a.url) : a = a || {};
            var r, s = function(e) {
                a.loaderBar && o.loaderBar.toggle(e)
            }, c = function() {
                s(!1)
            }, l = function(n, i, o) {
                var a = {
                    timeout: "服务器繁忙，请稍后再试！",
                    error: "服务器错误，请联系管理员",
                    errorInternal: "服务器错误，请联系管理员",
                    parsererror: "返回格式不合法，请联系管理员"
                }
                    , r = a[i] || n.message || a.error
                    , s = e.Event("yp/loader/ajax/fail/" + n.type);
                t.pub(s, n),
                s.isDefaultPrevented() || t.config.debug && e.pub("error/ui", {
                    code: "ajax/fail",
                    message: r,
                    data: n
                })
            };
            a.type = a.type || i.loader.type || "get",
                a.timeout = a.timeout || 5e3,
            a.noCache && (a.data = e.extend(a.data || {}, {
                _v: Math.floor(e.now() / 6e4)
            })),
                a.dataType = a.dataType || "json",
                s(!0);
            var u = function(e) {
                10001 != e.code || e.message || t.pub("page/login/logout")
            };
            return r = o[a.type].ajax(n, a),
                r.fail(l).always(c).done(u)
        }
            , s = function(t, n) {
            "string" != typeof t ? (n = t,
                t = n.url) : n = n || {};
            var o = e(n.target)
                , s = e.Deferred();
            if (!o.length)
                return e.pub("error/ui", {
                    code: "noFormSubmit",
                    message: "没有找到要提交的表单"
                }),
                    s;
            var c = o.find(":submit[type=submit]");
            if (o.trigger("form-pre-serialize", [o, c]),
            n.beforeSubmit && !1 === n.beforeSubmit(o, n))
                return s;
            var l = e.Event("form-submit-validate");
            if (o.trigger(l),
                l.isDefaultPrevented())
                return s;
            if (t = t || o.attr("action"),
                a[t])
                return a[t];
            var u = n.type || "post"
                , d = []
                , f = i.loader.data_pre;
            f && d.push(f);
            var p = o.serialize();
            p && d.push(p),
                p = n.data,
            p && "string" != typeof p && (p = e.param(p)),
            p && d.push(p),
                p = d.join("&");
            var h = n.success
                , m = n.loaderBar;
            return o.trigger("form-submit-notify"),
                a[t] = r(t, {
                    type: u,
                    data: p,
                    loaderBar: m,
                    timeout: n.timeout || 5e3
                }).done(h).always(function(e) {
                    delete a[t],
                        o.trigger("form-ajax-always", [o, c])
                })
        };
        o.ajax = n.ajax = r,
            o.ajaxSubmit = s,
            e.fn.ajaxSubmit = function(e, t) {
                return "string" != typeof e ? t = e || {} : (t = t || {},
                    t.url = e),
                    t.target = this,
                    this.ajax = s(t),
                    this
            }
            ,
            e.sub("form-pre-serialize", function(e, t, n) {
                n.prop("disabled", !0)
            }),
            e.sub("form-validate-fail form-ajax-always", function(e, t, n) {
                n.prop("disabled", !1)
            }),
            t.each(["get", "post"], function(t) {
                o[t] = e
            }),
            o.loaderBar = {
                dom: e("<div>loading...</div>"),
                toggle: function(e) {
                    e ? this.dom.appendTo("body") : this.dom.remove()
                }
            },
            define("yp.loader", [], function() {
                return o
            })
    }(jQuery, yp),
    function(e, t) {
        var n = this
            , i = t
            , o = i.global
            , a = i.ui;
        a.$win = e(n),
            a.$doc = e(document),
            a.$body = e(document.body),
            a.support = {},
            a.$wrapper = e("#yp-wrapper"),
            o.zIndex = 999;
        var r = function() {
            o.width = a.$win.width(),
                o.height = a.$win.height()
        };
        e.sub("ui/resize.ui", function() {
            r(),
                e.pub("ui/main/resize")
        }),
            e.sub("ui/main/resize.ui", function() {
                a.$wrapper.toggleClass("screen", o.width < 1400)
            }),
            e.sub("ui/form/submit.ui", function(t, n) {
                e(n).submit()
            }),
            e.sub("ui/showMenu.ui", function(t, n) {
                var i = n.target
                    , o = e(i)
                    , r = n.toggle
                    , s = !o.is(":visible")
                    , c = o.attr("data-keep") || o.find(".close").length
                    , l = "dropdown" === r;
                s && !c && (a.$doc.off("click.page.target"),
                    setTimeout(function() {
                        a.$doc.on("click.page.target", function(t) {
                            e(t.target).closest(i).length || (a.$doc.off("click.page.target"),
                                l ? o.trigger("hidden.yp.menu") : o.hide())
                        })
                    })),
                    l ? o.trigger("show.yp.menu") : o.toggle(s, !0)
            }),
            e.sub("ui/toggleClass.ui", function(t, n) {
                var i = n.target
                    , o = n.className
                    , a = n.flag;
                e(i).toggleClass(o, a)
            }),
            e.sub("ui/keydown.ui", function(e, t) {
                t.code
            }),
            e.sub("ui/blink.ui.event", function(t, n) {
                var i = n.delay || 200
                    , o = n.count || 3
                    , a = n.className || "blink"
                    , r = n.target
                    , s = r && e(r)
                    , c = n.fn || s.toggleClass.bind(s, a)
                    , l = function() {
                    c(!0),
                        setTimeout(function() {
                            c(!1),
                            --o && setTimeout(function() {
                                l()
                            }, i)
                        }, i)
                };
                l()
            }),
            e.sub("error/ui.ui", function(n, i) {
                var n = e.Event("yp/ui/error/" + i.code);
                t.pub(n, i.data),
                n.isDefaultPrevented() || alert(i.message)
            }),
            e.sub("error/sys.ui", function(e, t) {}),
            a.cssSupports = function() {
                var e = document.createElement("div")
                    , t = "Khtml O Moz Webkit".split(" ")
                    , n = t.length;
                return function(i) {
                    if (i in e.style)
                        return !0;
                    if ("-ms-" + i in e.style)
                        return !0;
                    for (i = i.replace(/^[a-z]/, function(e) {
                        return e.toUpperCase()
                    }); n--; )
                        if (t[n] + i in e.style)
                            return !0;
                    return !1
                }
            }();
        var s = function() {
            var e, n = ["webkit", "moz", "o", "ms"];
            return t.each(n, function(t) {
                void 0 !== document[t + "Hidden"] && (e = t)
            }),
                e
        }();
        s && document.addEventListener(s + "visibilitychange", function(e) {
            t.documentVisible = document[s + "VisibilityState"],
                t.pub("page/document/vidible", {
                    visible: document[s + "VisibilityState"]
                })
        }),
            define("yp.global", [], function() {
                return o
            }),
            define("yp.ui", [], function() {
                return a
            })
    }(jQuery, yp),
    function(e, t) {
        var n = t
            , i = n.ui;
        n.event;
        i.$win.on("resize.ui.event", function() {
            e.pub("ui/resize")
        }),
            i.$win.resize(),
            i.$doc.on("keydown.ui.event", function(t) {
                e.pub("ui/keydown", {
                    code: t.which,
                    target: t.target,
                    event: t
                })
            }),
            t.sub("page/domCreate.ui.event", function(t, n) {
                return e.pub("ui/update", n.target),
                    !1
            }),
            i.$doc.on("dom.create.ui.event", function(t) {
                return e.pub("ui/update", t.target),
                    !1
            }),
            i.$doc.on("submit.ui.event", "form", function() {
                if (null == e(this).attr("data-jump"))
                    return !1
            }),
            i.$doc.on("click.ui.event", 'a[type="submit"]', function(t) {
                t.preventDefault(),
                    e.pub("ui/form/submit", e(this).closest("form"))
            }),
            i.$doc.ajaxComplete(function() {
                e.pub("loader/ajaxDone")
            }),
            e.sub("mods/ready/ibox.ui.event", function() {
                var n = e.ibox.data();
                t.mix(!0, n.tmpl, {
                    head: {
                        ico: ""
                    },
                    foot: {
                        ico: ""
                    }
                }),
                    t.mix(!0, n.classes, {
                        ibox: "iboxMsg",
                        cur: "active",
                        hd: "popover-title",
                        closeBox: "close",
                        bd: "popover-content",
                        ibox_msg: "ibox-msg",
                        ft: "ibox-ft",
                        ico: {
                            msg: "ico_32_color",
                            btnY: "",
                            btnN: ""
                        }
                    }),
                    n.defaults = {
                        hasMask: !1,
                        btnY_text: "确 定",
                        btnN_text: "取 消",
                        dragHandle: ".popover-title",
                        tmpl: {
                            title: "{0}",
                            hd: '<h3 class="{0}"></h3>',
                            closeBox: '<a href="javascript:;" class="close icon-cancel-circle"></a>',
                            ibox: '<div class="popover {0}">{1}</div>'
                        }
                    },
                    e.ibox.data("zIndex", 2050, 2999),
                    e.ibox.init()
            }),
            e.sub("mods/ready/bootstrap.ui.event", function() {
                e.fn.popover.Constructor.DEFAULTS.template = '<div class="popover">      <div class="arrow"><em>◆</em><span>◆</span></div>      <h3 class="popover-title"></h3>      <div class="popover-content"></div>      </div>'
            }),
            e.sub("mods/ready/tabs", function(t, n) {
                e(function() {
                    e(".tabs").tabs({
                        cur: "active"
                    })
                })
            }),
            e.sub("mods/ready/scroll", function(t, n) {
                e(function() {
                    e(".js-scroll").each(function() {
                        var t = e(this);
                        t.slimscroll({
                            height: t.attr("data-height") || "auto",
                            size: t.attr("data-size") || "8",
                            color: t.attr("data-color") || "#e2e1e6",
                            opacity: 1,
                            railBorderRadius: "0px",
                            borderRadius: "0px",
                            allowPageScroll: 1 == t.attr("data-pagescroll")
                        })
                    })
                })
            }),
            t.sub("page/dom/append", function(t, n) {
                e(n.target).slimscroll()
            }),
            e.sub("mods/ready/lazyload", function(t, n) {
                e(function() {
                    e("img.js-lazy").lazyload({
                        threshold: 100,
                        effect: "fadeIn",
                        failure_limit: 5
                    })
                })
            }),
            t.sub("page/showMenu.ui.event", function(t, n) {
                e.pub("ui/showMenu", n)
            }),
            i.$doc.on("click.ui.event", "[data-target],[data-target-menu]", function(t) {
                t.preventDefault();
                var n = e(this)
                    , i = n.attr("data-target") || n.attr("data-target-menu") || n.attr("href");
                i && e.pub("ui/showMenu", {
                    target: i
                })
            }),
            i.$doc.on("click.ui.event", "[data-toggle-class]", function(t) {
                t.preventDefault();
                var n = e(this)
                    , i = n.attr("data-target-toggle") || n.attr("href")
                    , o = n.attr("data-toggle-class");
                i && e.pub("ui/toggleClass", {
                    target: i,
                    className: o
                })
            }),
            i.$doc.on("click.ui.event", ".ibox .close", function(t) {
                t.preventDefault(),
                    e(this).closest(".ibox").trigger("hidden.yp.modal")
            }),
            i.$doc.on("hidden.yp.modal", function(t) {
                e(t.target).hide(),
                    i.$doc.off("click.page.target")
            }),
            i.$doc.on("show.yp.menu", function(t) {
                e.pub("ui/toggleClass", {
                    target: t.target,
                    className: "open",
                    flag: !0
                })
            }),
            i.$doc.on("hidden.yp.menu", function(t) {
                e.pub("ui/toggleClass", {
                    target: t.target,
                    className: "open",
                    flag: !1
                })
            }),
            t.sub("page/resize.ui.event", function() {
                e.pub("ui/resize")
            }),
            e.sub("ui/main/resize.ui.event", function() {
                t.pub("ui/resize")
            }),
            t.sub("page/blink.ui.event", function(t, n) {
                e.pub("ui/blink", n)
            }),
            e(function() {
                e(".js-panel-tabLine").each(function() {
                    var n = e(this)
                        , i = n.find("li.active")
                        , o = n.find("li:eq(0)").position().left
                        , a = i.position().left - o
                        , r = t.format('<div class="active-line" style="left:${0}px"></div>', a);
                    n.after(r),
                        n.closest(".tabs").on("tabsactive", function(t, n) {
                            var i = (n.index,
                                e(this))
                                , a = i.find(".active-line")
                                , r = i.find("li.active")
                                , s = r.position().left - o
                                , c = r.outerWidth();
                            a.stop().animate({
                                left: s,
                                width: c
                            }, 300)
                        })
                })
            })
    }(jQuery, yp),
    function(e, t) {
        var n = this
            , i = t
            , o = i.config
            , a = i.loader
            , r = i.ui
            , s = {
            init: function() {
                this.modList = o.mods.modList,
                    this.css = o.mods.css,
                    this.jq = o.mods.jq,
                    this.update = {},
                    this.updateList = {}
            },
            addMods: function(n) {
                return n = [].concat(n),
                    t.each(n, function(t) {
                        var n = s.modList[t];
                        n.isLoad || (n.isLoad = !0,
                            e.pub("mods/init/css", [s.css[t], t]),
                            e.pub("mods/init/jq", [s.jq[t], t]),
                            e.pub("mods/init/update", [s.update[t], t, r.$body]),
                            e.pub("mods/ready/" + t, n))
                    }),
                    this
            },
            updateAll: function(n) {
                var i = e(n);
                t.each(s.updateList, function(e) {
                    e(i)
                })
            }
        };
        s.init(),
            e.sub("loader/ready.mods", function(e, t) {
                t in s.modList && s.addMods(t)
            }),
            e.sub("mods/init/css", function(e, n) {
                n && t.each(n, function(e) {
                    a.loadCSS(e)
                })
            }),
            e.sub("mods/init/jq", function(n, i, o) {
                if (i) {
                    i.length || (i = [o]);
                    !function(n) {
                        t.each(i, function(t) {
                            var n = e.fn[t];
                            e.fn[t] = function() {
                                return this.length ? n.apply(this, arguments) : this
                            }
                        })
                    }()
                }
            }),
            e.sub("mods/init/update", function(e, t, n, i) {
                t && (t(i),
                    s.updateList[n] = t)
            }),
            n.ibox_init = !1,
            e.sub("mods/ready/ibox", function() {
                n.ibox = e.ibox,
                    n.ibox.form = e.ibox.form,
                    n.alert = function() {
                        var t = e.ibox.alert.apply(e.ibox, arguments);
                        t.dom;
                        return t
                    }
                    ,
                    n.confirm = function() {
                        var t = e.ibox.confirm.apply(e.ibox, arguments);
                        t.dom;
                        return t
                    }
                    ,
                    t.notify = function(n, i, o) {
                        var a = 0 == i ? "success" : "error"
                            , r = 0 == i ? 1e3 : 2e3
                            , s = t.mix({}, {
                            content: n,
                            type: a,
                            delay: r
                        }, o);
                        return e.inotify(s)
                    }
                    ,
                    t.successNotify = function(e, n) {
                        return t.notify(e, 0, n)
                    }
                    ,
                    t.errorNotify = function(e, n) {
                        return t.notify(e, 1, n)
                    }
                    ,
                    t.ajaxNotice = function(e) {
                        var n = e.extraMsg ? e.extraMsg.msg : e.message;
                        e.extraMsg && "alert" == e.extraMsg.type ? alert(n) : n && t.notify(n, e.code)
                    }
            }),
            e.sub("ui/update.mods", function(t, n) {
                e.pub("mods/updateAll", n),
                    s.updateAll(n)
            }),
            i.mods = s,
            define("yp.mods", [], function() {
                return s
            })
    }(jQuery, yp),
    function(e, t) {
        var n = this
            , i = t
            , o = i.cache;
        o.list = {},
            o.create = function(e) {
                return o.list[e || "memory"]
            }
            ,
            o.init = function() {
                var i = {};
                t.each(["memory", "cookie", "localStorage"], function(t) {
                    var a = t;
                    if ("localStorage" === a && ("object" == typeof n.localStorage ? i = n.localStorage : a = "cookie"),
                    "cookie" === a && (i = {
                        removeItem: function(t) {
                            e.cookie(t, null)
                        }
                    },
                        i.getItem = i.setItem = e.cookie),
                    "memory" === a) {
                        var r = n.top;
                        i = {
                            data: function(e, t) {
                                var n = r._CACHE || (r._CACHE = {});
                                return void 0 === t ? n[e] : (n[e] = t,
                                    this)
                            },
                            removeItem: function(e) {
                                var t = r._CACHE;
                                return t && t[e] && delete t[e],
                                    this
                            }
                        },
                            i.getItem = i.setItem = i.data
                    }
                    o.list[t] = i
                })
            }
            ,
            o.init(),
            i.localStorage = o.localStorage = o.create("localStorage"),
            define("yp.cache", [], function() {
                return o
            })
    }(jQuery, yp),
    function(e) {
        "use strict";
        var t = function(e, t) {
            this.type = this.options = this.timeout = this.$element = null,
                this.init("code", e, t)
        };
        t.DEFAULTS = {
            size: 4,
            timeExpires: 12e4,
            fieldName: "code",
            selectorTxt: "input:text",
            selectorImg: "img",
            selectorBtn: "a,img"
        },
            t.prototype.init = function(t, n, i) {
                this.type = t,
                    this.$element = e(n),
                    this.options = this.getOptions(i),
                    this.$element.on("focus." + this.type, this.options.selectorTxt, e.proxy(this.update, this, !1)),
                    this.$element.on("click." + this.type, this.options.selectorBtn, e.proxy(this.update, this, !0)),
                    this.show()
            }
            ,
            t.prototype.getDefaults = function() {
                return t.DEFAULTS
            }
            ,
            t.prototype.getOptions = function(t) {
                return t = e.extend({}, this.getDefaults(), this.$element.data(), t),
                    t.reCode = new RegExp("^\\w{" + t.size + "}$"),
                    t
            }
            ,
            t.prototype.setOptions = function(t) {
                return e.extend(this.options, t),
                    !0
            }
            ,
            t.prototype.getValue = function() {
                return this.$element.find(this.options.selectorTxt).val()
            }
            ,
            t.prototype.show = function(t) {
                var n = e.Event("show.yp." + this.type);
                this.$element.trigger(n),
                n.isDefaultPrevented() || (this.$element.show(),
                    this.$element.find(this.options.selectorTxt).prop("disabled", !1),
                    this.$element.trigger("shown.yp." + this.type))
            }
            ,
            t.prototype.hide = function(t) {
                var n = e.Event("hide.yp." + this.type);
                this.$element.trigger(n),
                n.isDefaultPrevented() || (this.$element.hide(),
                    this.$element.find(this.options.selectorTxt).prop("disabled", !0),
                    this.$element.trigger("hidden.yp." + this.type))
            }
            ,
            t.prototype.update = function(e, t) {
                t && t.preventDefault();
                var n = this;
                if (e || this.timeout || (e = !0),
                    e) {
                    var i = this.options.urlImg + "&t=" + +new Date;
                    this.$element.find(this.options.selectorImg).attr("src", i),
                        this.$element.find(this.options.selectorTxt).val(""),
                        this.timeout = setTimeout(function() {
                            n.timeout = null
                        }, this.options.timeExpires)
                }
            }
            ,
            t.prototype.check = function() {
                var t = e.Event("check.yp." + this.type);
                if (this.$element.trigger(t),
                    !t.isDefaultPrevented()) {
                    var n, i = this.$element.find(this.options.selectorTxt).val();
                    if (i)
                        if (this.options.reCode.test(i)) {
                            if (this.options.urlCheck) {
                                var t = e.Event("checkAjax.yp." + this.type);
                                if (this.$element.trigger(t),
                                    t.isDefaultPrevented())
                                    return;
                                var o = {};
                                o[this.options.fieldName] = i,
                                    n = yp.ajax(this.options.urlCheck, {
                                        data: o
                                    }).always(function(e) {
                                        this.$element.trigger("checkedAjax.yp." + this.type, e)
                                    })
                            }
                        } else
                            n = "typeError";
                    else
                        n = "empty";
                    return this.$element.trigger("checked.yp." + this.type),
                        n
                }
            }
            ,
            e.fn.code = function(n) {
                return this.each(function() {
                    var i = e(this)
                        , o = i.data("yp.code")
                        , a = "object" == typeof n && n;
                    o || i.data("yp.code", o = new t(this,a)),
                    "string" == typeof n && o[n]()
                })
            }
            ,
            e.fn.code.Constructor = t
    }(window.jQuery),
    function(e, t) {
        var n = this
            , i = n.console || {}
            , o = t.config
            , a = t.loger;
        if (a.mode = "console",
            a.log = function() {
                var t = this.mode || a.mode;
                "console" === t && i.log ? i.log.apply(i, ["yp调试："].concat(arguments)) : "alert" === t ? alert("yp调试：" + arguments[0]) : e("body").append("<div>yp调试：" + arguments[0] + "</div>")
            }
            ,
            a.init = function() {
                o.debug || (a.log = i.log = function() {}
                ),
                    t.log = a.log
            }
            ,
            a.init(),
        i && i.info) {
            i.info("%c   zzzzzzzzzzzzzzzzzzz   hhhhh         hhhhh          aaaaaaaaa           nnnnnnn           nnnnn       qqqqqqqqq          iiii\n  z::::::::::::::::::z   h:::h         h:::h         a:::a  a:::a         n:::::n           n:::n    qq:::::::::qq        i::::i\n  z:zzzzzzzzzzzzz::::z   h:::h         h:::h         a:::a  a:::a         n::::::n          n:::n   qq:::::::::::::qq      iiii\n  zz           z::::z    h:::h         h:::h        a:::a    a:::a        n:::::::n         n:::n  q:::::::qqq:::::::q\n  z           z::::z     h:::h         h:::h        a:::a    a:::a        n:::nn::::n       n:::n  q::::::q   q::::::q   iiiiiii\n             z::::z      h:::h         h:::h       a:::a      a:::a       n:::n n::::n      n:::n  q:::::q     q:::::q   i:::::i\n            z::::z       h:::h         h:::h       a:::a      a:::a       n:::n  n::::n     n:::n  q:::::q     q:::::q    i::::i\n           z::::z        h:::hhhhhhhhhhh:::h      a:::a        a:::a      n:::n   n::::n    n:::n  q:::::q     q:::::q    i::::i\n          z::::z         h:::::::::::::::::h      a:::aaaaaaaaaa:::a      n:::n    n::::n   n:::n  q:::::q     q:::::q    i::::i\n         z::::z          h:::hhhhhhhhhhh:::h     a::::::::::::::::::a     n:::n     n::::n  n:::n  q:::::q     q:::::q    i::::i\n        z::::z           h:::h         h:::h     a:::aaaaaaaaaaaa:::a     n:::n      n::::n n:::n  q:::::q   qqq:::::q    i::::i\n       z::::z            h:::h         h:::h    a:::a            a:::a    n:::n       n::::nn:::n  q::::::q q::::::::q    i::::i\n      z::::z         zz  h:::h         h:::h    a:::a            a:::a    n:::n        n::::::::n  q:::::::qq::::::::q    i::::i\n     z::::z         z:z  h:::h         h:::h   a:::a              a:::a   n:::n         n:::::::n   qq::::::::::::::q    i::::::i\n    z::::zzzzzzzzzzz::z  h:::h         h:::h   a:::a              a:::a   n:::n          n::::::n     qq:::::::::::q     i::::::i\n   z:::::::::::::::::zz  h:::h         h:::h  a:::a                a:::a  n:::n           n:::::n       qqqqqqqq::::qq   iiiiiiii\n  zzzzzzzzzzzzzzzzzzzz   hhhhh         hhhhh  aaaaa                aaaaa  nnnnn           nnnnnnn                q:::::q iiiiiiii\n                                                                                                                  qqqqqq\n", "color:#12b7f5;text-shadow:15px 15px 5px #12b7f5, 15px 15px 10px #12b7f5, 15px 15px 15px #12b7f5, 15px 15px 30px #12b7f5;font-weight:bolder;font-size:15px")
        }
        define("yp.loger", [], function() {
            return a
        })
    }(jQuery, yp),
    function(e, t, n) {
        n.ObjectList = function() {
            "use strict";
            return {
                data: [],
                $list: {},
                $one: null,
                id: "id",
                reset: function() {
                    this.data = [],
                        this.$list = {}
                },
                getList: function(e) {
                    return null != e ? this.data[e] : this.data
                },
                getOne: function(e) {
                    return null != e ? this.$list[e] : this.$one
                },
                setOne: function(e, t) {
                    return this.$one = e
                },
                markIn: function(e) {
                    var t = this
                        , n = this.data.slice(e);
                    e = e || 0;
                    for (var i, o = n.length; o--; )
                        i = n[o],
                        t._markIn && t._markIn(i),
                            t.$list[i[t.id]] = i
                },
                markOut: function(e) {
                    this._markOut && this._markOut(e),
                        delete this.$list[e[this.id]]
                },
                add: function(e) {
                    var t = this.data.length;
                    return this.data.push.apply(this.data, [].concat(e)),
                        this.markIn(t)
                },
                replace: function(e, t) {
                    return t = t || 0,
                        e = [t, e.length].concat(e),
                        this.data.splice.apply(this.data, e),
                        this.markIn(t),
                        this.data
                },
                del: function(e) {
                    this.data.remove(e),
                        this.markOut(e)
                },
                delById: function(e) {
                    for (var t, n = this.data.length; n--; )
                        t = this.data[n],
                        t[this.id] == e && this.del(t)
                }
            }
        }()
    }(0, 0, this),
    function(e) {
        "use strict";
        var t = yp.format
            , n = function(t, i) {
            this.$element = e(t),
                this.options = i,
                n.prototype.fSendMsg = i.fSendMsg,
                this.oScrollLimit = {
                    lastTime: 0,
                    timeCD: 0,
                    beTimeCDIng: !1,
                    scrollDelay: 300
                },
                this.msgToAppend = ""
        };
        n.DEFAULTS = {
            t_msgItem: "<li>{0}</li>",
            clearKeepSelector: "",
            autoScroll: !0
        },
            n.prototype = {
                fOnMsg: function(n, i) {
                    var o = this;
                    if (o.msgToAppend += t(this.options.t_msgItem, n),
                        i) {
                        var a = this.oScrollLimit
                            , r = e.now()
                            , s = r - a.lastTime;
                        a.scrollDelay < s ? (o.fAppendMsg(),
                            o.fDoScroll()) : a.beTimeCDIng || (a.beTimeCDIng = !0,
                            a.timeCD = setTimeout(function() {
                                o.fAppendMsg(),
                                    o.fDoScroll()
                            }, a.scrollDelay - s))
                    } else
                        o.fAppendMsg();
                    return this
                },
                fAppendMsg: function() {
                    var e = this;
                    e.$element.append(e.msgToAppend),
                        e.msgToAppend = ""
                },
                fDoScroll: function() {
                    var t = this
                        , n = t.oScrollLimit;
                    t.options.$scrollObj.scrollTop(t.options.$scrollObj[0].scrollHeight),
                        t.options.$scrollObj.trigger("scrollTop.yp.chat", {
                            scrollTo: t.$element[0].scrollHeight
                        }),
                        n.beTimeCDIng = !1,
                        n.lastTime = e.now()
                },
                fSendMsg: function(e) {
                    return this
                },
                fClear: function() {
                    return this.options.clearKeepSelector ? this.$element.children(":not(" + this.options.clearKeepSelector + ")").remove() : this.$element.empty(),
                        this
                }
            },
            e.fn.chat = function(t) {
                return this.each(function() {
                    var i = e(this)
                        , o = i.data("yp.chat")
                        , a = e.extend({}, n.DEFAULTS, i.data(), "object" == typeof t && t)
                        , r = "string" == typeof t ? t : "";
                    o || i.data("yp.chat", o = new n(this,a)),
                    r && o[r]()
                })
            }
            ,
            e.fn.chat.Constructor = n
    }(window.jQuery),
    yp.module = {
        registModule: {},
        fRegistModule: function(e, t) {
            this.registModule[e] = t
        },
        fCallFun: function(e, t, n) {
            var i = this;
            return i.registModule[e] && "function" == typeof i.registModule[e][t] ? i.registModule[e][t].apply(i.registModule[e], n || []) : null
        },
        fPropertyVal: function(e, t, n) {
            var i = this;
            return i.registModule[e] && "function" != typeof i.registModule[e][t] ? void 0 == n ? i.registModule[e][t] : void (i.registModule[e][t] = n) : null
        },
        fGetModule: function(e) {
            return this.registModule[e] || null
        },
        fLoadModule: function(e, t, n) {
            this.registModule[e] ? n && n() : yp.use(t, function(e, t) {
                t.init(),
                n && n()
            })
        }
    },
    define("j/yp/yp-base", function() {}),
    function(e) {
        "use strict";
        var t = function(n, i) {
            this.$element = e(n),
                this.options = e.extend(t.DEFAULTS, i),
                this._ui = {},
                this.init()
        };
        t.DEFAULTS = {
            sGetPhoneCodeUrl: "",
            aCountryCodeUseVoice: [],
            defaultCountryCode: "+86"
        },
            t.prototype = {
                init: function() {
                    this.view(),
                        this.bindEvent()
                },
                view: function() {
                    var t = this
                        , n = t.$element;
                    t._ui.$country_select = n.find(".js-country-select-panel"),
                        t._ui.$country_code_show = n.find(".js-country-code-show"),
                        t._ui.$phone = n.find(".js-phone-input"),
                        t._ui.$phone_show = n.find(".js-phone-show"),
                        t._ui.$phone_code_btn = n.find(".js-phone-code-btn"),
                        t._ui.$voice_txt = n.find(".js-voice-txt"),
                        t._ui.$country_select.zqSelect({
                            defaultVal: t.options.defaultCountryCode
                        }),
                        t._ui.$voice_txt.toggle(-1 < e.inArray(t.options.defaultCountryCode, t.options.aCountryCodeUseVoice))
                },
                bindEvent: function() {
                    var t = this;
                    t._ui.$country_select.on("selectChange", function() {
                        var n = t._ui.$country_select.data("selectVal")();
                        t._ui.$country_code_show.html(n),
                            t._ui.$voice_txt.toggle(-1 < e.inArray(n, t.options.aCountryCodeUseVoice))
                    }),
                        t._ui.$phone_code_btn.on("click", function(e) {
                            e.preventDefault(),
                                t.fGetPhoneCode()
                        })
                },
                fGetPhoneCode: function() {
                    var t = this
                        , n = t._ui
                        , i = t.options;
                    if ("" == n.$phone.val())
                        return i.fCheckError && i.fCheckError(n.$phone, "null"),
                            !1;
                    if (n.$phone_code_btn.data("timing"))
                        return !1;
                    var o = n.$country_select.data("selectVal")()
                        , a = -1 < e.inArray(o, t.options.aCountryCodeUseVoice) ? "voice" : "";
                    n.$phone_code_btn.data("timing", !0),
                        t.fCodeTimeCutdown(n.$phone_code_btn, "voice" == a ? 300 : 60),
                        yp.ajax(t.options.sGetPhoneCodeUrl, {
                            type: "post",
                            dataType: "json",
                            data: {
                                mobile: n.$phone.val(),
                                countryCode: o,
                                type: a
                            }
                        }).done(function(e) {
                            0 == e.code || (i.fCheckError && i.fCheckError(t.$element.find('[name="mobile"]'), e),
                                t.fCodeTimeReset(n.$phone_code_btn, "重新获取"))
                        }).fail(function() {
                            t.fCodeTimeReset(n.$phone_code_btn, "重新获取")
                        })
                },
                fCodeTimeCutdown: function(e, t) {
                    var n = this
                        , i = 0;
                    e.data("timeout") && clearTimeout(e.data("timeout")),
                        t ? (e.addClass("again-voice-btn").html("发送中(" + t + ")"),
                            i = setTimeout(function() {
                                t--,
                                    n.fCodeTimeCutdown(e, t)
                            }, 1e3),
                            e.data("timeout", i)) : n.fCodeTimeReset(e, "重新获取")
                },
                fCodeTimeReset: function(e, t) {
                    this._ui.$phone_code_btn.data("timing", !1),
                    e.data("timeout") && clearTimeout(e.data("timeout")),
                        e.removeClass("again-voice-btn").html(t)
                },
                codeTimeReset: function(e) {
                    var t = this;
                    t.fCodeTimeReset(t._ui.$phone_code_btn, e)
                },
                countryReset: function() {
                    this._ui.$country_select.trigger("selectReset")
                },
                setCountryCodeDisable: function(e) {
                    this._ui.$country_select.zqSelect("setDisable", e)
                },
                setCountryCode: function(e) {
                    this._ui.$country_select.data("selectVal")(e)
                },
                setPhoneVal: function(e) {
                    var t = this;
                    t._ui.$phone_show.html(e),
                        t._ui.$phone.val(e)
                },
                setPhoneValDisable: function(e) {
                    var t = this;
                    t._ui.$phone_show.toggle(!!e),
                        t._ui.$phone.toggle(!e)
                }
            };
        var n = e.fn.phoneCode;
        e.fn.phoneCode = function(n) {
            var i = 2 <= arguments.length ? [].slice.call(arguments, 1) : [];
            return this.each(function() {
                var o = e(this)
                    , a = o.data("oPhoneCode")
                    , r = e.extend({}, t.DEFAULTS, o.data(), "object" == typeof n && n);
                a || o.data("oPhoneCode", a = new t(this,r)),
                "string" == typeof n && a[n].apply(a, i)
            })
        }
            ,
            e.fn.phoneCode.Constructor = t,
            e.fn.phoneCode.noConflict = function() {
                return e.fn.phoneCode = n,
                    this
            }
    }(window.jQuery),
    define("phoneCode", function() {}),
    function() {
        var e = {
            _ui: {},
            init: function(e, t, n, i, o) {
                var a = this;
                this.view(e, n),
                    this.bindEvent(),
                    this.listenYpEvent(),
                    this.showModal(e, t, n, i, o),
                    this.init = function(e, t, n, i, o) {
                        a.showModal(e, t, n, i, o)
                    }
            },
            view: function(e) {
                var t = this
                    , n = t._ui;
                n.$panel = $('<div class="common-ibox message-tit-ibox voice-ibox" id="js-phone-check-panel" style="display:none;">          <div class="content">            <div class="hd">              消费提醒              <a href="javascript:;" class="close js-close"></a>            </div>            <div class="bd">              <form action="">                <div class="row clearfix">                  <div class="row-left">手机号码：</div>                  <div class="row-right">                    <span class="num js-phone-show"></span>                    \x3c!-- 重新发送加类again-voice-btn  --\x3e                    <input type="text" class="js-phone-input js-require" style="display:none;" data-require="填写手机号" name="mobile" />                    <a href="javascript:;" class="send-voice-btn di js-phone-code-btn">发送验证码</a>                    <span class="js-country-code-show" style="display:none;"></span>                  </div>                </div>                <div class="row clearfix z10">                  <div class="row-left">选择地区：</div>                  <div class="row-right">                    <div class="select js-country-select-panel">                      <select>                        <option value="+86">中国大陆</option>                        <option value="+852">香港</option>                        <option value="+853">澳门</option>                        <option value="+886">台湾</option>                        <option value="+61">澳大利亚(Australia)</option>                        <option value="+1">加拿大(Canada)</option>                        <option value="+33">法国(France)</option>                        <option value="+49">德国(Germany)</option>                        <option value="+39">意大利(Italy)</option>                        <option value="+81">日本(Japan)</option>                        <option value="+82">韩国(Korea)</option>                        <option value="+60">马来西亚(Malaysia)</option>                        <option value="+64">新西兰(New Zealand)</option>                        <option value="+31">荷兰(Netherlands)</option>                        <option value="+7">俄罗斯(Russia)</option>                        <option value="+65">新加坡(Singapore)</option>                        <option value="+66">泰国(Thailand)</option>                        <option value="+44">英国(UK)</option>                        <option value="+1">美国(USA)</option>                        <option value="+84">越南(Vietnam)</option>                        <option value="+351">葡萄牙(Portugal)</option>                        <option value="+62">印度尼西亚(Indonesia)</option>                      </select>                    </div>                  </div>                </div>                <div class="row clearfix">                  <div class="row-left">验证码：</div>                  <div class="row-right">                    <input type="text" class="input dv js-require" name="code" data-require="请输入验证码">                    <span class="dv tit-text">六位数验证码</span>                  </div>                </div>                <div class="voice-tit">                  <i class="triangle"></i>                  <div class="clearfix">                    <i class="tit-icon"></i>                    <div class="title">为了保证您的资金安全，需要验证您的手机号请留意来自057156960566或类似电话播报验证码</div>                  </div>                </div>                <div class="btn-area textC">                  <button type="submit" class="ibox-define-btn di">提交</button>                </div>              </form>            </div>          </div>        </div>'),
                    $("body").append(n.$panel),
                    n.$panel.phoneCode({
                        sGetPhoneCodeUrl: oPageConfig.oUrl.sGlobalGetPhoneCodeUrl,
                        aCountryCodeUseVoice: ["+86"]
                    })
            },
            bindEvent: function() {
                var e = this
                    , t = e._ui;
                t.$panel.find("form").on("submit", function(t) {
                    t.preventDefault(),
                        e.fCheckSubmit($(this))
                }),
                    t.$panel.on("click", ".js-close", function(t) {
                        t.preventDefault(),
                            e.hideModal()
                    })
            },
            listenYpEvent: function() {
                var e = this;
                e._ui;
                yp.sub("page/logout/done", function() {
                    e.hideModal()
                })
            },
            showModal: function(e, t, n, i, o) {
                var a = this
                    , r = a._ui;
                r.$panel.show(),
                    r.$panel.find("form").attr("action", o),
                    r.$panel.find("form")[0].reset(),
                    r.$panel.phoneCode("countryReset"),
                    r.$panel.phoneCode("setPhoneVal", e),
                    r.$panel.phoneCode("setCountryCode", t),
                    r.$panel.phoneCode("setCountryCodeDisable", !n),
                    r.$panel.phoneCode("setPhoneValDisable", !i),
                    r.$panel.phoneCode("codeTimeReset", "发送验证码")
            },
            hideModal: function() {
                this._ui.$panel.hide()
            },
            fCheckSubmit: function(e) {
                var t = this;
                t._ui;
                if (e.data("ajaxing"))
                    return !1;
                var n = !0;
                if (yp.each(e.find(".js-require"), function(e) {
                    "" == e.value && (yp.errorNotify($(e).data("require")),
                        n = !1)
                }),
                    !n)
                    return !1;
                e.data("ajaxing", !0).ajaxSubmit().ajax.done(function(e) {
                    0 == e.code ? (yp.successNotify("提交成功"),
                        t.hideModal()) : e.message && yp.errorNotify(e.message)
                }).always(function() {
                    e.data("ajaxing", !1)
                })
            }
        };
        "undefined" != typeof module ? module.exports = e : "function" == typeof define && define.amd ? define("phoneCheck", [], function() {
            return e
        }) : window.oPhoneCheck = e
    }(),
    yp.ready(function() {
        var e = window.oPageConfig;
        e.oUrl,
            e.oLoginInfo,
            yp.ui,
            yp.noop;
        ({
            create: function() {
                var t = this;
                !window.noneGoTop && yp.ui.$win.goTop(),
                    function() {
                        var e = getQueryString("bf_ref_url")
                            , t = $.cookie("bf_ref_url");
                        if (e && !t) {
                            var n = new Date;
                            n.setTime(n.getTime() + 36e5),
                                $.cookie("bf_ref_url", e, {
                                    expires: n,
                                    path: "/"
                                })
                        }
                    }(),
                    yp.sub("page/logout/done", function() {
                        yp.pub("page/cookie/oLoginInfo", {
                            cookie_value: 1
                        }),
                            $.removeCookie("oViewer"),
                        window.bUserCenter && (window.location.href = e.oUrl.sUrlHost)
                    }),
                    yp.sub("page/login/done", function(e, t) {
                        yp.pub("page/cookie/oLoginInfo", {
                            cookie_value: t
                        }),
                            $.removeCookie("oViewer"),
                            yp.pub("page/sgamer/active")
                    }),
                    yp.sub("page/regist/done", function(e, t) {
                        yp.pub("page/cookie/oLoginInfo", {
                            cookie_value: t
                        }),
                            $.removeCookie("oViewer")
                    }),
                    yp.sub("page/site/level/update", function(t, n) {
                        e.oLoginInfo && e.oLoginInfo.uid == n.uid && (e.oLoginInfo.slevel = n,
                            yp.pub("page/site/level/update/next"))
                    });
                var n = null;
                yp.sub("page/site/phone/check", function(t, i) {
                    if (!e.oLoginInfo)
                        return !1;
                    var o, a, r = e.oLoginInfo.bindMobile, s = r ? r.split("-") : [];
                    if (!s.length)
                        return !1;
                    o = s[0],
                        a = s[1],
                        a ? -1 == o.indexOf("+") && (o = "+" + o) : (a = o,
                            o = "+86"),
                        n ? n.init(a, o, i.bCountrySelect, i.bPhoneInput, i.submitUrl) : yp.use("phoneCheck", function(e, t) {
                            n = t,
                                n.init(a, o, i.bCountrySelect, i.bPhoneInput, i.submitUrl)
                        }, !0)
                }),
                    yp.sub("page/geetest/load", function(e) {
                        if (window.Geetest)
                            return !1;
                        s = document.createElement("script"),
                            s.src = "//api.geetest.com/get.php?callback=geetestLoaded",
                            $("head")[0].appendChild(s)
                    }),
                    window.geetestLoaded = function() {
                        yp.pub("page/geetest/loaded")
                    }
                    ,
                    yp.sub("page/follow/error", function(e, t) {
                        t.message && yp.errorNotify(t.message)
                    }),
                    yp.sub("page/v/regist", function(e, n) {
                        t.vHash[n.uid] = n
                    }),
                    $("body").on("mouseenter", ".js-v-icon", function(e) {
                        t.fShowVPanel($(this))
                    }).on("mouseleave", ".js-v-icon", function(e) {
                        t.fHideV()
                    }),
                    function() {
                        yp.sub("page/follow", function(t, n) {
                            var i = n.flag
                                , o = n.uid
                                , a = yp.format(e.oUrl.sUrlFollow, {
                                type: i ? "" : ".unfollow"
                            })
                                , r = n.callback || null;
                            yp.ajax(a, {
                                type: "post",
                                data: {
                                    uid: o
                                }
                            }).done(function(e) {
                                e.data;
                                e.code ? yp.pub("page/follow/error", {
                                    uid: o,
                                    flag: i,
                                    follow: e.data.follow,
                                    message: e.message
                                }) : yp.pub("page/follow/done", {
                                    uid: o,
                                    flag: i,
                                    follow: e.data.follow
                                }),
                                r && r(e)
                            })
                        })
                    }(),
                    function() {
                        yp.sub("page/watchrecord/add", function(t, n) {
                            2 == n.type && yp.ajax(yp.format(e.oUrl.sWatchRecord, {
                                id: n.id,
                                type: n.type
                            }))
                        })
                    }(),
                    $(".zq-tool-download").on("click", function(e) {
                        e.preventDefault();
                        var t = $(this).attr("href");
                        confirm("您正在下载战旗主播工具，是否立即下载？", "", "", {
                            btns: [{
                                name: "是",
                                callback: function() {
                                    this.close(),
                                        window.open(t)
                                }
                            }, {
                                name: "否"
                            }]
                        })
                    }),
                    fLoadKefu(),
                    $("body").on("click", ".js-open-kefu", function(e) {
                        e.preventDefault(),
                            fOpenKefu()
                    }),
                    yp.module.fCallFun("oRich", "fGetRich")
            },
            ajax: function(e, t) {
                return yp.ajax(e, t).done(function(e) {})
            },
            vHash: {},
            $vContainer: null,
            fAppendVContainer: function() {
                var e = this
                    , t = "";
                t = '<div class="site-v-panel" style="display:none;">                <div class="img-box">                  <img class="js-avatar" src="" alt="">                </div>                <div class="v-info">                  <span class="v-nickname js-nickname"></span><i class="site-v-icon js-v-type"></i>                  <span class="v-desc js-v-desc"></span>                </div>              </div>',
                    e.$vContainer = $(t),
                    $("body").append(e.$vContainer)
            },
            vPanelBeHover: !1,
            fShowVPanel: function(e) {
                var t = this
                    , n = e.data("uid");
                t.vPanelBeHover = !0,
                    t.vHash[n] ? t.fDescAppend(t.vHash[n], e) : t.fLoadVdesc(n, e)
            },
            fDescAppend: function(e, t) {
                var n = this;
                n.$vContainer || n.fAppendVContainer();
                var i = t.offset();
                i.top = i.top - 105,
                    i.left = i.left - 96,
                $(window).width() < i.left && (i.left = $(window).width() - 100),
                0 > i.top && (i.top = i.top + 104 + 21),
                    n.$vContainer.show().css(i),
                    n.$vContainer.find(".js-nickname").html(e.nickname),
                    n.$vContainer.find(".js-avatar").attr("src", e.avatar + "-big"),
                    n.$vContainer.find(".js-v-desc").html(e.vdesc),
                n.$vContainer.find(".js-v-type").data("old") && n.$vContainer.find(".js-v-type").removeClass(n.$vContainer.find(".js-v-type").data("old")),
                    n.$vContainer.find(".js-v-type").addClass("site-v-icon-" + e.vlevel).data("old", "site-v-icon-" + e.vlevel)
            },
            fLoadVdesc: function(t, n) {
                var i = this;
                i.$vContainer && i.$vContainer.hide(),
                    yp.ajax(yp.format(e.oUrl.sLoadVdescUrl, {
                        uid: t
                    }), {
                        type: "get",
                        dataType: "json"
                    }).done(function(e) {
                        0 == e.code ? (i.vHash[e.data.uid] = e.data,
                        i.vPanelBeHover && i.fDescAppend(e.data, n)) : i.fHideV()
                    })
            },
            fHideV: function() {
                var e = this;
                e.vPanelBeHover = !1,
                e.$vContainer && e.$vContainer.hide()
            }
        }).create()
    }),
    function(e) {
        "use strict";
        var t = window.oPageConfig
            , n = (t.oUrl,
            {
                getUname: function() {
                    return yp.localStorage.getItem("login-uname") || ""
                },
                setUname: function(e) {
                    yp.localStorage.setItem("login-uname", e)
                }
            });
        yp.login = n
    }(jQuery),
    yp.ready(function() {
        ({
            init: function() {
                this.view(),
                    this.listen()
            },
            view: function() {},
            listen: function() {
                var e = this;
                yp.sub("loginCallback", function(t, n) {
                    e.createIframe(n.code)
                })
            },
            createIframe: function(e) {
                if ("20001" == e)
                    return void (window.pageType && "login" == window.pageType && "register" == window.pageType || (window.location.href = window.location.href));
                yp.login.hideModal(),
                    yp.regist.hideModal();
                var t = document.createElement("iframe");
                t.src = "http://" + window.location.host + "/api/auth/partner/bindpage?code=" + e,
                    t.id = "bandWin",
                    t.style.position = "absolute",
                    t.style.top = "50%",
                    t.style.left = "50%",
                    t.style.marginLeft = "-300px",
                    t.style.marginTop = "-200px",
                    t.style.width = "400px",
                    t.style.height = "289px",
                    t.style.zIndex = "9999",
                    document.body.appendChild(t),
                    $(t).removeClass("hidden")
            }
        }).init()
    }),
    function(e, t, n) {
        var i = yp.ui
            , o = {
            $element: null,
            init: function(e) {
                this.$element = e,
                    this.view(),
                    this.bindEvent()
            },
            view: function() {
                var t = oPageConfig.oUparea && oPageConfig.oUparea.hideLive ? "display:none;" : ""
                    , n = oPageConfig.oUparea && oPageConfig.oUparea.hideApp ? "display:none;" : ""
                    , i = '<div class="up-area js-up-area">                    <div class="hd"></div>                    <div class="bd">                      <a href="' + oPageConfig.oUrl.sAnchorApplyUrl + '" target="_blank" class="my-live" style="' + t + '"></a>                      <a href="' + oPageConfig.oUrl.sAppDownUrl + '" target="_blank" style="" class="mobile-app png"  style="' + n + '" onclick="mfTongji(\'uparea_app_download_click\')">                        <i></i><div class="bg"></div>                      </a>                      <a href="javascript:;" class="feedback-icon js-open-kefu" ></a>                      <a href="javascript:;" class="up-icon up-iconz hidden" ></a>                    </div>                    <div class="ft"></div>                  </div>';
                e("body").append(i)
            },
            bindEvent: function() {
                function t() {
                    var e = n.$element.scrollTop();
                    if (i.$win.height() + o - yp.global.height < e) {
                        if (!i.$goTop.hasClass("hidden"))
                            return;
                        i.$goTop.removeClass("hidden")
                    } else {
                        if (!i.$goTop.is(":visible"))
                            return;
                        i.$goTop.addClass("hidden")
                    }
                }
                var n = this
                    , o = 0;
                i.$goTop = e(".up-iconz"),
                    i.$up_area = e(".js-up-area"),
                    n.$element.scroll(function() {
                        t()
                    }),
                    t(),
                    i.$goTop.click(function() {
                        n.$element.scrollTop(o),
                            i.$goTop.addClass("hidden")
                    })
            },
            fShowAppQRCode: function() {
                if (t.pageType && "index" == t.pageType) {
                    var n = e('<div class="bg-1"><span class="close js-close" onclick="mfTongji(\'for_app_daoliu_close\', \'uparea\')"></span></div>');
                    i.$up_area.find(".mobile-app").addClass("active").append(n),
                        i.$up_area.find(".mobile-app .bg").addClass("hidden"),
                        n.on("click", ".js-close", function(e) {
                            e.preventDefault(),
                                n.remove(),
                                i.$up_area.find(".mobile-app .bg").removeClass("hidden"),
                                i.$up_area.find(".mobile-app").removeClass("active")
                        })
                }
            }
        };
        e.fn.goTop = function() {
            o.init(e(this))
        }
    }(jQuery, window),
    yp.ready(function() {
        yp.sub("page/alert/bindphone", function(e, t) {
            var n = t && t.action ? t.action : "领取";
            $.ibox({
                title: "提示",
                width: 300,
                content: "您需要绑定手机后才可" + n + "，立即去绑定吧！",
                btns: [{
                    name: "去绑定",
                    callback: function() {
                        window.open("/user/bind/mobile")
                    }
                }]
            })
        })
    }),
    function(e, t) {
        t.oHistorySearch = {
            fAddHistorySearch: function(t) {
                var n = this
                    , i = n.fHistorySearchKey()
                    , o = n.fGetHistorySearch(i);
                if (!t)
                    return !1;
                t = "" + t;
                var a = e.inArray(t, o);
                -1 < a ? o.splice(a, 1) : 10 <= o.length && o.pop(),
                    o.unshift(t),
                    n.fSetHistorySearch(o, i)
            },
            fClearHistorySearch: function(e) {
                var t = this
                    , n = t.fHistorySearchKey()
                    , i = t.fGetHistorySearch(n);
                void 0 !== e ? i.splice(e, 1) : i = [],
                    t.fSetHistorySearch(i.length ? i : "", n)
            },
            fSetHistorySearch: function(e, n) {
                var i = this
                    , n = n || i.fHistorySearchKey()
                    , o = e && 0 < e.length ? JSON.stringify(e) : "";
                o ? t.localStorage.setItem(n, o) : t.localStorage.removeItem(n),
                    t.pub("page/searchhistory/change")
            },
            fGetHistorySearch: function(e) {
                var n = this
                    , e = e || n.fHistorySearchKey()
                    , i = t.localStorage.getItem(e) || "";
                return i ? JSON.parse(i) : []
            },
            fHistorySearchKey: function() {
                return "guest"
            }
        }
    }(jQuery, yp),
    define("j/web4.0/zhanqi", function() {}),
    yp.use("j/yp/lib/require-jquery-min, j/yp/yp-mods-config, j/yp/yp-base, j/web4.0/zhanqi", "", !0),
    define("j/pkg/zhanqi", function() {});
(function(win) {
    win.modsHash = {
        "yp-objList": "9ede79e9",
        "yp-socket": "0a66fd62",
        "yp-socket-avatar": "0284f23b",
        "yp-chat": "3b1c34b0",
        "mvvm": "c4a80a8e",
        "ibox": "1c854ecb",
        "flashobject": "97ec4d1d",
        "json": "5b078431",
        "bootstrap": "2d088974",
        "tabs": "89b3d28d",
        "scroll": "08a99c32",
        "lazyload": "89c45121",
        "mousewheel": "7bb4ab2f",
        "fileupload": "e71c324a",
        "colpick": "80dc5a79",
        "editor": "9b59cbbe",
        "ZeroClipboard": "5f52d5ef",
        "xss": "25985a69",
        "inotify": "5eeb7402",
        "pagination": "c1cfe15e",
        "pagination2": "b2429322",
        "titleTips": "7b224279",
        "WdatePicker": "76207359",
        "qrcode": "a989bf5a",
        "lottery": "82e7ad0b",
        "jCopy": "52818dc9",
        "videoHover": "7cd8c430",
        "carousel": "55c094ca",
        "rich": "22712308",
        "pca": "44605141",
        "pcaData": "1bb7b7e9",
        "highcharts": "239284d6",
        "rotate": "c9215f80",
        "carousel2": "fae9c5af",
        "state-machine": "89f2e78f",
        "stardesc": "02e44979",
        "newsroll": "2464afbb",
        "velocity.min": "195ea0fc",
        "snowfall.jquery": "59168368",
        "jquery.vote": "0164383c",
        "rechargeData": "19dd01c0",
        "zqSelect": "87c9ac2d",
        "zqSearch": "91877beb",
        "phoneCode": "e18c9657",
        "phoneCheck": "9be9d9a0",
        "keepCard": "34151705",
        "buyFire": "c48b6dbc",
        "logList": "a75dd70b",
        "videoList": "8af95833",
        "paginationList": "61300866",
        "jquery.pagetransitions": "1f466d08",
        "buyCar": "423723f4",
        "buyDefend": "52c3396f",
        "buyProp": "a0a6211f",
        "jquery.gameSelect": "2ad0fddc",
        "geetest": "0d41ea07",
        "checkMobile": "b85b9435",
        "createjs": "54e1c372",
        "propList": "918c1418",
        "loginModule": "25d93bff",
        "registModule": "6a89f13e",
        "popRecharge": "3b7005cb",
        "adminpanel": "ed617f96",
        "base": "e2196be0",
        "defendNew": "d6449d0d",
        "forAnchor": "9b9f6672",
        "gift": "dbc244ad",
        "firework": "aa187b1f",
        "welfare": "21282745",
        "addSpeed": "b0d3ba51",
        "googleNew": "a7707e00",
        "guess": "8f7c868a",
        "anchorLottery": "1ac1caaf",
        "anchorTask": "01ea3ecd",
        "danmuSetting": "39b127da",
        "main": "f2064db6",
        "planeTicketNew": "533f35f5",
        "task": "e43ebe55",
        "taskDataManage": "ff289227",
        "taskPanel": "20524750",
        "gameTaskGroup": "34dbaef5",
        "taskGroup": "463c9da8",
        "activationCode": "8f3b358d",
        "translateNew": "c0597315",
        "tianshuNew": "15288081",
        "guide": "468efb56",
        "hongbaoNew": "4025d7f6",
        "popRechargeNew": "ce2af826",
        "roomPackage": "defdc0a7",
        "keyWordDropping": "5753d24f",
        "lotterySlot": "9379887a",
        "slotmachine": "2cbd54fd",
        "lotteryTopic": "ae308668",
        "oRoomMedal": "7b57f9f8",
        "roomBannerCamp": "8ed18418",
        "specialEffects": "00221c28",
        "roomReport": "cd048f07",
        "beanRank": "890e52d2",
        "packageEnter": "d8217873",
        "chargeEnter": "61f2278b",
        "luckyBag": "f169127b",
        "activeCenter": "930cb996",
        "videoPlayer": "fc61de5c",
        "videoJS": "ddaa4baf",
        "videoJSForHLS": "64e77cfa",
        "dfp": "b570c014",
        "timegift": "42794e23",
        "frameAnimation": "e5a7b66a",
        "jquery.fullPage": "587b3a8e",
        "wsdk": "0c9ee84e",
        "im": "78c45d7c",
        "simpleIm": "80b8be32",
        "fullIm": "e11b8755",
        "roomIm": "83385464",
        "friendManage": "4a2b5268",
        "imSetting": "241bd7f5",
        "imBobao": "be134ad6",
        "imZan": "b3a9d3c7",
        "imComment": "4a941576",
        "drag": "eb90a111",
        "geetest3": "220c8acb",
        "verification": "e884621d",
        "localStorageExpire": "e3d97776",
        "audioPlayer": "91c51dbe",
        "loginRegistModule": "a9ad6017",
        "Base64": "a9e14c80",
        "defendEffect": "ea7fd69e",
        "car2017": "a4af7436",
        "jquery.carousel3": "f3febc5e",
        "carPanel": "5a58c608",
        "waterfall": "32d80efc",
        "makeQrcode": "517b55d3",
        "dbqb": "719cff51",
        "pushNew": "35f9ec6f",
        "oMedalPanel": "1137cb6b",
        "guardPanel": "a03f41c0",
        "propPanel2017": "f54d31b5",
        "chargePanel": "6233afef",
        "zqCharge": "7dc106fa",
        "dexie": "41288ebd",
        "cropper": "1530c02e",
        "chatTextarea": "4248426c",
        "businessCard": "3c068c01",
        "bodyMovin": "9580c3ff",
        "md5": "269ca85b",
        "emotData": "5b4d0b0d",
        "anchorRelay": "589d5200",
        "eatchicken": "7e2161ec",
        "callVideo": "d49a35aa",
        "activeGuess": "104df96f",
        "appgameSlide": "582cc9ee",
        "zqShare": "747dcc29",
        "videoComment": "6ed3a065",
        "videoRecommend": "9084655a",
        "videoSendComment": "80546da3",
        "vipLevel": "b21eb56a",
        "matchGuess": "88e4d62d",
        "xplay": "b623ea6d",
        "userForbidden": "fe25cbbc",
        "apng": "6556ab13",
        "roomRecommend": "ce5f7548",
        "vue": "3fe1abe2",
        "bootstrapNew": "5869c96c",
        "moment": "91fef1ef",
        "flatpickr": "ad03606f",
        "playerMaskProp": "624c3be8",
        "glDatePicker": "7d69958f",
        "roomIntoGif": "2844afcd",
        "prayPanel": "b74e06bb"
    }
}(window));
