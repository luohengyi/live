+function() {
    var e = {
        oSdk: null,
        bLogined: !1,
        bListening: !1,
        appkey: "23764198",
        sUrlBase: "/api/im/v2.1",
        userid: "",
        password: "",
        bKick: !1,
        specialIMUid: {
            system: "系统消息",
            admin: "f6ua65"
        },
        specialUid: {
            admin: 111369817
        },
        init: function() {
            var e = this;
            yp.module.fRegistModule("zqIM", e),
                this.fListenYpEvent(),
                e.fLogStore("success", "IM module loaded")
        },
        fListenYpEvent: function() {
            var e = this;
            yp.sub("page/zqim/login", function() {
                e.fGetLoginCredential().done(function(t) {
                    e.fIMLogin(t)
                })
            }),
                yp.sub("page/zqim/relogin", function() {
                    e.fIMReLogin()
                }),
                yp.sub("page/zqim/kick", function() {
                    e.bListening && e.fStopListenAllMsg(),
                        e.bKick = !0,
                        e.bLogined = !1
                }),
                yp.sub("page/document/vidible", function() {
                    "visible" == yp.documentVisible && e.bKick && yp.pub("page/zqim/relogin")
                }),
                yp.sub("page/zqim/ready", function() {
                    e.fLogStore("success", "IM 准备完成")
                })
        },
        fNewWSDK: function() {
            var e = this;
            !e.oSdk && (e.oSdk = new WSDK),
                e.fLogStore("success", "IM 初始化WSDK")
        },
        fIMLogin: function(e) {
            var t = this;
            if (e && (t.userid = e.userid,
                t.password = e.password),
            !t.userid || !t.password)
                return !1;
            t.fLogStore("start", "IM 开始登录"),
                t.oSdk.Base.login({
                    uid: t.userid,
                    appkey: t.appkey,
                    credential: t.password,
                    timeout: 4e3,
                    success: function(e) {
                        t.bLogined = !0,
                            t.fPubGet("login", e),
                            console.log("login success", e)
                    },
                    error: function(e) {
                        t.fPubError({
                            type: "login",
                            reason: "error",
                            data: e
                        }),
                            console.log("login fail", e)
                    }
                })
        },
        beAtRelogin: !1,
        fIMReLogin: function() {
            var e = this;
            return !(!window.imType || "full" != window.imType) && (!(!e.userid || !e.password || e.bLogined) && (e.beAtRelogin = !0,
                e.fLogStore("start", "IM 开始重连"),
                void e.oSdk.Base.login({
                    uid: e.userid,
                    appkey: e.appkey,
                    credential: e.password,
                    timeout: 4e3,
                    success: function(t) {
                        e.beAtRelogin = !1,
                            e.bLogined = !0,
                            e.bKick = !1,
                            e.fPubGet("relogin", t),
                            console.log("relogin success", t),
                            e.fStartListenAllMsg(),
                            e.fMapMethodQuery()
                    },
                    error: function(t) {
                        e.beAtRelogin = !1,
                            e.fPubError({
                                type: "relogin",
                                reason: "error",
                                data: t
                            }),
                            console.log("login fail", t)
                    }
                })))
        },
        fIMLogout: function() {
            var e = this;
            if (!e.bLogined)
                return e.fPubError({
                    type: "logout",
                    reason: "guest"
                }),
                    !1;
            e.bListening && e.fStopListenAllMsg(),
                e.oSdk.Base.logout({
                    success: function(t) {
                        e.bLogined = !1,
                            e.fPubGet("logout", t),
                            console.log("logout success", t)
                    },
                    error: function(t) {
                        e.fPubError({
                            type: "logout",
                            reason: "error",
                            data: t
                        }),
                            console.log("logout fail", t)
                    }
                })
        },
        fGetLoginCredential: function() {
            var e = this
                , t = $.Deferred();
            return e.fLogStore("start", "IM 请求登录数据"),
                oPageConfig.oLoginInfo ? yp.ajax(e.sUrlBase + "/user/info", {
                    noCache: !0
                }).done(function(o) {
                    e.fLogStore("success", "IM 请求登录数据 成功"),
                        0 == o.code ? t.resolve(o.data) : t.reject()
                }).fail(function(o) {
                    e.fLogStore("error", "IM 请求登录数据失败 失败: [" + o.readyState + ", " + o.responseText + ", " + o.status + "]"),
                        t.reject()
                }) : t.reject(),
                t
        },
        fGetUnread: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "unread",
                    reason: "guest"
                }),
                    !1;
            t.fLogStore("start", "IM 开始获取未读消息"),
                t.oSdk.Base.getUnreadMsgCount({
                    count: e.count,
                    success: function(e) {
                        t.fPubGet("unread", e),
                            console.log("get unread msg count success", e)
                    },
                    error: function(e) {
                        t.fPubError({
                            type: "unread",
                            reason: "error",
                            data: e
                        }),
                            console.log("get unread msg count fail", e)
                    }
                })
        },
        fGetCurContacts: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "curcontacts",
                    reason: "guest"
                }),
                    !1;
            e = e || {},
                t.fLogStore("start", "IM 开始获取最近联系人"),
                t.oSdk.Base.getRecentContact({
                    count: e.count || 10,
                    success: function(e) {
                        if (e.data && e.data.cnts && e.data.cnts.length > 0) {
                            for (var o = [], r = 0; r < e.data.cnts.length; r++)
                                "0ur5yk" != t.fGetNick(e.data.cnts[r].uid) && o.push(e.data.cnts[r]);
                            e.data.cnts = o
                        }
                        t.fPubGet("curcontacts", e),
                            console.log("get recent contact success", e)
                    },
                    error: function(e) {
                        t.fPubError({
                            type: "curcontacts",
                            reason: "error",
                            data: e
                        }),
                            console.log("get recent contact success", e)
                    }
                })
        },
        fStartListenAllMsg: function() {
            var e = this;
            if (!e.bLogined)
                return e.fPubError({
                    type: "listenall",
                    reason: "guest"
                }),
                    !1;
            console.log("开始监听"),
                e.oSdk.Event.on("CHAT.MSG_RECEIVED", function(t) {
                    e.fPubGet("listenall", t),
                        console.log("我能收到成功的单聊消息", t)
                }),
                e.oSdk.Event.on("KICK_OFF", function(e) {
                    console.log("啊，我被踢了", e),
                        yp.pub("page/zqim/kick")
                }),
                e.bListening = !0,
                e.oSdk.Base.startListenAllMsg()
        },
        fStopListenAllMsg: function() {
            var e = this;
            if (!e.bLogined)
                return e.fPubError({
                    type: "listenallstop",
                    reason: "guest"
                }),
                    !1;
            console.log("取消监听"),
                e.oSdk.Event.off("CHAT.MSG_RECEIVED"),
                e.oSdk.Event.off("KICK_OFF"),
                e.bListening = !1,
                e.oSdk.Base.stopListenAllMsg()
        },
        fGetNick: function(e) {
            return this.oSdk.Base.getNick(e)
        },
        fChatSetReadState: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "setreadstate",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Chat.setReadState({
                touid: "" + e.touid,
                timestamp: e.timestamp || Math.floor(+new Date / 1e3),
                hasPrefix: e.hasPrefix || !1,
                success: function(e) {
                    t.fPubGet("setreadstatedone", e),
                        console.log("set read state success", e)
                },
                error: function(e) {
                    t.fPubError({
                        type: "setreadstate",
                        reason: "error",
                        data: e
                    }),
                        console.log("set read state fail", e)
                }
            })
        },
        fChatGetHistory: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "chatgethistory",
                    reason: "guest"
                }),
                    !1;
            var o = "" + e.touid;
            t.fLogStore("start", "IM 开始获取历史消息"),
                t.oSdk.Chat.getHistory({
                    touid: o,
                    hasPrefix: e.hasPrefix || !1,
                    nextkey: e.nextkey,
                    count: e.count || 10,
                    success: function(e) {
                        e.data.touid2 = o,
                            t.fPubGet("chatgethistory", e),
                            console.log("get history msg success", e)
                    },
                    error: function(e) {
                        t.fPubError({
                            type: "chatgethistory",
                            reason: "error",
                            data: e
                        }),
                            console.log("get history msg fail", e)
                    }
                })
        },
        fChatStartListenMsg: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "chatlistenall",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Event.on("CHAT_START_RECEIVE_MSG", function(e) {
                t.fPubGet("chatlistenall", e),
                    console.log("单发，接收到所有消息，无论成功失败", e)
            }),
                t.oSdk.Chat.startListenMsg({
                    touid: "" + e.touid
                })
        },
        fChatStopListenMsg: function() {
            var e = this;
            if (!e.bLogined)
                return e.fPubError({
                    type: "chatlistenallstop",
                    reason: "guest"
                }),
                    !1;
            e.oSdk.Event.off("CHAT_START_RECEIVE_MSG"),
                e.oSdk.Base.stopListenMsg()
        },
        fChatGetUserStatus: function(e, t) {
            var o = this;
            if (!o.bLogined)
                return o.fPubError({
                    type: "getuserstatus",
                    reason: "guest"
                }),
                    !1;
            o.oSdk.Chat.getUserStatus({
                uids: e,
                hasPrefix: t,
                appkey: o.appkey,
                success: function(e) {
                    o.fPubGet("getuserstatus", e)
                },
                error: function(e) {
                    o.fPubError({
                        type: "getuserstatus",
                        reason: "error",
                        data: e
                    }),
                        console.log("getUserStatus fail")
                }
            })
        },
        fTribeGetHistory: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "tribegethistory",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Tribe.getHistory({
                tid: e.tid,
                count: e.count || 10,
                nextkey: e.nextkey,
                success: function(e) {
                    t.fPubGet("tribegethistory", e),
                        console.log("get history msg success", e)
                },
                error: function(e) {
                    t.fPubError({
                        type: "tribegethistory",
                        reason: "error",
                        data: e
                    }),
                        console.log("get history msg fail", e)
                }
            })
        },
        fTribeGetTribeInfo: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "gettribeinfo",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Tribe.getTribeInfo({
                tid: e.tid,
                success: function(e) {
                    t.fPubGet("gettribeinfo", e),
                        console.log(e)
                },
                error: function(e) {
                    t.fPubError({
                        type: "gettribeinfo",
                        reason: "error",
                        data: e
                    }),
                        console.log(e)
                }
            })
        },
        fTribeGetTribeList: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "gettribelist",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Tribe.getTribeList({
                tribeTypes: e.types,
                success: function(e) {
                    t.fPubGet("gettribelist", e),
                        console.log(e)
                },
                error: function(e) {
                    t.fPubError({
                        type: "gettribelist",
                        reason: "error",
                        data: e
                    }),
                        console.log(e)
                }
            })
        },
        fTribeGetTribeMembers: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "gettribemembers",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Tribe.getTribeMembers({
                tid: e.tid,
                success: function(e) {
                    t.fPubGet("gettribemembers", e),
                        console.log(e)
                },
                error: function(e) {
                    t.fPubError({
                        type: "gettribemembers",
                        reason: "error",
                        data: e
                    }),
                        console.log(e)
                }
            })
        },
        fTribeResponseInviteIntoTribe: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "responseinvitetribe",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Tribe.responseInviteIntoTribe({
                tid: e.tid,
                recommender: "",
                validatecode: "",
                manager: "",
                success: function(e) {
                    t.fPubGet("responseinvitetribe", e),
                        console.log(e)
                },
                error: function(e) {
                    t.fPubError({
                        type: "responseinvitetribe",
                        reason: "error",
                        data: e
                    }),
                        console.log(e)
                }
            })
        },
        fImgUpload: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "imgupload",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Plugin.Image.upload({
                target: e.input,
                timeout: 2e4,
                success: function(e) {
                    t.fPubGet("imgupload", e),
                        console.log(e)
                },
                error: function(e) {}
            })
        },
        fImgUploadBase64: function(e) {
            var t = this;
            if (!t.bLogined)
                return t.fPubError({
                    type: "imguploadbase64",
                    reason: "guest"
                }),
                    !1;
            t.oSdk.Plugin.Image.upload({
                base64Img: e.base64Img,
                ext: e.ext,
                timeout: 2e4,
                success: function(e) {
                    t.fPubGet("imguploadbase64", e),
                        console.log(e)
                },
                error: function(e) {}
            })
        },
        fGetBBAndIAUnRead: function() {
            return yp.ajax("/api/user/im/unread", {
                noCache: !0,
                data: {
                    platform: 1
                }
            })
        },
        fGetMultiRelationList: function() {
            var e = this;
            return e.fLogStore("start", "IM 请求/user/relation"),
                yp.ajax(e.sUrlBase + "/user/relation", {
                    noCache: !0
                }).done(function(t) {
                    0 == t.code && (t.data.fuids = t.data.fuids || [],
                        t.data.fuids.push(e.specialIMUid.admin)),
                        e.fLogStore("success", "IM 请求/user/relation 成功")
                }).fail(function(t) {
                    e.fLogStore("error", "IM 请求/user/relation 失败: [" + t.readyState + ", " + t.responseText + ", " + t.status + "]")
                })
        },
        fGetUserSetting: function() {
            var e = this;
            return yp.ajax(e.sUrlBase + "", {
                noCache: !0
            })
        },
        fGetContactSaved: function() {
            var e = this;
            return e.fLogStore("start", "IM 请求/sessions/list"),
                yp.ajax(e.sUrlBase + "/sessions/list", {
                    noCache: !0
                }).done(function(t) {
                    e.fLogStore("success", "IM 请求/sessions/list 成功")
                }).fail(function(t) {
                    e.fLogStore("error", "IM 请求/sessions/list 失败: [" + t.readyState + ", " + t.responseText + ", " + t.status + "]")
                })
        },
        oUidToImuidHash: {},
        fGetNewContactInfo: function(e) {
            var t = this
                , o = $.Deferred();
            return t.oUidToImuidHash["" + e] ? o.resolve(t.oUidToImuidHash["" + e]) : yp.ajax(t.sUrlBase + "/user/get?uid=" + e, {
                noCache: !0
            }).done(function(r) {
                0 == r.code ? (t.oUidToImuidHash["" + e] = r,
                    o.resolve(r)) : o.reject()
            }).fail(function() {
                o.reject()
            }),
                o
        },
        fSubmitMsg: function(e, t) {
            var o = this;
            if (!t && o.bKick) {
                o.fAddMethodQuery("fSubmitMsg", e);
                var r = $.Deferred();
                return r.resolve({
                    code: 0
                }),
                    r
            }
            return yp.ajax(o.sUrlBase + "/msg/custom", {
                type: "post",
                data: e,
                dataType: "json"
            })
        },
        fContactDel: function(e) {
            var t = this;
            return yp.ajax(t.sUrlBase + "/sessions/list", {
                type: "post",
                data: e,
                dataType: "json"
            })
        },
        fGetBlocksList: function() {
            var e = this;
            return yp.ajax(e.sUrlBase + "/user/blacks", {
                noCache: !0
            })
        },
        fSetBlock: function(e) {
            var t = this;
            return yp.ajax(t.sUrlBase + "/user/blacks", {
                type: "post",
                data: e,
                dataType: "json"
            })
        },
        fGetFollowList: function() {
            var e = this;
            return yp.ajax(e.sUrlBase + "/user/follows", {
                noCache: !0
            })
        },
        fSetFollow: function(e) {
            var t = this;
            return yp.ajax(t.sUrlBase + "/user/follows", {
                type: "post",
                data: e,
                dataType: "json"
            })
        },
        fGetFriendList: function() {
            var e = this;
            return yp.ajax(e.sUrlBase + "/user/friends", {
                noCache: !0
            })
        },
        fCheckRelation: function(e) {
            var t = this;
            return yp.ajax(t.sUrlBase + "/user/follows?userid=" + e, {
                noCache: !0
            })
        },
        fPubError: function(e) {
            this.fLogStore("error", "IM 失败回调: " + JSON.stringify(e)),
                yp.pub("page/zqim/error", e)
        },
        fPubGet: function(e, t) {
            var o = this;
            1e3 == t.code ? (o.fLogStore("success", "IM 成功回调: " + e),
                yp.pub("page/zqim/get", {
                    type: e,
                    data: t.data
                })) : o.fPubError({
                type: e,
                reason: "fail",
                data: t
            })
        },
        aMethodQuery: [],
        fAddMethodQuery: function(e, t) {
            var o = this;
            o.aMethodQuery.push({
                method: e,
                data: t
            }),
            1 != o.aMethodQuery.length || yp.documentVisible && "visible" != yp.documentVisible || !o.bKick || o.beAtRelogin || yp.pub("page/zqim/relogin")
        },
        fMapMethodQuery: function() {
            var e, t = this;
            t.aMethodQuery.length && (e = t.aMethodQuery.shift(),
                t[e.method](e.data, !0),
                t.fMapMethodQuery())
        },
        fLogStore: function(e, t) {},
        fGetSuperAdminContact: function() {
            var e = this
                , t = $.Deferred();
            return e.oUidToImuidHash[e.specialUid.admin] ? t.resolve(e.oUidToImuidHash[e.specialUid.admin]) : e.fGetNewContactInfo(e.specialUid.admin).done(function(o) {
                t.resolve(e.oUidToImuidHash[e.specialUid.admin])
            }),
                t
        },
        fDoSuperAdminSpecialAction: function(e, t) {
            var o, r = this, n = $.Deferred();
            if (t[r.specialIMUid.admin]) {
                var s;
                yp.each(e, function(e, t) {
                    if (e.uid == r.specialIMUid.admin)
                        return s = t,
                            !1
                }),
                    o = e.splice(s, 1),
                    e.unshift(o[0]),
                    n.resolve()
            } else
                r.fGetSuperAdminContact().done(function(n) {
                    0 == n.code && (o = {
                        uid: n.data.userid,
                        avatar: n.data.icon_url,
                        nickname: n.data.nick
                    },
                        o.uidWithPrefix = o.uid,
                        o.uid = o.uid,
                        o.unread = 0,
                        o.status = 1,
                        o.bBlocked = 0,
                        o.avatar = o.avatar,
                        e.unshift(o),
                        t[r.specialIMUid.admin] = o)
                }).always(function() {
                    n.resolve()
                });
            return n
        }
    };
    "undefined" != typeof module ? module.exports = e : "function" == typeof define && define.amd ? define([], function() {
        return e
    }) : window.zqIM = e
}();
