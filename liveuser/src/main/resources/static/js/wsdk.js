window.Messenger = function() {
    function e(e, t) {
        var i = "";
        if (arguments.length < 2 ? i = "target error - target and name are both required" : "object" != typeof e ? i = "target error - target itself must be window object" : "string" != typeof t && (i = "target error - target name must be string type"),
            i)
            throw new Error(i);
        this.target = e,
            this.name = t
    }
    function t(e, t) {
        this.targets = {},
            this.name = e,
            this.listenFunc = [],
            i = t || i,
        "string" != typeof i && (i = i.toString()),
            this.msgTester = new RegExp("^" + i),
            this.initListen()
    }
    var i = "[PROJECT_NAME]"
        , r = "postMessage"in window;
    return e.prototype.send = r ? function(e) {
            try {
                this.target.postMessage(i + e, "*")
            } catch (e) {
                if (window.__WSDK__POSTMESSAGE__DEBUG__)
                    if ("function" == typeof window.__WSDK__POSTMESSAGE__DEBUG__)
                        try {
                            window.__WSDK__POSTMESSAGE__DEBUG__.call(null, e)
                        } catch (e) {}
                    else
                        alert("对不起,当前浏览器不支持聊天,请更换浏览器")
            }
        }
        : function(e) {
            var t = window.navigator[i + this.name];
            if ("function" != typeof t)
                throw new Error("target callback function is not defined");
            t(i + e, window)
        }
        ,
        t.prototype.addTarget = function(t, i) {
            var r = new e(t,i);
            this.targets[i] = r
        }
        ,
        t.prototype.initListen = function() {
            var e = this
                , t = function(t) {
                if ("object" == typeof t && t.data && (t = t.data),
                    e.msgTester.test(t)) {
                    t = t.slice(i.length);
                    for (var r = 0; r < e.listenFunc.length; r++)
                        e.listenFunc[r](t)
                }
            };
            r ? "addEventListener"in document ? window.addEventListener("message", t, !1) : "attachEvent"in document && window.attachEvent("onmessage", t) : window.navigator[i + this.name] = t
        }
        ,
        t.prototype.listen = function(e) {
            this.listenFunc.push(e)
        }
        ,
        t.prototype.clear = function() {
            this.listenFunc = []
        }
        ,
        t.prototype.send = function(e) {
            var t, i = this.targets;
            for (t in i)
                i.hasOwnProperty(t) && i[t].send(e)
        }
        ,
        t
}(),
    function(e) {
        "use strict";
        var t, i = {}, r = window, n = r.document, o = "localStorage", a = "script";
        if (i.disabled = !1,
            i.version = "1.3.17",
            i.set = function(e, t) {}
            ,
            i.get = function(e, t) {}
            ,
            i.has = function(e) {
                return void 0 !== i.get(e)
            }
            ,
            i.remove = function(e) {}
            ,
            i.clear = function() {}
            ,
            i.transact = function(e, t, r) {
                null == r && (r = t,
                    t = null),
                null == t && (t = {});
                var n = i.get(e, t);
                r(n),
                    i.set(e, n)
            }
            ,
            i.getAll = function() {}
            ,
            i.forEach = function() {}
            ,
            i.serialize = function(e) {
                return JSON.stringify(e)
            }
            ,
            i.deserialize = function(e) {
                if ("string" == typeof e)
                    try {
                        return JSON.parse(e)
                    } catch (t) {
                        return e || void 0
                    }
            }
            ,
            function() {
                try {
                    return o in r && r[o]
                } catch (e) {
                    return !1
                }
            }())
            t = r[o],
                i.set = function(e, r) {
                    return void 0 === r ? i.remove(e) : (t.setItem(e, i.serialize(r)),
                        r)
                }
                ,
                i.get = function(e, r) {
                    var n = i.deserialize(t.getItem(e));
                    return void 0 === n ? r : n
                }
                ,
                i.remove = function(e) {
                    t.removeItem(e)
                }
                ,
                i.clear = function() {
                    t.clear()
                }
                ,
                i.getAll = function() {
                    var e = {};
                    return i.forEach(function(t, i) {
                        e[t] = i
                    }),
                        e
                }
                ,
                i.forEach = function(e) {
                    for (var r = 0; r < t.length; r++) {
                        var n = t.key(r);
                        e(n, i.get(n))
                    }
                }
            ;
        else if (n.documentElement.addBehavior) {
            var s, c;
            try {
                c = new ActiveXObject("htmlfile"),
                    c.open(),
                    c.write("<" + a + ">document.w=window</" + a + '><iframe src="/favicon.ico"></iframe>'),
                    c.close(),
                    s = c.w.frames[0].document,
                    t = s.createElement("div")
            } catch (e) {
                t = n.createElement("div"),
                    s = n.body
            }
            var l = function(e) {
                return function() {
                    var r = Array.prototype.slice.call(arguments, 0);
                    r.unshift(t),
                        s.appendChild(t),
                        t.addBehavior("#default#userData"),
                        t.load(o);
                    var n = e.apply(i, r);
                    return s.removeChild(t),
                        n
                }
            }
                , u = new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]","g")
                , d = function(e) {
                return e.replace(/^d/, "___$&").replace(u, "___")
            };
            i.set = l(function(e, t, r) {
                return t = d(t),
                    void 0 === r ? i.remove(t) : (e.setAttribute(t, i.serialize(r)),
                        e.save(o),
                        r)
            }),
                i.get = l(function(e, t, r) {
                    t = d(t);
                    var n = i.deserialize(e.getAttribute(t));
                    return void 0 === n ? r : n
                }),
                i.remove = l(function(e, t) {
                    t = d(t),
                        e.removeAttribute(t),
                        e.save(o)
                }),
                i.clear = l(function(e) {
                    var t = e.XMLDocument.documentElement.attributes;
                    for (e.load(o); t.length; )
                        e.removeAttribute(t[0].name);
                    e.save(o)
                }),
                i.getAll = function(e) {
                    var t = {};
                    return i.forEach(function(e, i) {
                        t[e] = i
                    }),
                        t
                }
                ,
                i.forEach = l(function(e, t) {
                    for (var r, n = e.XMLDocument.documentElement.attributes, o = 0; r = n[o]; ++o)
                        t(r.name, i.deserialize(e.getAttribute(r.name)))
                })
        }
        try {
            var m = "__storejs__";
            i.set(m, m),
            i.get(m) != m && (i.disabled = !0),
                i.remove(m)
        } catch (e) {
            i.disabled = !0
        }
        i.enabled = !i.disabled,
            e.store = i
    }(this),
    function(e) {
        var t = location.protocol + "//"
            , i = location.host;
        e.WSDKCons = {
            PROJECT_NAME: "WSDK",
            CUSTOM_PAGE: "CUSTOM_PAGE",
            PROXY_PAGE: "PROXY_PAGE",
            UPLOAD_PAGE: "UPLOAD_PAGE",
            ERROR_PROXY_PAGE: "ERROR_PAGE",
            PROXY_URL: "https://chat.etao.com/proxy.htm",
            LOGIN_URL: "https://chat.etao.com/login/oauth",
            TB_LOGIN_URL: "https://chat.im.taobao.com/taobaoLogin/taobao_oauth",
            GET_USER_STATUS_URL: "https://amos.alicdn.com/mullidstatus.aw",
            UPLOAD_URL: "https://mobileim.etao.com/api/file/uploadFile.json",
            UPLOAD_SUCCESS_PROXY: "https://chat.etao.com/uploadproxy.htm",
            GET_APP_INFO_URL: "http://tcms-openim.wangxin.taobao.com/getprefix",
            DEFAULT_LOGIN_TIMEOUT: 5e3,
            DEFAULT_UPLOAD_TIMEOUT: 3e4,
            CODE: {
                1e3: "SUCCESS",
                1001: "NOT_LOGIN",
                1002: "TIMEOUT",
                1003: "OTHER_ERROR",
                1004: "PARSE_ERROR",
                1005: "NET_ERROR",
                1006: "KICK_OFF",
                1007: "LOGIN_INFO_ERROR",
                1008: "ALREADY_LOGIN",
                1009: "NO_MESSAGE",
                1010: "PARAM_ERROR",
                1011: "LOGIN_TOO_QUICK_FROM_SERVER",
                1012: "LOGIN_ALREADY_FROM_SERVER",
                1013: "NOT_EXIST_USER",
                1014: "ERROR_PASSWORD",
                1015: "UIC ERROR"
            },
            MAX_IMAGE_SIZE: 5242880,
            PROXY_ACTION_API: {
                recvMsg: t + i + "/msg/msg_recv",
                sendMsg: t + i + "/msg/send",
                roamMsg: t + i + "/msg/roaming",
                listen: t + i + "/msg/msg_listen",
                unreadMsg: t + i + "/msg/readtimes",
                recentContact: t + i + "/msg/recent",
                recentContactNew: t + i + "/msg/recentExt",
                setReadState: t + i + "/msg/setReadState",
                setOpenimReadState: t + i + "/msg/setOpenimReadState",
                getRealCid: t + "amos.alicdn.com/getRealCid.aw",
                tribe: t + i + "/msg/tribe",
                getUserStatus: t + "amos.alicdn.com/mullidstatus.aw",
                uploadToken: t + i + "/msg/getToken",
                loginOut: t + i + "/msg/ImReqLogoff",
                getAnonymous: "https://chat.etao.com/openim/getanonymous"
            },
            UPLOAD_ERRORS: {
                "-1": "PARAM ERROR",
                "-2": "EMPTY FILE",
                "-3": "NOT SUPPORT",
                "-4": "SIZE LIMIT",
                "-5": "CAN NOT LOAD IMAGE",
                "-6": "TIME OUT",
                "-7": "UPLOAD FAIL",
                "-8": "GET TOKEN ERROR"
            }
        }
    }(this),
    function(e) {
        function t(e) {
            return Object.prototype.toString.call(e)
        }
        window.console && window.console.log || (window.console = {
            log: function() {}
        });
        var i = e.WSDKUtil || (e.WSDKUtil = {});
        Array.prototype.forEach || (Array.prototype.forEach = function(e) {
                if (!e)
                    return !1;
                for (var t = 0, i = this.length; i > t; t++)
                    e(this[t], t, this)
            }
        ),
        String.prototype.trim || (String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, "")
            }
        ),
            ["String", "Array", "Object", "Function", "Number", "Boolean", "Date", "RegExp", " Null", "Undefined", "NaN", "Arguments"].forEach(function(e) {
                i["is" + e] = function(i) {
                    return t(i) === "[object " + e + "]"
                }
            });
        var r = new RegExp("(http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?","gi")
            , n = {};
        i.Event = {
            on: function(e, t, i, r) {
                return "function" == typeof t ? (e = e.split(/\s+/),
                    e.forEach(function(e) {
                        (n[e] || (n[e] = [])).push({
                            callback: t,
                            context: i || this,
                            once: !!r
                        })
                    }),
                    this) : void 0
            },
            one: function(e, t, r) {
                return i.Event.on(e, t, r, 1)
            },
            off: function(e, t) {
                return e || t ? (e = e.split(/\s+/),
                    e.forEach(function(e) {
                        var i = n[e];
                        if (!i)
                            return this;
                        if (!t)
                            return delete n[e],
                                this;
                        for (var r = 0, o = i.length; o > r; r++)
                            if (t === i[r].callback) {
                                i.splice(r, 1);
                                break
                            }
                    }),
                    this) : (n = {},
                    this)
            },
            fire: function(e, t) {
                return arguments.length ? (e = e.split(/\s+/),
                    e.forEach(function(e) {
                        var r = n[e];
                        return r ? void r.forEach(function(r) {
                            r.once && i.Event.off(e, r.callback),
                                r.callback.call(r.context, t)
                        }) : this
                    }),
                    this) : this
            }
        },
            i.extend = function(e) {
                for (var t, i = e || {}, r = e ? 1 : 0, n = arguments.length; n > r; r++)
                    for (t in arguments[r])
                        if (arguments[r].hasOwnProperty(t)) {
                            var o = arguments[r][t];
                            "" !== o && null !== o && void 0 !== o && (i[t] = o)
                        }
                return i
            }
            ,
            i.checkParam = function(e, t) {
                var i = "param lost:"
                    , r = !1;
                if (e ? t.forEach(function(t) {
                    void 0 === e[t] && (i += t + ";",
                        r = !0)
                }) : (i += "param",
                    r = !0),
                    r)
                    throw new Error(i);
                return e.uid && (e.uid = e.uid.toLowerCase()),
                e.touid && (e.touid = e.touid.toLowerCase()),
                    !0
            }
            ,
            i.callbackHandler = function(e, t) {
                t && (t.success || t.error) && i.Event.one(e, function(e) {
                    e && 1e3 === e.code ? t.success && t.success.call(null, e) : t.error && t.error.call(null, e)
                })
            }
            ,
            i.getScript = function(t, i, r, n) {
                var o, a = document.createElement("script"), s = document.getElementsByTagName("head")[0], c = function() {
                    o && clearTimeout(o),
                    i && i()
                };
                e.addEventListener ? a.addEventListener("load", function() {
                    c()
                }, !1) : a.onreadystatechange = function() {
                    ("loaded" == this.readyState || "complete" == this.readyState) && (a.onreadystatechange = null,
                        c())
                }
                    ,
                    a.src = t,
                    s.insertBefore(a, s.firstChild),
                    o = setTimeout(function() {
                        r && r()
                    }, n || 3e3)
            }
            ,
            i.htmlEncode = function(e) {
                return e = e.replace(/&/g, "&amp;"),
                    e = e.replace(/>/g, "&gt;"),
                    e = e.replace(/</g, "&lt;"),
                    e = e.replace(/"/g, "&quot;"),
                    e = e.replace(/'/g, "&#39;")
            }
        ;
        i.jsonp = function(t, i, r, n) {
            var o = +new Date
                , a = document.createElement("script")
                , s = document.getElementsByTagName("head")[0]
                , c = "wsdk_" + o
                , l = null;
            n && a.setAttribute("charset", n),
                a.type = "text/javascript",
                a.id = c,
                e[c] = function(t) {
                    try {
                        var r = document.getElementById(c);
                        l && clearTimeout(l),
                        r && r.parentNode.removeChild(r),
                            delete e[c],
                        i && i.call(null, t)
                    } catch (e) {}
                }
                ,
                t += (t.indexOf("?") > -1 ? "&" : "?") + "callback=" + c,
                s.insertBefore(a, s.firstChild),
                a.src = t,
                l = setTimeout(function() {
                    r && r.call(null, {
                        code: 1002,
                        resultText: "TIMEOUT"
                    })
                }, 4e3)
        }
            ,
            i.param = function(e) {
                var t = "";
                if (e) {
                    for (var i in e)
                        t += "&" + i + "=" + encodeURIComponent(e[i]);
                    t = t.substr(1)
                }
                return t
            }
            ,
            i.unparam = function(e) {
                var t = {};
                if (e) {
                    e = e.split("&");
                    for (var i = 0, r = e.length; r > i; i++) {
                        var n = e[i].split("=");
                        try {
                            t[n[0]] = decodeURIComponent(n[1])
                        } catch (e) {
                            t[n[0]] = n[1]
                        }
                    }
                }
                return t
            }
            ,
            i.getRealMsgAndType = function(e, t, r) {
                var n = e[r]
                    , o = [];
                if (4 != t)
                    if (i.isArray(n) && n.length) {
                        var a = ""
                            , s = "";
                        n.forEach(function(e, r) {
                            a = "",
                                "P" === e[0] || 0 === e[1].indexOf("http://interface.im") || 0 === e[1].indexOf("https://interface.im") || 0 === e[1].indexOf("http://mobileim.im") || 0 === e[1].indexOf("https://mobileim.etao") ? (a = e[1],
                                    t = i.unparam(a.split("?")[1]).type || 1,
                                    a = /^https:/.test(a) ? a.replace("https://interface.im.taobao", "https://mobileim.etao") : a.replace("http://interface.im.taobao", "http://mobileim.etao"),
                                    t = parseInt(t)) : "T" === e[0] && (a = e[1]),
                            n[r + 1] && "Z" === n[r + 1][0] && (s = n[r + 1][1]),
                            a.trim() && o.unshift({
                                type: t,
                                msg: a,
                                extInfo: s
                            })
                        })
                    } else
                        o.push({
                            type: t,
                            msg: n
                        });
                else
                    t = 66,
                        o.push({
                            type: t,
                            msg: n
                        });
                return o
            }
            ,
            i.urlReplacer = function(e) {
                return e.replace(r, function(e) {
                    return 0 == e.indexOf("https://g.alicdn.com/aliww/h5-openim/0.0.1/faces/") ? e : '<a href="' + e + '" target="_blank">' + e + "</a>"
                })
            }
        ;
        var o = (new Date).getTimezoneOffset() / 60;
        i.dateFormatter = function(e) {
            var t, i, r, n, a, s, c = new Date(e), l = new Date, u = +c, d = +l - (+l - 36e5 * o) % 864e5;
            return t = c.getFullYear(),
                i = this.numFormatter(c.getMonth() + 1),
                r = this.numFormatter(c.getDate()),
                n = this.numFormatter(c.getHours()),
                a = this.numFormatter(c.getMinutes()),
                s = this.numFormatter(c.getSeconds()),
                l.getFullYear() == t ? u >= d && d + 864e5 > u ? "今天 " + n + ":" + a + ":" + s : i + "-" + r + " " + n + ":" + a + ":" + s : t + "-" + i + "-" + r + " " + n + ":" + a + ":" + s
        }
            ,
            i.numFormatter = function(e) {
                return e = parseInt(e),
                    e > 9 ? e : "0" + e
            }
            ,
            i.addClass = function(e, t) {
                return e && t ? (t = t.trim(),
                    e.classList ? t.split(/\s+/).forEach(function(t) {
                        e.classList.add(t)
                    }) : this.hasClass(e, t) || (e.className += " " + t),
                    this) : this
            }
            ,
            i.ajax = function(e) {
                var t = null;
                try {
                    t = new ActiveXObject("Msxml2.XMLHTTP")
                } catch (e) {
                    try {
                        t = new ActiveXObject("Microsoft.XMLHTTP")
                    } catch (e) {
                        try {
                            t = new XMLHttpRequest,
                            t.overrideMimeType && t.overrideMimeType("text/xml")
                        } catch (e) {
                            alert("您的浏览器不支持Ajax")
                        }
                    }
                }
                var i = (e.method || "GET").toUpperCase()
                    , r = Math.random();
                if ("object" == typeof e.data) {
                    var n = "";
                    for (var o in e.data)
                        n += o + "=" + e.data[o] + "&";
                    e.data = n.replace(/&$/, "")
                }
                "GET" == i ? (e.data ? t.open("GET", e.url + "?" + e.data, !0) : t.open("GET", e.url + "?t=" + r, !0),
                    t.send()) : "POST" == i && (t.open("POST", e.url, !0),
                    t.setRequestHeader("Content-type", "application/x-www-form-urlencoded"),
                    t.send(e.data)),
                    t.onreadystatechange = function() {
                        4 == t.readyState && (200 == t.status ? e.success && e.success(t.responseText) : e.error && e.error(t.status))
                    }
            }
    }(this),
    function(e) {
        function t(e) {
            this.init(e)
        }
        function i(e, t, i) {
            var r = document.createElement("form")
                , n = "";
            r.action = d,
                r.method = "post",
                r.enctype = "multipart/form-data",
                r.name = i,
                r.target = t,
                r.style.display = "none",
                r.acceptCharset = "UTF-8";
            for (var o in e)
                n += '<input type="hidden" name="' + o + '" value="' + e[o] + '" />';
            return r.innerHTML = n,
                document.body.appendChild(r),
                r
        }
        function r(e) {
            var t;
            try {
                t = document.createElement('<iframe name="' + e + '">')
            } catch (e) {
                t = document.createElement("iframe")
            }
            return t.name = e,
                t.style.display = "none",
                document.body.appendChild(t),
                t
        }
        var n = e.WSDKUtil
            , o = e.WSDKCons
            , a = n.Event
            , s = o.UPLOAD_PAGE
            , c = o.MAX_IMAGE_SIZE
            , l = o.DEFAULT_UPLOAD_TIMEOUT
            , u = "WSDK_FRAME"
            , d = o.UPLOAD_URL
            , m = o.UPLOAD_SUCCESS_PROXY
            , h = {
            wx_web_token: "",
            user_id: "",
            mimetype: "",
            base64ed_file_data: "",
            redirect_url: m,
            type: 1,
            msgType: 0,
            receiver_id: "",
            message_id: "",
            width: "",
            height: "",
            mediaSize: ""
        }
            , g = o.UPLOAD_ERRORS;
        !e.WSDKPlgs && (e.WSDKPlgs = {}),
            t.prototype = {
                constructor: t,
                __IsTokenFetching: !1,
                __Token: "",
                init: function(e) {
                    this.Base = e.Base,
                    e.isDaily && (d = "http://interface.im.daily.taobao.net/api/file/uploadFile.json",
                        m = "http://chat.im.daily.taobao.net/uploadproxy.htm",
                        h.redirect_url = m),
                        this.messengerSender = e.Base.messengerSender,
                        this.Messenger = e.__Messenger,
                        this.__events()
                },
                upload: function(e) {
                    n.checkParam(e, ["base64Str", "ext", "success"]),
                    !e.error && (e.error = function() {}
                    ),
                    (!e.maxSize || e.maxSize > c) && (e.maxSize = c),
                    !e.type && (e.type = 1),
                        this.param = e,
                        this.upBase64(e)
                },
                __events: function() {
                    var e = this;
                    a.on("GET_UPLOAD_TOKEN", function(t) {
                        e.__IsTokenFetching = !1,
                            1e3 === t.code ? (e.__Token = t.data.token,
                                e.onGetUpTokenSuccess(),
                                setTimeout(function() {
                                    e.__Token = ""
                                }, parseInt(t.data.expire) / 3 * 2 * 1e3)) : e.onGetUpTokenError()
                    }),
                        a.on("UPLOAD_IMAGE", function(t) {
                            e.timeout && clearTimeout(e.timeout),
                                e.timoeut = null,
                                1e3 == t.code ? e.uploadSuccess(t) : e.uploadError(t)
                        })
                },
                getUpToken: function() {
                    this.__IsTokenFetching || (this.__IsTokenFetching = !0,
                        this.__Token ? this.onGetUpTokenSuccess() : this.messengerSender.call(this.Base, {
                            action: "GET_UPLOAD_TOKEN"
                        }, !0))
                },
                upBase64: function() {
                    var e = this.param.base64Str;
                    return e = e.substring(e.indexOf(",") + 1),
                        this.param.base64StrForUpload = e,
                        3 * e.length / 4 > this.param.maxSize ? this.param.error({
                            code: -4,
                            errorText: g[-4]
                        }) : void this.getUpToken()
                },
                onGetUpTokenSuccess: function() {
                    this.__IsTokenFetching = !1,
                        h.wx_web_token = this.__Token,
                        h.base64ed_file_data = this.param.base64StrForUpload,
                        h.mimetype = this.param.ext,
                        h.user_id = this.Base.LoginInfo.prefix + this.Base.LoginInfo.uid,
                        h.type = this.param.type;
                    var e = this
                        , t = +new Date;
                    this.form = i(h, u + t, "WSDK_FORM" + t),
                        this.frame = r(u + t),
                        this.timeout = setTimeout(function() {
                            e.param.error && e.param.error({
                                code: -3,
                                errorText: g[-3]
                            }),
                                e.__Token = "",
                                e.clear()
                        }, this.param.timeout || l),
                        this.Messenger.addTarget(this.frame.contentWindow, s),
                        this.form.submit()
                },
                uploadSuccess: function(e) {
                    this.param.success && this.param.success({
                        code: 1e3,
                        resultCode: "SUCCESS",
                        data: {
                            url: e.data,
                            base64Str: this.param.base64Str
                        }
                    }),
                        this.clear()
                },
                uploadError: function(e) {
                    this.param.error && this.param.error({
                        code: -7,
                        errorText: g[-7] + " " + e.resultText
                    }),
                        this.__Token = "",
                        this.clear()
                },
                clear: function() {
                    this.form && this.form.parentNode.removeChild(this.form),
                    this.frame && this.frame.parentNode.removeChild(this.frame),
                        this.form = null,
                        this.frame = null
                },
                onGetUpTokenError: function() {
                    this.param.error && this.param.error({
                        code: -8,
                        errorText: g[-8]
                    })
                }
            },
            e.WSDKPlgs.Uploader = t
    }(this),
    function(e) {
        function t(e) {
            this.init(e)
        }
        var i = e.WSDKUtil
            , r = e.WSDKCons
            , n = r.UPLOAD_ERRORS;
        !e.WSDKPlgs && (e.WSDKPlgs = {}),
            t.prototype = {
                constructor: t,
                init: function(e) {
                    this.Uploader = e.Plugin.Uploader
                },
                upload: function(e) {
                    i.checkParam(e, ["success"]),
                    !e.error && (e.error = function() {}
                    );
                    var t = e.error;
                    this.param = e,
                        e.target ? this.upTarget(e) : e.base64Img && e.ext ? this.upBase64(e) : t({
                            code: -1,
                            errorText: n[-1]
                        })
                },
                upTarget: function() {
                    var e = this.param
                        , t = e.target.files
                        , i = e.target.value;
                    if (!i)
                        return e.error({
                            code: -2,
                            errorText: n[-3]
                        });
                    if (!this.isSupport() || !t || !t[0])
                        return e.error({
                            code: -3,
                            errorText: n[-3]
                        });
                    var r = t[0]
                        , o = r.size
                        , a = i.split(".")
                        , s = a[a.length - 1];
                    if (o > e.maxSize)
                        return e.error({
                            code: -4,
                            errorText: n[-4]
                        });
                    var c = this;
                    this.getImageWH(r, function(e) {
                        var t = c.getImageBase64(e, s);
                        c.param.base64Img = t,
                            c.param.ext = s,
                            c.upBase64()
                    }, function() {
                        e.error({
                            code: -5,
                            errorText: n[-5]
                        })
                    })
                },
                upBase64: function() {
                    var e = this
                        , t = this.param.base64Img;
                    return 3 * t.length / 4 > this.param.maxSize ? this.param.error({
                        code: -4,
                        errorText: n[-4]
                    }) : void this.Uploader.upload({
                        base64Str: t,
                        ext: this.param.ext,
                        type: 1,
                        timeout: this.param.timeout,
                        maxSize: this.param.maxSize,
                        success: function(t) {
                            e.param.success && e.param.success({
                                code: 1e3,
                                resultCode: "SUCCESS",
                                data: {
                                    url: t.data.url,
                                    base64Str: t.data.base64Str
                                }
                            })
                        },
                        error: function(t) {
                            e.param.error && e.param.error(t)
                        }
                    })
                },
                getImageWH: function(t, i, r) {
                    var o = this;
                    try {
                        var a = e.URL || e.webkitURL
                            , s = a.createObjectURL(t)
                            , c = new Image;
                        c.onload = function() {
                            i && i(this),
                                c.onload = c.onerror = null
                        }
                            ,
                            c.onerror = function() {
                                r && r(),
                                    c.onload = c.onerror = null
                            }
                            ,
                            c.src = s
                    } catch (e) {
                        return o.param.error({
                            code: -3,
                            errorText: n[-3]
                        })
                    }
                },
                getType: function(e) {
                    return e = e.toLowerCase(),
                        "png" == e ? "image/png" : "image/jpeg"
                },
                getImageBase64: function(e, t) {
                    var i = this
                        , r = "";
                    try {
                        var o = document.createElement("canvas")
                            , a = o.getContext("2d");
                        o.width = e.width,
                            o.height = e.height,
                            a.drawImage(e, 0, 0, e.width, e.height),
                            r = o.toDataURL(this.getType(t), 1)
                    } catch (e) {
                        i.param.error({
                            code: -3,
                            errorText: n[-3]
                        })
                    }
                    return r
                },
                isSupport: function() {
                    return !!document.createElement("canvas").getContext
                }
            },
            e.WSDKPlgs.ImgUp = t
    }(this),
    function(e) {
        function t() {
            this.init()
        }
        function i(e) {
            return e = e.replace(/\[([A-Z\u4e00-\u9fa5]{1,20}?)\]/gi, "@#[$1]@#"),
                e.split("@#")
        }
        function r(e) {
            return /\[([A-Z\u4e00-\u9fa5]{1,20}?)\]/gi.test(e)
        }
        function n(e) {
            for (var t = e.replace("[", "").replace("]", ""), i = null, r = 0, n = l.length; n > r; r++)
                if (l[r].title == t) {
                    i = l[r];
                    break
                }
            return i
        }
        function o(e) {
            return n(e).code
        }
        function a(e, t, i) {
            var r = t * i
                , n = []
                , o = 0
                , a = 0;
            return e.forEach(function(e) {
                o == r && (o = 0),
                0 == o && (n[a++] = []),
                r > o && (o++,
                    n[a - 1].push(e))
            }),
                n
        }
        var s = e.WSDKUtil
            , c = ["加油", "色情狂", "委屈", "悲泣", "飞机", "生病", "不会吧", "痛哭", "怀疑", "花痴", "偷笑", "握手", "玫瑰", "享受", "对不起", "感冒", "凄凉", "困了", "尴尬", "生气", "残花", "电话", "害羞", "流口水", "皱眉", "鼓掌", "迷惑", "忧伤", "时钟", "大笑", "邪恶", "单挑", "大哭", "隐形人", "CS", "I服了U", "欠扁", "炸弹", "惊声尖叫", "微笑", "钱", "购物", "好累", "嘘", "成交", "红唇", "招财猫", "抱抱", "好主意", "很晚了", "收邮件", "举杯庆祝", "疑问", "惊讶", "露齿笑", "天使", "呼叫", "闭嘴", "小样", "跳舞", "无奈", "查找", "大怒", "算帐", "爱慕", "再见", "恭喜发财", "强", "胜利", "财迷", "思考", "晕", "流汗", "爱心", "摇头", "背", "没钱了", "惊愕", "小二", "支付宝", "鄙视你", "吐舌头", "鬼脸", "财神", "等待", "傻笑", "学习雷锋", "心碎", "吐", "漂亮MM", "亲亲", "飞吻", "帅哥", "礼物", "无聊", "呆若木鸡", "再见", "老大", "安慰"]
            , l = [{
            title: "加油",
            img: "10",
            code: "/:012"
        }, {
            title: "色情狂",
            img: "26",
            code: "/:015"
        }, {
            title: "委屈",
            img: "47",
            code: "/:>_<"
        }, {
            title: "悲泣",
            img: "48",
            code: "/:018"
        }, {
            title: "飞机",
            img: "97",
            code: "/:plane"
        }, {
            title: "生病",
            img: "56",
            code: "/:>M<"
        }, {
            title: "不会吧",
            img: "40",
            code: "/:816"
        }, {
            title: "痛哭",
            img: "50",
            code: "/:020"
        }, {
            title: "怀疑",
            img: "33",
            code: "/:817"
        }, {
            title: "花痴",
            img: "14",
            code: "/:814"
        }, {
            title: "偷笑",
            img: "3",
            code: "/:815"
        }, {
            title: "握手",
            img: "82",
            code: "/:)-("
        }, {
            title: "玫瑰",
            img: "84",
            code: "/:-F"
        }, {
            title: "享受",
            img: "25",
            code: "/:818"
        }, {
            title: "对不起",
            img: "52",
            code: "/:819"
        }, {
            title: "感冒",
            img: "37",
            code: "/:028"
        }, {
            title: "凄凉",
            img: "43",
            code: "/:027"
        }, {
            title: "困了",
            img: "44",
            code: "/:(Zz...)"
        }, {
            title: "尴尬",
            img: "38",
            code: "/:026"
        }, {
            title: "生气",
            img: "65",
            code: "/:>W<"
        }, {
            title: "残花",
            img: "85",
            code: "/:-W"
        }, {
            title: "电话",
            img: "92",
            code: "/:~B"
        }, {
            title: "害羞",
            img: "1",
            code: "/:^$^"
        }, {
            title: "流口水",
            img: "24",
            code: "/:813"
        }, {
            title: "皱眉",
            img: "54",
            code: "/:812"
        }, {
            title: "鼓掌",
            img: "81",
            code: "/:8*8"
        }, {
            title: "迷惑",
            img: "29",
            code: "/:811"
        }, {
            title: "忧伤",
            img: "46",
            code: "/:810"
        }, {
            title: "时钟",
            img: "94",
            code: "/:clock"
        }, {
            title: "大笑",
            img: "5",
            code: "/:^O^"
        }, {
            title: "邪恶",
            img: "71",
            code: "/:036"
        }, {
            title: "单挑",
            img: "72",
            code: "/:039"
        }, {
            title: "大哭",
            img: "49",
            code: "/:>O<"
        }, {
            title: "隐形人",
            img: "74",
            code: "/:046"
        }, {
            title: "CS",
            img: "73",
            code: "/:045"
        }, {
            title: "I服了U",
            img: "51",
            code: "/:044"
        }, {
            title: "欠扁",
            img: "62",
            code: "/:043"
        }, {
            title: "炸弹",
            img: "75",
            code: "/:048"
        }, {
            title: "惊声尖叫",
            img: "76",
            code: "/:047"
        }, {
            title: "微笑",
            img: "0",
            code: "/:^_^"
        }, {
            title: "钱",
            img: "88",
            code: "/:$"
        }, {
            title: "购物",
            img: "89",
            code: "/:%"
        }, {
            title: "好累",
            img: "55",
            code: '/:"'
        }, {
            title: "嘘",
            img: "34",
            code: "/:!"
        }, {
            title: "成交",
            img: "80",
            code: "/:(OK)"
        }, {
            title: "红唇",
            img: "83",
            code: "/:lip"
        }, {
            title: "招财猫",
            img: "79",
            code: "/:052"
        }, {
            title: "抱抱",
            img: "9",
            code: "/:H"
        }, {
            title: "好主意",
            img: "20",
            code: "/:071"
        }, {
            title: "很晚了",
            img: "96",
            code: "/:C"
        }, {
            title: "收邮件",
            img: "91",
            code: "/:@"
        }, {
            title: "举杯庆祝",
            img: "93",
            code: "/:U*U"
        }, {
            title: "疑问",
            img: "30",
            code: "/:?"
        }, {
            title: "惊讶",
            img: "59",
            code: "/:069"
        }, {
            title: "露齿笑",
            img: "15",
            code: "/:^W^"
        }, {
            title: "天使",
            img: "22",
            code: "/:065"
        }, {
            title: "呼叫",
            img: "17",
            code: "/:066"
        }, {
            title: "闭嘴",
            img: "61",
            code: "/:067"
        }, {
            title: "小样",
            img: "35",
            code: "/:068"
        }, {
            title: "跳舞",
            img: "6",
            code: "/:081"
        }, {
            title: "无奈",
            img: "41",
            code: '/:\'""'
        }, {
            title: "查找",
            img: "16",
            code: "/:080"
        }, {
            title: "大怒",
            img: "64",
            code: "/:808"
        }, {
            title: "算帐",
            img: "18",
            code: "/:807"
        }, {
            title: "爱慕",
            img: "4",
            code: "/:809"
        }, {
            title: "再见",
            img: "23",
            code: "/:804"
        }, {
            title: "恭喜发财",
            img: "68",
            code: "/:803"
        }, {
            title: "强",
            img: "12",
            code: "/:b"
        }, {
            title: "胜利",
            img: "11",
            code: "/:806"
        }, {
            title: "财迷",
            img: "19",
            code: "/:805"
        }, {
            title: "思考",
            img: "28",
            code: "/:801"
        }, {
            title: "晕",
            img: "45",
            code: "/:*&*"
        }, {
            title: "流汗",
            img: "42",
            code: "/:802"
        }, {
            title: "爱心",
            img: "86",
            code: "/:Y"
        }, {
            title: "摇头",
            img: "36",
            code: "/:079"
        }, {
            title: "背",
            img: "58",
            code: "/:076"
        }, {
            title: "没钱了",
            img: "31",
            code: "/:077"
        }, {
            title: "老大",
            img: "70",
            code: "/:O=O"
        }, {
            title: "小二",
            img: "69",
            code: "/:074"
        }, {
            title: "支付宝",
            img: "98",
            code: "/:075"
        }, {
            title: "鄙视你",
            img: "63",
            code: "/:P"
        }, {
            title: "吐舌头",
            img: "2",
            code: "/:Q"
        }, {
            title: "鬼脸",
            img: "21",
            code: "/:072"
        }, {
            title: "财神",
            img: "66",
            code: "/:073"
        }, {
            title: "等待",
            img: "95",
            code: "/:R"
        }, {
            title: "傻笑",
            img: "39",
            code: "/:007"
        }, {
            title: "学习雷锋",
            img: "67",
            code: "/:008"
        }, {
            title: "心碎",
            img: "87",
            code: "/:qp"
        }, {
            title: "吐",
            img: "57",
            code: "/:>@<"
        }, {
            title: "漂亮MM",
            img: "77",
            code: "/:girl"
        }, {
            title: "亲亲",
            img: "13",
            code: "/:^x^"
        }, {
            title: "飞吻",
            img: "7",
            code: "/:087"
        }, {
            title: "帅哥",
            img: "78",
            code: "/:man"
        }, {
            title: "礼物",
            img: "90",
            code: "/:(&)"
        }, {
            title: "无聊",
            img: "32",
            code: "/:083"
        }, {
            title: "呆若木鸡",
            img: "27",
            code: "/:084"
        }, {
            title: "再见",
            img: "53",
            code: "/:085"
        }, {
            title: "惊愕",
            img: "60",
            code: "/:O"
        }, {
            title: "安慰",
            img: "8",
            code: "/:086"
        }]
            , u = "https://gw.alicdn.com/tps/TB1.eFIKXXXXXbUXFXXXXXXXXXX-320-675.jpg"
            , d = "https://img.alicdn.com/tps/TB1O2CKHVXXXXbMXVXXXXXXXXXX.jpg"
            , m = {
            trigger: !0,
            emots: c,
            emotsImg: u,
            emotSize: 45,
            row: 7,
            col: 3,
            customStyle: !1,
            onEmotClick: function() {}
        };
        !e.WSDKPlgs && (e.WSDKPlgs = {}),
            t.prototype = {
                constructor: t,
                emotTitles: c,
                emotW640: d,
                emotW320: u,
                init: function() {},
                render: function(t) {
                    s.checkParam(t, ["container"]);
                    var i = s.extend({}, m, t)
                        , r = ""
                        , n = ""
                        , o = 0
                        , c = i.row * i.col
                        , l = i.row * i.emotSize
                        , u = i.emotSize * i.col
                        , h = i.emotsImg == d ? 2 : 0
                        , g = document.createElement("div");
                    if (a(i.emots, i.col, i.row).forEach(function(e, t) {
                        emotWrapItemStyle = i.customStyle ? "" : 'style="display:' + (0 == t ? "block" : "none") + ";background:url(" + i.emotsImg + ") no-repeat 0 -" + t * (u - h) + "px;width:" + l + "px;height:" + u + "px;" + (h ? "background-size:100%;" : "") + '"',
                            emotItemStyle = i.customStyle ? "" : 'style="display:block;float:left;cursor:pointer;width:' + i.emotSize + "px;height:" + i.emotSize + 'px;"',
                            r += '<div class="wsdk-emot-wrap" ' + emotWrapItemStyle + ">",
                        i.trigger && (n += '<i class="wsdk-emot-trigger-item' + (0 == t ? " wsdk-active" : "") + '" data-index="' + t + '"></i>'),
                            e.forEach(function(e, i) {
                                r += '<span title="' + e + '" data-index="' + (i + t * c) + '" ' + emotItemStyle + "></span>"
                            }),
                            r += "</div>"
                    }),
                        g.innerHTML = r,
                    i.customStyle || (g.style.width = l + "px",
                        g.style.height = u + "px",
                        g.style.overflow = "hidden"),
                        g.className = "wsdk-emot-con",
                        i.container.appendChild(g),
                        i.trigger) {
                        var f = document.createElement("div");
                        f.innerHTML = n,
                            f.className = "wsdk-emot-trigger",
                            i.container.appendChild(f);
                        var p = f.getElementsByTagName("i")
                    }
                    var _ = g.getElementsByTagName("div")
                        , E = function(t) {
                        t = t || e.event;
                        var r = t.target || t.srcElement;
                        r && "SPAN" == r.tagName.toUpperCase() && i.onEmotClick("[" + r.title + "]")
                    }
                        , T = function(t) {
                        t = t || e.event;
                        var i = t.target || t.srcElement;
                        if (i && "I" == i.tagName.toUpperCase()) {
                            var r = i.getAttribute("data-index");
                            if (r == o)
                                return;
                            _[o].style.display = "none",
                                _[r].style.display = "block",
                                p[o].className = "wsdk-emot-trigger-item",
                                p[r].className = "wsdk-emot-trigger-item wsdk-active",
                                o = r
                        }
                    };
                    e.addEventListener ? (g.addEventListener("click", E, !1),
                    i.trigger && f.addEventListener("mouseover", T, !1)) : e.attachEvent && (g.attachEvent("onclick", E),
                    i.trigger && f.attachEvent("onmouseover", T))
                },
                encode: function(e) {
                    var t, n = "";
                    return e = i(e),
                        e.forEach(function(e) {
                            n += r(e) && (t = o(e)) ? t : e
                        }),
                        n
                },
                htmlEncode: function(e) {
                    var t, n = "";
                    return e = i(e),
                        e.forEach(function(e) {
                            n += r(e) && (t = o(e)) ? t : s.htmlEncode(e)
                        }),
                        n
                },
                htmlDecode: function(e) {
                    var t, o, a = "";
                    return e = i(e),
                        e.forEach(function(e) {
                            r(e) && (t = n(e)) ? (o = parseInt(t.img) + 1,
                                o = 10 > o ? "0" + o : o,
                                o = "s0" + o + ".png",
                                a += '<img class="wsdk-emot" src="https://g.alicdn.com/aliww/h5-openim/0.0.1/faces/' + o + '" alt="' + t.title + '" />') : a += s.htmlEncode(e)
                        }),
                        a
                },
                decode: function() {
                    var e = ""
                        , t = l.length;
                    return l.forEach(function(t) {
                        e += "|" + t.code.substring(2)
                    }),
                        e = e.substring(1),
                        e = e.replace(/([\^?()\.\*\$])/g, "\\$1"),
                        e = new RegExp("/:(" + e + ")","g"),
                        function(i) {
                            return i && (i = i.replace(e, function(e) {
                                for (var i = !1, r = "", n = 0; t > n; n++)
                                    if (l[n].code == e) {
                                        i = l[n].img,
                                            r = l[n].title;
                                        break
                                    }
                                return i && (e = "[" + r + "]"),
                                    e
                            })),
                                i = this.htmlDecode(i)
                        }
                }(),
                isEmot: o,
                isEmotLike: r,
                splitByEmot: i
            },
            e.WSDKPlgs.Emot = t
    }(this),
    function(e) {
        function t(e, t) {
            this.init(e, t)
        }
        function i(e, t, i, s) {
            r(t.frameObj);
            var c, d = {
                __ProxyFrame: null,
                __ProxyForm: null,
                __ProxyFrameLoadTimeOut: null,
                __ProxyFrameCallback: function() {
                    i && (i.__IsProxyFrameLoad = !0,
                        i.__setLoginInfo.call(i))
                }
            }, h = t.timeout;
            delete t.timeout,
                delete t.frameObj;
            try {
                c = document.createElement('<iframe name="WSDK_FRAME">')
            } catch (e) {
                c = document.createElement("iframe")
            }
            if (c.style.display = "none",
                c.name = "WSDK_FRAME",
                document.body.appendChild(c),
                d.__ProxyFrameLoadTimeOut = setTimeout(function() {
                    d.__ProxyFrameLoadTimeOut = null,
                        l.fire("LOGIN", {
                            code: 1002,
                            resultText: u[1002]
                        })
                }, h),
                c.attachEvent ? c.attachEvent("onload", d.__ProxyFrameCallback) : c.onload = d.__ProxyFrameCallback,
                d.__ProxyFrame = c,
                c)
                try {
                    i.context.__Messenger.addTarget(c.contentWindow, m)
                } catch (e) {}
            i.context.__Messenger.listen(function(e) {
                n(e)
            });
            var g;
            return s && "POST" != s.toUpperCase() ? (e += "?" + a.param(t),
                c.src = e) : g = o(e, t),
                d.__ProxyForm = g,
                d
        }
        function r(e) {
            e && (e.__ProxyFrame && (e.__ProxyFrame.detachEvent ? e.__ProxyFrame.detachEvent("onload", e.__ProxyFrameCallback) : e.__ProxyFrame.onload = null,
                e.__ProxyFrame.parentNode.removeChild(e.__ProxyFrame),
                clearTimeout(e.__ProxyFrameLoadTimeOut),
                e.__ProxyFrameLoadTimeOut = null,
                e.__ProxyFrame = null),
            e.__ProxyForm && (e.__ProxyForm.parentNode.removeChild(e.__ProxyForm),
                e.__ProxyForm = null))
        }
        function n(e) {
            e = JSON.parse(e),
                l.fire(e.action, e.result),
            e.result && 1e3 == e.result.code && ("CHAT_START_RECEIVE_MSG" === e.action ? (l.fire("MSG_RECEIVED", e.result),
            16908304 == e.result.data.cmdid && l.fire("CHAT.MSG_RECEIVED", e.result)) : "START_RECEIVE_ALL_MSG" === e.action && (l.fire("MSG_RECEIVED", e.result),
                16908304 == e.result.data.cmdid ? l.fire("CHAT.MSG_RECEIVED", e.result) : 16908545 == e.result.data.cmdid && l.fire("TRIBE.MSG_RECEIVED", e.result)))
        }
        function o(e, t) {
            var i = document.createElement("form")
                , r = "";
            for (var n in t)
                r += '<input type="hidden" name="' + n + '" value="' + t[n] + '" />';
            return i.action = e,
                i.target = "WSDK_FRAME",
                i.method = "post",
                i.innerHTML = r,
                i.style.display = "none",
                document.body.appendChild(i),
                i.submit(),
                i
        }
        var a = e.WSDKUtil
            , s = e.WSDKCons
            , c = s.PROXY_ACTION_API
            , l = a.Event
            , u = s.CODE
            , d = s.DEFAULT_LOGIN_TIMEOUT
            , m = s.PROXY_PAGE
            , h = s.LOGIN_URL
            , g = s.PROXY_URL;
        !e.WSDKMods && (e.WSDKMods = {}),
            t.prototype = {
                constructor: t,
                init: function(e, t) {
                    t && t.debug && "daily" === t.env && (h = "http://chat.im.daily.taobao.net:7001/login/oauth",
                        g = "http://chat.im.daily.taobao.net:7001/proxy.htm",
                        e.isDaily = !0),
                        this.context = e,
                        this.__events()
                },
                __events: function() {
                    var e = this;
                    l.on("SET_LOGIN_INFO", function(t) {
                        e.frameObj && e.frameObj.__ProxyFrameLoadTimeOut && (clearTimeout(e.frameObj.__ProxyFrameLoadTimeOut),
                            e.__IsLogin = !0,
                            e.LoginInfo.prefix = t.data.prefix,
                            e.LoginInfo.toPrefix = t.data.toPrefix,
                            e.context.LoginInfo = e.LoginInfo,
                            l.fire("LOGIN", {
                                code: 1e3,
                                resultText: u[1e3]
                            }))
                    }),
                        l.on("KICK_OFF_INSIDE", function() {
                            e.__IsLogin = !1
                        })
                },
                login: function(e) {
                    if (this.context && this.context.__Messenger && this.context.__Messenger.clear(),
                        e.anonymous) {
                        var t = this
                            , i = !1;
                        if (a.checkParam(e, ["anonymous", "appkey"]),
                        store && store.get("anonymous"))
                            try {
                                var r = JSON.parse(store.get("anonymous"));
                                r.appkey === e.appkey && r.uid && r.pwd && (e.uid = r.uid.substring(8),
                                    e.credential = r.pwd,
                                    e.tokenFlag = 67,
                                    i = !0,
                                    t._login(e))
                            } catch (e) {}
                        if (i)
                            return;
                        a.ajax({
                            url: c.getAnonymous,
                            method: "POST",
                            data: {
                                appkey: e.appkey,
                                deviceinfo: "{}"
                            },
                            success: function(i) {
                                if (!i)
                                    return void (e.error && e.error({
                                        code: 1003,
                                        resultText: "获取匿名账号失败"
                                    }));
                                try {
                                    if (i = JSON.parse(i),
                                    i.userid && i.password) {
                                        try {
                                            store.set("anonymous", JSON.stringify({
                                                appkey: e.appkey,
                                                uid: i.userid,
                                                pwd: i.password
                                            }))
                                        } catch (e) {}
                                        e.uid = i.userid.substring(8),
                                            e.credential = i.password,
                                            e.tokenFlag = 67,
                                            t._login(e)
                                    } else
                                        e.error && e.error({
                                            code: 1003,
                                            resultText: "获取匿名账号失败"
                                        })
                                } catch (t) {
                                    e.error && e.error({
                                        code: 1004,
                                        resultText: u[1004]
                                    })
                                }
                            },
                            error: function(t) {
                                console.log(t),
                                e.error && e.error({
                                    code: 1005,
                                    resultText: u[1005]
                                })
                            }
                        })
                    } else
                        a.checkParam(e, ["uid", "appkey", "credential"]),
                            this._login(e)
                },
                _login: function(e) {
                    a.checkParam(e, ["uid", "appkey", "credential"]);
                    var t = this
                        , r = e.uid
                        , n = e.appkey
                        , o = e.toAppkey || n
                        , s = e.credential
                        , c = void 0 === e.tokenFlag ? 64 : e.tokenFlag
                        , m = e.timeout || d
                        , g = e.error || function() {}
                    ;
                    if (e.error = function(e) {
                        t.__IsLogin = !1,
                            clearTimeout(t.frameObj && t.frameObj.__ProxyFrameLoadTimeOut),
                            g(e)
                    }
                        ,
                        a.callbackHandler("LOGIN", e),
                        this.__IsLogin)
                        return void l.fire("LOGIN", {
                            code: 1008,
                            resultText: u[1008]
                        });
                    this.LoginInfo = {
                        uid: r,
                        appkey: n,
                        toAppkey: o,
                        credential: s,
                        tokenFlag: c
                    };
                    var f = {
                        cross: !0,
                        tokenFlag: c,
                        uid: r,
                        credential: s,
                        appkey: n,
                        toAppkey: o,
                        ver: WSDK.version,
                        lpath: "yw",
                        timeout: m,
                        frameObj: t.frameObj
                    };
                    createMethod = "POST";
                    try {
                        t.frameObj = i(h, f, t, createMethod)
                    } catch (e) {}
                    return this
                },
                logout: function(e) {
                    !e && (e = {});
                    var t = this
                        , i = e.success;
                    return e.success = function(e) {
                        t.__IsLogin = !1,
                            t.destroy(),
                        i && i(e)
                    }
                        ,
                        a.callbackHandler("LOGOUT", e),
                        this.messengerSender({
                            action: "LOGOUT",
                            result: {}
                        }, !0),
                        this
                },
                messengerSender: function(e, t) {
                    t && !this.__IsLogin ? ("TRIBE_GET_HISTORY_MSG" === e.action || "TRIBE_GET_INTO" === e.action ? e.action += e.result.tid : "GET_HISTORY_MSG" === e.action && (e.action += e.result.touid),
                        l.fire(e.action, {
                            code: 1001,
                            resultText: u[1001]
                        })) : this.context.__Messenger.targets[m].send(JSON.stringify(e))
                },
                getUnreadMsgCount: function(e) {
                    return a.callbackHandler("GET_UNREAD_MSG_COUNT", e),
                        this.messengerSender({
                            action: "GET_UNREAD_MSG_COUNT",
                            result: {
                                count: e && e.count || 30
                            }
                        }, !0),
                        this
                },
                getRecentContact: function(e) {
                    return a.callbackHandler("GET_RECENT_CONTACT_NEW", e),
                        this.messengerSender({
                            action: "GET_RECENT_CONTACT_NEW",
                            result: {
                                count: e && e.count || 30
                            }
                        }, !0),
                        this
                },
                startListenAllMsg: function() {
                    if (!this.__IsLogin)
                        throw new Error("未登录");
                    var e = this;
                    return this.__IsStartListen || (this.__IsStartListen = !0,
                        this.messengerSender({
                            action: "START_RECEIVE_ALL_MSG"
                        }, !0),
                        l.one("STOP_RECEIVE_ALL_MSG", function() {
                            e.__IsStartListen = !1
                        })),
                        this
                },
                stopListenAllMsg: function() {
                    return this.__IsStartListen && (this.__IsStartListen = !1,
                        this.messengerSender({
                            action: "STOP_RECEIVE_ALL_MSG"
                        })),
                        this
                },
                getNick: function(e) {
                    return e && e.length > 8 && (0 == e.indexOf(this.LoginInfo.prefix) || 0 == e.indexOf(this.LoginInfo.toPrefix)) ? e.substring(8) : e
                },
                destroy: function() {
                    r(this.frameObj),
                        this.stopListenAllMsg(),
                        l.fire("$destroy"),
                        this.__IsLogin = !1
                },
                getPrefixByAppkey: function(e) {
                    return a.checkParam(e, ["appkey"]),
                        a.jsonp(s.GET_APP_INFO_URL + "?appkey=" + e.appkey, function(t) {
                            t.prefix ? e.success && e.success({
                                code: 1e3,
                                data: {
                                    prefix: t.prefix
                                }
                            }) : e.error && e.error({
                                code: 1003,
                                resultText: t.errmsg
                            })
                        }, function(t) {
                            e.error && e.error(t)
                        }),
                        this
                },
                __setLoginInfo: function() {
                    this.messengerSender({
                        action: "SET_LOGIN_INFO",
                        result: {
                            appkey: this.LoginInfo.appkey,
                            uid: this.LoginInfo.uid
                        }
                    })
                }
            },
            e.WSDKMods.Base = t
    }(this),
    function(e) {
        function t(e) {
            this.init(e)
        }
        function i(t, i) {
            e.online && (r = e.online),
                e.online = [],
                n.getScript(c + "?" + n.param({
                    uids: t,
                    charset: i.charset || "utf-8",
                    beginnum: 0
                }), function() {
                    i.success && i.success({
                        code: 1e3,
                        resultText: s[1e3],
                        data: {
                            status: e.online
                        }
                    }),
                    r && (e.online = r),
                        r = null
                }, function() {
                    i.error && i.error({
                        code: 1005,
                        resultText: s[1005]
                    })
                })
        }
        var r, n = e.WSDKUtil, o = n.Event, a = e.WSDKCons, s = a.CODE, c = a.GET_USER_STATUS_URL;
        !e.WSDKMods && (e.WSDKMods = {}),
            t.prototype = {
                constructor: t,
                init: function(e) {
                    this.Base = e.Base,
                        this.messengerSender = e.Base.messengerSender,
                        this.__events()
                },
                __events: function() {
                    var e = this;
                    o.on("$destroy", function() {
                        e.stopListenMsg()
                    })
                },
                getHistory: function(e) {
                    return n.checkParam(e, ["touid"]),
                        n.callbackHandler("GET_HISTORY_MSG" + e.touid, e),
                        this.messengerSender.call(this.Base, {
                            action: "GET_HISTORY_MSG",
                            result: {
                                touid: e.touid,
                                count: e.count || 20,
                                nextkey: e.nextkey || "",
                                hasPrefix: !!e.hasPrefix,
                                type: 1
                            }
                        }, !0),
                        this
                },
                setReadState: function(e) {
                    return n.checkParam(e, ["touid"]),
                        n.callbackHandler("CHAT_SET_READ_STATE", e),
                        this.messengerSender.call(this.Base, {
                            action: "CHAT_SET_READ_STATE",
                            result: {
                                touid: e.touid,
                                timestamp: e.timestamp,
                                hasPrefix: !!e.hasPrefix
                            }
                        }, !0),
                        this
                },
                setMsgReadState: function(e) {
                    if (n.checkParam(e, ["touid", "msgs"]),
                        !n.isArray(e.msgs))
                        throw new Error("msgs必须为数组");
                    var t = [];
                    if (e.msgs.forEach(function(e) {
                        e.msgId && e.time && t.push(e)
                    }),
                        !t.length)
                        throw new Error("msgs内至少包含一个{id:xxx,time:xxx}");
                    return n.callbackHandler("CHAT_SET_OPENIM_READ_STATE", e),
                        this.messengerSender.call(this.Base, {
                            action: "CHAT_SET_OPENIM_READ_STATE",
                            result: {
                                touid: e.touid,
                                msgs: t,
                                hasPrefix: !!e.hasPrefix
                            }
                        }, !0),
                        this
                },
                startListenMsg: function(e) {
                    n.checkParam(e, ["touid"]);
                    var t = this;
                    return this.__IsStartRecv || (this.__IsStartRecv = !0,
                        this.messengerSender.call(this.Base, {
                            action: "CHAT_START_RECEIVE_MSG",
                            result: {
                                touid: e.touid,
                                hasPrefix: !!e.hasPrefix
                            }
                        }, !0),
                        o.one("CHAT_STOP_RECEIVE_MSG", function() {
                            t.__IsStartRecv = !1
                        })),
                        this
                },
                stopListenMsg: function() {
                    return this.__IsStartRecv && (this.__IsStartRecv = !1,
                        this.messengerSender({
                            action: "CHAT_STOP_RECEIVE_MSG"
                        })),
                        this
                },
                getRealChatId: function(e) {
                    return n.checkParam(e, ["touid"]),
                        n.callbackHandler("CHAT_GET_REAL_ID", e),
                        this.messengerSender.call(this.Base, {
                            action: "CHAT_GET_REAL_ID",
                            result: {
                                touid: e.touid,
                                nocache: e.nocache,
                                groupid: e.groupid,
                                hasPrefix: !!e.hasPrefix
                            }
                        }, !0),
                        this
                },
                getUserStatus: function(e) {
                    if (n.checkParam(e, ["uids"]),
                        !n.isArray(e.uids))
                        throw new Error("uids必须为数组");
                    if (e.uids.length && e.uids.length > 30)
                        throw new Error("一次最多传30个用户id");
                    var t = "";
                    if (e.hasPrefix)
                        t = e.uids.join(";"),
                            i(t, e);
                    else {
                        if (!e.appkey)
                            throw new Error("需要appkey");
                        this.Base.getPrefixByAppkey({
                            appkey: e.appkey,
                            success: function(r) {
                                var n = r.data.prefix
                                    , o = [];
                                e.uids.forEach(function(e) {
                                    o.push(n + e)
                                }),
                                    t = o.join(";"),
                                    i(t, e)
                            },
                            error: function() {
                                e.error && e.error({
                                    code: 1002,
                                    resultText: s[1002]
                                })
                            }
                        })
                    }
                    return this
                }
            },
            e.WSDKMods.Chat = t
    }(this),
    function(e) {
        function t(e) {
            this.init(e)
        }
        var i = e.WSDKUtil;
        !e.WSDKMods && (e.WSDKMods = {}),
            t.prototype = {
                constructor: t,
                init: function(e) {
                    this.Base = e.Base,
                        this.messengerSender = e.Base.messengerSender
                },
                getHistory: function(e) {
                    return i.checkParam(e, ["tid"]),
                        i.callbackHandler("TRIBE_GET_HISTORY_MSG" + e.tid, e),
                        this.messengerSender.call(this.Base, {
                            action: "TRIBE_GET_HISTORY_MSG",
                            result: {
                                tid: e.tid + "",
                                count: e.count || 20,
                                nextkey: e.nextkey || "",
                                type: 2
                            }
                        }, !0),
                        this
                },
                getTribeInfo: function(e) {
                    return i.checkParam(e, ["tid"]),
                        i.callbackHandler("TRIBE_GET_INTO" + e.tid, e),
                        this.messengerSender.call(this.Base, {
                            action: "TRIBE_GET_INTO",
                            result: {
                                tid: parseInt(e.tid),
                                excludeFlag: e.excludeFlag || 0
                            }
                        }, !0),
                        this
                },
                getTribeList: function(e) {
                    return i.callbackHandler("TRIBE_GET_LIST", e),
                        this.messengerSender.call(this.Base, {
                            action: "TRIBE_GET_LIST",
                            result: {
                                tribeTypes: e.types || [0, 1, 2]
                            }
                        }, !0),
                        this
                },
                getTribeMembers: function(e) {
                    return i.checkParam(e, ["tid"]),
                        i.callbackHandler("TRIBE_GET_MEMBERS", e),
                        this.messengerSender.call(this.Base, {
                            action: "TRIBE_GET_MEMBERS",
                            result: {
                                tid: parseInt(e.tid)
                            }
                        }, !0),
                        this
                },
                responseInviteIntoTribe: function(e) {
                    return i.checkParam(e, ["tid", "validatecode", "manager", "recommender"]),
                        i.callbackHandler("TRIBE_RESPONSE_INVITE", e),
                        this.messengerSender.call(this.Base, {
                            action: "TRIBE_RESPONSE_INVITE",
                            result: {
                                tid: parseInt(e.tid),
                                recommender: e.recommender,
                                validatecode: e.validatecode,
                                manager: e.manager
                            }
                        }, !0),
                        this
                }
            },
            e.WSDKMods.Tribe = t
    }(this),
    function(e, t) {
        "use strict";
        "undefined" != typeof module && module.exports ? module.exports = t() : "function" == typeof define && (define.amd || define.cmd) ? define(t) : e.WSDK = t.call(e)
    }(this, function() {
        function e(t) {
            return this instanceof e ? this.init(t) : new e
        }
        var t = this.WSDKMods
            , i = this.WSDKCons
            , r = this.WSDKUtil
            , n = this.WSDKPlgs
            , o = i.PROJECT_NAME
            , a = i.CUSTOM_PAGE;
        return e.version = "2.2.7",
            e.prototype = {
                constructor: e,
                init: function(e) {
                    return this.__Messenger = new Messenger(a,o),
                        this.Event = r.Event,
                        this.Base = new t.Base(this,e),
                        this.Chat = new t.Chat(this),
                    t.Tribe && (this.Tribe = new t.Tribe(this)),
                        this.Plugin = {},
                        this.Plugin.Uploader = new n.Uploader(this),
                        this.Plugin.Image = new n.ImgUp(this),
                        this.Plugin.Emot = new n.Emot(this),
                        this
                },
                LoginInfo: {}
            },
            e
    });
