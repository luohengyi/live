$(document).ready(function () {
    ({
        _ui: {},
        lastWinWidth: 0,
        oCollapseStatus: {
            leftSide: 0,
            rightSide: 1
        },
        oBlockWidth: {
            leftSide: {
                close: 66,
                open: 230
            },
            rightSide: {
                close: 0,
                open: 340
            },
            playChatSpace: {
                close: 0,
                open: 20
            }
        },
        playerSize: {
            max: 1680,
            min: 790
        },
        spaceSize: {
            max: 102,
            min: 20
        },
        rightSideClosedByUse: !1,
        leftSideOpendByUser: !1,
        init: function () {
            yp.module.fRegistModule("oCommon", this),
                this.view(),
                this.bindEvent(),
                this.listenYpEvent(),
                this.fFixCurStatusAfterOpen(),
                this.fAutoScrollToPlayer()
        },
        view: function () {
            var e = this,
                i = e._ui;
            i.$win = $(window),
                i.$body = $("body"),
                i.$room_super_panel = $("#js-room-super-panel"),
                i.$room_main_layer = $("#js-room-main-layer"),
                i.$room_content_layer = $("#js-room-content-layer"),
                i.$js_right_chat_hide_btn = $("#js-right-chat-hide-btn"),
                i.$js_right_chat_show_btn = $("#js-right-chat-show-btn"),
                i.$room_module_area = $("#js-room-module-area"),
                i.$room_anchor_desc = i.$room_module_area.find(".js-room-anchor-desc-panel"),
                i.$js_right_chat_panel = $("#js-right-chat-panel"),
                i.$js_chat_msg_scroll = i.$js_right_chat_panel.find(".js-chat-msg-scroll"),
                i.$room_anchor_info_area = $("#js-room-anchor-info-area"),
                i.$js_flash_layer = $("#js-flash-layer"),
                i.$js_flash_panel = i.$js_flash_layer.find(".js-flash-panel"),
                i.$js_chat_layer = i.$js_right_chat_panel.find(".js-right-chat-layer"),
                i.$live_message = $(".liveMessage")
        },
        bindEvent: function () {
            var e = this,
                i = e._ui;
            i.$js_right_chat_hide_btn.on("click", function (i) {
                i.preventDefault(),
                    e.fChatListCloseAction(!0),
                    yp.pub("page/chatlist/collapse/click", {
                        bClose: !0
                    })
            }),
                i.$js_right_chat_show_btn.on("click", function (i) {
                    i.preventDefault(),
                        e.fChatListCloseAction(!1),
                        yp.pub("page/chatlist/collapse/click", {
                            bClose: !1
                        })
                })
        },
        listenYpEvent: function () {
            var e = this,
                i = e._ui;
            yp.sub("ui/resize", function () {
                e.fResizeDelay()
            }),
                yp.sub("page/follow/done", function () {
                    e.fSetRoomTitleSize()
                }),
                yp.sub("page/freebanner/got", function () {
                    e.fSetRoomTitleSize()
                }),
                yp.sub("page/leftpanel/collapse", function () {
                    e.oCollapseStatus.leftSide = i.$body.hasClass("open-left") ? 1 : 0
                }),
                yp.sub("page/chatlist/collapse", function (i, t) {
                    e.oCollapseStatus.rightSide = t.bClose ? 0 : 1
                }),
                yp.sub("page/leftpanel/collapse/click", function (i, t) {
                    if (e.leftSideOpendByUser = !t.bClose, t.bClose) {
                        var o = e.fGetCurPlayerMaxWidth(e.lastWinWidth);
                        if (e.leftSpaceWidth = e.spaceSize.max, e.rightSpaceWidth = e.spaceSize.max, e.playerWidth = e.lastWinWidth - e.oBlockWidth.leftSide.close - e.leftSpaceWidth - (0 == e.oCollapseStatus.rightSide ? e.oBlockWidth.rightSide.close + e.oBlockWidth.playChatSpace.close : e.oBlockWidth.rightSide.open + e.oBlockWidth.playChatSpace.open) - e.rightSpaceWidth, e.playerWidth < e.playerSize.min) {
                            var a = e.playerSize.min - e.playerWidth;
                            e.playerWidth = e.playerSize.min,
                                e.leftSpaceWidth = e.leftSpaceWidth - Math.ceil(a / 2),
                                e.rightSpaceWidth = e.rightSpaceWidth - Math.floor(a / 2)
                        }
                        e.playerWidth = Math.min(Math.max(e.playerWidth, e.playerSize.min), o)
                    } else 40 < e.rightSpaceWidth || 40 < e.leftSpaceWidth ? (e.playerWidth = e.playerWidth - (e.oBlockWidth.leftSide.open - e.oBlockWidth.leftSide.close - e.rightSpaceWidth - e.leftSpaceWidth + 80), e.leftSpaceWidth = 40, e.rightSpaceWidth = 40) : e.playerWidth = e.playerWidth - (e.oBlockWidth.leftSide.open - e.oBlockWidth.leftSide.close),
                        e.playerWidth = Math.max(e.playerSize.min, e.playerWidth);
                    e.fRenderWithSize()
                }),
                yp.sub("page/chatlist/collapse/click", function (t, o) {
                    if (e.rightSideClosedByUse = o.bClose, o.bClose) {
                        var a = e.fGetCurPlayerMaxWidth(e.lastWinWidth);
                        e.playerWidth = e.playerWidth + (e.oBlockWidth.rightSide.open - e.oBlockWidth.rightSide.close) + (e.oBlockWidth.playChatSpace.open - e.oBlockWidth.playChatSpace.close),
                            e.playerWidth = Math.min(Math.max(e.playerWidth, e.playerSize.min), a)
                    } else e.playerWidth = e.playerWidth - (e.oBlockWidth.rightSide.open - e.oBlockWidth.rightSide.close) - (e.oBlockWidth.playChatSpace.open - e.oBlockWidth.playChatSpace.close),
                        e.playerWidth = Math.max(e.playerSize.min, e.playerWidth);
                    i.$body.hasClass("theatre") || e.fSetRoomTitleSize(),
                        e.fRenderWithSize()
                }),
                yp.sub("page/theatre/mode", function (t, o) {
                    o ? i.$js_right_chat_show_btn.css({
                        "z-index": 14
                    }) : i.$js_right_chat_show_btn.css({
                        "z-index": 5
                    }),
                        e.fRenderWithSize(),
                        e.fMouseMoveOnPlayer(0 == e.oCollapseStatus.rightSide)
                }),
                yp.sub("page/room/chatsize/reset", function () {
                    e.fChatListResize()
                }),
                yp.sub("page/base/ready", function () {})
        },
        fChatListCloseAction: function (e) {
            var i = this,
                t = i._ui;
            t.$room_super_panel.toggleClass("close-right", e),
                t.$js_right_chat_panel.toggle(!e),
                yp.pub("page/chatlist/collapse", {
                    bClose: e
                }),
                i.fMouseMoveOnPlayer(e)
        },
        fFixCurStatusAfterOpen: function () {
            var e = this;
            e._ui;
            e.bDelay = !0;
            var i, t, o, a = e.fGetCurWinWidth(),
                l = e.oBlockWidth.leftSide.close,
                h = e.oBlockWidth.rightSide.open,
                s = e.oBlockWidth.playChatSpace.open,
                r = a - l - h - s;
            e.lastWinWidth = a,
                1420 <= a ? (i = e.spaceSize.max, t = e.spaceSize.max, o = r - i - t) : 1420 > a && 1256 <= a ? (o = e.playerSize.min, i = Math.ceil((r - o) / 2), t = Math.floor((r - o) / 2)) : (e.fChatListCloseAction(!0), h = e.oBlockWidth.rightSide.close, s = e.oBlockWidth.playChatSpace.close, r = a - l - h - s, i = e.spaceSize.min, t = e.spaceSize.min, o = Math.max(r - i - t, e.playerSize.min)),
                e.leftSpaceWidth = i,
                e.rightSpaceWidth = t,
                e.playerWidth = o,
                e.fRenderWithSize(),
                e.bDelay = !1
        },
        leftSpaceWidth: 0,
        rightSpaceWidth: 0,
        playerWidth: 0,
        fRenderWithSize: function () {
            var e = this,
                i = e._ui,
                t = e.playerWidth,
                o = (0 == e.oCollapseStatus.leftSide ? e.oBlockWidth.leftSide.close : e.oBlockWidth.leftSide.open, 0 == e.oCollapseStatus.rightSide ? e.oBlockWidth.rightSide.close : e.oBlockWidth.rightSide.open),
                a = 0 == e.oCollapseStatus.rightSide ? e.oBlockWidth.playChatSpace.close : e.oBlockWidth.playChatSpace.open,
                l = o + a + t,
                h = o + a + e.leftSpaceWidth + e.rightSpaceWidth + t;
            e.fFlashResize(),
            i.$body.hasClass("maxScreen") || (e.fLiveMessageResize(), i.$body.hasClass("theatre") ? i.$win.width() < 960 && i.$js_right_chat_show_btn.hide() : (e.fModuleHeightResize(h, l), e.fSetRoomTitleSize(), e.fAnchorDescImgAuto(l)), e.fChatListResize())
        },
        fSetRoomTitleSize: function () {
            var e = this,
                i = e._ui,
                t = i.$room_anchor_info_area.width(),
                o = i.$room_anchor_info_area.find(".img-box").outerWidth(!0),
                a = i.$room_anchor_info_area.find(".js-room-follow-area").outerWidth(!0),
                l = i.$room_anchor_info_area.find(".js-report").outerWidth(!0),
                h = i.$room_anchor_info_area.find("#js-now-rank-ze").outerWidth(!0),
                s = i.$room_anchor_info_area.find(".lanstory-gg").outerWidth(!0),
                r = t - o - a - l - h - s - 20;
            i.$room_anchor_info_area.find(".room-name").css({
                maxWidth: r,
                minWidth: "auto",
                width: "auto"
            })
        },
        fChatListResize: function () {
            var e, i = this,
                t = i._ui,
                o = t.$body.hasClass("theatre") ? t.$win.height() : t.$room_content_layer.height(),
                a = t.$js_right_chat_panel.find(".js-fans-list-panel").length ? t.$js_right_chat_panel.find(".js-fans-list-panel").outerHeight() : 0,
                l = t.$js_right_chat_panel.find(".js-chat-ads").length && t.$js_right_chat_panel.find(".js-chat-ads").is(":visible") ? t.$js_right_chat_panel.find(".js-chat-ads").outerHeight() : 0,
                h = t.$js_right_chat_panel.find(".js-chat-control").outerHeight();
            e = Math.max(o - a - l - h - 11, 0),
                t.$js_chat_layer.height(e),
                t.$js_chat_msg_scroll.add(t.$js_chat_msg_scroll.closest(".slimScrollDiv")).height(e)
        },
        fFlashResize: function () {
            var e, i = this,
                t = i._ui;
            if (t.$body.hasClass("maxScreen")) e = {
                width: "100%",
                height: "100%"
            };
            else if (t.$body.hasClass("theatre")) {
                var o = t.$win.width(),
                    a = t.$win.height();
                e = {
                    width: o > 960 && !t.$room_super_panel.hasClass("close-right") ? o - 340 + "px" : "100%",
                    height: o > 960 && !t.$room_super_panel.hasClass("close-right") ? a - 90 + "px" : "100%"
                }
            } else {
                var l = Math.floor(9 * i.playerWidth / 16);
                e = {
                    width: i.playerWidth,
                    height: l
                }
            }
            e.paddingTop = 0,
                t.$js_flash_panel.css(e),
                i.fPubPlayerResize()
        },
        fModuleHeightResize: function (e, i) {
            var t = this,
                o = t._ui;
            o.$room_main_layer.css({
                width: i,
                paddingLeft: 0,
                paddingRight: 0
            }),
                o.$room_content_layer.css({
                    width: t.playerWidth
                }),
                yp.pub("page/room/contentlayer/resize", {
                    width: t.playerWidth
                }),
                o.$room_super_panel.find(".live-room-layout-area").css({
                    width: e
                }).toggleClass("min-size", 900 >= t.playerWidth),
            o.$style_module_div || (o.$style_module_div = $('<div style="display:none;"></div>'), o.$body.append(o.$style_module_div));
            var a = Math.floor((i - 90) / 5) - 25,
                l = a,
                h = Math.floor(9 * a / 16),
                s = Math.floor(9 * l / 16),
                r = s + 54,
                n = Math.floor((i - 90) / 4) - 30,
                d = 6 * (n - 14) / 10,
                c = Math.floor(2 * d / 3),
                p = n - 14 - d,
                f = 97 < c ? "block;" : "none;",
                m = Math.floor((i - 90) / 4) - 30,
                g = 4 * (n + 28),
                _ = m / 73 * 49,
                S = _ / 16 * 9,
                W = m - _ - 12,
                y = 120 < S ? "block;" : "none;";
            o.$style_module_div.html('<style type="text/css">          .live-rooms-bottom .qniang-tj li, .live-rooms-bottom .hot-tg .bd li, .live-rooms-bottom .qniang-live .lives-list li{ width: ' + a + "px; }          .live-rooms-bottom .qniang-live .lives-list .li .img-box .time{ width: " + (a - 10) + "px; }          .live-rooms-bottom .qniang-live .lives-list li{ height: " + r + "px; }          .live-rooms-bottom .qniang-live .lives-list a, .lives-list .room-name{ width: " + l + "px; }          .live-rooms-bottom .qniang-tj .bd .img-box, .live-rooms-bottom .hot-tg .bd li{ height: " + h + "px; }          .live-rooms-bottom .qniang-live .lives-list .img-box{ height: " + s + "px; }          .live-rooms-bottom .qniang-tj .bd .room-name{ width: " + (a - 10) + "px; }          .live-rooms-bottom .qniang-tj .bd .meat .anchor{ width: " + (a - 26) + "px; }          .live-rooms-bottom .qniang-live .lives-list .meat .anchor{ max-width: " + (l - 75) + "px; }          .information-ul li{ width: " + n + "px; height: auto; }          .information-ul li .right-meat{ width: " + p + "px }          .information-ul .img-box{ width: " + d + "px; height: " + c + "px; }          .information-ul .right-meat .info{ display: " + f + "; }          .live-room-page .room-game-tj .bd {width: " + (g - 28) + "px}          .live-room-page .room-game-tj .bd .gallery{ width: " + g + "px; height: " + S + "px; }          .live-room-page .room-game-tj .bd .gallery .li {width: " + m + "px; height: auto;}          .live-room-page .room-game-tj .bd .gallery .li .img-box{ width: " + _ + "px; height: " + S + "px; }          .live-room-page .room-game-tj .bd .gallery .li .label-area{ width: " + W + "px ;  height: " + (S - 18) + "px;}          .live-room-page .room-game-tj .bd .gallery .li .label-area .label { display: " + y + ";}          </style>"),
                yp.pub("page/newgame/topic")
        },
        fLiveMessageResize: function () {
            var e = this,
                i = e._ui,
                t = i.$win.width(),
                o = t - 340 - 39;
            i.$body.hasClass("theatre") ? t > 960 && !i.$room_super_panel.hasClass("close-right") ? (i.$live_message.show(), i.$live_message.css({
                width: o
            }), yp.pub("page/room/contentlayer/resize", {
                width: o + 39
            }), i.$live_message.find(".js-room-fun-area-left").toggle(t > 1200)) : i.$live_message.hide() : (i.$live_message.show(), i.$live_message.css({
                width: ""
            }), i.$live_message.find(".js-room-fun-area-left").show())
        },
        fAnchorDescImgAuto: function (e) {
            var i = this,
                t = i._ui;
            if (!t.$room_anchor_desc.length) return !1;
            var o = (t.$room_anchor_desc.find(".js-anchor-desc-content"), e);
            t.$style_div || (t.$style_div = $('<div style="display:none;"></div>'), t.$body.append(t.$style_div)),
                t.$style_div.html('<style type="text/css">.live-room-page .anchor-details .bd img{ max-width: ' + o + "px !important; height: auto !important; }</style>")
        },
        bDelay: !1,
        delayCD: 0,
        fResizeDelay: function () {
            var e = this;
            e.bDelay || (e.bDelay = !0, e.delayCD = setTimeout(function () {
                e.fResizeStart()
            }, 200))
        },
        fResizeStart: function () {
            var e = this,
                i = e._ui,
                t = e.fGetCurWinWidth();
            if (e.lastWinWidth == t) return !i.$body.hasClass("maxScreen") && i.$body.hasClass("theatre") && (e.fFlashResize(), e.fChatListResize()),
                e.bDelay = !1,
                !1;
            var o = e.lastWinWidth - t;
            0 < o ? e.fWindowBecomeSmaller(o, t) : e.fWindowBecomeLarger(Math.abs(o), t),
                e.fRenderWithSize(),
                e.lastWinWidth = t,
                e.bDelay = !1
        },
        fWindowBecomeSmaller: function (e) {
            var i = this;
            if (0 < e) {
                if (i.playerSize.min < i.playerWidth) {
                    var t = i.playerWidth - e;
                    return i.playerSize.min > t ? (i.playerWidth = i.playerSize.min, e = i.playerWidth - t, i.fWindowBecomeSmaller(e)) : void(i.playerWidth = t)
                }
                if (i.spaceSize.min < i.leftSpaceWidth || i.spaceSize.min < i.rightSpaceWidth) {
                    var o = i.leftSpaceWidth - Math.floor(e / 2),
                        a = i.rightSpaceWidth - Math.ceil(e / 2);
                    return i.spaceSize.min > o || i.spaceSize.min > a ? (i.leftSpaceWidth = i.spaceSize.min, i.rightSpaceWidth = i.spaceSize.min, e = i.leftSpaceWidth + i.rightSpaceWidth - a - o, i.fWindowBecomeSmaller(e)) : (i.leftSpaceWidth = o, void(i.rightSpaceWidth = a))
                }
                if (1 == i.oCollapseStatus.leftSide) return yp.pub("page/collapse/left", {
                    collapse: !0
                }),
                    i.playerWidth = i.playerWidth + (i.oBlockWidth.leftSide.open - i.oBlockWidth.leftSide.close),
                    i.fWindowBecomeSmaller(e);
                if (1 == i.oCollapseStatus.rightSide) return i.fChatListCloseAction(!0),
                    i.playerWidth = i.playerWidth + (i.oBlockWidth.rightSide.open - i.oBlockWidth.rightSide.close) + (i.oBlockWidth.playChatSpace.open - i.oBlockWidth.playChatSpace.close),
                    i.fWindowBecomeSmaller(e)
            }
        },
        index: 1,
        fWindowBecomeLarger: function (e, i) {
            var t = this,
                o = t.playerSize.min + (t.oBlockWidth.rightSide.open - t.oBlockWidth.rightSide.close) + (t.oBlockWidth.playChatSpace.open - t.oBlockWidth.playChatSpace.close),
                a = t.playerSize.min + t.oBlockWidth.leftSide.open - t.oBlockWidth.leftSide.close,
                l = 0 == t.oCollapseStatus.rightSide && !t.rightSideClosedByUse,
                h = 0 == t.oCollapseStatus.leftSide && t.leftSideOpendByUser,
                s = t.fGetCurPlayerMaxWidth(i);
            if (l && o <= t.playerWidth) return t.fChatListCloseAction(!1),
                t.playerWidth = t.playerWidth - (t.oBlockWidth.rightSide.open - t.oBlockWidth.rightSide.close) - (t.oBlockWidth.playChatSpace.open - t.oBlockWidth.playChatSpace.close),
                t.fWindowBecomeLarger(e, i);
            if (h && a <= t.playerWidth) return yp.pub("page/collapse/left", {
                collapse: !1
            }),
                t.playerWidth = t.playerWidth - (t.oBlockWidth.leftSide.open - t.oBlockWidth.leftSide.close),
                t.fWindowBecomeLarger(e, i);
            if (0 < e) {
                if (l || h) {
                    var r = t.playerWidth + e;
                    if (t.playerWidth = l && o <= r ? o : h && a <= r ? a : r, t.playerWidth <= r) return e = r - t.playerWidth,
                        t.fWindowBecomeLarger(e, i);
                    return
                }
                if (0 < e && (t.spaceSize.max > t.leftSpaceWidth || t.spaceSize.max > t.rightSpaceWidth)) {
                    var n = t.leftSpaceWidth + Math.ceil(e / 2),
                        d = t.rightSpaceWidth + Math.floor(e / 2);
                    return t.spaceSize.max < n || t.spaceSize.max < d ? (t.leftSpaceWidth = t.spaceSize.max, t.rightSpaceWidth = t.spaceSize.max, e = d + n - (t.leftSpaceWidth + t.rightSpaceWidth), t.fWindowBecomeLarger(e, i)) : (t.leftSpaceWidth = n, void(t.rightSpaceWidth = d))
                }
                if (s > t.playerWidth) {
                    var r = Math.min(t.playerWidth + e, s);
                    t.playerWidth = r
                }
            }
        },
        fGetCurPlayerMaxWidth: function (e) {
            var i = this;
            e = e || i.lastWinWidth;
            var t = 1 == i.oCollapseStatus.leftSide ? i.oBlockWidth.leftSide.open : i.oBlockWidth.leftSide.close,
                o = 1 == i.oCollapseStatus.rightSide ? i.oBlockWidth.rightSide.open : i.oBlockWidth.rightSide.close,
                a = 1 == i.oCollapseStatus.rightSide ? i.oBlockWidth.playChatSpace.open : i.oBlockWidth.playChatSpace.close;
            return Math.max(e - i.rightSpaceWidth - i.leftSpaceWidth - o - a - t, i.playerSize.min)
        },
        fGetCurWinWidth: function () {
            var e = this,
                i = e._ui;
            return Math.min(Math.max(i.$win.width(), 896), 1920)
        },
        showBtnHideCD: 0,
        fMouseMoveOnPlayer: function (e) {
            var i = this,
                t = i._ui;
            i.showBtnHideCD && clearTimeout(i.showBtnHideCD),
                t.$js_right_chat_show_btn.toggle(e),
                t.$js_right_chat_show_btn.off("mouseenter.check").off("mouseleave.check"),
                t.$js_flash_panel.off("mousemove.check"),
            e && t.$body.hasClass("theatre") && (i.fHideShowBtn(), t.$js_right_chat_show_btn.on("mouseenter.check", function () {
                i.showBtnHideCD && clearTimeout(i.showBtnHideCD)
            }).on("mouseleave.check", function () {
                i.fHideShowBtn()
            }), t.$js_flash_panel.on("mousemove.check", function (e) {
                var o = e.pageX,
                    a = e.pageY,
                    l = t.$js_flash_panel.offset(),
                    h = t.$js_flash_panel.width(),
                    s = t.$js_flash_panel.height(),
                    r = {
                        from: h - 50,
                        to: h
                    },
                    n = {
                        from: Math.floor(s / 2) - 70,
                        to: Math.floor(s / 2) + 70
                    };
                o -= l.left,
                    a -= l.top,
                !t.$js_right_chat_show_btn.is(":visible") && r.from <= o && r.to >= o && n.from <= a && n.to >= a && i.lastWinWidth >= 960 && (t.$js_right_chat_show_btn.show(), i.showBtnHideCD && clearTimeout(i.showBtnHideCD), i.fHideShowBtn())
            }))
        },
        fHideShowBtn: function () {
            var e = this,
                i = e._ui;
            e.showBtnHideCD = setTimeout(function () {
                i.$js_right_chat_show_btn.hide()
            }, 3e3)
        },
        fPubPlayerResize: function () {
            var e = this,
                i = e._ui;
            yp.pub("page/room/player/resize", {
                width: i.$js_flash_panel.width(),
                height: i.$js_flash_panel.height()
            })
        },
        fAutoScrollToPlayer: function () {
            var e = this,
                i = e._ui,
                t = i.$room_main_layer.offset().top;
            t > 80 && setTimeout(function () {
                $("html, body").animate({
                    scrollTop: t - 80
                })
            }, 2e3)
        }
    }).init()
}),
    define("j/web4.0/room/roomSize", function () {});