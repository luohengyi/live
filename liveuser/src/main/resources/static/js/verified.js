yp.ready(function() {
    var a = {
        $choose_verify_area: $("#js-choose-verify-area"),
        $manual_certification_area: $("#js-manual-certification-area"),
        $verify_success_area: $("#js-verify-success-area"),
        $verify_fail_area: $("#js-verify-fail-area"),
        $review_area: $("#js-review-area"),
        $jump_btn: $(".js-jump-btn")
    };
    ({
        data: {
            idCardFront: "",
            idCardBack: "",
            idCardInOne: "",
            isDomestic: 1
        },
        init: function() {
            this.bindEvent(),
                this.fCertQuery()
        },
        fInitialize: function(e) {
            var i = this;
            if (e) {
                a.$manual_certification_area.find(".js-name-idcart-input").val("").data("isok", !1);
                var t = a.$manual_certification_area.find(".js-identity-card-view");
                t.find("input").val(""),
                    t.find("img").attr("src", "").hide(),
                    t.find("a.upload-btn-1").html("点击上传"),
                    i.fUploadIdetityCard("front"),
                    i.fUploadIdetityCard("end"),
                    i.fUploadIdetityCard("back"),
                    i.data.idCardFront = "",
                    i.data.idCardBack = "",
                    i.data.idCardInOne = "",
                    i.data.isDomestic = 1
            } else
                a.$choose_verify_area.find(".js-name-idcart-input").val("").data("isok", !1),
                    a.$choose_verify_area.find("#js-img-ibox").html(),
                    a.$choose_verify_area.find(".js-code-area").addClass("hidden")
        },
        fCertQuery: function() {
            yp.ajax({
                url: "/api/user/cert_query",
                type: "get",
                dataType: "json",
                data: {
                    _v: Math.floor($.now() / 1e3)
                }
            }).done(function(e) {
                if (0 == e.code)
                    switch (e.data.passed) {
                        case 0:
                            a.$verify_success_area.removeClass("hidden");
                            break;
                        case 1:
                            a.$review_area.removeClass("hidden");
                            break;
                        case 2:
                            a.$verify_fail_area.removeClass("hidden").find(".js-fail-reason").html("未通过原因：" + e.data.remark);
                            break;
                        default:
                            a.$choose_verify_area.removeClass("hidden")
                    }
            })
        },
        bindEvent: function() {
            var e = this
                , i = {
                normal: e.fCheckNormal,
                identityCard: e.fCheckIdentityCard
            }
                , t = a.$manual_certification_area.find(".js-name-idcart-input")
                , r = a.$choose_verify_area.find(".js-name-idcart-input");
            t.on("change", function() {
                var a, e = $(this);
                a = i[e.data("type")](e),
                    e.data("isok", a)
            }),
                r.on("change", function() {
                    var a, t = $(this);
                    a = 1 != e.data.isDomestic || i[t.data("type")](t),
                        t.data("isok", a)
                }),
                a.$manual_certification_area.on("click", ".js-submit-btn", function(a) {
                    a.preventDefault(),
                        e.fManualFormSubmit($(this))
                }).on("click", ".js-radio", function(a) {
                    var i = $(this).data("type");
                    e.data.isDomestic = i
                }),
                a.$choose_verify_area.on("click", ".js-submit-btn", function(a) {
                    a.preventDefault(),
                        e.fVerifyFormSubmit($(this))
                }),
                a.$jump_btn.on("click", function(i) {
                    i.preventDefault();
                    var t = $(this).data("type")
                        , r = "manualReview" == t;
                    $(this).data("style") && "agin" == $(this).data("style") && a.$verify_fail_area.addClass("hidden"),
                        a.$manual_certification_area.toggleClass("hidden", !r),
                        a.$choose_verify_area.toggleClass("hidden", r),
                        e.fInitialize(r)
                })
        },
        fUploadIdetityCard: function(a) {
            var e = this
                , i = !1;
            $("#js-upload-identity-card-" + a + "-input").off("change.upload").on("change.upload", function() {
                if (i)
                    return !1;
                i = !0,
                    $.ajaxFileUpload({
                        url: "/api/user/widget.upload",
                        fileElementId: "js-upload-identity-card-" + a + "-input",
                        dataType: "json",
                        secureuri: !1,
                        success: function(t) {
                            if (i = !1,
                            0 == t.code) {
                                var r = $('.js-identity-card-view[data-type="' + a + '"]');
                                r.find("input:hidden").val(t.data.file),
                                    r.find("img").attr("src", t.data.file).addClass("js-idcard-" + a + "-img").show(),
                                    "front" == a ? e.data.idCardFront = t.data.file : "back" == a ? e.data.idCardBack = t.data.file : e.data.idCardInOne = t.data.file,
                                    r.find("a.upload-btn-1").html("重新上传")
                            } else
                                t.message && yp.errorNotify(t.message)
                        },
                        error: function() {
                            i = !1,
                                e.fUploadIdetityCard(a)
                        }
                    })
            })
        },
        fCheckNormal: function(a) {
            var e = a.val();
            return !("" == e)
        },
        fCheckIdentityCard: function(a) {
            var e = a.val()
                , i = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}(\d|x|X)$/;
            return !("" == e || !i.test(e))
        },
        fManualFormSubmit: function(e) {
            var i = this
                , t = ""
                , r = ""
                , d = !0
                , n = a.$manual_certification_area.find(".js-name-idcart-input");
            if (e.data("ajaxing", !1),
                e.data("ajaxing"))
                return !1;
            if (yp.each(n, function(a) {
                var e = $(a);
                "normal" == e.data("type") ? e.data("isok") ? t = a.value : d = !1 : 1 == i.data.isDomestic ? e.data("isok") ? r = a.value : d = !1 : a.value ? r = a.value : d = !1
            }),
                !d) {
                if (!t)
                    return yp.errorNotify("请输入您的真实姓名"),
                        !1;
                if (!r)
                    return yp.errorNotify("请输入正确的身份证号码"),
                        !1
            }
            if (!i.data.idCardFront || !i.data.idCardBack || !i.data.idCardInOne)
                return yp.errorNotify("请上传身份证照片"),
                    !1;
            e.data("ajaxing", !0),
                yp.ajax({
                    url: "/api/user/cert_data",
                    type: "post",
                    dataType: "json",
                    data: {
                        _v: Math.floor($.now() / 1e3),
                        certName: t,
                        certNo: r,
                        certFront: i.data.idCardFront,
                        certBack: i.data.idCardBack,
                        certInOne: i.data.idCardInOne,
                        certType: i.data.isDomestic
                    }
                }).done(function(i) {
                    e.data("ajaxing", !1),
                        0 == i.code ? (a.$review_area.removeClass("hidden"),
                            a.$manual_certification_area.addClass("hidden"),
                            $("html , body").animate({
                                scrollTop: 0
                            }, "normal")) : i.message && yp.errorNotify(i.message)
                }).always(function() {
                    e.data("ajaxing", !1)
                })
        },
        fVerifyFormSubmit: function(e) {
            var i = this
                , t = ""
                , r = ""
                , d = !0
                , n = a.$choose_verify_area.find(".js-name-idcart-input");
            if (e.data("ajaxing", !1),
                e.data("ajaxing"))
                return !1;
            if (yp.each(n, function(a) {
                var e = $(a);
                e.data("isok") ? "normal" == e.data("type") ? t = a.value : r = a.value : d = !1
            }),
                !d) {
                if (!t)
                    return yp.errorNotify("请输入您的真实姓名"),
                        !1;
                if (!r)
                    return yp.errorNotify("请输入正确的身份证号码"),
                        !1
            }
            e.data("ajaxing", !0),
                yp.ajax({
                    url: "/api/user/cert_url?platform=pc",
                    type: "post",
                    dataType: "json",
                    data: {
                        _v: Math.floor($.now() / 1e3),
                        idCardName: t,
                        idCardNo: r
                    }
                }).done(function(t) {
                    e.data("ajaxing", !1),
                        0 == t.code ? ($("#js-img-ibox").html(""),
                            i.makeShareCode("js-img-ibox", 157, t.data.certUrl),
                            a.$choose_verify_area.find(".js-code-area").removeClass("hidden")) : t.message && yp.errorNotify(t.message)
                }).always(function() {
                    e.data("ajaxing", !1)
                })
        },
        makeShareCode: function(a, e, i) {
            var t = this;
            t._ui;
            yp.use("makeQrcode", function(t, r) {
                new QRCode(a,{
                    width: e,
                    height: e,
                    correctLevel: 0
                }).makeCode(i)
            })
        }
    }).init()
}),
    define("j/web4.0/usercenter/user/verified", function() {});
