var oldAjax =$.ajax;

$(function () {
    var ajax = $.ajax;
//特殊行情况调用ajax

    var _node=null;
//  修改ajax方法的默认实现
    $.ajax = function (options) {

        var success = options.success;

        //  对用户配置的success方法进行代理
        function _success(datas) {
            // datas =JSON.parse(datas);
            console.log(datas);
            //此处实现响应拦截
            switch (datas.code) {
                case 500:
                    layui.use('layer', function () {
                        var layer = layui.layer;
                        layer.msg(datas.error);
                    });
                    break;
                case 200:
                    layui.use('layer', function () {
                        var layer = layui.layer;
                        layer.open({
                            type: 1
                            // ,offset: type //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
                            // ,id: 'layerDemo'+type //防止重复弹出
                            , content: '<div style="padding: 20px 100px;">' + datas.data + '</div>'
                            , btn: '确定'
                            , btnAlign: 'c' //按钮居中
                            , shade: 0 //不显示遮罩
                            , yes: function () {
                                if (datas.url !== '' && datas.url != null) {
                                    location.href = datas.url;
                                }
                            }
                        });
                    });
                    break;
                case 201: //跳转指定列表
                    location.href = datas.url;
                    break;
                case 202:
                    layui.use('layer', function () {
                        var layer = layui.layer;
                        layer.open({
                            type: 1
                            // ,offset: type //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
                            // ,id: 'layerDemo'+type //防止重复弹出
                            , content: '<div style="padding: 20px 100px;">' + datas.data + '</div>'
                            , btn: '确定'
                            , btnAlign: 'c' //按钮居中
                            , shade: 0 //不显示遮罩
                            , yes: function () {
                                location.reload();
                            }
                        });
                    });
                    break;

            }

            //使用节点上 success 属性 去回调此方法
            if (null!=_node) {
                var _back = _node.attr('success');
                if (_back) {
                    eval(_back + '()');
                }
            }


            return success(datas);
        }

        //  代理嵌入调用
        options.success = _success;
        return ajax(options);
    }


//表单的提交
    $('.form').submit(function () {
        _node = $(this);
        // var formData = new FormData($('form').get(0));
        $.ajax({
            url: _node.attr('action'),
            type: _node.attr('method'),
            data: _node.serialize(),
            dataType: "json",
            // processData: false,
            // contentType: false,
            success: function (data) {

            }
        }, _node)
        return false;
    })
//a链接发起ajax请求
    $('.ajax').click(function () {
        var _this = $(this);
        _node = $(this);
        $.ajax({
            url: _this.attr('href'),
            type: 'post',
            dataType:'json',
            success: function () {
                console("AJAX返回")
            }
        })
        return false;
    })

//弹窗的封装


})