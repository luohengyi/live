package com.rimi.live.util;

import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class UploadUtil {

    /**
     * 上传失败，返回一个null，成功返回url
     * @param file
     * @return
     */
    public static String uploading(MultipartFile file) {
        System.out.println("++++++++++++++++++");
        StringBuffer relativeUrl = new StringBuffer();
        File servicefile = null;
        try {
            System.out.println(ClassUtils.getDefaultClassLoader().getResource("").getPath());
            String staticUrl = ClassUtils.getDefaultClassLoader().getResource("").getPath() + "static/upload/";
            String[] split = file.getOriginalFilename().split("\\.");
            String suffix = split[split.length - 1];
            if (!suffix.equals("jpg") && !suffix.equals("png")  && !suffix.equals("jpeg")) {
                return null;
            }
            UUID uuid = UUID.randomUUID();
            String uuidString = uuid.toString();
            System.out.println("文件上传路"+staticUrl+uuidString+"."+suffix);
            StringBuilder url = new StringBuilder(ClassUtils.getDefaultClassLoader().getResource("").getPath())
                    .append("static").append(File.separator).append("upload")
                    .append(File.separator).append(uuidString).append(".").append(suffix);
            //文件写入服务器
            servicefile = new File(url.toString());

            file.transferTo(servicefile);
            //文件上传成功,生成文件的相对路径
            String separator = File.separator;
            //高贵的MAC系统执行else条件不用做改变
//             if(separator.equals("\\")){
//                 relativeUrl.append(separator).append(separator).append("upload")
//                         .append(separator).append(separator).append(uuidString).append(".").append(suffix);
//             }else {
                 relativeUrl.append(separator).append("upload")
                         .append(separator).append(uuidString).append(".").append(suffix);
//             }


        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("文件上传失败！");
            //文件上传失败！
            return null;
        }
        return relativeUrl.toString();
    }
}
