package com.rimi.live.controller;

import com.rimi.live.bean.LiveKind;
import com.rimi.live.bean.Page;
import com.rimi.live.service.LiveKindsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author rimi
 * @date 2019-02-19
 */
@Controller
@RequestMapping("/livekinds")
public class LiveKindsController {

    @Autowired
    private LiveKindsService liveKindsService;
    private MultipartFile file;
    private String neme;


    /**
     * @return
     *
     */
    @RequestMapping("/index")
    public String index(ModelMap modelMap , Page page , LiveKind liveKind){
        List<LiveKind> liveKinds = liveKindsService.getAllLiveKinds(page , liveKind);
        modelMap.addAttribute("livekinds",liveKinds);
        modelMap.addAttribute("page" , page);
        return "livekinds/index";
    }

    /**
     * @return
     * 添加页面
     */
    @RequestMapping("/add")
    public String add(){
        return "livekinds/add";
    }


    /**
     * @return
     * 添加的方法
     */
    @RequestMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam(value="imgfile")MultipartFile file,LiveKind liveKind){

        liveKindsService.insertLiveKinds(file,liveKind);
        return liveKindsService.getResponseSave();
    }

    /**
     * @return
     * 修改页面
     */
    @RequestMapping("/edit")
    public String edit(@Validated(value={LiveKind.Update.class})LiveKind liveKind , ModelMap modelMap){
     modelMap.addAttribute("livekinds", liveKindsService.getLiveKindsByName(liveKind).get(0));

        return "livekinds/edit";
    }

    /**
     * @return
     * 修改方法
     */
    @RequestMapping("/update")
    @ResponseBody
    public String update(@RequestParam(value="imgfile")MultipartFile file,LiveKind liveKind) {
        liveKindsService.updateLiveKinds(file,liveKind);
        return liveKindsService.getResponseSave();
    }

    /**
     * @return
     * 删除方法
     */
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@Validated(value={LiveKind.Update.class}) LiveKind liveKind){
        liveKindsService.delete(liveKind);
        return liveKindsService.getResponseDelete();
    }

}
