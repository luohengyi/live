package com.rimi.live.controller;

import com.alibaba.fastjson.JSON;
import com.rimi.live.bean.Page;
import com.rimi.live.bean.Role;
import com.rimi.live.service.RoleService;
import com.rimi.live.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author luohengyi
 * 角色模块控制器
 */
@Controller
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleServer;

    @Autowired
    private RuleService ruleService;

    @RequestMapping("/index")
    public String index(ModelMap map, Page page, Role role) {
        List<Role> allRole = roleServer.getAllRole(page, role);
        map.addAttribute("lists", allRole);
        map.addAttribute("page", page);
        return "role/index";
    }


    @RequestMapping("/add")
    public String add(ModelMap modelMap) {


        return "role/add";
    }

    @RequestMapping("/insert")
    @ResponseBody
    public String insert(@Validated(value = {Role.Insert.class}) Role role) {
        roleServer.insertRole(role);
        return roleServer.getResponseSave();
    }

    @RequestMapping("/edit")
    public String edit(@Validated(value = {Role.Update.class}) Role role, ModelMap modelMap) {

        modelMap.addAttribute("role", roleServer.getRoleById(role).get(0));
        return "role/edit";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String update(@Validated(value = {Role.Update.class, Role.Insert.class}) Role role) {
        roleServer.updateRole(role);
        return roleServer.getResponseSave();
    }


    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@Validated(value = {Role.Update.class}) Role role) {
        roleServer.delete(role);
        return roleServer.getResponseDelete();
    }

    /**
     * 授权页面
     *
     * @return
     */
    @RequestMapping("/accredit")
    public String accredit(ModelMap modelMap, Integer id) {

        //所有的权限接口
        modelMap.addAttribute("menuJson", JSON.toJSON(ruleService.getAllRuleShow()));

        //角色的权限
        Role role = new Role();
        role.setId(id);
        role = roleServer.getRoleById(role).get(0);

        modelMap.addAttribute("role",role);

        return "role/accredit";
    }

    @RequestMapping("/updateRole")
    @ResponseBody
    public String updateRole(Integer[] ids, Integer id) {

        String Rule = JSON.toJSON(ids).toString();

        roleServer.authorization(ids, id);

        return roleServer.getResponseSave();
    }


    public String change() {
        return null;
    }

}
