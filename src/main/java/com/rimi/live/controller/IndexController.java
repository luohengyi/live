package com.rimi.live.controller;


//import com.rimi.live.mapper.AdminMapper;
import com.rimi.live.service.IndexService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.function.SupplierUtils;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author luohengyi
 */
@Controller
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private IndexService indexService;

    @RequestMapping("/index")
    public String index() {
        System.out.println(indexService.getAllUsers());
        System.out.println(1111112312);
        return "index/index";
    }
    @RequestMapping("/out")
    public String out() {
        Subject subject = SecurityUtils.getSubject();
        if (null!=subject){
            subject.logout();
        }
        return "login/index";
    }


}
