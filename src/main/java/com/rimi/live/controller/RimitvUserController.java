package com.rimi.live.controller;


import com.rimi.live.bean.Page;
import com.rimi.live.bean.RimitvUser;
import com.rimi.live.service.RimitvUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
/**
 * 用户控制器
 *
 * @author rimi
 * @date 2019-02-15
 */
@Controller
@RequestMapping("/rimitvuser")
public class RimitvUserController {

    @Autowired
    private RimitvUserService rimitvUserServer;
    @RequestMapping("/index")
    public String index(ModelMap map, Page page,RimitvUser rimitvUser){
        List<RimitvUser> allRimitvUser=rimitvUserServer.getAllRimitnUsers(page,rimitvUser);
        map.addAttribute("rimitvuser",allRimitvUser);
        map.addAttribute("page",page);
        return  "rimitvuser/index";
    }
    /**
     * @return
     * 添加页面
     */
    @RequestMapping("/add")
    public String add(ModelMap map){
        return  "rimitvuser/add";
    }


    /**
     *
     * @return
     * 添加管理员
     */

    @RequestMapping("/insert")
    @ResponseBody
    public  String insert(@Validated(value = {RimitvUser.Insert.class}) RimitvUser rimitvUser){
        rimitvUserServer.insertRimitvUser(rimitvUser);
        return  rimitvUserServer.getResponseSave();
    }

    @RequestMapping("/edit")
    public String edit(@Validated(value = {RimitvUser.Update.class})RimitvUser rimitvUser,ModelMap map) {
        map.addAttribute("rimitvuser",rimitvUserServer.getAllRimitnUserById(rimitvUser).get(0));

        return "rimitvuser/edit";
    }

    /**
     * @return
     * 修改方法
     */
    @RequestMapping("/update")
    @ResponseBody
    public String update(@Validated(value = {RimitvUser.Update.class,RimitvUser.Insert.class})RimitvUser rimitvUser) {
        rimitvUserServer.updateRimitvUser(rimitvUser);
        return rimitvUserServer.getResponseSave();
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@Validated(value = {RimitvUser.Update.class})RimitvUser rimitvUser) {
        rimitvUserServer.delete(rimitvUser);

        return rimitvUserServer.getResponseDelete();
    }


    public String change() {
        return null;
    }



}
