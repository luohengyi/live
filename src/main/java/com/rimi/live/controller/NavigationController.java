package com.rimi.live.controller;

import com.rimi.live.bean.Navigation;
import com.rimi.live.service.NavigationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author luohengyi
 * 导航栏
 */
@Controller
@RequestMapping("/navigation")
public class NavigationController {
    @Autowired
    private NavigationService navigationService;

    @RequestMapping("/index")
//    @ResponseBody
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("list",navigationService.getLisetPage());

        return "navigation/index";
    }


    @RequestMapping("/add")
    public String add() {

        return "navigation/add";
    }

    @RequestMapping("/insert")
    @ResponseBody
    public String insert(@Validated(value = {Navigation.Insert.class}) Navigation navigation) {
        navigationService.insertNavigation(navigation);
       return navigationService.getResponseSave();
    }

    @RequestMapping("/edit")
    public String edit(@Validated(value = {Navigation.Update.class}) Navigation navigation,ModelMap modelMap) {
        modelMap.addAttribute("navigation",navigationService.getNavigationById(navigation).get(0));
        return "navigation/edit";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String update(@Validated(value = {Navigation.Insert.class,Navigation.Update.class}) Navigation navigation) {
        navigationService.update(navigation);
        return navigationService.getResponseSave();
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@Validated(value = {Navigation.Update.class}) Navigation navigation) {
        navigationService.deteleById(navigation);
        return navigationService.getResponseDelete();
    }


    public String change() {
        return null;
    }
}
