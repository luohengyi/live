package com.rimi.live.controller;

import com.rimi.live.bean.AnchorPuls;
import com.rimi.live.service.TestService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class TestCon {
    @Resource
    private TestService testService;

    @RequestMapping("/test")
    public  String test(){
        List<AnchorPuls> test = testService.test();
        System.out.println(test);
        System.out.println("XXXXXXXXXXX");
        for (AnchorPuls anchorPuls : test) {
            Integer id = anchorPuls.getId();
            System.out.println("----------------------");
            System.out.println(id);
        }
        return "index/index";
    }
}
