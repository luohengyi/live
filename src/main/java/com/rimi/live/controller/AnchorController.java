package com.rimi.live.controller;

import com.rimi.live.bean.*;
import com.rimi.live.service.AnchorService;
import com.rimi.live.service.LiveKindsService;
import com.rimi.live.vo.AnchorQvo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/anchor")
public class AnchorController {

    @Resource
    private AnchorService anchorService;
    @Resource
    private LiveKindsService liveKindsService;

    @RequestMapping("/index")
        public String index(ModelMap map, Page page, AnchorPuls anchorPuls){

        System.out.println(anchorPuls);
        AnchorQvo anchorQvo = new AnchorQvo();
        anchorQvo.setAnchorPuls(anchorPuls);
        List<AnchorPuls> anchorPuls1 = anchorService.getAnchorPuls(page, anchorQvo);

        System.out.println("主页查询结果：++++++++++++++");
        for (AnchorPuls puls : anchorPuls1) {
            System.out.println(puls);
        }
        map.addAttribute("anchors", anchorPuls1);
        map.addAttribute("page", page);

        return "anchor/index";
    }

    /**
     * @return
     * 添加页面
     */
    @RequestMapping("/add")
    public String add(){
        return "admin/add";
    }

    /**
     * @param admin
     * @return
     * 添加管理员
     */
    @RequestMapping("/insert")
    @ResponseBody
    public String insert(@Validated Admin admin){
       return "";
    }


    public String edit() {
        return null;
    }


    public String update() {
        return null;
    }



    public String delete() {
        return null;
    }

    //修改主播状态.
    @RequestMapping("/change")
    @ResponseBody
    public String change(Integer flag,AnchorPuls anchorPuls  ) {
        System.out.println(flag);
        System.out.println(anchorPuls);
        anchorPuls.setFlag(flag);
        anchorService.changeAnchorFlag(flag,anchorPuls);
        return anchorService.getResponseSave();

    }
    @RequestMapping("/show")
    public  String show(Model model ,AnchorPuls anchor){

        System.out.println("查询ID"+
             anchor.getUserId()
        );

        AnchorPuls getanchorByID = anchorService.getAnchorByID(anchor);
        System.out.println("通过ID查询结果"+getanchorByID);

        Page page = new Page(1);


        List<LiveKind> allLiveKinds = liveKindsService.getAllLiveKinds(page,new LiveKind());
        for (LiveKind allLiveKind : allLiveKinds) {

        }
        model.addAttribute("livekind",allLiveKinds);
        model.addAttribute("anchor",getanchorByID);
        return "/anchor/show";
    }


    @RequestMapping("/update")
    @ResponseBody
    public String update(@RequestParam("fileimg1") MultipartFile file1,AnchorPuls anchorPuls,@RequestParam("fileimg2") MultipartFile file2) {

        System.out.println("修改啊秀嘎");
        anchorService.update(file1,file2,anchorPuls);
        return anchorService.getResponseSave();
    }

}
