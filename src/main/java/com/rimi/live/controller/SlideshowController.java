package com.rimi.live.controller;

import com.rimi.live.bean.Slideshow;
import com.rimi.live.service.CommonService;
import com.rimi.live.service.SlideshowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author luohengyi
 */
@Controller
@RequestMapping("/slideshow")
public class SlideshowController {
    @Autowired
    private SlideshowService slideshowService;

    @RequestMapping("/index")
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("list", slideshowService.getAll());
        return "slideshow/index";
    }

    @RequestMapping("/add")
    public String add() {
        return "slideshow/add";
    }

    @RequestMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam("fileimg") MultipartFile file, Slideshow slideshow) {
        slideshowService.insertSlideshow(file, slideshow);
        return slideshowService.getResponseSave();
    }

    @RequestMapping("/edit")
    public String edit( Slideshow slideshow,ModelMap modelMap) {
        modelMap.addAttribute("data", slideshowService.getSlideshowById(slideshow).get(0));
        return "slideshow/edit";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String update(@RequestParam("fileimg") MultipartFile file, @Validated(value = {Slideshow.Update.class}) Slideshow slideshow,String oldUrl) {

        slideshowService.update(file,slideshow,oldUrl);
        return slideshowService.getResponseSave();
    }
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@Validated(value = {Slideshow.Update.class}) Slideshow slideshow) {

        slideshowService.delete(slideshow);
        return slideshowService.getResponseDelete();
    }


    public String change() {
        return null;
    }
}
