package com.rimi.live.controller;

import com.rimi.live.bean.Admin;
import com.rimi.live.bean.Page;
import com.rimi.live.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 管理员控制器
 *
 * @author rimi
 * @date 2019-02-15
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping("/index")
    public String index(ModelMap map, Page page ,Admin admin){
        List<Admin> allAdmins= adminService.getAllAdmins(page , admin);
        map.addAttribute("admins",allAdmins);
        map.addAttribute("page",page); 
        return "admin/index";
    }

    /**
     * @return
     * 添加页面
     */
    @RequestMapping("/add")
    public String add(){
        return "admin/add";
    }

    /**
     * @param admin
     * @return
     * 添加管理员
     */
    @RequestMapping("/insert")
    @ResponseBody
    public String insert(@Validated(value = {Admin.Insert.class}) Admin admin){
        adminService.insertAdmin(admin);
        return adminService.getResponseSave();
    }

    @RequestMapping("/edit")
    public String edit(@Validated(value = {Admin.Update.class})Admin admin,ModelMap map) {
        map.addAttribute("admin", adminService.getAdminByName(admin).get(0));

        return "admin/edit";
    }


    /**
     * @return
     * 修改方法
     */
    @RequestMapping("/update")
    @ResponseBody
    public String update(@Validated(value = {Admin.Update.class,Admin.Insert.class}) Admin admin) {
        adminService.updateAdmin(admin);
        return adminService.getResponseSave();
    }


    /**
     * @return
     * 删除方法
     */
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@Validated(value= {Admin.Update.class})Admin admin )
    {
        adminService.deleteAdmin(admin);
        return adminService.getResponseDelete();
    }


    public String change() {
        return null;
    }

}
