package com.rimi.live.controller;

import com.rimi.live.bean.*;
import com.rimi.live.service.AdminService;
import com.rimi.live.service.AnchorService;
import com.rimi.live.service.BanchorPulsService;
import com.rimi.live.vo.AnchorQvo;
import com.rimi.live.vo.BeAnchorQvo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/beanchor")
public class BeanchorController {
    @Resource
    private BanchorPulsService banchorPulsService;
    @Resource
    private AdminService adminService;
    @Resource
    private  AnchorService anchorService;

    @RequestMapping("/index")
    public  String index(Model model , Page page , BeanchorPuls  beanchorPuls){
        System.out.println(beanchorPuls);
        BeAnchorQvo beAnchorQvo = new BeAnchorQvo();
        beAnchorQvo.setBanchorPuls(beanchorPuls);

        System.out.println("审批差选条件：");
        System.out.println(beanchorPuls.toString());

        List<BeanchorPuls> beanchor = banchorPulsService.getBeanchor(page, beAnchorQvo);



        System.out.println("审批查询结果：++++++++++++++");
        for (BeanchorPuls puls : beanchor) {
            System.out.println(
                    puls
            );
        }
        model.addAttribute("beanchors", beanchor);
        model.addAttribute("page", page);

        return "beanchor/index";
    }
    //修改主播状态.
    @RequestMapping("/change")
    @ResponseBody
    public String change(Integer status, BeanchorPuls beanchorPuls , HttpSession session) {
        //得到当前管理员
        Admin seead= (Admin) session.getAttribute("user");
        Admin admin = adminService.findByName(seead.getName());

        System.out.println(admin);
        //审核主播申请表
        System.out.println(beanchorPuls+"审批前台传回");

        //设置审批的ID
        beanchorPuls.setAdminId(admin.getId().toString());
        //设置审批时间
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String format = df.format(new Date());
        Date data = null;
        try {
            data = df.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //设置审批的时间
        beanchorPuls.setLasttime(data);
        //设置id
        beanchorPuls.setId(beanchorPuls.getId());
        //封装到vo
        BeAnchorQvo beAnchorQvo = new BeAnchorQvo();
        beAnchorQvo.setBanchorPuls(beanchorPuls);

        banchorPulsService.changeStatus(status,beAnchorQvo);



        //通过前台ID查询后台主播申请表
        BeAnchorQvo beAnchorQvo1 = new BeAnchorQvo();
        beAnchorQvo1.setBanchorPuls(beanchorPuls);
        Beanchor inserBeanchor = banchorPulsService.getBeanchorById(beAnchorQvo1);

        //复制申请表信息到主播表bean对象
        System.out.println("需要复制的数据"+inserBeanchor.toString());
        System.out.println(new Anchor().toString());
        AnchorPuls anchorPuls = new AnchorPuls();
        BeanUtils.copyProperties(inserBeanchor,anchorPuls);
        AnchorPuls anchorByID = anchorService.getAnchorByID(anchorPuls);
        if(anchorByID==null ){
            System.out.println(anchorPuls+"要插入到主播表的数据");
            //插入信息到主播表
            anchorService.insertAnchor(anchorPuls);
            return  banchorPulsService.getResponseSave();
        }else{
            banchorPulsService.setErrorMsg("申请信息重复");
            return banchorPulsService.getResponseSave();
        }


    }

    @RequestMapping("/show")
    public  String show(Model model ,BeanchorPuls beanchorPuls){
        BeAnchorQvo beAnchorQvo = new BeAnchorQvo();
        beAnchorQvo.setBanchorPuls(beanchorPuls);
        Beanchor beanchorById = banchorPulsService.getBeanchorById(beAnchorQvo);
        model.addAttribute("inf",beanchorById);

        return "/beanchor/show";
    }






}
