package com.rimi.live.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 资源类控制器式例
 * @author luohengyi
 */
public abstract class CommonController {

    /**
     * 资源列表
     * @return
     */
    @RequestMapping
    public abstract String index();

    /**
     * 添加资源
     * @return
     */
    public abstract String add();

    /**
     * 添加功能
     * @return
     */
    public abstract String insert();

    /**
     * 修改页面
     * @return
     */
    public abstract String edit();

    /**
     * 修改功能
     * @return
     */
    public abstract String update();

    /**
     * 删除功能
     * @return
     */
    public abstract String delete();

    /**
     * 改变状态
     * @return
     */
    public abstract String change();


}
