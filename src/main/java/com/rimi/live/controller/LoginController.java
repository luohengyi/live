package com.rimi.live.controller;



import com.github.pagehelper.util.StringUtil;
import com.rimi.live.bean.Admin;
import com.rimi.live.bean.Response;
import com.rimi.live.util.CodeUntil;
import org.apache.catalina.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;

/**
 * @author luohengyi
 */
@Controller()
@RequestMapping("/login")
@SessionAttributes("user")
public class LoginController {

    @RequestMapping("/index")
    public String index(){

        return "login/index";
    }

    @RequestMapping("/login")
    @ResponseBody
    public String login(Admin admin, ModelMap modelMap){

        UsernamePasswordToken token = new UsernamePasswordToken(admin.getName(),admin.getPassword());
        Subject subject = SecurityUtils.getSubject();
       try {
           subject.login(token);
           Admin sessionAdmin = (Admin) subject.getPrincipal();
           modelMap.addAttribute("user",admin);
           return Response.creatSuccesstUrl("/admin/index");
       }catch (Exception e){
           return Response.creatError("账号或密码错误！");
       }

    }


    @RequestMapping("/code")
    public void code(HttpServletResponse response) {
        CodeUntil.outHttpImg(response);
    }

}
