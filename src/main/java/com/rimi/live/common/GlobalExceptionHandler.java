package com.rimi.live.common;

import com.rimi.live.bean.Response;
import org.apache.catalina.loader.ResourceEntry;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author luohengyi
 * 自定义validation返回类
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(BindException.class)
    public String resp(BindException bindException){
        StringBuilder errorMsg = new StringBuilder();
        bindException.getAllErrors().forEach(data->errorMsg.append(data.getDefaultMessage()).append(","));
        return  Response.creatError(errorMsg.toString());
    }
}
