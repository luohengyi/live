package com.rimi.live.mapper;

import com.rimi.live.bean.Slideshow;
import com.rimi.live.bean.SlideshowExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SlideshowMapper {
    long countByExample(SlideshowExample example);

    int deleteByExample(SlideshowExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Slideshow record);

    int insertSelective(Slideshow record);

    List<Slideshow> selectByExample(SlideshowExample example);

    Slideshow selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Slideshow record, @Param("example") SlideshowExample example);

    int updateByExample(@Param("record") Slideshow record, @Param("example") SlideshowExample example);

    int updateByPrimaryKeySelective(Slideshow record);

    int updateByPrimaryKey(Slideshow record);
}