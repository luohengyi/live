package com.rimi.live.mapper;

import com.rimi.live.bean.Beanchor;
import com.rimi.live.bean.BeanchorPuls;
import com.rimi.live.vo.AnchorQvo;
import com.rimi.live.vo.BeAnchorQvo;

import java.util.List;

public interface BeanchorPulsMapper {
    //获取主播申请表
    List<BeanchorPuls> getBeanchorList(BeAnchorQvo beAnchorQvo);

    //获取查询条件总条数
    Long getCount(BeAnchorQvo beAnchorQvo);

    //主播审批
    int updataBeanchor(BeAnchorQvo beAnchorQvo);

    //获取主播申请人通过i
    BeanchorPuls getBeanchorById(BeAnchorQvo beAnchorQvo);
}
