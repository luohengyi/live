package com.rimi.live.mapper;

import com.rimi.live.bean.LiveKind;
import com.rimi.live.bean.LiveKindExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LiveKindMapper {
    long countByExample(LiveKindExample example);

    int deleteByExample(LiveKindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(LiveKind record);

    int insertSelective(LiveKind record);

    List<LiveKind> selectByExample(LiveKindExample example);

    LiveKind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") LiveKind record, @Param("example") LiveKindExample example);

    int updateByExample(@Param("record") LiveKind record, @Param("example") LiveKindExample example);

    int updateByPrimaryKeySelective(LiveKind record);

    int updateByPrimaryKey(LiveKind record);
}