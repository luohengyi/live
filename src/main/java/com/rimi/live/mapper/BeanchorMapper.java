package com.rimi.live.mapper;

import com.rimi.live.bean.Beanchor;
import com.rimi.live.bean.BeanchorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BeanchorMapper {
    long countByExample(BeanchorExample example);

    int deleteByExample(BeanchorExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Beanchor record);

    int insertSelective(Beanchor record);

    List<Beanchor> selectByExample(BeanchorExample example);

    Beanchor selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Beanchor record, @Param("example") BeanchorExample example);

    int updateByExample(@Param("record") Beanchor record, @Param("example") BeanchorExample example);

    int updateByPrimaryKeySelective(Beanchor record);

    int updateByPrimaryKey(Beanchor record);
}