package com.rimi.live.mapper;

import com.rimi.live.bean.AnchorPuls;
import com.rimi.live.vo.AnchorQvo;

import java.util.List;

public interface AnchorPulsMapper {
    //查询主播表数据
    List<AnchorPuls> getAnchor(AnchorQvo anchorQvo);
    //获取查询条件总条数
    Long getCount(AnchorQvo anchorQvo);
    //改变主播状态
    int  changeAnchorFlag(AnchorQvo anchorQvo);

    AnchorPuls getAnchorByID(AnchorQvo anchorQvo);


    int UpDataAnchor(AnchorQvo anchorQvo);
}
