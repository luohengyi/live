package com.rimi.live.mapper;

import com.rimi.live.bean.RimitvUser;
import com.rimi.live.bean.RimitvUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RimitvUserMapper {
    long countByExample(RimitvUserExample example);

    int deleteByExample(RimitvUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(RimitvUser record);

    int insertSelective(RimitvUser record);

    List<RimitvUser> selectByExample(RimitvUserExample example);

    RimitvUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") RimitvUser record, @Param("example") RimitvUserExample example);

    int updateByExample(@Param("record") RimitvUser record, @Param("example") RimitvUserExample example);

    int updateByPrimaryKeySelective(RimitvUser record);

    int updateByPrimaryKey(RimitvUser record);
}