package com.rimi.live.config;


import com.rimi.live.bean.Rule;
import com.rimi.live.service.RuleService;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author luohengyi
 */ //启动自定配置
@Configuration
public class ShiroConfiguration {
    @Autowired
    private RuleService ruleService;
    //路由过滤器，验证是否符合权限，注入安全管理器
    @Bean("shiroFilter")
    public ShiroFilterFactoryBean filterFactoryBean(@Qualifier("securityManager") SecurityManager securityManager) {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(securityManager);

        //设置登陆页面
        bean.setLoginUrl("/login/index");
        //登陆成功页面
//        bean.setSuccessUrl("/");
        //没有权限跳转
        bean.setUnauthorizedUrl("/unauthorized");
        //拦截定义     请求   拦截器
        LinkedHashMap<String, String> filterchain = new LinkedHashMap<>();
        //  authc需要验证 anon放行
            //放行静态资源
        filterchain.put("/anchorInof/**", "anon");
        filterchain.put("/css/**", "anon");
        filterchain.put("/img/**", "anon");
        filterchain.put("/js/**", "anon");
        filterchain.put("/layui/**", "anon");

        filterchain.put("/login/*", "anon");
        //具有edit权限（permission）的人才拥有访问权
        List<Rule> allRuleShow = ruleService.getAllRuleShow();
        for (Rule rule : allRuleShow) {
            /**
             * 具有perms[功能名]拥有访问 rule.getRule() 路径的权限，用户拥有那些功能名需要在
             * 去实现 AuthorizingRealm接口，在doGetAuthorizationInfo方法中实现
             */
            filterchain.put(rule.getRule(), "perms["+rule.getName()+"]");
        }

        bean.setFilterChainDefinitionMap(filterchain);

        return bean;
    }

    //安全管理器
    @Bean("securityManager")
    public SecurityManager securityManager(@Qualifier("authRealm") AuthRealm authRealm){
        DefaultSecurityManager manager = new DefaultWebSecurityManager();
        manager.setRealm(authRealm);
        return manager;
    }

    //  验证用户，验证权限，注入自己的密码校验器
    @Bean("authRealm")
    public AuthRealm authRealm(@Qualifier("credentialMatcher") CredentialMatcher credentialMatcher){
        AuthRealm authRealm = new AuthRealm();
        authRealm.setCredentialsMatcher(credentialMatcher);
        //开启缓存
        authRealm.setCacheManager(new MemoryConstrainedCacheManager());
        return authRealm;
    }

    // 密码校验器，定制自己的密码校验规则
    @Bean("credentialMatcher")
    public CredentialMatcher credentialMatcher(){
        return  new CredentialMatcher();
    }

    //spring - shiro 关联配置
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(@Qualifier("securityManager") SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return  defaultAdvisorAutoProxyCreator;
    }


}
