package com.rimi.live.config;

import com.rimi.live.bean.Admin;
import com.rimi.live.bean.Role;
import com.rimi.live.bean.Rule;
import com.rimi.live.service.AdminService;
import com.rimi.live.service.RoleService;
import com.rimi.live.service.RuleService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AuthRealm extends AuthorizingRealm {

    @Autowired
    private AdminService adminService;
    @Autowired
    private RoleService roleService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //类似从session中获取用户，然后通过用户获取角色，通过角色获取权限列表，应为权限是第不可重复的故此使用 set！！
        Admin admin = (Admin) principalCollection.fromRealm(this.getClass().getName()).iterator().next();
        //权限列表
        List<String> permissionList = new ArrayList<>();
        //角色列表
        List<String> roleNameList = new ArrayList<>();


        Role role = roleService.getRuleById(admin.getRoleId());

        roleNameList.add(role.getName());
        List<Rule> rules = role.getRules();
        if (CollectionUtils.isNotEmpty(rules)) {
            for (Rule rule : rules) {
                permissionList.add(rule.getName());
            }
        }

        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        //将权限名放入  SimpleAuthorizationInfo 的  StringPermissions
        System.out.println(permissionList);
        simpleAuthorizationInfo.addStringPermissions(permissionList);
        //将角色放入
        simpleAuthorizationInfo.addRoles(roleNameList);
        return simpleAuthorizationInfo;
    }

    //认证登陆
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("认证登陆");
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        String username = usernamePasswordToken.getUsername();
        Admin admin = adminService.findByName(username);
        return new SimpleAuthenticationInfo(admin, admin.getPassword(), this.getClass().getName());
    }
}