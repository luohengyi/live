package com.rimi.live.bean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

public class RimitvUser {
    public interface Update{};
    public interface Insert{};
    @Min(value = 1,message = "非法数据",groups = Role.Update.class)
    private Integer id;
    @NotEmpty(message = "角色名称不能为空",groups = Role.Insert.class)



    private String username;

    private String password;

    private String account;

    private Integer isRole;

    private String attention;

    private Date time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getIsRole() {
        return isRole;
    }

    public void setIsRole(Integer isRole) {
        this.isRole = isRole;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention == null ? null : attention.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", account=").append(account);
        sb.append(", isRole=").append(isRole);
        sb.append(", attention=").append(attention);
        sb.append(", time=").append(time);
        sb.append("]");
        return sb.toString();
    }
}