package com.rimi.live.bean;

public class AnchorPuls  extends  Anchor{
    //主播增强类
    private  Integer flag;

   private  String username;

   private  String anchorkind;

   private  String headline;

   private  String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setAnchorkind(String anchorkind) {
        this.anchorkind = anchorkind;
    }

    public String getAnchorkind() {
        return anchorkind;
    }



    public String getUsername() {
        return username;
    }



    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {

        return "AnchorPuls{" +
                "flag=" + flag +"account="+account+
                ", username='" + username + '\'' +
                ", anchorkind='" + anchorkind + '\'' +"直播标题"+headline+
                '}'+"父类"+super.toString();
    }
}



