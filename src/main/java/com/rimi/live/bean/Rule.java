package com.rimi.live.bean;

import java.util.LinkedHashSet;

public class Rule {
    private Integer id;

    private String name;

    private Integer pid;

    private String rule;

    private Integer show;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule == null ? null : rule.trim();
    }

    public Integer getShow() {
        return show;
    }

    public void setShow(Integer show) {
        this.show = show;
    }


    private LinkedHashSet<Rule> childRole=null;
    public void addChildRole(Rule rule){
        if (null == childRole){
            childRole = new LinkedHashSet<>();
        }
        childRole.add(rule);
    }

    public LinkedHashSet<Rule> getChildRole() {
        return childRole;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", pid=").append(pid);
        sb.append(", rule=").append(rule);
        sb.append(", show=").append(show);
        sb.append("]");
        return sb.toString();
    }
}