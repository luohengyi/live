package com.rimi.live.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BeanchorPuls extends  Beanchor {
    //主播申请增强类
    //用户名称
    private String  username;

    //主播分类
    private  String  kind;

    private  String applytime;

    private  String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getApplytime() {
        Date date=getTime();
        SimpleDateFormat temp=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date1=temp.format(date);
        return date1;
    }

    private  String judgeTime;

    public String getJudgeTime() {
        if(getLasttime()!=null){
            Date date=getLasttime();
            SimpleDateFormat temp=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String date1=temp.format(date);
            return date1;
        }

        return  null;
    }

    public void setJudgeTime(String judgeTime) {
        this.judgeTime = judgeTime;
    }

    private  String adminname;

    public void setAdminname(String adminname) {
        this.adminname = adminname;
    }

    public String getAdminname() {
        return adminname;
    }

    public String getUsername() {
        return username;
    }

    public String getKind() {
        return kind;
    }



    public void setUsername(String username) {
        this.username = username;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    @Override
    public String toString() {
        return "BeanchorPuls{" +
                "username='" + username + '\'' +
                ", kind='" + kind + '\'' +
                '}'+"原始类："+super.toString();
    }
}
