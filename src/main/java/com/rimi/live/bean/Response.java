package com.rimi.live.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * post方法响应类
 */
public class Response {
    private  String data;
    private  int code;
    private  String url;
    private  String error;

    /**
     *
     * @param code 错误码
     * @param error 错误信息
     */
    private Response(int code, String error) {
        this.code = code;
        this.error = error;
    }


    /**
     *
     * @param data 数据
     * @param url 跳转url
     */
    public Response(String data, String url) {
        this.data = data;
        this.url = url;
    }

    public Response(String data, int code) {
        this.data = data;
        this.code = code;
    }

    public Response(String data, int code, String url, String error) {
        this.data = data;
        this.code = code;
        this.url = url;
        this.error = error;
    }

    /**
     * 警告信息
     * code 500
     * @param error
     * @return
     */
    public static String creatError(  String error){
        Response Response = new Response(Code.ERROR,error);
        return  Response.jsonOfResponse();
    }

    /**
     * 提示后跳转到指定地址
     * code 200
     * @param data
     * @param url
     * @return
     */
    public  static String creatSuccess(String data,String url){
        Response Response = new Response(data,Code.SUCCESS,url,null);
        return  Response.jsonOfResponse();
    }

    /**
     * 无提示直接跳转到指定页面
     * code 201
     * @param url
     * @return
     */
    public  static String creatSuccesstUrl(String url){
        Response Response = new Response(null,Code.SUCCESSURl,url,null);
        return  Response.jsonOfResponse();
    }

    /**
     * 有提示刷新当前页面
     * code 202
     * @param data
     * @return
     */
    public static String creatSuccestUrlReload(String data){
        Response Response = new Response(data,Code.SUCCESSRELOAD);
        return  Response.jsonOfResponse();
    }



    /**
     * 序列化该类
     * @return String
     */
    private String jsonOfResponse(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data",this.data);
        jsonObject.put("code",this.code);
        jsonObject.put("url",this.url);
        jsonObject.put("error",this.error);
        return  jsonObject.toString();
    }
}
