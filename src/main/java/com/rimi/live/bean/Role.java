package com.rimi.live.bean;

import com.rimi.live.controller.RoleController;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author luohengyi
 */
@Data
public class Role {
    public interface Update{};
    public interface Insert{};

    @Min(value = 1,message = "非法数据",groups = Update.class)
    private Integer id;

    @NotEmpty(message = "角色名称不能为空",groups = Insert.class)
    private String name;

    private String ruleconten;

    private List<Rule> rules;

    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", ruleconten=").append(ruleconten);
        sb.append("]");
        return sb.toString();
    }
}