package com.rimi.live.bean;

public class Code {

    /**
     * 500以上为处理失败
     */
    public static final int ERROR= 500;

    /**
     * 200-300为处理成功
     * 有提示信息
     * 默认跳转
     */
    public static final int SUCCESS= 200;

    /**
     * 无提示
     * 跳转到指定url
     */
    public static final int SUCCESSURl=201;

    /**
     * 刷新当前页面
     */
    public static final int SUCCESSRELOAD=202;

}
