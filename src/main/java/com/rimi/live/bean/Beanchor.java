package com.rimi.live.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Beanchor {
    private Integer id;

    private String  userId;

    private String idcard;

    private Integer kindId;

    private String cardinformation1;

    private String cardinformation2;


    private Date time;

    private String adminId;

    private Integer status;

    private Date lasttime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard == null ? null : idcard.trim();
    }

    public Integer getKindId() {
        return kindId;
    }

    public void setKindId(Integer kindId) {
        this.kindId = kindId;
    }

    public String getCardinformation1() {
        return cardinformation1;
    }

    public void setCardinformation1(String cardinformation1) {
        this.cardinformation1 = cardinformation1 == null ? null : cardinformation1.trim();
    }

    public String getCardinformation2() {
        return cardinformation2;
    }

    public void setCardinformation2(String cardinformation2) {
        this.cardinformation2 = cardinformation2 == null ? null : cardinformation2.trim();
    }

    public Date getTime() {
        System.out.println("时间++++++++++++++++++");

        return time;
    }

    public void setTime(Date time) {


        this.time = time;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId == null ? null : adminId.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getLasttime() {
        return lasttime;
    }

    public void setLasttime(Date lasttime) {
        this.lasttime = lasttime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", idcard=").append(idcard);
        sb.append(", kindId=").append(kindId);
        sb.append(", cardinformation1=").append(cardinformation1);
        sb.append(", cardinformation2=").append(cardinformation2);
        sb.append(", time=").append(time);
        sb.append(", adminId=").append(adminId);
        sb.append(", status=").append(status);
        sb.append(", lasttime=").append(lasttime);
        sb.append("]");
        return sb.toString();
    }
}