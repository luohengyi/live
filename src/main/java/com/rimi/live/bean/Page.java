package com.rimi.live.bean;

public class Page {
    // 每页多少条
    public int pageSize = 10;
    // 取第几页数据
    public int pageNo = 1;
    // 当前页数据从哪一行开始
    public int start = 0;
    // 数据库总页数
    public int pageNum;
    // 数据库总条数
    public int totNuml;

    public Page() {
    }

    public Page(String pageNo) {
        int defaultNo=1;
        if (null!=pageNo){
            try {
                defaultNo = Integer.parseInt(pageNo);
            } catch (NumberFormatException asd) {
                //防止用户输入中文页面
            }
            this.pageNo = defaultNo <= 0 ? 1 : defaultNo;
        }else {
            this.pageNo = defaultNo;
        }


    }

    public Page(int pageNo) {
        this.pageNo = pageNo <= 0 ? 1 : pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTotNuml() {
        return totNuml;
    }

    /**
     * 计算总页数
     *
     * @param totNuml
     */
    public void setTotNuml(int totNuml) {
        this.totNuml = totNuml;
        this.pageNum = (totNuml / pageSize) + (totNuml % pageSize == 0 ? 0 : 1);
        if (pageNo > pageNum) {
            pageNo = pageNum;
        }
    }

    public void setTotNumAll(int totNuml) {
        this.totNuml = totNuml;
        this.pageSize = totNuml;
    }

    // 计算起始位置
    public int getStart() {
        this.start = (pageNo - 1) * pageSize;
        return start;
    }

    public int getPageNum() {
        return pageNum;
    }


    @Override
    public String toString() {
        return "Page{" +
                "pageSize=" + pageSize +
                ", pageNo=" + pageNo +
                ", start=" + start +
                ", pageNum=" + pageNum +
                ", totNuml=" + totNuml +
                '}';
    }
}
