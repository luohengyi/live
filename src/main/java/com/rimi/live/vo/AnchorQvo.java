package com.rimi.live.vo;

import com.rimi.live.bean.AnchorPuls;

public class AnchorQvo {
    //主播包装类
        private AnchorPuls anchorPuls;

    public void setAnchorPuls(AnchorPuls anchorPuls) {
        this.anchorPuls = anchorPuls;
    }

    public AnchorPuls getAnchorPuls() {
        return anchorPuls;
    }
}
