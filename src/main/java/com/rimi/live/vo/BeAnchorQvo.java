package com.rimi.live.vo;

import com.rimi.live.bean.BeanchorPuls;

public class BeAnchorQvo {
    private BeanchorPuls banchorPuls;

    public BeanchorPuls getBanchorPuls() {
        return banchorPuls;
    }

    public void setBanchorPuls(BeanchorPuls banchorPuls) {
        this.banchorPuls = banchorPuls;
    }

}
