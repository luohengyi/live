package com.rimi.live.service;

import com.rimi.live.bean.Response;

/**
 * server层公共响应方法的封装
 *
 * @author luohengyi
 */
public class CommonService {

    protected boolean type = true;
    protected String msg;
    protected String listUrl;

    public void setSuccessMsg(String msg) {
        this.msg = msg;
    }

    public void setErrorMsg(String msg) {
        type=false;
        this.msg = msg;
    }

    /**
     * 添加，修改
     * @return Response
     */
    public String getResponseSave() {
        System.out.println(listUrl);
        if (type) {
            return Response.creatSuccess(msg, listUrl);
        }
        return creatErrorResponse();
    }

    /**
     * 删除方法的响应
     *
     * @return Response
     */
    public String getResponseDelete() {
        if (type) {
            return Response.creatSuccestUrlReload(msg);
        }
        return creatErrorResponse();
    }

    /**
     * 根据信息构建一个警告信息
     *
     * @return Response
     */
    private String creatErrorResponse() {
        return Response.creatError(msg);
    }


}
