package com.rimi.live.service;

import com.github.pagehelper.PageHelper;
import com.rimi.live.bean.*;
import com.rimi.live.mapper.AdminMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author rimi
 * @date 2019-02-15
 */
@Service
public class AdminService extends CommonService {
    @Resource
    AdminMapper adminMapper;


    {
        listUrl="/admin/index";
    }

    /**
     * @param admin
     * 添加管理员
     */
    public void insertAdmin(Admin admin) {
        final AdminExample adminExample=new AdminExample();
       adminExample.createCriteria().andNameEqualTo(admin.getName());
        List <Admin> admins = adminMapper.selectByExample(adminExample);
        if (!admins.isEmpty()) {
            setErrorMsg("用户已存在");
            return;
        }
        int back = adminMapper.insert(admin);
        if (back > 0) {
            setSuccessMsg("添加成功！");
        } else {
            setErrorMsg("添加失败！");
        }
    }

    /**
     * 分页查询
     * @return
     */
    public List<Admin> getAllAdmins(Page page , Admin admin){
        PageHelper.startPage(page.pageNo, page.pageSize);
        AdminExample adminExample = new AdminExample();
        if (null != admin.getName() && !admin.getName().isEmpty()){
            adminExample.createCriteria().andNameLike("%"+admin.getName()+"%");
        }
        List<Admin> admins = adminMapper.selectByExample(adminExample);
        long countByExample = adminMapper.countByExample(adminExample);
        page.setTotNuml((int) countByExample);
        return admins;
    }

    /**
     * @param admin
     * 修改方法
     */
    public void updateAdmin(Admin admin){
        AdminExample adminExample = new AdminExample();
        adminExample.createCriteria().andIdEqualTo(admin.getId());
        List<Admin> admins = adminMapper.selectByExample(adminExample);
        if (!admins.isEmpty()){
            setErrorMsg("操作错误");

            return;
        }
        int back = adminMapper.updateByPrimaryKey(admin);
        if (back>0){
            setSuccessMsg("修改成功");
        }else {
            setErrorMsg("修改失败");
        }
    }

    /**
     * @param admin
     * @return
     * 根据ID获取admin
     */
    public List<Admin> getAdminByName(Admin admin){
        AdminExample adminExample = new AdminExample();
        adminExample.createCriteria().andIdEqualTo(admin.getId());
        return adminMapper.selectByExample(adminExample);
    }

    /**
     * @param admin
     * 删除管理员
     */
    public void deleteAdmin(Admin admin){
        AdminExample adminExample = new AdminExample();
        adminExample.createCriteria().andIdEqualTo(admin.getId());
        final int i=adminMapper.deleteByExample(adminExample);
        if (i>0){
            setSuccessMsg("删除成功");
        }else {
            setErrorMsg("删除失败");
        }
    }

    public Admin findByName(String name) {
        AdminExample adminExample = new AdminExample();
        adminExample.createCriteria().andNameEqualTo(name);
        List<Admin> admins = adminMapper.selectByExample(adminExample);
        if (null==admins || admins.size()==0){
            return null;
        }
        return  admins.get(0);

    }
}
