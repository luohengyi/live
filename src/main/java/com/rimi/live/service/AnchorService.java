package com.rimi.live.service;

import com.github.pagehelper.PageHelper;
import com.rimi.live.bean.*;
import com.rimi.live.mapper.AnchorMapper;
import com.rimi.live.mapper.AnchorPulsMapper;
import com.rimi.live.util.UploadUtil;
import com.rimi.live.vo.AnchorQvo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AnchorService extends CommonService {

    @Resource
    private AnchorPulsMapper anchorPulsMapper;
    @Resource
    private  AdminService adminService;

    @Resource
    private  AnchorMapper anchorMapper;

    {
        listUrl = "/anchor/index";
    }

    //查询，带分页功能
    public List<AnchorPuls> getAnchorPuls(Page page, AnchorQvo anchorQvo) {
        PageHelper.startPage(page.pageNo, page.pageSize);
        List<AnchorPuls> anchor = anchorPulsMapper.getAnchor(anchorQvo);

        Long count = anchorPulsMapper.getCount(anchorQvo);
        //控制器传回来的page这里做了赋值
        page.setTotNuml(Math.toIntExact(count));


        return anchor;
    }

    /**
     * 添加，修改
     *
     * @return Response
     */
    public void changeAnchorFlag(int flag, AnchorPuls anchorPuls) {

        anchorPuls.setFlag(flag);
        AnchorQvo anchorQvo = new AnchorQvo();
        anchorQvo.setAnchorPuls(anchorPuls);
        int back = anchorPulsMapper.changeAnchorFlag(anchorQvo);
        if (back == 1) {
            setSuccessMsg("修改成功！");
        } else {
            setErrorMsg("修改失败！");

        }
    }

    /*
     * 通过ID查找一位主播
     *
     *
     * */
    public AnchorPuls getAnchorByID(AnchorPuls anchorPuls) {
        AnchorQvo anchorQvo = new AnchorQvo();
        anchorQvo.setAnchorPuls(anchorPuls);
        AnchorPuls resoult = anchorPulsMapper.getAnchorByID(anchorQvo);

        return resoult;
    }


    public void update(MultipartFile file1, MultipartFile file2, AnchorPuls anchorPuls) {
        AnchorQvo anchorQvo = new AnchorQvo();
        if (file1!=null&&file2!=null){
            String zheng = UploadUtil.uploading(file1);
            String fan = UploadUtil.uploading(file2);
            System.out.println("正面路径"+zheng);
            System.out.println("反面路径"+fan);
            anchorPuls.setCardinformation1(zheng);
            anchorPuls.setCardinformation2(fan);
            anchorQvo.setAnchorPuls(anchorPuls);
            try{
                System.out.println(anchorPuls+"更新主播信息");
                int i = anchorPulsMapper.UpDataAnchor(anchorQvo);
                System.out.println(i+"更新得条数");
            }catch (Exception E){
                E.printStackTrace();
                setErrorMsg("修改失败");
                return;
            }
            setSuccessMsg("修改成功");
        }else {
            setErrorMsg("修改失败");
        }


    }
    //插入到主播表
    public  void insertAnchor(AnchorPuls anchorPuls){
        Anchor anchor = new Anchor();
            BeanUtils.copyProperties(anchorPuls,anchor);

        System.out.println("插入到主播表的数据");
        System.out.println(anchor);
        anchor.setId(0);
        int insert = anchorMapper.insert(anchor);
        if(insert==1){
            setSuccessMsg("操作成功");
        }else {
            setErrorMsg("操作失败");
        }

    }
}
