package com.rimi.live.service;

import com.rimi.live.bean.Navigation;
import com.rimi.live.bean.NavigationExample;
import com.rimi.live.mapper.NavigationMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author luohengyi
 */
@Service
public class NavigationService extends CommonService {
    {
        listUrl = "/navigation/index";
    }

    @Resource
    private NavigationMapper navigationMapper;

    public List<Navigation> getLisetPage() {
        return navigationMapper.selectByExample(null);
    }

    /**
     * 添加导航栏菜单
     * @param navigation
     */
    public void insertNavigation(Navigation navigation) {
        int insert = navigationMapper.insert(navigation);
        if (insert > 0) {
            setSuccessMsg("添加成功！");
        } else {
            setErrorMsg("添加失败！");
        }

    }

    /**
     * 根基id获取导航栏
     * @param navigation
     * @return
     */
    public List<Navigation> getNavigationById(Navigation navigation){
        NavigationExample navigationExample = new NavigationExample();
        navigationExample.createCriteria().andIdEqualTo(navigation.getId());
        return navigationMapper.selectByExample(navigationExample);
    }

    public void update(Navigation navigation) {
        int i = navigationMapper.updateByPrimaryKey(navigation);
        if (i>0){
            setSuccessMsg("修改成功！");
        }else {
            setErrorMsg("修改失败！");
        }
    }


    public void deteleById(Navigation navigation) {
        int i = navigationMapper.deleteByPrimaryKey(navigation.getId());
        if (i>0){
            setSuccessMsg("删除成功！");
        }else {
            setErrorMsg("删除失败！");
        }
    }
}
