package com.rimi.live.service;

import com.github.pagehelper.PageHelper;
import com.rimi.live.bean.*;
import com.rimi.live.mapper.BeanchorMapper;
import com.rimi.live.mapper.BeanchorPulsMapper;
import com.rimi.live.vo.BeAnchorQvo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BanchorPulsService extends CommonService {
    @Resource
    private BeanchorPulsMapper beanchorPulsMapper;
    @Resource
    private  BeanchorMapper beanchorMapper;

    @Resource
            private  AnchorService anchorService;
    {
        listUrl="/beanchor/index";
    }
    //查询，带分页功能
    public List<BeanchorPuls > getBeanchor(Page page, BeAnchorQvo beAnchorQvo){
        PageHelper.startPage(page.pageNo, page.pageSize);

        List<BeanchorPuls> beanchorList = beanchorPulsMapper.getBeanchorList(beAnchorQvo);



        Long count = beanchorPulsMapper.getCount(beAnchorQvo);
        page.setTotNuml(Math.toIntExact(count));


        return  beanchorList;
    }

    //主播审批功能
    public void changeStatus(Integer status, BeAnchorQvo beAnchorQvo) {
        System.out.println("主播审批功能");
        System.out.println(beAnchorQvo.getBanchorPuls());
        beAnchorQvo.getBanchorPuls().setStatus(status);

        Beanchor beanchor = new Beanchor();

        BeanUtils.copyProperties(beAnchorQvo.getBanchorPuls(),beanchor);
        System.out.println(beanchor.toString()+"更新的信息");
        BeanchorExample beanchorExample = new BeanchorExample();
        BeanchorExample.Criteria criteria = beanchorExample.createCriteria();
        criteria.andUserIdEqualTo(beanchor.getUserId());

        int back = beanchorMapper.updateByExampleSelective(beanchor,beanchorExample);

        if(back==1){
            setSuccessMsg("修改成功");
        }else {
            setErrorMsg("修改失败");
        }
    }



    //通过ID查找一个主播申请人的信息
    public Beanchor getBeanchorById(BeAnchorQvo beAnchorQvo){

        BeanchorPuls beanchorById = beanchorPulsMapper.getBeanchorById(beAnchorQvo);
        return beanchorById;

    }

}
