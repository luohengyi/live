package com.rimi.live.service;

import com.github.pagehelper.PageHelper;
import com.rimi.live.bean.LiveKind;
import com.rimi.live.bean.LiveKindExample;
import com.rimi.live.bean.Page;
import com.rimi.live.mapper.LiveKindMapper;
import com.rimi.live.util.UploadUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author rimi
 * @date 2019-02-19
 */

@Service
public class LiveKindsService extends CommonService {
    @Resource
    LiveKindMapper liveKindMapper;

    {
        listUrl="/livekinds/index";
    }

    /**
     * @param liveKind
     * 添加直播类别的方法
     */
    public void insertLiveKinds(MultipartFile file , LiveKind liveKind){
        String fileimg =UploadUtil.uploading(file);

        liveKind.setImg(fileimg);

        LiveKindExample liveKindExample = new LiveKindExample();
        liveKindExample.createCriteria().andNameEqualTo(liveKind.getName());
        List<LiveKind> liveKinds = liveKindMapper.selectByExample(liveKindExample);
        if (!liveKinds.isEmpty()){
            setErrorMsg("直播类别已存在");
            return;
        }
        int back = liveKindMapper.insert(liveKind);
        if (back>0){
            setSuccessMsg("添加成功");
        }else {
            setErrorMsg("添加失败");
        }
    }

    public void updateLiveKinds(MultipartFile file , LiveKind liveKind){
        final String uploading=UploadUtil.uploading(file);
        liveKind.setImg(uploading);
        LiveKindExample liveKindExample = new LiveKindExample();
        liveKindExample.createCriteria().andIdEqualTo(liveKind.getId());
        List<LiveKind> liveKinds = liveKindMapper.selectByExample(liveKindExample);
        if (!liveKinds.isEmpty()){
            setErrorMsg("未找到该直播类别");
        }
        int back = liveKindMapper.updateByPrimaryKey(liveKind);
        if (back>0){
            setSuccessMsg("修改成功");
        }else {
            setErrorMsg("修改失败");
        }

    }

    /**
     * @param page
     * @param liveKind
     * @return
     * 获取到所有直播类别，并分页
     */
    public List<LiveKind> getAllLiveKinds(Page page , LiveKind liveKind){
        PageHelper pageHelper = new PageHelper();
        LiveKindExample liveKindExample = new LiveKindExample();
        if (null != liveKind.getName() && !liveKind.getName().isEmpty()){
            liveKindExample.createCriteria().andNameLike("%"+liveKind.getName()+"%");
        }
        List<LiveKind> liveKinds = liveKindMapper.selectByExample(liveKindExample);
        long countByExample=liveKindMapper.countByExample(liveKindExample);
        page.setTotNuml((int) countByExample);
        return liveKinds;
    }

    public List<LiveKind> getLiveKindsByName(LiveKind liveKind){
        LiveKindExample liveKindExample = new LiveKindExample();
        liveKindExample.createCriteria().andIdEqualTo(liveKind.getId());
        return liveKindMapper.selectByExample(liveKindExample);
    }

    public void delete(LiveKind liveKind){
        if (liveKindMapper.deleteByPrimaryKey(liveKind.getId())>0){
            setSuccessMsg("删除成功");
        }else {
            setErrorMsg("删除失败");
        }
    }
}
