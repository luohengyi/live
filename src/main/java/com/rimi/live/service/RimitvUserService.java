package com.rimi.live.service;


import com.github.pagehelper.PageHelper;
import com.rimi.live.bean.Page;
import com.rimi.live.bean.RimitvUser;
import com.rimi.live.bean.RimitvUserExample;
import com.rimi.live.mapper.RimitvUserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author rimi
 * @date 2019-02-15
 */
@Service
public class RimitvUserService extends CommonService {
    @Resource
     RimitvUserMapper rimitvUserMapper;
    {
        listUrl="/rimitvuser/index";

    }
    /**
     * @param rimitvUser
     * 添加用户
     */
    public void insertRimitvUser(RimitvUser rimitvUser) {
        final  RimitvUserExample rimitvUserExample = new RimitvUserExample();
        rimitvUserExample.createCriteria().andAccountEqualTo(rimitvUser.getAccount());
        List<RimitvUser> rimitvUsers = rimitvUserMapper.selectByExample(rimitvUserExample);
        if (!rimitvUsers.isEmpty()) {
            setErrorMsg("该账号已存在");
            return;
        }
        rimitvUser.setTime(new Date());
        int back = rimitvUserMapper.insert(rimitvUser);
        if (back > 0) {
            setSuccessMsg("添加成功！");
        } else {
            setErrorMsg("添加失败！");
        }
    }


    /**
     * @param page
     * @param rimitvUser
     * 分页查询
     * @return
     */

    public  List<RimitvUser> getAllRimitnUsers(Page page,RimitvUser rimitvUser){
        PageHelper.startPage(page.pageNo,page.pageSize);
        RimitvUserExample rimitvUserExample=new RimitvUserExample();
        if (null!=rimitvUser.getUsername() && !rimitvUser.getUsername().isEmpty()){
            rimitvUserExample.createCriteria().andUsernameLike("%"+rimitvUser.getUsername()+"%");
        }
        List<RimitvUser> rimitvUsers=rimitvUserMapper.selectByExample(rimitvUserExample);
        long countByExample=rimitvUserMapper.countByExample(rimitvUserExample);
        page.setTotNuml((int) countByExample);
        return rimitvUsers;
    }

    /**
     * @param rimitvUser
     * 修改方法
     */
    public void updateRimitvUser(RimitvUser rimitvUser){
        RimitvUserExample rimitvUserExample=new RimitvUserExample();
        rimitvUserExample.createCriteria().andIdEqualTo(rimitvUser.getId());
        List<RimitvUser> rimitvUsers=rimitvUserMapper.selectByExample(rimitvUserExample);
//        if (!rimitvUsers.isEmpty()){
//            setErrorMsg("操作错误");
//            return;
//        }
        int back=rimitvUserMapper.updateByPrimaryKey(rimitvUser);
        if (back>0){
            setSuccessMsg("修改成功");
        }else {
            setErrorMsg("修改失败");
        }
    }

    /**
     * @param rimitvUser
     * @return
     * 根据ID获取rimitvuser
     */
    public List<RimitvUser> getAllRimitnUserById(RimitvUser rimitvUser){
        RimitvUserExample rimitvUserExample =new RimitvUserExample();
        rimitvUserExample.createCriteria().andIdEqualTo(rimitvUser.getId());

        return rimitvUserMapper.selectByExample(rimitvUserExample);
    }


    /**
     * 删除角色
     * @param rimitvUser
     */
    public void delete(RimitvUser rimitvUser){
        if (rimitvUserMapper.deleteByPrimaryKey(rimitvUser.getId())>0){
            setSuccessMsg("删除成功！");
        }else {
            setErrorMsg("删除失败！");
        }
    }
}
