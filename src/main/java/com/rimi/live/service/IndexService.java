package com.rimi.live.service;


import com.rimi.live.bean.Admin;
import com.rimi.live.mapper.AdminMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class IndexService {

    @Resource
    private AdminMapper adminMapper;

    public List<Admin> getAllUsers(){
        return adminMapper.selectByExample(null);
    }


}
