package com.rimi.live.service;

import com.rimi.live.bean.Slideshow;
import com.rimi.live.bean.SlideshowExample;
import com.rimi.live.mapper.SlideshowMapper;
import com.rimi.live.util.UploadUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author luohengyi
 */
@Service
public class SlideshowService extends CommonService {
    {
        listUrl = "/slideshow/index";
    }

    @Resource
    private SlideshowMapper slideshowMapper;

    public List<Slideshow> getAll() {
        return slideshowMapper.selectByExample(null);
    }

    public void insertSlideshow(MultipartFile file, Slideshow slideshow) {
        String relativeUrl = UploadUtil.uploading(file);
        System.out.println("上传失败");
        if (null== relativeUrl){
            setErrorMsg("文件上传失败！");
            return;
        }
        slideshow.setSkipUrl(relativeUrl);
        int insert = slideshowMapper.insert(slideshow);
        if (insert > 0) {
            setSuccessMsg("添加成功！");
        } else {
            //数据库添加失败，删除该文件
//            servicefile.delete();
            setErrorMsg("添加失败");
        }


    }

    public List<Slideshow> getSlideshowById(Slideshow slideshow) {
        SlideshowExample criteria = new SlideshowExample();
        criteria.createCriteria().andIdEqualTo(slideshow.getId());
        return slideshowMapper.selectByExample(criteria);
    }

    public void update(MultipartFile file, Slideshow slideshow,String oldUrl){
        if (file.getSize()>0) {
            String uploading = UploadUtil.uploading(file);
            slideshow.setSkipUrl(uploading);
        }else {
            slideshow.setSkipUrl(oldUrl);
        }
        int i = slideshowMapper.updateByPrimaryKey(slideshow);
        if (i>0){
            setSuccessMsg("修改成功！");
        }else {
            setErrorMsg("修改失败！");
        }
    }

    public void delete(Slideshow slideshow) {
        int i = slideshowMapper.deleteByPrimaryKey(slideshow.getId());
        if (i>0){
            setSuccessMsg("删除成功！");
        }else {
            setErrorMsg("删除失败！");
        }
    }
}
