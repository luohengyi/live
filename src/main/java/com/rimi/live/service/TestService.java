package com.rimi.live.service;

import com.rimi.live.bean.AnchorPuls;
import com.rimi.live.mapper.AnchorPulsMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TestService {

    @Resource
    private AnchorPulsMapper anchorPulsMapper;


    public List<AnchorPuls> test(){
        List<AnchorPuls> anchors = anchorPulsMapper.getAnchor(null);
        return anchors;

    }
}
