package com.rimi.live.service;

import com.rimi.live.bean.Rule;
import com.rimi.live.bean.RuleExample;
import com.rimi.live.mapper.RuleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author luohengyi
 */
@Service
public class RuleService {
    @Resource
    private RuleMapper ruleMapper;

    /**
     * 获取所有的rule
     *
     * @return
     */
    public List<Rule> getAllRuleShow() {
//        RuleExample ruleExample = new RuleExample();
//        ruleExample.createCriteria().andShowEqualTo(1);
        return ruleMapper.selectByExample(null);
    }
}
