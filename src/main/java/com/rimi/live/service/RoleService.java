package com.rimi.live.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.rimi.live.bean.*;
import com.rimi.live.mapper.RoleMapper;
import com.rimi.live.mapper.RuleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class RoleService extends CommonService {
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private RuleMapper ruleMapper;

    {
        listUrl = "/role/index";
    }

    /**
     * 添加数据
     *
     * @param role
     */
    public void insertRole(Role role) {
        RoleExample roleExample = new RoleExample();
        roleExample.createCriteria().andNameEqualTo(role.getName());
        List<Role> roles = roleMapper.selectByExample(roleExample);
        if (!roles.isEmpty()) {
            setErrorMsg("角色名称已存在");
            return;
        }
        int back = roleMapper.insert(role);
        if (back > 0) {
            setSuccessMsg("添加成功！");
        } else {
            setErrorMsg("添加失败！");
        }
    }

    /**
     * 分页查询数据
     *
     * @param page
     * @param role
     * @return
     */
    public List<Role> getAllRole(Page page, Role role) {
        PageHelper.startPage(page.pageNo, page.pageSize);
        RoleExample roleExample = new RoleExample();
        if (null != role.getName() && !role.getName().isEmpty()) {
            roleExample.createCriteria().andNameLike("%" + role.getName() + "%");
        }
        List<Role> roles = roleMapper.selectByExample(roleExample);
        long countByExample = roleMapper.countByExample(roleExample);
        page.setTotNuml((int) countByExample);

        return roles;
    }


    /**
     * 根据id获取role
     *
     * @param role
     * @return
     */
    public List<Role> getRoleById(Role role) {
        RoleExample roleExample = new RoleExample();
        roleExample.createCriteria().andIdEqualTo(role.getId());
        return roleMapper.selectByExample(roleExample);
    }

    /**
     * 修改角色
     *
     * @param role
     */
    public void updateRole(Role role) {
        RoleExample roleExample = new RoleExample();
        roleExample.createCriteria().andNameEqualTo(role.getName()).andIdNotEqualTo(role.getId());
        List<Role> roles = roleMapper.selectByExample(roleExample);
        if (!roles.isEmpty()) {
            setErrorMsg("角色名称已存在");

            return;
        }
//        int back = roleMapper.updateByPrimaryKey(role);
        int back = roleMapper.updateByPrimaryKeySelective(role);
        if (back > 0) {
            setSuccessMsg("修改成功！");
        } else {
            setErrorMsg("修改失败！");

        }
    }

    public void authorization(Integer[] rule, int id) {
        Role role = roleMapper.selectByPrimaryKey(id);
        if (null == role) {
            this.setErrorMsg("角色不存在！");
            return;
        }
        role.setRuleconten(JSON.toJSON(rule).toString());
        this.updateRole(role);


    }

    /**
     * 删除角色
     *
     * @param role
     */
    public void delete(Role role) {
        if (roleMapper.deleteByPrimaryKey(role.getId()) > 0) {
            setSuccessMsg("删除成功！");
        } else {
            setErrorMsg("删除失败！");
        }
    }

    public LinkedHashSet<Rule> getMenu() {
        List<Rule> rules = ruleMapper.selectByExample(null);
        Map<Integer, Rule> ruleMap = new HashMap<>();
        for (Rule rule : rules) {
            ruleMap.put(rule.getId(), rule);
        }

        for (Rule rule : rules) {
            int pid = rule.getPid();
            if (!ruleMap.containsKey(pid)) {
                ruleMap.put(pid, new Rule());
            }

            Rule pRule = ruleMap.get(pid);
            pRule.addChildRole(rule);
        }

        return ruleMap.get(0).getChildRole();

    }

    public Role getRuleById(int id) {
        Role role = roleMapper.selectByPrimaryKey(id);
        String ruleconten = role.getRuleconten();

        List<Integer> ids = JSONObject.parseArray(ruleconten, Integer.class);
        RuleExample ruleExample = new RuleExample();
        ruleExample.createCriteria().andIdIn(ids);
        List<Rule> rules = ruleMapper.selectByExample(ruleExample);
        role.setRules(rules);
        return role;
    }

}
